the results of cashpoint tests in popcornX 

 POPCORNX 


 DOMAIN/PROBLEM 1 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile01.pddl
Number of literals: 6
Constructing lookup tables:
Post filtering unreachable actions: 
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000
b (5.000 | 1.000)b (4.000 | 1.000)b (3.000 | 6.001)b (1.000 | 8.002);;;; Solution Found
; States evaluated: 8
; Cost: 8.003
0.000: (buy_with_cash item0 location0 currency0)  [1.000]
1.001: (goto location0 bank0)  [5.000]
6.002: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
8.003: (save_for_later currency0) 

 Execution time of Domain\Problem 1 
00:00:00.4830197

 DOMAIN/PROBLEM 2 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile02.pddl
Number of literals: 9
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

60% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 11.000
b (10.000 | 1.000)b (9.000 | 1.000)b (8.000 | 2.001)b (7.000 | 2.001)b (6.000 | 7.002)b (4.000 | 9.003)b (3.000 | 9.004)b (1.000 | 11.004);;;; Solution Found
; States evaluated: 15
; Cost: 11.005
0.000: (buy_with_cash item1 location0 currency0)  [1.000]
1.001: (buy_with_cash item0 location0 currency0)  [1.000]
2.002: (goto location0 bank0)  [5.000]
7.003: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
9.004: (save_for_later currency1) 
9.004: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
11.005: (save_for_later currency0) 

 Execution time of Domain\Problem 2 
00:00:00.4826424

 DOMAIN/PROBLEM 3 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile03.pddl
Number of literals: 11
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

58% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 5.000)b (13.000 | 6.001)b (12.000 | 6.001)b (11.000 | 7.002)b (10.000 | 7.002)b (9.000 | 12.003)b (7.000 | 14.004)b (6.000 | 14.005)b (4.000 | 16.005)b (3.000 | 16.006)b (1.000 | 18.006);;;; Solution Found
; States evaluated: 21
; Cost: 18.007
0.000: (goto location0 location1)  [5.000]
5.001: (buy_with_cash item0 location1 currency2)  [1.000]
6.002: (buy_with_cash item1 location1 currency0)  [1.000]
7.003: (goto location1 bank0)  [5.000]
12.004: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
14.005: (save_for_later currency2) 
14.005: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
16.006: (save_for_later currency1) 
16.006: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
18.007: (save_for_later currency0) 

 Execution time of Domain\Problem 3 
00:00:00.5725319

 DOMAIN/PROBLEM 4 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile04.pddl
Number of literals: 12
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 14.000
b (13.000 | 1.000)b (12.000 | 1.000)b (11.000 | 7.002)b (10.000 | 7.002)b (9.000 | 12.003)b (7.000 | 19.005)b (5.000 | 21.006)b (4.000 | 21.007)b (3.000 | 26.007)b (1.000 | 34.010);;;; Solution Found
; States evaluated: 33
; Cost: 34.011
0.000: (buy_with_cash item2 location0 currency1)  [1.000]
1.001: (goto location0 location2)  [5.000]
6.002: (buy_with_cash item0 location2 currency0)  [1.000]
7.003: (goto location2 location0)  [5.000]
12.004: (goto location0 bank0)  [5.000]
17.005: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
19.006: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
21.007: (save_for_later currency0) 
21.007: (goto bank0 location0)  [5.000]
26.008: (buy_with_cash item1 location0 currency1)  [1.000]
27.009: (goto location0 bank0)  [5.000]
32.010: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
34.011: (save_for_later currency1) 

 Execution time of Domain\Problem 4 
00:00:00.6669732

 DOMAIN/PROBLEM 5 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile05.pddl
Number of literals: 15
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 5.000)b (15.000 | 6.001)b (14.000 | 6.001)b (13.000 | 7.002)b (12.000 | 7.002)b (11.000 | 8.003)b (10.000 | 8.003)b (9.000 | 13.004)b (7.000 | 20.006)b (5.000 | 22.007)b (4.000 | 22.008)b (3.000 | 27.008)b (1.000 | 35.011);;;; Solution Found
; States evaluated: 38
; Cost: 35.012
0.000: (goto location2 location3)  [5.000]
5.001: (buy_with_cash item4 location3 currency1)  [1.000]
6.002: (buy_with_cash item2 location3 currency1)  [1.000]
7.003: (buy_with_cash item0 location3 currency1)  [1.000]
8.004: (goto location3 location1)  [5.000]
13.005: (goto location1 bank0)  [5.000]
18.006: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
20.007: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,113.000])
22.008: (save_for_later currency0) 
22.008: (goto bank0 location1)  [5.000]
27.009: (buy_with_cash item1 location1 currency1)  [1.000]
28.010: (goto location1 bank0)  [5.000]
33.011: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
35.012: (save_for_later currency1) 

 Execution time of Domain\Problem 5 
00:00:00.6764030

 DOMAIN/PROBLEM 6 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile06.pddl
Number of literals: 17
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 20.000
b (19.000 | 5.000)b (18.000 | 6.001)b (17.000 | 6.001)b (16.000 | 7.002)b (15.000 | 7.002)b (13.000 | 14.004)b (12.000 | 14.005)b (10.000 | 16.005)b (9.000 | 16.006)b (7.000 | 18.006)b (6.000 | 18.007)b (5.000 | 23.007)b (4.000 | 24.008)b (3.000 | 24.008)b (2.000 | 29.009)b (1.000 | 30.010);;;; Solution Found
; States evaluated: 35
; Cost: 30.010
0.000: (goto location2 location3)  [5.000]
5.001: (buy_with_cash item4 location3 currency2)  [1.000]
6.002: (buy_with_cash item3 location3 currency0)  [1.000]
7.003: (goto location3 bank0)  [5.000]
12.004: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
14.005: (save_for_later currency2) 
14.005: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
16.006: (save_for_later currency1) 
16.006: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
18.007: (save_for_later currency0) 
18.007: (goto bank0 location3)  [5.000]
23.008: (buy_with_cash item2 location3 currency2)  [1.000]
24.009: (goto location3 location1)  [5.000]
29.010: (buy_with_cash item0 location1 currency0)  [1.000]

 Execution time of Domain\Problem 6 
00:00:00.7260627

 DOMAIN/PROBLEM 7 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile07.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

66% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 25.000
b (24.000 | 5.000)b (23.000 | 6.001)b (22.000 | 6.001)b (21.000 | 7.002)b (20.000 | 7.002)b (19.000 | 13.004)b (18.000 | 13.004)b (17.000 | 14.005)b (16.000 | 14.005)b (15.000 | 19.006)b (13.000 | 26.008)b (12.000 | 26.009)b (10.000 | 28.009)b (9.000 | 28.010)b (7.000 | 30.010)b (6.000 | 30.011)b (4.000 | 32.011)b (3.000 | 32.012)b (2.000 | 37.012)b (1.000 | 38.013);;;; Solution Found
; States evaluated: 44
; Cost: 38.013
0.000: (goto location3 location2)  [5.000]
5.001: (buy_with_cash item2 location2 currency2)  [1.000]
6.002: (buy_with_cash item1 location2 currency1)  [1.000]
7.003: (goto location2 location0)  [5.000]
12.004: (buy_with_cash item3 location0 currency3)  [1.000]
13.005: (buy_with_cash item0 location0 currency1)  [1.000]
14.006: (goto location0 location2)  [5.000]
19.007: (goto location2 bank0)  [5.000]
24.008: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
26.009: (save_for_later currency3) 
26.009: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
28.010: (save_for_later currency2) 
28.010: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
30.011: (save_for_later currency1) 
30.011: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
32.012: (save_for_later currency0) 
32.012: (goto bank0 location2)  [5.000]
37.013: (buy_with_cash item4 location2 currency0)  [1.000]

 Execution time of Domain\Problem 7 
00:00:00.8369454

 DOMAIN/PROBLEM 8 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile08.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 24.000
b (23.000 | 1.000)b (22.000 | 1.000)b (21.000 | 2.001)b (20.000 | 2.001)b (19.000 | 7.002)b (18.000 | 13.004)b (17.000 | 13.004)b (15.000 | 20.006)b (14.000 | 20.007)b (12.000 | 22.007)b (10.000 | 24.008)b (9.000 | 29.009)b (8.000 | 36.012)b (7.000 | 41.013)b (6.000 | 47.015)b (4.000 | 49.016)b (3.000 | 49.017)b (1.000 | 51.017);;;; Solution Found
; States evaluated: 133
; Cost: 51.018
0.000: (buy_with_cash item0 location2 currency1)  [1.000]
1.001: (buy_with_cash item1 location2 currency0)  [1.000]
2.002: (goto location2 location5)  [5.000]
7.003: (goto location5 location1)  [5.000]
12.004: (buy_with_cash item3 location1 currency2)  [1.000]
13.005: (goto location1 bank0)  [5.000]
18.006: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
20.007: (save_for_later currency2) 
20.007: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
22.008: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
24.009: (goto bank0 location5)  [5.000]
29.010: (buy_with_cash item2 location5 currency1)  [1.000]
30.011: (goto location5 location1)  [5.000]
35.012: (buy_with_cash item5 location1 currency1)  [1.000]
36.013: (goto location1 location5)  [5.000]
41.014: (buy_with_cash item4 location5 currency0)  [1.000]
42.015: (goto location5 bank0)  [5.000]
47.016: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
49.017: (save_for_later currency1) 
49.017: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
51.018: (save_for_later currency0) 

 Execution time of Domain\Problem 8 
00:00:02.5807476

 DOMAIN/PROBLEM 9 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile09.pddl
Number of literals: 20
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 5.000)b (26.000 | 6.001)b (25.000 | 6.001)b (24.000 | 12.003)b (23.000 | 12.003)b (22.000 | 17.004)b (21.000 | 23.006)b (20.000 | 23.006)b (18.000 | 30.008)b (17.000 | 30.009)b (15.000 | 32.009)b (13.000 | 34.010)b (12.000 | 34.011)b (11.000 | 36.011)b (10.000 | 42.013)b (9.000 | 42.013)b (8.000 | 48.015)b (6.000 | 55.017)b (5.000 | 61.019)b (4.000 | 61.019)b (3.000 | 66.020)b (1.000 | 75.023);;;; Solution Found
; States evaluated: 100
; Cost: 75.024
0.000: (goto location4 location2)  [5.000]
5.001: (buy_with_cash item4 location2 currency1)  [1.000]
6.002: (goto location2 location1)  [5.000]
11.003: (buy_with_cash item2 location1 currency2)  [1.000]
12.004: (goto location1 location2)  [5.000]
17.005: (goto location2 location0)  [5.000]
22.006: (buy_with_cash item1 location0 currency2)  [1.000]
23.007: (goto location0 bank0)  [5.000]
28.008: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
30.009: (save_for_later currency3) 
30.009: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
32.010: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
32.010: (save_for_later currency2) 
34.011: (save_for_later currency0) 
34.011: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
36.012: (goto bank0 location0)  [5.000]
41.013: (buy_with_cash item0 location0 currency2)  [1.000]
42.014: (buy_with_cash item5 location0 currency0)  [1.000]
43.015: (goto location0 location2)  [5.000]
48.016: (goto location2 bank0)  [5.000]
53.017: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
55.018: (goto bank0 location2)  [5.000]
60.019: (buy_with_cash item3 location2 currency0)  [1.000]
61.020: (goto location2 bank0)  [5.000]
66.021: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,32.000])
68.022: (goto bank0 changeoffice0)  [5.000]
73.023: (exchange_in_changeoffice changeoffice0 currency0 currency1)  [2.000]  (?cash [20.000,225.000])
75.024: (save_for_later currency1) 

 Execution time of Domain\Problem 9 
00:00:01.9741876

 DOMAIN/PROBLEM 10 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile10.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (balance currency4) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (inpocket currency4) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency4) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency4) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 5.000)b (27.000 | 6.001)b (26.000 | 6.001)b (25.000 | 11.002)b (24.000 | 12.003)b (23.000 | 12.003)b (22.000 | 13.004)b (21.000 | 13.004)b (20.000 | 18.005)b (19.000 | 19.006)b (18.000 | 19.006)b (16.000 | 26.008)b (15.000 | 26.009)b (13.000 | 28.009)b (12.000 | 28.010)b (10.000 | 30.010)b (8.000 | 32.011)b (7.000 | 32.012)b (5.000 | 34.012)b (4.000 | 34.013)b (3.000 | 39.013)b (1.000 | 47.016);;;; Solution Found
; States evaluated: 71
; Cost: 47.017
0.000: (goto location2 location4)  [5.000]
5.001: (buy_with_cash item3 location4 currency0)  [1.000]
6.002: (goto location4 location1)  [5.000]
11.003: (buy_with_cash item1 location1 currency3)  [1.000]
12.004: (buy_with_cash item0 location1 currency2)  [1.000]
13.005: (goto location1 location0)  [5.000]
18.006: (buy_with_cash item2 location0 currency1)  [1.000]
19.007: (goto location0 bank0)  [5.000]
24.008: (withdraw bank0 currency4)  [2.000]  (?cash [20.000,250.000])
26.009: (save_for_later currency4) 
26.009: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
28.010: (save_for_later currency3) 
28.010: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
30.011: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
32.012: (save_for_later currency1) 
32.012: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
34.013: (save_for_later currency0) 
34.013: (goto bank0 location0)  [5.000]
39.014: (buy_with_cash item4 location0 currency2)  [1.000]
40.015: (goto location0 bank0)  [5.000]
45.016: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
47.017: (save_for_later currency2) 

 Execution time of Domain\Problem 10 
00:00:01.7209009

 DOMAIN/PROBLEM 11 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile11.pddl
Number of literals: 20
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

77% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 34.000

 Execution time of Domain\Problem 11 
00:30:00.1882508

 DOMAIN/PROBLEM 12 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile12.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 38.000
b (37.000 | 5.000)b (36.000 | 6.001)b (35.000 | 6.001)b (34.000 | 7.002)b (33.000 | 7.002)b (32.000 | 8.003)b (31.000 | 8.003)b (30.000 | 14.005)b (29.000 | 14.005)b (28.000 | 21.007)b (27.000 | 23.008)b (26.000 | 25.009)b (25.000 | 27.010)b (24.000 | 29.011)b (23.000 | 31.012)b (22.000 | 33.013)b (21.000 | 35.014)b (20.000 | 35.015)b (19.000 | 37.015)b (18.000 | 39.016)b (17.000 | 41.017)b (16.000 | 43.018)b (15.000 | 45.019)b (14.000 | 49.021)b (13.000 | 49.021)b (11.000 | 56.023)b (9.000 | 58.024)b (8.000 | 63.025)b (7.000 | 64.026)b (6.000 | 64.026)b (5.000 | 65.027)b (4.000 | 65.027)b (3.000 | 70.028)b (2.000 | 82.031)b (1.000 | 83.032);;;; Solution Found
; States evaluated: 223
; Cost: 83.032
0.000: (goto location2 location4)  [5.000]
5.001: (buy_with_cash item4 location4 currency2)  [1.000]
6.002: (buy_with_cash item3 location4 currency0)  [1.000]
7.003: (buy_with_cash item1 location4 currency0)  [1.000]
8.004: (goto location4 location0)  [5.000]
13.005: (buy_with_cash item0 location0 currency2)  [1.000]
14.006: (goto location0 bank0)  [5.000]
19.007: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,180.000])
21.008: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
23.009: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,160.000])
25.010: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
27.011: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,140.000])
29.012: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
31.013: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,120.000])
33.014: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
35.015: (save_for_later currency1) 
35.015: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,100.000])
37.016: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
39.017: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,80.000])
41.018: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
43.019: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,60.000])
43.019: (save_for_later currency0) 
45.020: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,40.000])
47.021: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,20.000])
49.022: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
51.023: (goto bank0 changeoffice0)  [5.000]
56.024: (exchange_in_changeoffice changeoffice0 currency0 currency2)  [2.000]  (?cash [20.000,319.000])
58.025: (goto changeoffice0 location4)  [5.000]
58.025: (save_for_later currency2) 
63.026: (buy_with_cash item5 location4 currency1)  [1.000]
64.027: (buy_with_cash item2 location4 currency1)  [1.000]
65.028: (goto location4 location0)  [5.000]
70.029: (goto location0 bank0)  [5.000]
75.030: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
77.031: (goto bank0 location0)  [5.000]
82.032: (buy_with_cash item6 location0 currency0)  [1.000]

 Execution time of Domain\Problem 12 
00:00:09.2629311

 DOMAIN/PROBLEM 13 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile13.pddl
Number of literals: 21
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 40.000
b (39.000 | 1.000)b (38.000 | 1.000)b (37.000 | 2.001)b (36.000 | 2.001)b (35.000 | 3.002)b (34.000 | 3.002)b (33.000 | 9.004)b (32.000 | 9.004)b (31.000 | 14.005)b (30.000 | 20.007)b (29.000 | 20.007)b (28.000 | 25.008)b (26.000 | 32.010)b (24.000 | 36.012)b (22.000 | 40.014)b (20.000 | 44.016)b (19.000 | 46.018)b (17.000 | 48.018)b (15.000 | 52.020)b (13.000 | 56.022)b (12.000 | 56.022)b (11.000 | 58.023)b (10.000 | 58.023)b (8.000 | 60.024)b (7.000 | 69.027)b (5.000 | 71.028)b (4.000 | 76.029)b (3.000 | 77.030)b (2.000 | 77.030)b (1.000 | 88.034);;;; Solution Found
; States evaluated: 309
; Cost: 88.035
0.000: (buy_with_cash item2 location3 currency1)  [1.000]
1.001: (buy_with_cash item5 location3 currency0)  [1.000]
2.002: (buy_with_cash item3 location3 currency0)  [1.000]
3.003: (goto location3 location2)  [5.000]
8.004: (buy_with_cash item1 location2 currency2)  [1.000]
9.005: (goto location2 location3)  [5.000]
14.006: (goto location3 location1)  [5.000]
19.007: (buy_with_cash item0 location1 currency2)  [1.000]
20.008: (goto location1 location3)  [5.000]
25.009: (goto location3 bank0)  [5.000]
30.010: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,166.000])
32.011: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
34.012: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,146.000])
36.013: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
38.014: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,126.000])
40.015: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
42.016: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,106.000])
44.017: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
46.018: (save_for_later currency1) 
46.018: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,86.000])
48.019: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
50.020: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,66.000])
52.021: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,46.000])
54.022: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,26.000])
56.023: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
58.024: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
60.025: (goto bank0 changeoffice0)  [5.000]
65.026: (exchange_in_changeoffice changeoffice0 currency0 currency2)  [2.000]  (?cash [20.000,500.000])
67.027: (exchange_in_changeoffice changeoffice0 currency0 currency2)  [2.000]  (?cash [20.000,500.000])
69.028: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]  (?cash [20.000,500.000])
71.029: (goto changeoffice0 location3)  [5.000]
71.029: (save_for_later currency2) 
76.030: (buy_with_cash item4 location3 currency1)  [1.000]
77.031: (goto location3 bank0)  [5.000]
82.032: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
84.033: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
86.034: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
88.035: (save_for_later currency0) 

 Execution time of Domain\Problem 13 
00:00:14.9045945

 DOMAIN/PROBLEM 14 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile14.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 22.000
b (21.000 | 5.000)b (20.000 | 6.001)b (19.000 | 6.001)b (17.000 | 13.003)b (15.000 | 15.004)b (14.000 | 20.005)b (13.000 | 27.008)b (12.000 | 27.008)b (11.000 | 28.009)b (10.000 | 28.009)b (9.000 | 33.010)b (8.000 | 34.011)b (7.000 | 34.011)b (6.000 | 40.013)b (4.000 | 42.014)b (3.000 | 42.015)b (1.000 | 44.015);;;; Solution Found
; States evaluated: 123
; Cost: 44.016
0.000: (goto location4 location5)  [5.000]
5.001: (buy_with_cash item5 location5 currency1)  [1.000]
6.002: (goto location5 bank0)  [5.000]
11.003: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
13.004: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
15.005: (goto bank0 location5)  [5.000]
20.006: (buy_with_cash item2 location5 currency0)  [1.000]
21.007: (goto location5 location3)  [5.000]
26.008: (buy_with_cash item6 location3 currency0)  [1.000]
27.009: (buy_with_cash item1 location3 currency0)  [1.000]
28.010: (goto location3 location0)  [5.000]
33.011: (buy_with_cash item4 location0 currency0)  [1.000]
34.012: (buy_with_cash item0 location0 currency1)  [1.000]
35.013: (goto location0 bank0)  [5.000]
40.014: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
42.015: (save_for_later currency1) 
42.015: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
44.016: (save_for_later currency0) 

 Execution time of Domain\Problem 14 
00:00:01.8960048

 DOMAIN/PROBLEM 15 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile15.pddl
Number of literals: 23
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 32.000
b (31.000 | 5.000)b (30.000 | 11.002)b (29.000 | 11.002)b (28.000 | 18.004)b (27.000 | 18.004)b (26.000 | 20.005)b (25.000 | 20.005)b (24.000 | 22.006)b (23.000 | 22.006)b (22.000 | 24.007)b (21.000 | 24.007)b (19.000 | 26.008)b (17.000 | 28.009)b (16.000 | 33.010)b (15.000 | 34.011)b (14.000 | 34.011)b (13.000 | 40.013)b (12.000 | 40.013)b (11.000 | 47.016)b (10.000 | 47.016)b (9.000 | 52.017)b (8.000 | 54.019)b (7.000 | 54.019)b (6.000 | 59.020)b (4.000 | 61.021)b (3.000 | 61.022)b (1.000 | 63.022);;;; Solution Found
; States evaluated: 175
; Cost: 63.023
0.000: (goto location6 location5)  [5.000]
5.001: (goto location5 location1)  [5.000]
10.002: (buy_with_cash item4 location1 currency1)  [1.000]
11.003: (goto location1 bank0)  [5.000]
16.004: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
18.005: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
20.006: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
22.007: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
24.008: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
26.009: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
28.010: (goto bank0 location5)  [5.000]
33.011: (buy_with_cash item2 location5 currency0)  [1.000]
34.012: (goto location5 location0)  [5.000]
39.013: (buy_with_cash item5 location0 currency0)  [1.000]
40.014: (buy_with_cash item6 location0 currency0)  [1.000]
41.015: (goto location0 location5)  [5.000]
46.016: (buy_with_cash item0 location5 currency0)  [1.000]
47.017: (goto location5 location1)  [5.000]
52.018: (buy_with_cash item3 location1 currency1)  [1.000]
53.019: (buy_with_cash item1 location1 currency1)  [1.000]
54.020: (goto location1 bank0)  [5.000]
59.021: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
61.022: (save_for_later currency1) 
61.022: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
63.023: (save_for_later currency0) 

 Execution time of Domain\Problem 15 
00:00:04.0190096

 DOMAIN/PROBLEM 16 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile16.pddl
Number of literals: 24
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000

 Execution time of Domain\Problem 16 
00:30:00.1899197

 DOMAIN/PROBLEM 17 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile17.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank3 currency0) are preferable
Have identified that bigger values of (bank-reserve bank3 currency1) are preferable
Have identified that bigger values of (bank-reserve bank3 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 30.000
b (29.000 | 1.000)b (28.000 | 1.000)b (27.000 | 6.001)b (26.000 | 7.002)b (25.000 | 7.002)b (24.000 | 8.003)b (23.000 | 8.003)b (22.000 | 14.005)b (21.000 | 14.005)b (20.000 | 21.007)b (19.000 | 21.007)b (17.000 | 23.008)b (15.000 | 25.009)b (14.000 | 25.010)b (12.000 | 27.010)b (11.000 | 27.011)b (10.000 | 32.011)b (9.000 | 39.014)b (8.000 | 44.015)b (6.000 | 51.017)b (5.000 | 56.018)b (4.000 | 57.019)b (3.000 | 57.019)b (2.000 | 69.022)b (1.000 | 70.023);;;; Solution Found
; States evaluated: 160
; Cost: 70.023
0.000: (buy_with_cash item2 location4 currency1)  [1.000]
1.001: (goto location4 location1)  [5.000]
6.002: (buy_with_cash item7 location1 currency2)  [1.000]
7.003: (buy_with_cash item6 location1 currency2)  [1.000]
8.004: (goto location1 location0)  [5.000]
13.005: (buy_with_cash item5 location0 currency2)  [1.000]
14.006: (goto location0 bank0)  [5.000]
19.007: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
21.008: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
23.009: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
23.009: (save_for_later currency2) 
25.010: (save_for_later currency1) 
25.010: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
27.011: (save_for_later currency0) 
27.011: (goto bank0 location1)  [5.000]
32.012: (goto location1 location0)  [5.000]
37.013: (buy_with_cash item3 location0 currency1)  [1.000]
38.014: (buy_with_cash item0 location0 currency1)  [1.000]
39.015: (goto location0 location1)  [5.000]
44.016: (goto location1 bank0)  [5.000]
49.017: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
51.018: (goto bank0 location1)  [5.000]
56.019: (buy_with_cash item4 location1 currency1)  [1.000]
57.020: (goto location1 bank0)  [5.000]
62.021: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
64.022: (goto bank0 location1)  [5.000]
69.023: (buy_with_cash item1 location1 currency2)  [1.000]

 Execution time of Domain\Problem 17 
00:00:04.3454286

 DOMAIN/PROBLEM 18 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile18.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 38.000
b (37.000 | 5.000)b (36.000 | 6.001)b (35.000 | 6.001)b (34.000 | 7.002)b (33.000 | 7.002)b (32.000 | 13.004)b (31.000 | 13.004)b (30.000 | 14.005)b (29.000 | 14.005)b (28.000 | 21.007)b (27.000 | 21.007)b (26.000 | 23.008)b (25.000 | 23.008)b (24.000 | 25.009)b (23.000 | 25.009)b (21.000 | 27.010)b (19.000 | 29.011)b (18.000 | 29.012)b (16.000 | 31.012)b (14.000 | 33.013)b (13.000 | 38.014)b (12.000 | 50.017)b (11.000 | 56.019)b (10.000 | 56.019)b (9.000 | 61.020)b (8.000 | 67.022)b (7.000 | 67.022)b (6.000 | 79.025)b (5.000 | 85.027)b (4.000 | 85.027)b (3.000 | 90.028)b (2.000 | 102.031)b (1.000 | 103.032);;;; Solution Found
; States evaluated: 597
; Cost: 103.032
0.000: (goto location0 location6)  [5.000]
5.001: (buy_with_cash item2 location6 currency3)  [1.000]
6.002: (buy_with_cash item1 location6 currency2)  [1.000]
7.003: (goto location6 location1)  [5.000]
12.004: (buy_with_cash item5 location1 currency2)  [1.000]
13.005: (buy_with_cash item4 location1 currency0)  [1.000]
14.006: (goto location1 bank0)  [5.000]
19.007: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
21.008: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
23.009: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
25.010: (withdraw bank0 currency3)  [2.000]  (?cash [20.000,250.000])
27.011: (withdraw bank0 currency2)  [2.000]  (?cash [20.000,250.000])
27.011: (save_for_later currency3) 
29.012: (save_for_later currency2) 
29.012: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
31.013: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
31.013: (save_for_later currency1) 
33.014: (goto bank0 location6)  [5.000]
33.014: (save_for_later currency0) 
38.015: (goto location6 bank1)  [5.000]
43.016: (withdraw bank1 currency3)  [2.000]  (?cash [20.000,250.000])
45.017: (goto bank1 location6)  [5.000]
50.018: (goto location6 location2)  [5.000]
55.019: (buy_with_cash item3 location2 currency3)  [1.000]
56.020: (goto location2 location6)  [5.000]
61.021: (goto location6 location1)  [5.000]
66.022: (buy_with_cash item6 location1 currency3)  [1.000]
67.023: (goto location1 bank0)  [5.000]
72.024: (withdraw bank0 currency1)  [2.000]  (?cash [20.000,250.000])
74.025: (goto bank0 location6)  [5.000]
79.026: (goto location6 location1)  [5.000]
84.027: (buy_with_cash item0 location1 currency1)  [1.000]
85.028: (goto location1 location6)  [5.000]
90.029: (goto location6 bank0)  [5.000]
95.030: (withdraw bank0 currency0)  [2.000]  (?cash [20.000,250.000])
97.031: (goto bank0 location6)  [5.000]
102.032: (buy_with_cash item7 location6 currency0)  [1.000]

 Execution time of Domain\Problem 18 
00:00:22.6825275

 DOMAIN/PROBLEM 19 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile19.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank3 currency0) are preferable
Have identified that bigger values of (bank-reserve bank3 currency1) are preferable
Have identified that bigger values of (bank-reserve bank3 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000

 Execution time of Domain\Problem 19 
00:30:00.1615017

 DOMAIN/PROBLEM 20 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile20.pddl
Number of literals: 30
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (balance currency4) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (inpocket currency4) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency4) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency4) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 52.000
b (51.000 | 5.000)b (50.000 | 6.001)b (49.000 | 6.001)b (48.000 | 12.003)b (47.000 | 12.003)b (46.000 | 13.004)b (45.000 | 13.004)b (44.000 | 18.005)b (43.000 | 24.007)b (42.000 | 24.007)b (41.000 | 30.009)b (40.000 | 30.009)b (39.000 | 35.010)b (38.000 | 42.012)b (37.000 | 42.012)b (36.000 | 44.013)b (35.000 | 44.013)b (34.000 | 46.014)b (33.000 | 48.015)b (32.000 | 48.016)b (31.000 | 50.016)b (30.000 | 52.017)b (29.000 | 54.018)b (28.000 | 56.019)b (27.000 | 58.020)b (26.000 | 60.021)b (25.000 | 62.022)b (24.000 | 64.023)b (23.000 | 66.024)b (22.000 | 68.025)b (21.000 | 68.026)b (20.000 | 70.026)b (19.000 | 72.027)b (18.000 | 74.028)b (17.000 | 76.029)b (16.000 | 78.030)b (15.000 | 82.032)b (14.000 | 88.034)b (13.000 | 93.035)b (12.000 | 94.036)b (11.000 | 94.036)b (10.000 | 95.037)b (9.000 | 95.037)b (8.000 | 102.039)b (6.000 | 122.046)
Resorting to best-first search

 Execution time of Domain\Problem 20 
00:30:00.1783799
