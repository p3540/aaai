the results of rovers tests in POPCORNX 

 POPCORNX 


 DOMAIN/PROBLEM 13 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile13.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 60.000
b (59.000 | 10.000)b (58.000 | 15.002)b (57.000 | 15.002)b (56.000 | 17.003)b (55.000 | 17.003)b (54.000 | 33.005)b (51.000 | 34.006)b (50.000 | 34.006)b (47.000 | 35.007)b (46.000 | 35.007)b (45.000 | 36.008)b (44.000 | 36.008)b (43.000 | 38.010)b (42.000 | 38.010)b (41.000 | 39.011)b (40.000 | 39.011)b (39.000 | 57.013)b (38.000 | 70.014)b (33.000 | 132.017)b (32.000 | 150.019)b (31.000 | 150.019)b (30.000 | 150.021)b (29.000 | 150.021)b (28.000 | 157.022)b (27.000 | 157.022)b (26.000 | 162.023)b (25.000 | 162.023)b (24.000 | 165.024)b (23.000 | 165.024)b (22.000 | 184.027)b (21.000 | 184.027)b (20.000 | 187.030)b (19.000 | 187.030)b (18.000 | 190.033)b (17.000 | 190.033)b (16.000 | 193.036)b (15.000 | 193.036)b (14.000 | 196.039)b (13.000 | 196.039)b (12.000 | 199.042)b (11.000 | 199.042)b (10.000 | 220.046)b (9.000 | 225.047)b (8.000 | 225.047)b (7.000 | 230.048)b (6.000 | 230.048)b (5.000 | 238.055)b (4.000 | 241.056)b (3.000 | 246.057)b (2.000 | 246.057)b (1.000 | 246.059);;;; Solution Found
; States evaluated: 1342
; Cost: 246.059
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
34.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
35.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.012: (go_to mine workshop)  [13.000]
52.013: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
57.014: (go_to workshop mine)  [13.000]
70.015: (go_to mine mahoganyforest)  [30.000]
100.016: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
102.017: (go_to mahoganyforest mine)  [30.000]
132.018: (go_to mine workshop)  [13.000]
145.019: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,12.750])
150.020: (place_ivy_chests ivychest)  [0.001]
150.022: (assemble_an_anvil iron1 workshop)  [7.000]
157.023: (assemble_a_loom wood1 workshop)  [5.000]
162.024: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,17.100])
165.025: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,17.250])
170.026: (go_to workshop mine)  [13.000]
183.027: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
184.028: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
185.029: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
186.030: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
187.031: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
188.032: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
189.033: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
190.034: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
191.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
192.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
193.037: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
194.038: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
195.039: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
196.040: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
197.041: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
198.042: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
199.043: (go_to mine workshop)  [13.000]
212.044: (go_to workshop forest)  [3.000]
215.045: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
217.046: (go_to forest workshop)  [3.000]
220.047: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,60.571])
225.048: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,12.114])
230.049: (place_beds bed1)  [0.001]
230.051: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,19.600])
233.052: (place_swords sword1)  [0.001]
233.054: (go_to workshop forest)  [3.000]
236.055: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
238.056: (go_to forest workshop)  [3.000]
241.057: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,22.125])
246.058: (place_chests chest1)  [0.001]

 Execution time of Domain\Problem 13 
00:08:02.8272322

 DOMAIN/PROBLEM 14 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile14.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 62.000
b (61.000 | 10.000)b (60.000 | 15.002)b (59.000 | 15.002)b (58.000 | 17.003)b (57.000 | 17.003)b (56.000 | 33.005)b (53.000 | 34.006)b (52.000 | 34.006)b (47.000 | 35.007)b (46.000 | 35.007)b (45.000 | 67.009)b (44.000 | 69.010)b (43.000 | 69.010)b (41.000 | 99.011)b (40.000 | 117.013)b (39.000 | 135.015)b (38.000 | 136.016)b (37.000 | 136.016)b (34.000 | 138.018)b (33.000 | 138.018)b (31.000 | 169.021)b (30.000 | 170.024)b (29.000 | 170.024)b (28.000 | 172.026)b (27.000 | 172.026)b (25.000 | 203.029)b (24.000 | 204.032)b (23.000 | 204.032)b (21.000 | 237.035)b (19.000 | 266.038)b (18.000 | 267.041)b (17.000 | 267.041)b (16.000 | 271.045)b (15.000 | 271.045)b (14.000 | 274.048)b (13.000 | 274.048)b (12.000 | 277.051)b (11.000 | 277.051)b (10.000 | 280.054)b (9.000 | 280.054)b (8.000 | 283.057)b (7.000 | 283.057)b (6.000 | 306.062)b (5.000 | 311.063)b (4.000 | 311.063)b (3.000 | 316.064)b (2.000 | 316.064)b (1.000 | 316.066);;;; Solution Found
; States evaluated: 486
; Cost: 316.066
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
34.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
35.008: (go_to mine mahoganyforest)  [30.000]
65.009: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
67.010: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
69.011: (go_to mahoganyforest mine)  [30.000]
99.012: (go_to mine workshop)  [13.000]
112.013: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
117.014: (assemble_a_loom wood1 workshop)  [5.000]
122.015: (go_to workshop mine)  [13.000]
135.016: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
136.017: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
137.018: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
138.019: (go_to mine workshop)  [13.000]
151.020: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,22.625])
156.021: (go_to workshop mine)  [13.000]
169.022: (place_chests chest1)  [0.001]
169.024: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
170.025: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
171.026: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
172.027: (go_to mine workshop)  [13.000]
185.028: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,16.500])
190.029: (go_to workshop mine)  [13.000]
203.030: (place_ivy_chests ivychest)  [0.001]
203.032: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
204.033: (go_to mine workshop)  [13.000]
217.034: (assemble_an_anvil iron1 workshop)  [7.000]
224.035: (go_to workshop mine)  [13.000]
237.036: (go_to mine workshop)  [13.000]
250.037: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,11.400])
253.038: (go_to workshop mine)  [13.000]
266.039: (place_swords sword1)  [0.001]
266.041: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
267.042: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
268.043: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
269.044: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
270.045: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
271.046: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
272.047: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
273.048: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
274.049: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
275.050: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
276.051: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
277.052: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
278.053: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
279.054: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
280.055: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
281.056: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
282.057: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
283.058: (go_to mine workshop)  [13.000]
296.059: (go_to workshop forest)  [3.000]
299.060: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
301.061: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
303.062: (go_to forest workshop)  [3.000]
306.063: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,66.000])
311.064: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,13.200])
316.065: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 14 
00:01:31.8100454

 DOMAIN/PROBLEM 15 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile15.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 66.000
b (65.000 | 10.000)b (64.000 | 15.002)b (63.000 | 15.002)b (62.000 | 17.003)b (61.000 | 17.003)b (60.000 | 19.004)b (59.000 | 19.004)b (58.000 | 35.006)b (55.000 | 36.007)b (54.000 | 36.007)b (51.000 | 37.008)b (50.000 | 37.008)b (49.000 | 38.009)b (48.000 | 38.009)b (47.000 | 39.010)b (46.000 | 39.010)b (45.000 | 41.012)b (44.000 | 41.012)b (43.000 | 42.013)b (42.000 | 42.013)b (41.000 | 60.015)b (40.000 | 73.016)b (35.000 | 135.019)b (33.000 | 166.022)b (32.000 | 169.025)b (31.000 | 169.025)b (29.000 | 200.028)b (28.000 | 200.030)b (27.000 | 200.030)b (25.000 | 233.033)b (24.000 | 236.036)b (23.000 | 236.036)b (21.000 | 265.039)b (20.000 | 266.042)b (19.000 | 266.042)b (18.000 | 269.045)b (17.000 | 269.045)b (16.000 | 272.048)b (15.000 | 272.048)b (14.000 | 275.051)b (13.000 | 275.051)b (12.000 | 278.054)b (11.000 | 278.054)b (10.000 | 309.060)b (9.000 | 309.062)b (8.000 | 309.062)b (5.000 | 354.066)b (4.000 | 357.067)b (3.000 | 362.068)b (2.000 | 362.068)b (1.000 | 362.070);;;; Solution Found
; States evaluated: 482
; Cost: 362.070
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
40.012: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
41.013: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
42.014: (go_to mine workshop)  [13.000]
55.015: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
60.016: (go_to workshop mine)  [13.000]
73.017: (go_to mine mahoganyforest)  [30.000]
103.018: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
105.019: (go_to mahoganyforest mine)  [30.000]
135.020: (go_to mine workshop)  [13.000]
148.021: (assemble_a_loom wood1 workshop)  [5.000]
153.022: (go_to workshop mine)  [13.000]
166.023: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
167.024: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
168.025: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
169.026: (go_to mine workshop)  [13.000]
182.027: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,30.000])
187.028: (go_to workshop mine)  [13.000]
200.029: (place_chests chest1)  [0.001]
200.031: (go_to mine workshop)  [13.000]
213.032: (assemble_an_anvil iron1 workshop)  [7.000]
220.033: (go_to workshop mine)  [13.000]
233.034: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
234.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
235.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
236.037: (go_to mine workshop)  [13.000]
249.038: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,30.000])
252.039: (go_to workshop mine)  [13.000]
265.040: (place_swords sword1)  [0.001]
265.042: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
266.043: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
267.044: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
268.045: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
269.046: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
270.047: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
271.048: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
272.049: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
273.050: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
274.051: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
275.052: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
276.053: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
277.054: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
278.055: (go_to mine workshop)  [13.000]
291.056: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,70.286])
296.057: (go_to workshop forest)  [3.000]
299.058: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
301.059: (go_to forest workshop)  [3.000]
304.060: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,14.057])
309.061: (place_beds bed1)  [0.001]
309.063: (go_to workshop forest)  [3.000]
312.064: (go_to forest mahoganyforest)  [20.000]
332.065: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
334.066: (go_to mahoganyforest forest)  [20.000]
354.067: (go_to forest workshop)  [3.000]
357.068: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,25.250])
362.069: (place_ivy_chests ivychest)  [0.001]

 Execution time of Domain\Problem 15 
00:02:14.7586999

 DOMAIN/PROBLEM 16 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile16.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 70.000
b (69.000 | 10.000)b (68.000 | 15.002)b (67.000 | 15.002)b (66.000 | 17.003)b (65.000 | 17.003)b (64.000 | 19.004)b (63.000 | 19.004)b (62.000 | 35.006)b (61.000 | 36.007)b (60.000 | 36.007)b (57.000 | 37.008)b (56.000 | 37.008)b (53.000 | 38.009)b (52.000 | 38.009)b (51.000 | 40.011)b (50.000 | 40.011)b (49.000 | 42.013)b (48.000 | 42.013)b (47.000 | 43.014)b (46.000 | 43.014)b (45.000 | 61.016)b (44.000 | 74.017)b (39.000 | 136.020)b (37.000 | 167.023)b (36.000 | 170.026)b (35.000 | 170.026)b (33.000 | 201.029)b (32.000 | 201.031)b (31.000 | 201.031)b (29.000 | 234.034)b (28.000 | 237.037)b (27.000 | 237.037)b (25.000 | 266.040)b (24.000 | 267.043)b (23.000 | 267.043)b (22.000 | 270.046)b (21.000 | 270.046)b (20.000 | 273.049)b (19.000 | 273.049)b (18.000 | 277.053)b (17.000 | 277.053)b (16.000 | 280.056)b (15.000 | 280.056)b (14.000 | 283.059)b (13.000 | 283.059)b (12.000 | 286.062)b (11.000 | 286.062)b (10.000 | 319.069)b (9.000 | 319.071)b (8.000 | 319.071)b (5.000 | 364.075)b (4.000 | 367.076)b (3.000 | 372.077)b (2.000 | 372.077)b (1.000 | 372.079);;;; Solution Found
; States evaluated: 884
; Cost: 372.079
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
40.012: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
41.013: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
42.014: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
43.015: (go_to mine workshop)  [13.000]
56.016: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
61.017: (go_to workshop mine)  [13.000]
74.018: (go_to mine mahoganyforest)  [30.000]
104.019: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
106.020: (go_to mahoganyforest mine)  [30.000]
136.021: (go_to mine workshop)  [13.000]
149.022: (assemble_a_loom wood1 workshop)  [5.000]
154.023: (go_to workshop mine)  [13.000]
167.024: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
168.025: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
169.026: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
170.027: (go_to mine workshop)  [13.000]
183.028: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,30.000])
188.029: (go_to workshop mine)  [13.000]
201.030: (place_chests chest1)  [0.001]
201.032: (go_to mine workshop)  [13.000]
214.033: (assemble_an_anvil iron1 workshop)  [7.000]
221.034: (go_to workshop mine)  [13.000]
234.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
235.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
236.037: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
237.038: (go_to mine workshop)  [13.000]
250.039: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,30.000])
253.040: (go_to workshop mine)  [13.000]
266.041: (place_swords sword1)  [0.001]
266.043: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
267.044: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
268.045: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
269.046: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
270.047: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
271.048: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
272.049: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
273.050: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
274.051: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
275.052: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
276.053: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
277.054: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
278.055: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
279.056: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
280.057: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
281.058: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
282.059: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
283.060: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
284.061: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
285.062: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
286.063: (go_to mine workshop)  [13.000]
299.064: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,91.286])
304.065: (go_to workshop forest)  [3.000]
307.066: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
309.067: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
311.068: (go_to forest workshop)  [3.000]
314.069: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,18.257])
319.070: (place_beds bed1)  [0.001]
319.072: (go_to workshop forest)  [3.000]
322.073: (go_to forest mahoganyforest)  [20.000]
342.074: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
344.075: (go_to mahoganyforest forest)  [20.000]
364.076: (go_to forest workshop)  [3.000]
367.077: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,25.250])
372.078: (place_ivy_chests ivychest)  [0.001]

 Execution time of Domain\Problem 16 
00:07:39.0961039

 DOMAIN/PROBLEM 17 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile17.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 66.000

 Execution time of Domain\Problem 17 
00:30:00.2233595

 DOMAIN/PROBLEM 18 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile18.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 68.000
b (67.000 | 10.000)b (66.000 | 15.002)b (65.000 | 15.002)b (64.000 | 17.003)b (63.000 | 17.003)b (62.000 | 19.004)b (61.000 | 19.004)b (60.000 | 35.006)b (57.000 | 36.007)b (56.000 | 36.007)b (53.000 | 37.008)b (52.000 | 37.008)b (51.000 | 38.009)b (50.000 | 38.009)b (49.000 | 39.010)b (48.000 | 39.010)b (47.000 | 41.012)b (46.000 | 41.012)b (45.000 | 42.013)b (44.000 | 42.013)b (43.000 | 60.015)b (42.000 | 73.016)b (37.000 | 135.019)b (35.000 | 166.022)b (33.000 | 198.026)b (32.000 | 198.028)b (31.000 | 198.028)b (29.000 | 231.031)b (28.000 | 234.034)b (27.000 | 234.034)b (25.000 | 263.037)b (24.000 | 264.040)b (23.000 | 264.040)b (22.000 | 267.043)b (21.000 | 267.043)b (20.000 | 270.046)b (19.000 | 270.046)b (18.000 | 273.049)b (17.000 | 273.049)b (16.000 | 276.052)b (15.000 | 276.052)b (14.000 | 279.055)b (13.000 | 279.055)b (12.000 | 283.059)b (11.000 | 283.059)b (10.000 | 314.065)b (9.000 | 314.067)b (8.000 | 314.067)b (5.000 | 359.071)b (4.000 | 362.072)b (3.000 | 367.073)b (2.000 | 367.073)b (1.000 | 367.075);;;; Solution Found
; States evaluated: 702
; Cost: 367.075
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
40.012: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
41.013: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
42.014: (go_to mine workshop)  [13.000]
55.015: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
60.016: (go_to workshop mine)  [13.000]
73.017: (go_to mine mahoganyforest)  [30.000]
103.018: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
105.019: (go_to mahoganyforest mine)  [30.000]
135.020: (go_to mine workshop)  [13.000]
148.021: (assemble_a_loom wood1 workshop)  [5.000]
153.022: (go_to workshop mine)  [13.000]
166.023: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
167.024: (go_to mine workshop)  [13.000]
180.025: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,30.000])
185.026: (go_to workshop mine)  [13.000]
198.027: (place_chests chest1)  [0.001]
198.029: (go_to mine workshop)  [13.000]
211.030: (assemble_an_anvil iron1 workshop)  [7.000]
218.031: (go_to workshop mine)  [13.000]
231.032: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
232.033: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
233.034: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
234.035: (go_to mine workshop)  [13.000]
247.036: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,23.200])
250.037: (go_to workshop mine)  [13.000]
263.038: (place_swords sword1)  [0.001]
263.040: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
264.041: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
265.042: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
266.043: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
267.044: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
268.045: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
269.046: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
270.047: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
271.048: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
272.049: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
273.050: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
274.051: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
275.052: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
276.053: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
277.054: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
278.055: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
279.056: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
280.057: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
281.058: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
282.059: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
283.060: (go_to mine workshop)  [13.000]
296.061: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,82.428])
301.062: (go_to workshop forest)  [3.000]
304.063: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
306.064: (go_to forest workshop)  [3.000]
309.065: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,16.486])
314.066: (place_beds bed1)  [0.001]
314.068: (go_to workshop forest)  [3.000]
317.069: (go_to forest mahoganyforest)  [20.000]
337.070: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
339.071: (go_to mahoganyforest forest)  [20.000]
359.072: (go_to forest workshop)  [3.000]
362.073: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,25.250])
367.074: (place_ivy_chests ivychest)  [0.001]

 Execution time of Domain\Problem 18 
00:04:50.5660268

 DOMAIN/PROBLEM 19 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile19.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 68.000

 Execution time of Domain\Problem 19 
00:30:00.2432138

 DOMAIN/PROBLEM 20 

File: ..\terraria_capacity\domain13to20.pddl
File: ..\terraria_capacity\pfile20.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 70.000
b (69.000 | 10.000)b (68.000 | 15.002)b (67.000 | 15.002)b (66.000 | 17.003)b (65.000 | 17.003)b (64.000 | 19.004)b (63.000 | 19.004)b (62.000 | 35.006)b (61.000 | 36.007)b (60.000 | 36.007)b (57.000 | 37.008)b (56.000 | 37.008)b (53.000 | 38.009)b (52.000 | 38.009)b (51.000 | 40.011)b (50.000 | 40.011)b (49.000 | 42.013)b (48.000 | 42.013)b (47.000 | 43.014)b (46.000 | 43.014)b (45.000 | 61.016)b (44.000 | 74.017)b (39.000 | 136.020)b (37.000 | 167.023)b (36.000 | 170.026)b (35.000 | 170.026)b (33.000 | 201.029)b (32.000 | 201.031)b (31.000 | 201.031)b (29.000 | 234.034)b (28.000 | 237.037)b (27.000 | 237.037)b (25.000 | 266.040)b (24.000 | 267.043)b (23.000 | 267.043)b (22.000 | 270.046)b (21.000 | 270.046)b (20.000 | 273.049)b (19.000 | 273.049)b (18.000 | 277.053)b (17.000 | 277.053)b (16.000 | 280.056)b (15.000 | 280.056)b (14.000 | 283.059)b (13.000 | 283.059)b (12.000 | 286.062)b (11.000 | 286.062)b (10.000 | 319.069)b (9.000 | 319.071)b (8.000 | 319.071)b (5.000 | 364.075)b (4.000 | 367.076)b (3.000 | 372.077)b (2.000 | 372.077)b (1.000 | 372.079);;;; Solution Found
; States evaluated: 884
; Cost: 372.079
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
40.012: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
41.013: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
42.014: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
43.015: (go_to mine workshop)  [13.000]
56.016: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
61.017: (go_to workshop mine)  [13.000]
74.018: (go_to mine mahoganyforest)  [30.000]
104.019: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
106.020: (go_to mahoganyforest mine)  [30.000]
136.021: (go_to mine workshop)  [13.000]
149.022: (assemble_a_loom wood1 workshop)  [5.000]
154.023: (go_to workshop mine)  [13.000]
167.024: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
168.025: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
169.026: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
170.027: (go_to mine workshop)  [13.000]
183.028: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,30.000])
188.029: (go_to workshop mine)  [13.000]
201.030: (place_chests chest1)  [0.001]
201.032: (go_to mine workshop)  [13.000]
214.033: (assemble_an_anvil iron1 workshop)  [7.000]
221.034: (go_to workshop mine)  [13.000]
234.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
235.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
236.037: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
237.038: (go_to mine workshop)  [13.000]
250.039: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,30.000])
253.040: (go_to workshop mine)  [13.000]
266.041: (place_swords sword1)  [0.001]
266.043: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
267.044: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
268.045: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
269.046: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
270.047: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
271.048: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
272.049: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
273.050: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
274.051: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
275.052: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
276.053: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
277.054: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
278.055: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
279.056: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
280.057: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
281.058: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
282.059: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
283.060: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
284.061: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
285.062: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
286.063: (go_to mine workshop)  [13.000]
299.064: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,91.286])
304.065: (go_to workshop forest)  [3.000]
307.066: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
309.067: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
311.068: (go_to forest workshop)  [3.000]
314.069: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,18.257])
319.070: (place_beds bed1)  [0.001]
319.072: (go_to workshop forest)  [3.000]
322.073: (go_to forest mahoganyforest)  [20.000]
342.074: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
344.075: (go_to mahoganyforest forest)  [20.000]
364.076: (go_to forest workshop)  [3.000]
367.077: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,25.250])
372.078: (place_ivy_chests ivychest)  [0.001]

 Execution time of Domain\Problem 20 
00:07:50.1980977
