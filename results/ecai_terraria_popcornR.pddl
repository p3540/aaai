the results of terraria tests in POPcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: ...\terraria_capacity\domain1.pddl
File: ...\terraria_capacity\p1.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 22.000
b (21.000 | 2.000)b (20.000 | 2.000)b (15.000 | 4.001)b (14.000 | 4.001)b (13.000 | 6.002)b (12.000 | 6.002)b (11.000 | 8.003)b (10.000 | 8.003)b (9.000 | 13.004)b (8.000 | 13.004)b (5.000 | 23.006)b (4.000 | 23.006)b (3.000 | 28.007)b (2.000 | 28.007)b (1.000 | 28.009);;;; Solution Found
; States evaluated: 19
; Cost: 28.009
0.000: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
2.001: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
4.002: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
6.003: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
8.004: (assemble_a_sawmill wood1 iron1 chain1)  [5.000]
13.005: (assemble_a_loom wood1)  [5.000]
18.006: (make_silk silk1 cobweb1)  [5.000]  (?silkmade [5.000,27.143])
23.007: (assemble_beds wood1 silk1 bed1)  [5.000]  (?beds [3.000,5.400])
28.008: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 1 
00:00:00.2289832

 DOMAIN/PROBLEM 2 

File: ...\terraria_capacity\domain2.pddl
File: ...\terraria_capacity\p2.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 30.000
b (29.000 | 2.000)b (28.000 | 2.000)b (27.000 | 4.001)b (26.000 | 4.001)b (21.000 | 6.002)b (20.000 | 6.002)b (19.000 | 8.003)b (18.000 | 8.003)b (17.000 | 10.004)b (16.000 | 10.004)b (15.000 | 12.005)b (14.000 | 12.005)b (13.000 | 14.006)b (12.000 | 14.006)b (11.000 | 16.007)b (10.000 | 16.007)b (9.000 | 21.008)b (8.000 | 21.008)b (5.000 | 31.010)b (4.000 | 31.010)b (3.000 | 36.011)b (2.000 | 36.011)b (1.000 | 36.013);;;; Solution Found
; States evaluated: 27
; Cost: 36.013
0.000: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
2.001: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
4.002: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
6.003: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
8.004: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
10.005: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
12.006: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
14.007: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
16.008: (assemble_a_sawmill wood1 iron1 chain1)  [5.000]
21.009: (assemble_a_loom wood1)  [5.000]
26.010: (make_silk silk1 cobweb1)  [5.000]  (?silkmade [5.000,52.857])
31.011: (assemble_beds wood1 silk1 bed1)  [5.000]  (?beds [3.000,10.571])
36.012: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 2 
00:00:00.3466741

 DOMAIN/PROBLEM 3 

File: ...\terraria_capacity\domain3.pddl
File: ...\terraria_capacity\p3.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 38.000
b (37.000 | 2.000)b (36.000 | 2.000)b (35.000 | 4.001)b (34.000 | 4.001)b (33.000 | 6.002)b (32.000 | 6.002)b (27.000 | 8.003)b (26.000 | 8.003)b (25.000 | 10.004)b (24.000 | 10.004)b (23.000 | 12.005)b (22.000 | 12.005)b (21.000 | 14.006)b (20.000 | 14.006)b (19.000 | 16.007)b (18.000 | 16.007)b (17.000 | 18.008)b (16.000 | 18.008)b (15.000 | 20.009)b (14.000 | 20.009)b (13.000 | 22.010)b (12.000 | 22.010)b (11.000 | 24.011)b (10.000 | 24.011)b (9.000 | 29.012)b (8.000 | 29.012)b (5.000 | 39.014)b (4.000 | 39.014)b (3.000 | 44.015)b (2.000 | 44.015)b (1.000 | 44.017);;;; Solution Found
; States evaluated: 35
; Cost: 44.017
0.000: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
2.001: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
4.002: (cut_a_tree wood1)  [2.000]  (?w [5.000,100.000])
6.003: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
8.004: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
10.005: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
12.006: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
14.007: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
16.008: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
18.009: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
20.010: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
22.011: (find_resources cobweb1 iron1 chain1)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
24.012: (assemble_a_sawmill wood1 iron1 chain1)  [5.000]
29.013: (assemble_a_loom wood1)  [5.000]
34.014: (make_silk silk1 cobweb1)  [5.000]  (?silkmade [5.000,78.571])
39.015: (assemble_beds wood1 silk1 bed1)  [5.000]  (?beds [3.000,15.714])
44.016: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 3 
00:00:00.5611503

 DOMAIN/PROBLEM 4 

File: ...\terraria_capacity\domain4.pddl
File: ...\terraria_capacity\p4.pddl
Number of literals: 8
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

63% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 33.000
b (32.000 | 10.000)b (31.000 | 15.002)b (30.000 | 15.002)b (29.000 | 17.003)b (28.000 | 17.003)b (27.000 | 20.004)b (22.000 | 35.006)b (21.000 | 35.006)b (20.000 | 37.007)b (19.000 | 37.007)b (18.000 | 39.008)b (17.000 | 39.008)b (16.000 | 41.009)b (15.000 | 41.009)b (14.000 | 43.010)b (13.000 | 43.010)b (12.000 | 45.011)b (11.000 | 45.011)b (10.000 | 58.012)b (9.000 | 63.013)b (8.000 | 63.013)b (7.000 | 68.014)b (6.000 | 68.014)b (5.000 | 73.015)b (4.000 | 73.015)b (3.000 | 78.016)b (2.000 | 78.016)b (1.000 | 78.018);;;; Solution Found
; States evaluated: 33
; Cost: 78.018
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
35.007: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
37.008: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
39.009: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
41.010: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
43.011: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
45.012: (go_to mine workshop)  [13.000]
58.013: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
63.014: (assemble_a_loom wood1 workshop)  [5.000]
68.015: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,52.857])
73.016: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,10.571])
78.017: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 4 
00:00:00.3886050

 DOMAIN/PROBLEM 5 

File: ...\terraria_capacity\domain5.pddl
File: ...\terraria_capacity\p5.pddl
Number of literals: 8
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

63% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000
b (40.000 | 10.000)b (39.000 | 15.002)b (38.000 | 15.002)b (37.000 | 17.003)b (36.000 | 17.003)b (35.000 | 19.004)b (34.000 | 19.004)b (33.000 | 22.005)b (28.000 | 37.007)b (27.000 | 37.007)b (26.000 | 39.008)b (25.000 | 39.008)b (24.000 | 41.009)b (23.000 | 41.009)b (22.000 | 43.010)b (21.000 | 43.010)b (20.000 | 45.011)b (19.000 | 45.011)b (18.000 | 47.012)b (17.000 | 47.012)b (16.000 | 49.013)b (15.000 | 49.013)b (14.000 | 51.014)b (13.000 | 51.014)b (12.000 | 53.015)b (11.000 | 53.015)b (10.000 | 66.016)b (9.000 | 71.017)b (8.000 | 71.017)b (7.000 | 76.018)b (6.000 | 76.018)b (5.000 | 81.019)b (4.000 | 81.019)b (3.000 | 86.020)b (2.000 | 86.020)b (1.000 | 86.022);;;; Solution Found
; States evaluated: 41
; Cost: 86.022
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
37.008: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
39.009: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
41.010: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
43.011: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
45.012: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
47.013: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
49.014: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
51.015: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
53.016: (go_to mine workshop)  [13.000]
66.017: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
71.018: (assemble_a_loom wood1 workshop)  [5.000]
76.019: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,78.571])
81.020: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,15.714])
86.021: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 5 
00:00:00.5863079

 DOMAIN/PROBLEM 6 

File: ...\terraria_capacity\domain6.pddl
File: ...\terraria_capacity\p6.pddl
Number of literals: 8
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%] [180%] [190%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

63% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000
b (40.000 | 10.000)b (39.000 | 15.002)b (38.000 | 15.002)b (37.000 | 17.003)b (36.000 | 17.003)b (35.000 | 19.004)b (34.000 | 19.004)b (33.000 | 22.005)b (28.000 | 37.007)b (27.000 | 37.007)b (26.000 | 55.009)b (25.000 | 55.009)b (24.000 | 60.010)b (23.000 | 60.010)b (22.000 | 77.013)b (21.000 | 77.013)b (20.000 | 79.014)b (19.000 | 79.014)b (18.000 | 81.015)b (17.000 | 81.015)b (16.000 | 85.017)b (15.000 | 85.017)b (14.000 | 87.018)b (13.000 | 87.018)b (12.000 | 89.019)b (11.000 | 89.019)b (10.000 | 93.021)b (9.000 | 93.021)b (8.000 | 95.022)b (7.000 | 95.022)b (6.000 | 108.023)b (5.000 | 113.024)b (4.000 | 113.024)b (3.000 | 118.025)b (2.000 | 118.025)b (1.000 | 118.027);;;; Solution Found
; States evaluated: 59
; Cost: 118.027
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
19.005: (go_to forest workshop)  [3.000]
22.006: (go_to workshop mine)  [13.000]
35.007: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
37.008: (go_to mine workshop)  [13.000]
50.009: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
55.010: (assemble_a_loom wood1 workshop)  [5.000]
60.011: (go_to workshop mine)  [13.000]
73.012: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
75.013: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
77.014: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
79.015: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
81.016: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
83.017: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
85.018: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
87.019: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
89.020: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
91.021: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
93.022: (find_resources cobweb1 iron1 chain1 mine)  [2.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?chain_found [1.000,10.000])
95.023: (go_to mine workshop)  [13.000]
108.024: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,76.857])
113.025: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,15.371])
118.026: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 6 
00:00:01.1038044

 DOMAIN/PROBLEM 7 

File: ...\terraria_capacity\domain7.pddl
File: ...\terraria_capacity\p7.pddl
Number of literals: 8
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock lead1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

60% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 10.000)b (27.000 | 15.002)b (26.000 | 15.002)b (25.000 | 18.003)b (18.000 | 41.005)b (17.000 | 41.005)b (16.000 | 59.007)b (15.000 | 59.007)b (14.000 | 64.008)b (13.000 | 64.008)b (11.000 | 82.010)b (10.000 | 102.012)b (9.000 | 102.012)b (8.000 | 112.013)b (7.000 | 112.013)b (6.000 | 133.017)b (5.000 | 138.018)b (4.000 | 138.018)b (3.000 | 143.019)b (2.000 | 143.019)b (1.000 | 143.021);;;; Solution Found
; States evaluated: 46
; Cost: 143.021
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (go_to forest workshop)  [3.000]
18.004: (go_to workshop mine)  [13.000]
31.005: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
41.006: (go_to mine workshop)  [13.000]
54.007: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
59.008: (assemble_a_loom wood1 workshop)  [5.000]
64.009: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,10.125])
69.010: (go_to workshop mine)  [13.000]
82.011: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
92.012: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
102.013: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
112.014: (go_to mine workshop)  [13.000]
125.015: (go_to workshop forest)  [3.000]
128.016: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
130.017: (go_to forest workshop)  [3.000]
133.018: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,29.000])
138.019: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,5.800])
143.020: (place_products bed1 chest1)  [0.001]

 Execution time of Domain\Problem 7 
00:00:00.7600305

 DOMAIN/PROBLEM 8 

File: ...\terraria_capacity\domain8.pddl
File: ...\terraria_capacity\p8.pddl
Number of literals: 9
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock lead1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 10.000)b (37.000 | 15.002)b (36.000 | 15.002)b (35.000 | 17.003)b (34.000 | 17.003)b (33.000 | 20.004)b (26.000 | 43.006)b (25.000 | 43.006)b (24.000 | 61.008)b (23.000 | 61.008)b (22.000 | 66.009)b (21.000 | 66.009)b (20.000 | 71.010)b (19.000 | 71.010)b (18.000 | 94.014)b (17.000 | 94.014)b (16.000 | 104.015)b (15.000 | 104.015)b (14.000 | 114.016)b (13.000 | 114.016)b (12.000 | 134.018)b (11.000 | 134.018)b (10.000 | 144.019)b (9.000 | 144.019)b (8.000 | 154.020)b (7.000 | 154.020)b (6.000 | 175.024)b (5.000 | 180.025)b (4.000 | 180.025)b (3.000 | 185.026)b (2.000 | 185.026)b (1.000 | 185.028);;;; Solution Found
; States evaluated: 74
; Cost: 185.028
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
43.007: (go_to mine workshop)  [13.000]
56.008: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
61.009: (assemble_a_loom wood1 workshop)  [5.000]
66.010: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,12.000])
71.011: (place_chests chest1)  [0.001]
71.013: (go_to workshop mine)  [13.000]
84.014: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
94.015: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
104.016: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
114.017: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
124.018: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
134.019: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
144.020: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
154.021: (go_to mine workshop)  [13.000]
167.022: (go_to workshop forest)  [3.000]
170.023: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
172.024: (go_to forest workshop)  [3.000]
175.025: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,53.000])
180.026: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,10.600])
185.027: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 8 
00:00:01.7093140

 DOMAIN/PROBLEM 9 

File: ...\terraria_capacity\domain9.pddl
File: ...\terraria_capacity\p9.pddl
Number of literals: 9
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock lead1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 10.000)b (37.000 | 15.002)b (36.000 | 15.002)b (35.000 | 17.003)b (34.000 | 17.003)b (33.000 | 20.004)b (28.000 | 43.006)b (27.000 | 43.006)b (26.000 | 53.007)b (25.000 | 53.007)b (24.000 | 71.009)b (23.000 | 71.009)b (22.000 | 76.010)b (21.000 | 76.010)b (20.000 | 81.011)b (19.000 | 81.011)b (18.000 | 104.015)b (17.000 | 104.015)b (16.000 | 124.017)b (15.000 | 124.017)b (14.000 | 144.019)b (13.000 | 144.019)b (12.000 | 164.021)b (11.000 | 164.021)b (10.000 | 184.023)b (9.000 | 184.023)b (8.000 | 204.025)b (7.000 | 204.025)b (6.000 | 225.029)b (5.000 | 230.030)b (4.000 | 230.030)b (3.000 | 235.031)b (2.000 | 235.031)b (1.000 | 235.033);;;; Solution Found
; States evaluated: 100
; Cost: 235.033
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
43.007: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
53.008: (go_to mine workshop)  [13.000]
66.009: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
71.010: (assemble_a_loom wood1 workshop)  [5.000]
76.011: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,14.250])
81.012: (place_chests chest1)  [0.001]
81.014: (go_to workshop mine)  [13.000]
94.015: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
104.016: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
114.017: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
124.018: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
134.019: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
144.020: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
154.021: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
164.022: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
174.023: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
184.024: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
194.025: (find_resources cobweb1 iron1 lead1 chain1 mine)  [10.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])
204.026: (go_to mine workshop)  [13.000]
217.027: (go_to workshop forest)  [3.000]
220.028: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
222.029: (go_to forest workshop)  [3.000]
225.030: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,52.286])
230.031: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,10.457])
235.032: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 9 
00:00:03.8187357

 DOMAIN/PROBLEM 10 

File: ...\terraria_capacity\domain10.pddl
File: ...\terraria_capacity\p10.pddl
Number of literals: 11
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

50% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 47.000
b (46.000 | 10.000)b (45.000 | 15.002)b (44.000 | 15.002)b (43.000 | 17.003)b (42.000 | 17.003)b (41.000 | 20.004)b (38.000 | 34.006)b (37.000 | 34.006)b (34.000 | 35.007)b (33.000 | 35.007)b (32.000 | 36.008)b (31.000 | 36.008)b (30.000 | 56.010)b (29.000 | 56.010)b (28.000 | 61.011)b (27.000 | 61.011)b (26.000 | 66.012)b (25.000 | 66.012)b (24.000 | 71.013)b (23.000 | 71.013)b (22.000 | 74.014)b (21.000 | 74.014)b (20.000 | 89.017)b (19.000 | 89.017)b (18.000 | 91.019)b (17.000 | 91.019)b (16.000 | 94.022)b (15.000 | 94.022)b (14.000 | 96.024)b (13.000 | 96.024)b (12.000 | 99.027)b (11.000 | 99.027)b (10.000 | 112.028)b (9.000 | 117.029)b (8.000 | 117.029)b (7.000 | 122.030)b (6.000 | 122.030)b (5.000 | 130.037)b (4.000 | 133.038)b (3.000 | 138.039)b (2.000 | 138.039)b (1.000 | 138.041);;;; Solution Found
; States evaluated: 838
; Cost: 138.041
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
34.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
35.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.009: (go_to mine workshop)  [13.000]
49.010: (assemble_an_anvil iron1 workshop)  [7.000]
56.011: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
61.012: (assemble_a_loom wood1 workshop)  [5.000]
66.013: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,17.750])
71.014: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,3.600])
74.015: (go_to workshop mine)  [13.000]
87.016: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
88.017: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
89.018: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
90.019: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
91.020: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
92.021: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
93.022: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
94.023: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
95.024: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
96.025: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
97.026: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
98.027: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
99.028: (go_to mine workshop)  [13.000]
112.029: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,52.857])
117.030: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,10.467])
122.031: (place_beds bed1)  [0.001]
122.033: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,16.599])
125.034: (place_swords sword1)  [0.001]
125.036: (go_to workshop forest)  [3.000]
128.037: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
130.038: (go_to forest workshop)  [3.000]
133.039: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,13.375])
138.040: (place_chests chest1)  [0.001]

 Execution time of Domain\Problem 10 
00:02:35.5911944

 DOMAIN/PROBLEM 11 

File: ...\terraria_capacity\domain11.pddl
File: ...\terraria_capacity\p11.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 54.000
b (53.000 | 10.000)b (52.000 | 15.002)b (51.000 | 15.002)b (50.000 | 17.003)b (49.000 | 17.003)b (48.000 | 33.005)b (45.000 | 34.006)b (44.000 | 34.006)b (41.000 | 35.007)b (40.000 | 35.007)b (39.000 | 36.008)b (38.000 | 36.008)b (37.000 | 38.010)b (36.000 | 38.010)b (35.000 | 40.012)b (34.000 | 40.012)b (33.000 | 58.014)b (32.000 | 71.015)b (31.000 | 103.017)b (29.000 | 133.018)b (28.000 | 153.020)b (27.000 | 153.020)b (26.000 | 158.021)b (25.000 | 158.021)b (24.000 | 163.022)b (23.000 | 163.022)b (22.000 | 166.023)b (21.000 | 166.023)b (20.000 | 171.024)b (19.000 | 171.024)b (18.000 | 171.026)b (17.000 | 171.026)b (16.000 | 184.029)b (15.000 | 184.029)b (14.000 | 185.032)b (13.000 | 185.032)b (12.000 | 187.034)b (11.000 | 187.034)b (10.000 | 190.037)b (9.000 | 190.037)b (8.000 | 192.039)b (7.000 | 192.039)b (6.000 | 213.043)b (5.000 | 218.044)b (4.000 | 218.044)b (3.000 | 223.045)b (2.000 | 223.045)b (1.000 | 223.047);;;; Solution Found
; States evaluated: 150
; Cost: 223.047
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
34.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
35.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.012: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
40.013: (go_to mine workshop)  [13.000]
53.014: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
58.015: (go_to workshop mine)  [13.000]
71.016: (go_to mine mahoganyforest)  [30.000]
101.017: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
103.018: (go_to mahoganyforest mine)  [30.000]
133.019: (go_to mine workshop)  [13.000]
146.020: (assemble_an_anvil iron1 workshop)  [7.000]
153.021: (assemble_a_loom wood1 workshop)  [5.000]
158.022: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,22.625])
163.023: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,22.900])
166.024: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,12.750])
171.025: (place_ivy_chests ivychest)  [0.001]
171.027: (go_to workshop mine)  [13.000]
184.028: (place_chests chest1)  [0.001]
184.030: (place_swords sword1)  [0.001]
184.032: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
185.033: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
186.034: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
187.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
188.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
189.037: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
190.038: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
191.039: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
192.040: (go_to mine workshop)  [13.000]
205.041: (go_to workshop forest)  [3.000]
208.042: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
210.043: (go_to forest workshop)  [3.000]
213.044: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,50.143])
218.045: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,10.029])
223.046: (place_beds bed1)  [0.001]

 Execution time of Domain\Problem 11 

00:00:12.8353305

 DOMAIN/PROBLEM 12 

File: ...\terraria_capacity\domain12.pddl
File: ...\terraria_capacity\p12.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (stock cobweb1) are preferable
Have identified that bigger values of (stock iron1) are preferable
Have identified that bigger values of (stock mahoganywood) are preferable
Have identified that bigger values of (stock lead1) are preferable
Have identified that bigger values of (stock chain1) are preferable
Have identified that bigger values of (stock silk1) are preferable
Have identified that bigger values of (stock platinum1) are preferable
Have identified that bigger values of (stock bed1) are preferable
Have identified that bigger values of (stock sword1) are preferable
Have identified that bigger values of (stock chest1) are preferable
Have identified that bigger values of (stock wood1) are preferable
Have identified that bigger values of (stock ivychest) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 56.000
b (55.000 | 10.000)b (54.000 | 15.002)b (53.000 | 15.002)b (52.000 | 17.003)b (51.000 | 17.003)b (50.000 | 33.005)b (49.000 | 34.006)b (48.000 | 34.006)b (45.000 | 35.007)b (44.000 | 35.007)b (43.000 | 36.008)b (42.000 | 36.008)b (41.000 | 38.010)b (40.000 | 38.010)b (39.000 | 39.011)b (38.000 | 39.011)b (37.000 | 57.013)b (36.000 | 70.014)b (33.000 | 132.017)b (32.000 | 152.019)b (31.000 | 152.019)b (30.000 | 157.020)b (29.000 | 157.020)b (28.000 | 160.021)b (27.000 | 160.021)b (26.000 | 165.022)b (25.000 | 165.022)b (24.000 | 165.024)b (23.000 | 165.024)b (21.000 | 183.026)b (20.000 | 187.030)b (19.000 | 187.030)b (18.000 | 190.033)b (17.000 | 190.033)b (16.000 | 193.036)b (15.000 | 193.036)b (14.000 | 196.039)b (13.000 | 196.039)b (12.000 | 199.042)b (11.000 | 199.042)b (10.000 | 220.046)b (9.000 | 225.047)b (8.000 | 225.047)b (7.000 | 230.048)b (6.000 | 230.048)b (5.000 | 238.052)b (4.000 | 238.052)b (3.000 | 238.054)b (2.000 | 238.054)b (1.000 | 238.056);;;; Solution Found
; States evaluated: 820
; Cost: 238.056
0.000: (go_to home workshop)  [10.000]
10.001: (go_to workshop forest)  [3.000]
13.002: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
15.003: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
17.004: (go_to forest workshop)  [3.000]
20.005: (go_to workshop mine)  [13.000]
33.006: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
34.007: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
35.008: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
36.009: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
37.010: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
38.011: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
39.012: (go_to mine workshop)  [13.000]
52.013: (assemble_a_sawmill wood1 iron1 chain1 workshop)  [5.000]
57.014: (go_to workshop mine)  [13.000]
70.015: (go_to mine mahoganyforest)  [30.000]
100.016: (cut_mahogany_tree mahoganywood mahoganyforest)  [2.000]  (?w [5.000,100.000])
102.017: (go_to mahoganyforest mine)  [30.000]
132.018: (go_to mine workshop)  [13.000]
145.019: (assemble_an_anvil iron1 workshop)  [7.000]
152.020: (assemble_a_loom wood1 workshop)  [5.000]
157.021: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,17.400])
160.022: (produce_ivy_chests ivychest mahoganywood lead1 iron1 workshop)  [5.000]  (?chests [3.000,12.750])
165.023: (place_ivy_chests ivychest)  [0.001]
165.025: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,18.250])
170.026: (go_to workshop mine)  [13.000]
183.027: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
184.028: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
185.029: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
186.030: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
187.031: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
188.032: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
189.033: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
190.034: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
191.035: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
192.036: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
193.037: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
194.038: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
195.039: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
196.040: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
197.041: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
198.042: (find_resources cobweb1 iron1 lead1 platinum1 chain1 mine)  [1.000]  (?cob [10.000,60.000])  (?iron [5.000,60.000])  (?lead [5.000,60.000])  (?chain_found [1.000,10.000])  (?platinum [5.000,60.000])
199.043: (go_to mine workshop)  [13.000]
212.044: (go_to workshop forest)  [3.000]
215.045: (cut_a_tree wood1 forest)  [2.000]  (?w [5.000,100.000])
217.046: (go_to forest workshop)  [3.000]
220.047: (make_silk silk1 cobweb1 workshop)  [5.000]  (?silkmade [5.000,60.857])
225.048: (assemble_beds wood1 silk1 bed1 workshop)  [5.000]  (?beds [3.000,12.171])
230.049: (place_beds bed1)  [0.001]
230.051: (produce_platinum_swords sword1 platinum1 workshop)  [3.000]  (?swords [3.000,19.600])
233.052: (produce_chests chest1 wood1 lead1 iron1 workshop)  [5.000]  (?chests [3.000,9.625])
238.053: (place_chests chest1)  [0.001]
238.055: (place_swords sword1)  [0.001]

 Execution time of Domain\Problem 12 

00:02:28.2272532

 Execution time of Domain\Problem 1 

00:00:00.2289832

 Execution time of Domain\Problem 2  

00:00:00.3466741

 Execution time of Domain\Problem 3 

00:00:00.5611503

 Execution time of Domain\Problem 4  

00:00:00.3886050

 Execution time of Domain\Problem 5 
 
00:00:00.5863079

 Execution time of Domain\Problem 6 
 
00:00:01.1038044

 Execution time of Domain\Problem 7 
 
00:00:00.7600305

 Execution time of Domain\Problem 8 
 
00:00:01.7093140

 Execution time of Domain\Problem 9 

00:00:03.8187357

 Execution time of Domain\Problem 10 

00:02:35.5911944

 Execution time of Domain\Problem 11 
 
00:00:12.8353305

 Execution time of Domain\Problem 12 
 
00:02:28.2272532
