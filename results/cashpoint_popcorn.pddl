the results of cashpoint tests in popcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile01.pddl
Number of literals: 6
Constructing lookup tables:
Post filtering unreachable actions: 
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000
b (5.000 | 1.000)b (4.000 | 1.000)b (3.000 | 6.001)b (1.000 | 8.002);;;; Solution Found
; States evaluated: 8
; Cost: 8.003
0.000: (buy_with_cash item0 location0 currency0)  [1.000]
1.001: (goto location0 bank0)  [5.000]
6.002: (withdraw bank0 currency0)  [2.000]
8.003: (save_for_later currency0) 

 Execution time of Domain\Problem 1 
00:00:00.4464719

 DOMAIN/PROBLEM 2 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile02.pddl
Number of literals: 9
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
60% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 11.000

 Execution time of Domain\Problem 2 
00:30:00.1973491

 DOMAIN/PROBLEM 3 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile03.pddl
Number of literals: 11
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
58% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000
b (15.000 | 5.000)b (14.000 | 10.001)b (13.000 | 12.002)b (12.000 | 12.002)b (11.000 | 12.003)b (10.000 | 18.004)b (9.000 | 23.005)b (8.000 | 31.008)b (7.000 | 31.008)b (6.000 | 36.009)b (4.000 | 38.010)b (3.000 | 38.011)b (1.000 | 40.011);;;; Solution Found
; States evaluated: 69
; Cost: 40.012
0.000: (goto location0 location1)  [5.000]
5.001: (goto location1 changeoffice0)  [5.000]
10.002: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
12.003: (save_for_later currency0) 
12.003: (goto changeoffice0 location1)  [5.000]
17.004: (buy_with_cash item0 location1 currency2)  [1.000]
18.005: (goto location1 changeoffice0)  [5.000]
23.006: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
25.007: (goto changeoffice0 location1)  [5.000]
30.008: (buy_with_cash item1 location1 currency0)  [1.000]
31.009: (goto location1 bank0)  [5.000]
36.010: (withdraw bank0 currency2)  [2.000]
38.011: (save_for_later currency2) 
38.011: (withdraw bank0 currency1)  [2.000]
40.012: (save_for_later currency1) 

 Execution time of Domain\Problem 3 
00:00:00.9844541

 DOMAIN/PROBLEM 4 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile04.pddl
Number of literals: 12
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 14.000
b (13.000 | 1.000)b (12.000 | 1.000)b (11.000 | 7.002)b (10.000 | 7.002)b (9.000 | 12.003)b (8.000 | 19.005)b (7.000 | 33.009)b (5.000 | 35.010)b (4.000 | 35.011)b (3.000 | 40.011)b (2.000 | 54.015)b (1.000 | 55.016);;;; Solution Found
; States evaluated: 305
; Cost: 55.016
0.000: (buy_with_cash item2 location0 currency1)  [1.000]
1.001: (goto location0 location2)  [5.000]
6.002: (buy_with_cash item0 location2 currency0)  [1.000]
7.003: (goto location2 location0)  [5.000]
12.004: (goto location0 changeoffice0)  [5.000]
17.005: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
19.006: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
21.007: (goto changeoffice0 location0)  [5.000]
26.008: (goto location0 bank0)  [5.000]
31.009: (withdraw bank0 currency1)  [2.000]
33.010: (withdraw bank0 currency0)  [2.000]
33.010: (save_for_later currency1) 
35.011: (save_for_later currency0) 
35.011: (goto bank0 location0)  [5.000]
40.012: (goto location0 changeoffice0)  [5.000]
45.013: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
47.014: (exchange_in_changeoffice changeoffice0 currency0 currency1)  [2.000]
49.015: (goto changeoffice0 location0)  [5.000]
54.016: (buy_with_cash item1 location0 currency1)  [1.000]

 Execution time of Domain\Problem 4 
00:00:03.2156543

 DOMAIN/PROBLEM 5 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile05.pddl
Number of literals: 15
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000

 Execution time of Domain\Problem 5 
00:30:00.1827297

 DOMAIN/PROBLEM 6 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile06.pddl
Number of literals: 17
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 21.000
b (20.000 | 5.000)b (19.000 | 10.001)b (18.000 | 12.002)b (17.000 | 12.002)b (16.000 | 14.004)b (14.000 | 26.006)b (13.000 | 26.007)b (11.000 | 28.007)b (10.000 | 28.008)b (9.000 | 33.008)b (8.000 | 43.012)b (7.000 | 48.013)b (6.000 | 49.014)b (5.000 | 49.014)b (4.000 | 50.015)b (3.000 | 50.015)b (2.000 | 55.016)b (1.000 | 56.017);;;; Solution Found
; States evaluated: 152
; Cost: 56.017
0.000: (goto location2 location3)  [5.000]
5.001: (goto location3 changeoffice0)  [5.000]
10.002: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
12.003: (exchange_in_changeoffice changeoffice0 currency0 currency2)  [2.000]
14.004: (save_for_later currency2) 
14.004: (goto changeoffice0 location3)  [5.000]
19.005: (goto location3 bank0)  [5.000]
24.006: (withdraw bank0 currency1)  [2.000]
26.007: (save_for_later currency1) 
26.007: (withdraw bank0 currency0)  [2.000]
28.008: (save_for_later currency0) 
28.008: (goto bank0 location3)  [5.000]
33.009: (buy_with_cash item4 location3 currency2)  [1.000]
34.010: (goto location3 bank0)  [5.000]
39.011: (withdraw bank0 currency2)  [2.000]
41.012: (withdraw bank0 currency0)  [2.000]
43.013: (goto bank0 location3)  [5.000]
48.014: (buy_with_cash item2 location3 currency2)  [1.000]
49.015: (buy_with_cash item3 location3 currency0)  [1.000]
50.016: (goto location3 location1)  [5.000]
55.017: (buy_with_cash item0 location1 currency0)  [1.000]

 Execution time of Domain\Problem 6 
00:00:02.1979224

 DOMAIN/PROBLEM 7 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile07.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
66% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 26.000
b (25.000 | 5.000)b (24.000 | 10.001)b (23.000 | 14.003)b (22.000 | 16.004)b (21.000 | 23.006)b (20.000 | 24.007)b (19.000 | 24.007)b (17.000 | 31.009)b (15.000 | 33.010)b (13.000 | 35.011)b (12.000 | 40.012)b (11.000 | 47.015)b (10.000 | 53.017)b (9.000 | 59.019)b (7.000 | 65.022)b (6.000 | 65.023)b (4.000 | 67.023)b (3.000 | 72.024)b (1.000 | 74.025);;;; Solution Found
; States evaluated: 740
; Cost: 74.026
0.000: (goto location3 location2)  [5.000]
5.001: (goto location2 changeoffice0)  [5.000]
10.002: (exchange_in_changeoffice changeoffice0 currency1 currency3)  [2.000]
12.003: (exchange_in_changeoffice changeoffice0 currency3 currency0)  [2.000]
14.004: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
16.005: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
18.006: (save_for_later currency0) 
18.006: (goto changeoffice0 location2)  [5.000]
23.007: (buy_with_cash item4 location2 currency0)  [1.000]
24.008: (goto location2 bank0)  [5.000]
29.009: (withdraw bank0 currency3)  [2.000]
31.010: (withdraw bank0 currency2)  [2.000]
33.011: (withdraw bank0 currency1)  [2.000]
35.012: (goto bank0 location2)  [5.000]
40.013: (buy_with_cash item1 location2 currency1)  [1.000]
41.014: (goto location2 location0)  [5.000]
46.015: (buy_with_cash item0 location0 currency1)  [1.000]
47.016: (buy_with_cash item3 location0 currency3)  [1.000]
48.017: (goto location0 location2)  [5.000]
53.018: (buy_with_cash item2 location2 currency2)  [1.000]
54.019: (goto location2 changeoffice0)  [5.000]
59.020: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
61.021: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
63.022: (exchange_in_changeoffice changeoffice0 currency1 currency2)  [2.000]
65.023: (save_for_later currency2) 
65.023: (exchange_in_changeoffice changeoffice0 currency3 currency1)  [2.000]
67.024: (save_for_later currency1) 
67.024: (goto changeoffice0 bank0)  [5.000]
72.025: (withdraw bank0 currency3)  [2.000]
74.026: (save_for_later currency3) 

 Execution time of Domain\Problem 7 
00:00:14.5470072

 DOMAIN/PROBLEM 8 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile08.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 25.000

 Execution time of Domain\Problem 8 
00:30:00.1870708

 DOMAIN/PROBLEM 9 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile09.pddl
Number of literals: 20
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 5.000)b (27.000 | 10.001)b (25.000 | 12.002)b (23.000 | 14.003)b (22.000 | 20.005)b (21.000 | 25.006)b (20.000 | 31.008)b (19.000 | 36.009)b (18.000 | 37.010)b (17.000 | 37.010)b (16.000 | 42.011)b (14.000 | 49.013)b (13.000 | 49.014)b (11.000 | 51.014)b (9.000 | 53.015)b (8.000 | 58.016)b (7.000 | 60.018)b (6.000 | 66.020)b (4.000 | 68.021)b (3.000 | 73.022)b (1.000 | 75.023);;;; Solution Found
; States evaluated: 363
; Cost: 75.024
0.000: (goto location4 location2)  [5.000]
5.001: (goto location2 changeoffice0)  [5.000]
10.002: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
12.003: (exchange_in_changeoffice changeoffice0 currency0 currency1)  [2.000]
14.004: (goto changeoffice0 location2)  [5.000]
19.005: (buy_with_cash item4 location2 currency1)  [1.000]
20.006: (goto location2 changeoffice0)  [5.000]
20.006: (save_for_later currency1) 
25.007: (goto changeoffice0 location1)  [5.000]
30.008: (buy_with_cash item2 location1 currency2)  [1.000]
31.009: (goto location1 location2)  [5.000]
36.010: (buy_with_cash item3 location2 currency0)  [1.000]
37.011: (goto location2 location0)  [5.000]
42.012: (goto location0 bank0)  [5.000]
47.013: (withdraw bank0 currency3)  [2.000]
49.014: (save_for_later currency3) 
49.014: (withdraw bank0 currency2)  [2.000]
51.015: (withdraw bank0 currency0)  [2.000]
53.016: (goto bank0 location0)  [5.000]
58.017: (buy_with_cash item1 location0 currency2)  [1.000]
59.018: (buy_with_cash item0 location0 currency2)  [1.000]
60.019: (buy_with_cash item5 location0 currency0)  [1.000]
61.020: (goto location0 changeoffice0)  [5.000]
66.021: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
68.022: (save_for_later currency0) 
68.022: (goto changeoffice0 bank0)  [5.000]
73.023: (withdraw bank0 currency2)  [2.000]
75.024: (save_for_later currency2) 

 Execution time of Domain\Problem 9 
00:00:04.1252144

 DOMAIN/PROBLEM 10 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile10.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (balance currency4) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (inpocket currency4) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency4) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency4) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
57% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 5.000)b (27.000 | 6.001)b (26.000 | 6.001)b (25.000 | 11.002)b (24.000 | 12.003)b (23.000 | 12.003)b (22.000 | 13.004)b (21.000 | 13.004)b (20.000 | 18.005)b (19.000 | 19.006)b (18.000 | 19.006)b (16.000 | 26.008)b (15.000 | 26.009)b (13.000 | 28.009)b (12.000 | 28.010)b (10.000 | 30.010)b (8.000 | 32.011)b (7.000 | 32.012)b (5.000 | 34.012)b (4.000 | 34.013)b (3.000 | 39.013)b (1.000 | 47.016);;;; Solution Found
; States evaluated: 71
; Cost: 47.017
0.000: (goto location2 location4)  [5.000]
5.001: (buy_with_cash item3 location4 currency0)  [1.000]
6.002: (goto location4 location1)  [5.000]
11.003: (buy_with_cash item1 location1 currency3)  [1.000]
12.004: (buy_with_cash item0 location1 currency2)  [1.000]
13.005: (goto location1 location0)  [5.000]
18.006: (buy_with_cash item2 location0 currency1)  [1.000]
19.007: (goto location0 bank0)  [5.000]
24.008: (withdraw bank0 currency4)  [2.000]
26.009: (save_for_later currency4) 
26.009: (withdraw bank0 currency3)  [2.000]
28.010: (save_for_later currency3) 
28.010: (withdraw bank0 currency2)  [2.000]
30.011: (withdraw bank0 currency1)  [2.000]
32.012: (save_for_later currency1) 
32.012: (withdraw bank0 currency0)  [2.000]
34.013: (save_for_later currency0) 
34.013: (goto bank0 location0)  [5.000]
39.014: (buy_with_cash item4 location0 currency2)  [1.000]
40.015: (goto location0 bank0)  [5.000]
45.016: (withdraw bank0 currency2)  [2.000]
47.017: (save_for_later currency2) 

 Execution time of Domain\Problem 10 
00:00:01.1993595

 DOMAIN/PROBLEM 11 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile11.pddl
Number of literals: 20
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
77% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 33.000

 Execution time of Domain\Problem 11 
00:30:00.1730611

 DOMAIN/PROBLEM 12 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile12.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 37.000

 Execution time of Domain\Problem 12 
00:30:00.1653691

 DOMAIN/PROBLEM 13 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile13.pddl
Number of literals: 21
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000

 Execution time of Domain\Problem 13 
00:30:00.2961239

 DOMAIN/PROBLEM 14 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile14.pddl
Number of literals: 22
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 22.000

 Execution time of Domain\Problem 14 
00:30:00.1721993

 DOMAIN/PROBLEM 15 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile15.pddl
Number of literals: 23
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000

 Execution time of Domain\Problem 15 
00:30:00.1900088

 DOMAIN/PROBLEM 16 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile16.pddl
Number of literals: 24
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000
b (40.000 | 5.000)b (39.000 | 6.001)b (38.000 | 6.001)b (37.000 | 11.002)b (36.000 | 12.003)b (35.000 | 12.003)b (34.000 | 18.005)b (33.000 | 18.005)b (32.000 | 23.006)b (31.000 | 29.008)b (30.000 | 29.008)b (29.000 | 30.009)b (28.000 | 30.009)b (27.000 | 35.010)b (26.000 | 41.012)b (25.000 | 41.012)b (24.000 | 46.013)b (22.000 | 53.015)b (21.000 | 58.016)b (20.000 | 63.017)b (15.000 | 65.018)b (14.000 | 65.018)b (13.000 | 67.019)b (12.000 | 75.022)b (11.000 | 80.023)b (10.000 | 82.024)

 Execution time of Domain\Problem 16 
00:00:41.9737861

 DOMAIN/PROBLEM 17 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile17.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank3 currency0) are preferable
Have identified that bigger values of (bank-reserve bank3 currency1) are preferable
Have identified that bigger values of (bank-reserve bank3 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000
b (28.000 | 5.000)b (27.000 | 9.002)b (26.000 | 9.002)b (24.000 | 11.003)b (23.000 | 17.005)b (22.000 | 17.005)b (21.000 | 22.006)b (20.000 | 23.007)b (19.000 | 23.007)b (18.000 | 24.008)b (17.000 | 24.008)b (16.000 | 30.010)b (15.000 | 30.010)b (14.000 | 37.013)b (13.000 | 38.014)b (12.000 | 38.014)b (11.000 | 45.016)b (9.000 | 58.020)b (8.000 | 69.024)b (7.000 | 71.025)b (6.000 | 71.025)b (4.000 | 73.026)b (3.000 | 73.027)b (1.000 | 75.027);;;; Solution Found
; States evaluated: 1209
; Cost: 75.028
0.000: (goto location4 changeoffice0)  [5.000]
5.001: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
7.002: (exchange_in_changeoffice changeoffice0 currency0 currency1)  [2.000]
9.003: (exchange_in_changeoffice changeoffice0 currency0 currency2)  [2.000]
11.004: (goto changeoffice0 location4)  [5.000]
16.005: (buy_with_cash item2 location4 currency1)  [1.000]
17.006: (goto location4 location1)  [5.000]
22.007: (buy_with_cash item7 location1 currency2)  [1.000]
23.008: (buy_with_cash item6 location1 currency2)  [1.000]
24.009: (goto location1 location0)  [5.000]
29.010: (buy_with_cash item5 location0 currency2)  [1.000]
30.011: (buy_with_cash item3 location0 currency1)  [1.000]
31.012: (buy_with_cash item0 location0 currency1)  [1.000]
32.013: (goto location0 location1)  [5.000]
37.014: (buy_with_cash item1 location1 currency2)  [1.000]
38.015: (goto location1 changeoffice0)  [5.000]
43.016: (exchange_in_changeoffice changeoffice0 currency2 currency0)  [2.000]
45.017: (exchange_in_changeoffice changeoffice0 currency0 currency1)  [2.000]
47.018: (goto changeoffice0 location1)  [5.000]
52.019: (buy_with_cash item4 location1 currency1)  [1.000]
53.020: (goto location1 changeoffice0)  [5.000]
58.021: (exchange_in_changeoffice changeoffice0 currency1 currency0)  [2.000]
60.022: (goto changeoffice0 bank0)  [5.000]
65.023: (withdraw bank0 currency2)  [2.000]
67.024: (save_for_later currency2) 
67.024: (withdraw bank0 currency1)  [2.000]
69.025: (withdraw bank0 currency0)  [2.000]
71.026: (withdraw bank0 currency1)  [2.000]
73.027: (save_for_later currency1) 
73.027: (withdraw bank0 currency0)  [2.000]
75.028: (save_for_later currency0) 

 Execution time of Domain\Problem 17 
00:00:19.6112996

 DOMAIN/PROBLEM 18 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile18.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000

 Execution time of Domain\Problem 18 
00:30:00.1769760

 DOMAIN/PROBLEM 19 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile19.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank3 currency0) are preferable
Have identified that bigger values of (bank-reserve bank3 currency1) are preferable
Have identified that bigger values of (bank-reserve bank3 currency2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000

 Execution time of Domain\Problem 19 
00:30:00.1666221

 DOMAIN/PROBLEM 20 

File: ..\cashpoint_simple\domain.pddl
File: ..\cashpoint_simple\pfile20.pddl
Number of literals: 30
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (balance currency0) are preferable
Have identified that bigger values of (balance currency1) are preferable
Have identified that bigger values of (balance currency2) are preferable
Have identified that bigger values of (balance currency3) are preferable
Have identified that bigger values of (balance currency4) are preferable
Have identified that bigger values of (inpocket currency0) are preferable
Have identified that bigger values of (inpocket currency1) are preferable
Have identified that bigger values of (inpocket currency2) are preferable
Have identified that bigger values of (inpocket currency3) are preferable
Have identified that bigger values of (inpocket currency4) are preferable
Have identified that bigger values of (bank-reserve bank0 currency0) are preferable
Have identified that bigger values of (bank-reserve bank0 currency1) are preferable
Have identified that bigger values of (bank-reserve bank0 currency2) are preferable
Have identified that bigger values of (bank-reserve bank0 currency3) are preferable
Have identified that bigger values of (bank-reserve bank0 currency4) are preferable
Have identified that bigger values of (bank-reserve bank1 currency0) are preferable
Have identified that bigger values of (bank-reserve bank1 currency1) are preferable
Have identified that bigger values of (bank-reserve bank1 currency2) are preferable
Have identified that bigger values of (bank-reserve bank1 currency3) are preferable
Have identified that bigger values of (bank-reserve bank1 currency4) are preferable
Have identified that bigger values of (bank-reserve bank2 currency0) are preferable
Have identified that bigger values of (bank-reserve bank2 currency1) are preferable
Have identified that bigger values of (bank-reserve bank2 currency2) are preferable
Have identified that bigger values of (bank-reserve bank2 currency3) are preferable
Have identified that bigger values of (bank-reserve bank2 currency4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 52.000

 Execution time of Domain\Problem 20 
00:30:00.1813982
