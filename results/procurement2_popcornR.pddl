the results of procurement2 tests using popcornX 

 popcornX 


 DOMAIN/PROBLEM 1 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile01.pddl
Number of literals: 67
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

78% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 14.000
b (13.000 | 5.000)b (12.000 | 11.002)b (11.000 | 11.002)b (10.000 | 16.003)b (9.000 | 22.005)b (8.000 | 22.005)b (7.000 | 23.006)b (5.000 | 28.007)b (3.000 | 29.008)b (2.000 | 34.009)b (1.000 | 35.010);;;; Solution Found
; States evaluated: 23
; Cost: 35.010
0.000: (go_to workshop1 workshop2)  [5.000]
5.001: (go_to workshop2 supplier2)  [5.000]
10.002: (supply_raw_material_p p supplier2)  [1.000]  (?batchsize [1.000,100.000])
11.003: (go_to supplier2 workshop2)  [5.000]
16.004: (go_to workshop2 supplier1)  [5.000]
21.005: (supply_raw_material_o o supplier1)  [1.000]  (?batchsize [1.000,100.000])
22.006: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
23.007: (go_to supplier1 workshop2)  [5.000]
28.008: (produce_k k n o p workshop2)  [1.000]  (?batchsize [1.000,16.667])
29.009: (go_to workshop2 customer0)  [5.000]
34.010: (deliver_to_customer customer0 k)  [1.000]

 Execution time of Domain\Problem 1 
00:00:00.7410963

 DOMAIN/PROBLEM 2 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile02.pddl
Number of literals: 89
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 25.000
b (24.000 | 6.001)b (23.000 | 6.001)b (22.000 | 7.002)b (21.000 | 7.002)b (20.000 | 8.003)b (19.000 | 8.003)b (18.000 | 13.004)b (16.000 | 14.005)b (15.000 | 20.007)b (14.000 | 20.007)b (13.000 | 25.008)b (12.000 | 31.010)b (10.000 | 36.011)b (9.000 | 37.012)b (7.000 | 42.013)b (5.000 | 43.014)b (3.000 | 44.015)b (2.000 | 49.016)b (1.000 | 50.017);;;; Solution Found
; States evaluated: 46
; Cost: 50.017
0.000: (go_to workshop1 supplier3)  [5.000]
5.001: (supply_raw_material_q q supplier3)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_q q supplier3)  [1.000]  (?batchsize [1.000,100.000])
7.003: (supply_raw_material_i i supplier3)  [1.000]  (?batchsize [1.000,100.000])
8.004: (go_to supplier3 workshop3)  [5.000]
13.005: (produce_l l q workshop3)  [1.000]  (?batchsize [1.000,100.000])
14.006: (go_to workshop3 supplier2)  [5.000]
19.007: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
20.008: (go_to supplier2 workshop3)  [5.000]
25.009: (go_to workshop3 supplier1)  [5.000]
30.010: (supply_raw_material_r r supplier1)  [1.000]  (?batchsize [1.000,100.000])
31.011: (go_to supplier1 workshop3)  [5.000]
36.012: (produce_m m r s workshop3)  [1.000]  (?batchsize [1.000,33.333])
37.013: (go_to workshop3 workshop1)  [5.000]
42.014: (produce_h h l m workshop1)  [1.000]  (?batchsize [1.000,16.667])
43.015: (produce_c c h i workshop1)  [1.000]  (?batchsize [1.000,16.667])
44.016: (go_to workshop1 customer3)  [5.000]
49.017: (deliver_to_customer customer3 c)  [1.000]

 Execution time of Domain\Problem 2 
00:00:01.0486964

 DOMAIN/PROBLEM 3 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile03.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

91% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 12.000
b (11.000 | 5.000)b (10.000 | 11.002)b (9.000 | 11.002)b (8.000 | 16.003)b (7.000 | 22.005)b (6.000 | 22.005)b (5.000 | 27.006)b (3.000 | 28.007)b (2.000 | 33.008)b (1.000 | 34.009);;;; Solution Found
; States evaluated: 19
; Cost: 34.009
0.000: (go_to workshop2 workshop3)  [5.000]
5.001: (go_to workshop3 supplier3)  [5.000]
10.002: (supply_raw_material_r r supplier3)  [1.000]  (?batchsize [1.000,100.000])
11.003: (go_to supplier3 workshop3)  [5.000]
16.004: (go_to workshop3 supplier2)  [5.000]
21.005: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
22.006: (go_to supplier2 workshop3)  [5.000]
27.007: (produce_m m r s workshop3)  [1.000]  (?batchsize [1.000,33.333])
28.008: (go_to workshop3 customer4)  [5.000]
33.009: (deliver_to_customer customer4 m)  [1.000]

 Execution time of Domain\Problem 3 
00:00:00.7138793

 DOMAIN/PROBLEM 4 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile04.pddl
Number of literals: 132
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 5.000)b (37.000 | 11.002)b (36.000 | 11.002)b (35.000 | 16.003)b (34.000 | 22.005)b (33.000 | 22.005)b (32.000 | 23.006)b (31.000 | 23.006)b (30.000 | 24.007)b (29.000 | 24.007)b (28.000 | 29.008)b (26.000 | 30.009)b (25.000 | 36.011)b (24.000 | 36.011)b (23.000 | 37.012)b (22.000 | 37.012)b (21.000 | 38.013)b (20.000 | 38.013)b (19.000 | 43.014)b (18.000 | 44.015)b (17.000 | 49.016)b (15.000 | 55.018)b (14.000 | 60.019)b (13.000 | 66.021)b (12.000 | 66.021)b (11.000 | 71.022)b (10.000 | 77.024)b (8.000 | 82.025)b (6.000 | 83.026)b (5.000 | 88.027)b (3.000 | 89.028)b (2.000 | 94.029)b (1.000 | 95.030);;;; Solution Found
; States evaluated: 89
; Cost: 95.030
0.000: (go_to workshop2 workshop4)  [5.000]
5.001: (go_to workshop4 supplier4)  [5.000]
10.002: (supply_raw_material_n n supplier4)  [1.000]  (?batchsize [1.000,100.000])
11.003: (go_to supplier4 workshop4)  [5.000]
16.004: (go_to workshop4 supplier2)  [5.000]
21.005: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
22.006: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
23.007: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
24.008: (go_to supplier2 workshop3)  [5.000]
29.009: (produce_l l q workshop3)  [1.000]  (?batchsize [1.000,100.000])
30.010: (go_to workshop3 supplier1)  [5.000]
35.011: (supply_raw_material_p p supplier1)  [1.000]  (?batchsize [1.000,100.000])
36.012: (supply_raw_material_o o supplier1)  [1.000]  (?batchsize [1.000,100.000])
37.013: (supply_raw_material_j j supplier1)  [1.000]  (?batchsize [1.000,100.000])
38.014: (go_to supplier1 workshop4)  [5.000]
43.015: (produce_k k n o p workshop4)  [1.000]  (?batchsize [1.000,16.667])
44.016: (go_to workshop4 workshop3)  [5.000]
49.017: (go_to workshop3 workshop0)  [5.000]
54.018: (produce_f f j k workshop0)  [1.000]  (?batchsize [1.000,16.667])
55.019: (go_to workshop0 workshop4)  [5.000]
60.020: (go_to workshop4 customer5)  [5.000]
65.021: (deliver_to_customer customer5 f)  [1.000]
66.022: (go_to customer5 workshop4)  [5.000]
71.023: (go_to workshop4 supplier0)  [5.000]
76.024: (supply_raw_material_r r supplier0)  [1.000]  (?batchsize [1.000,100.000])
77.025: (go_to supplier0 workshop4)  [5.000]
82.026: (produce_m m r s workshop4)  [1.000]  (?batchsize [1.000,33.333])
83.027: (go_to workshop4 workshop3)  [5.000]
88.028: (produce_h h l m workshop3)  [1.000]  (?batchsize [1.000,16.667])
89.029: (go_to workshop3 customer4)  [5.000]
94.030: (deliver_to_customer customer4 h)  [1.000]

 Execution time of Domain\Problem 4 
00:00:02.9430863

 DOMAIN/PROBLEM 5 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile05.pddl
Number of literals: 153
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

94% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 41.000
b (40.000 | 6.001)b (39.000 | 6.001)b (38.000 | 7.002)b (37.000 | 7.002)b (36.000 | 12.003)b (35.000 | 18.005)b (34.000 | 18.005)b (33.000 | 23.006)b (32.000 | 29.008)b (31.000 | 29.008)b (30.000 | 30.009)b (29.000 | 30.009)b (28.000 | 31.010)b (26.000 | 36.011)b (25.000 | 37.012)b (24.000 | 42.013)b (23.000 | 48.015)b (22.000 | 48.015)b (21.000 | 49.016)b (20.000 | 49.016)b (19.000 | 55.018)b (17.000 | 60.019)b (16.000 | 65.020)b (14.000 | 66.021)b (12.000 | 67.022)b (11.000 | 72.023)b (10.000 | 78.025)b (9.000 | 78.025)b (8.000 | 83.026)b (6.000 | 89.028)b (5.000 | 94.029)b (3.000 | 95.030)b (2.000 | 100.031)b (1.000 | 101.032);;;; Solution Found
; States evaluated: 110
; Cost: 101.032
0.000: (go_to workshop2 supplier5)  [5.000]
5.001: (supply_raw_material_p p supplier5)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_j j supplier5)  [1.000]  (?batchsize [1.000,100.000])
7.003: (go_to supplier5 workshop4)  [5.000]
12.004: (go_to workshop4 supplier4)  [5.000]
17.005: (supply_raw_material_o o supplier4)  [1.000]  (?batchsize [1.000,100.000])
18.006: (go_to supplier4 workshop4)  [5.000]
23.007: (go_to workshop4 supplier3)  [5.000]
28.008: (supply_raw_material_r r supplier3)  [1.000]  (?batchsize [1.000,100.000])
29.009: (supply_raw_material_i i supplier3)  [1.000]  (?batchsize [1.000,100.000])
30.010: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
31.011: (go_to supplier3 workshop4)  [5.000]
36.012: (produce_k k n o p workshop4)  [1.000]  (?batchsize [1.000,16.667])
37.013: (go_to workshop4 workshop3)  [5.000]
42.014: (go_to workshop3 supplier1)  [5.000]
47.015: (supply_raw_material_s s supplier1)  [1.000]  (?batchsize [1.000,100.000])
48.016: (supply_raw_material_q q supplier1)  [1.000]  (?batchsize [1.000,100.000])
49.017: (go_to supplier1 workshop4)  [5.000]
54.018: (produce_m m r s workshop4)  [1.000]  (?batchsize [1.000,33.333])
55.019: (go_to workshop4 workshop3)  [5.000]
60.020: (go_to workshop3 workshop2)  [5.000]
65.021: (produce_l l q workshop2)  [1.000]  (?batchsize [1.000,50.000])
66.022: (produce_f f j k workshop2)  [1.000]  (?batchsize [1.000,16.667])
67.023: (go_to workshop2 workshop3)  [5.000]
72.024: (go_to workshop3 customer0)  [5.000]
77.025: (deliver_to_customer customer0 f)  [1.000]
78.026: (go_to customer0 workshop3)  [5.000]
83.027: (go_to workshop3 workshop1)  [5.000]
88.028: (produce_h h l m workshop1)  [1.000]  (?batchsize [1.000,10.000])
89.029: (go_to workshop1 workshop3)  [5.000]
94.030: (produce_c c h i workshop3)  [1.000]  (?batchsize [1.000,10.000])
95.031: (go_to workshop3 customer3)  [5.000]
100.032: (deliver_to_customer customer3 c)  [1.000]

 Execution time of Domain\Problem 5 
00:00:04.3173028

 DOMAIN/PROBLEM 6 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile06.pddl
Number of literals: 175
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

95% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 24.000
b (23.000 | 5.000)b (22.000 | 11.002)b (21.000 | 11.002)b (20.000 | 12.003)b (19.000 | 12.003)b (18.000 | 17.004)b (17.000 | 23.006)b (16.000 | 23.006)b (15.000 | 24.007)b (14.000 | 24.007)b (13.000 | 29.008)b (11.000 | 30.009)b (10.000 | 36.011)b (9.000 | 36.011)b (8.000 | 41.012)b (6.000 | 42.013)b (5.000 | 47.014)b (4.000 | 48.015)b (3.000 | 48.015)b (2.000 | 53.016)b (1.000 | 54.017);;;; Solution Found
; States evaluated: 42
; Cost: 54.017
0.000: (go_to workshop2 workshop5)  [5.000]
5.001: (go_to workshop5 supplier6)  [5.000]
10.002: (supply_raw_material_r r supplier6)  [1.000]  (?batchsize [1.000,100.000])
11.003: (supply_raw_material_o o supplier6)  [1.000]  (?batchsize [1.000,100.000])
12.004: (go_to supplier6 workshop5)  [5.000]
17.005: (go_to workshop5 supplier1)  [5.000]
22.006: (supply_raw_material_p p supplier1)  [1.000]  (?batchsize [1.000,100.000])
23.007: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
24.008: (go_to supplier1 workshop5)  [5.000]
29.009: (produce_k k n o p workshop5)  [1.000]  (?batchsize [1.000,16.667])
30.010: (go_to workshop5 supplier0)  [5.000]
35.011: (supply_raw_material_s s supplier0)  [1.000]  (?batchsize [1.000,100.000])
36.012: (go_to supplier0 workshop5)  [5.000]
41.013: (produce_m m r s workshop5)  [1.000]  (?batchsize [1.000,33.333])
42.014: (go_to workshop5 customer4)  [5.000]
47.015: (deliver_to_customer customer4 k)  [1.000]
48.016: (go_to customer4 customer2)  [5.000]
53.017: (deliver_to_customer customer2 m)  [1.000]

 Execution time of Domain\Problem 6 
00:00:02.3483869

 DOMAIN/PROBLEM 7 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile07.pddl
Number of literals: 196
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

96% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 36.000
b (35.000 | 6.001)b (34.000 | 6.001)b (33.000 | 7.002)b (32.000 | 7.002)b (31.000 | 12.003)b (30.000 | 18.005)b (29.000 | 18.005)b (28.000 | 19.006)b (27.000 | 19.006)b (26.000 | 24.007)b (24.000 | 25.008)b (23.000 | 30.009)b (22.000 | 36.011)b (21.000 | 36.011)b (20.000 | 41.012)b (19.000 | 47.014)b (17.000 | 52.015)b (15.000 | 53.016)b (14.000 | 58.017)b (12.000 | 59.018)b (11.000 | 64.019)b (10.000 | 70.021)b (9.000 | 70.021)b (8.000 | 75.022)b (7.000 | 81.024)b (5.000 | 86.025)b (3.000 | 87.026)b (2.000 | 92.027)b (1.000 | 93.028);;;; Solution Found
; States evaluated: 96
; Cost: 93.028
0.000: (go_to workshop2 supplier8)  [5.000]
5.001: (supply_raw_material_s s supplier8)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_n n supplier8)  [1.000]  (?batchsize [1.000,100.000])
7.003: (go_to supplier8 workshop5)  [5.000]
12.004: (go_to workshop5 supplier6)  [5.000]
17.005: (supply_raw_material_p p supplier6)  [1.000]  (?batchsize [1.000,100.000])
18.006: (supply_raw_material_q q supplier6)  [1.000]  (?batchsize [1.000,100.000])
19.007: (go_to supplier6 workshop2)  [5.000]
24.008: (produce_l l q workshop2)  [1.000]  (?batchsize [1.000,50.000])
25.009: (go_to workshop2 workshop5)  [5.000]
30.010: (go_to workshop5 customer1)  [5.000]
35.011: (deliver_to_customer customer1 l)  [1.000]
36.012: (go_to customer1 workshop5)  [5.000]
41.013: (go_to workshop5 supplier1)  [5.000]
46.014: (supply_raw_material_r r supplier1)  [1.000]  (?batchsize [1.000,100.000])
47.015: (go_to supplier1 workshop5)  [5.000]
52.016: (produce_m m r s workshop5)  [1.000]  (?batchsize [1.000,33.333])
53.017: (go_to workshop5 workshop4)  [5.000]
58.018: (produce_h h l m workshop4)  [1.000]  (?batchsize [1.000,7.800])
59.019: (go_to workshop4 workshop0)  [5.000]
64.020: (go_to workshop0 customer2)  [5.000]
69.021: (deliver_to_customer customer2 h)  [1.000]
70.022: (go_to customer2 workshop0)  [5.000]
75.023: (go_to workshop0 supplier0)  [5.000]
80.024: (supply_raw_material_o o supplier0)  [1.000]  (?batchsize [1.000,100.000])
81.025: (go_to supplier0 workshop0)  [5.000]
86.026: (produce_k k n o p workshop0)  [1.000]  (?batchsize [1.000,16.667])
87.027: (go_to workshop0 customer8)  [5.000]
92.028: (deliver_to_customer customer8 k)  [1.000]

 Execution time of Domain\Problem 7 
00:00:06.2711688

 DOMAIN/PROBLEM 8 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile08.pddl
Number of literals: 89
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 42.000
b (41.000 | 6.001)b (40.000 | 6.001)b (39.000 | 7.002)b (38.000 | 7.002)b (37.000 | 12.003)b (36.000 | 18.005)b (35.000 | 18.005)b (34.000 | 19.006)b (33.000 | 19.006)b (32.000 | 20.007)b (31.000 | 21.008)b (30.000 | 21.008)b (28.000 | 26.009)b (26.000 | 27.010)b (25.000 | 32.011)b (24.000 | 38.013)b (23.000 | 38.013)b (22.000 | 39.014)b (21.000 | 39.014)b (20.000 | 40.015)b (19.000 | 40.015)b (18.000 | 41.016)b (17.000 | 41.016)b (16.000 | 46.017)b (15.000 | 47.018)b (14.000 | 52.019)b (12.000 | 53.020)b (11.000 | 58.021)b (10.000 | 64.023)b (9.000 | 64.023)b (8.000 | 69.024)b (6.000 | 70.025)b (5.000 | 75.026)b (4.000 | 76.027)b (3.000 | 76.027)b (2.000 | 81.028)b (1.000 | 82.029);;;; Solution Found
; States evaluated: 91
; Cost: 82.029
0.000: (go_to workshop1 supplier3)  [5.000]
5.001: (supply_raw_material_s s supplier3)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_g g supplier3)  [1.000]  (?batchsize [1.000,100.000])
7.003: (go_to supplier3 workshop3)  [5.000]
12.004: (go_to workshop3 supplier2)  [5.000]
17.005: (supply_raw_material_n n supplier2)  [1.000]  (?batchsize [1.000,100.000])
18.006: (supply_raw_material_n n supplier2)  [1.000]  (?batchsize [1.000,100.000])
19.007: (supply_raw_material_r r supplier2)  [1.000]  (?batchsize [1.000,100.000])
20.008: (supply_raw_material_r r supplier2)  [1.000]  (?batchsize [1.000,100.000])
21.009: (go_to supplier2 workshop3)  [5.000]
26.010: (produce_m m r s workshop3)  [1.000]  (?batchsize [1.000,50.000])
27.011: (go_to workshop3 workshop1)  [5.000]
32.012: (go_to workshop1 supplier1)  [5.000]
37.013: (supply_raw_material_p p supplier1)  [1.000]  (?batchsize [1.000,100.000])
38.014: (supply_raw_material_o o supplier1)  [1.000]  (?batchsize [1.000,100.000])
39.015: (supply_raw_material_j j supplier1)  [1.000]  (?batchsize [1.000,100.000])
40.016: (supply_raw_material_j j supplier1)  [1.000]  (?batchsize [1.000,100.000])
41.017: (go_to supplier1 workshop0)  [5.000]
46.018: (produce_k k n o p workshop0)  [1.000]  (?batchsize [1.000,33.333])
47.019: (go_to workshop0 workshop1)  [5.000]
52.020: (produce_f f j k workshop1)  [1.000]  (?batchsize [1.000,33.333])
53.021: (go_to workshop1 workshop0)  [5.000]
58.022: (go_to workshop0 supplier0)  [5.000]
63.023: (supply_raw_material_e e supplier0)  [1.000]  (?batchsize [1.000,100.000])
64.024: (go_to supplier0 workshop0)  [5.000]
69.025: (produce_b b e f g workshop0)  [1.000]  (?batchsize [1.000,33.333])
70.026: (go_to workshop0 customer2)  [5.000]
75.027: (deliver_to_customer customer2 m)  [1.000]
76.028: (go_to customer2 customer0)  [5.000]
81.029: (deliver_to_customer customer0 b)  [1.000]

 Execution time of Domain\Problem 8 
00:00:02.0734710

 DOMAIN/PROBLEM 9 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile09.pddl
Number of literals: 91
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 78.000
b (77.000 | 6.001)b (76.000 | 6.001)b (75.000 | 7.002)b (74.000 | 7.002)b (73.000 | 8.003)b (72.000 | 8.003)b (71.000 | 9.004)b (70.000 | 9.004)b (69.000 | 10.005)b (68.000 | 10.005)b (67.000 | 11.006)b (66.000 | 11.006)b (65.000 | 12.007)b (64.000 | 12.007)b (63.000 | 13.008)b (62.000 | 13.008)b (61.000 | 18.009)b (60.000 | 24.011)b (59.000 | 24.011)b (58.000 | 25.012)b (57.000 | 25.012)b (56.000 | 26.013)b (55.000 | 26.013)b (54.000 | 27.014)b (53.000 | 27.014)b (52.000 | 32.015)b (50.000 | 33.016)b (49.000 | 39.018)b (48.000 | 39.018)b (47.000 | 40.019)b (46.000 | 40.019)b (45.000 | 45.020)b (44.000 | 51.022)b (43.000 | 51.022)b (42.000 | 52.023)b (41.000 | 52.023)b (40.000 | 53.024)b (39.000 | 54.025)b (38.000 | 54.025)b (37.000 | 55.026)b (36.000 | 55.026)b (35.000 | 56.027)b (34.000 | 56.027)b (33.000 | 61.028)b (32.000 | 66.029)b (31.000 | 67.030)b (30.000 | 72.031)b (29.000 | 78.033)b (28.000 | 78.033)b (27.000 | 79.034)b (26.000 | 79.034)b (25.000 | 80.035)b (24.000 | 80.035)b (23.000 | 81.036)b (22.000 | 81.036)b (20.000 | 87.038)b (19.000 | 92.039)b (17.000 | 93.040)b (16.000 | 98.041)b (15.000 | 109.044)b (14.000 | 115.046)b (13.000 | 120.047)b (12.000 | 125.048)b (10.000 | 126.049)b (8.000 | 127.050)b (7.000 | 132.051)b (5.000 | 133.052)b (3.000 | 135.053)b (2.000 | 140.054)b (1.000 | 141.055);;;; Solution Found
; States evaluated: 170
; Cost: 141.055
0.000: (go_to workshop2 supplier4)  [5.000]
5.001: (supply_raw_material_r r supplier4)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_r r supplier4)  [1.000]  (?batchsize [1.000,100.000])
7.003: (supply_raw_material_j j supplier4)  [1.000]  (?batchsize [1.000,100.000])
8.004: (supply_raw_material_j j supplier4)  [1.000]  (?batchsize [1.000,100.000])
9.005: (supply_raw_material_j j supplier4)  [1.000]  (?batchsize [1.000,100.000])
10.006: (supply_raw_material_i i supplier4)  [1.000]  (?batchsize [1.000,100.000])
11.007: (supply_raw_material_e e supplier4)  [1.000]  (?batchsize [1.000,100.000])
12.008: (supply_raw_material_e e supplier4)  [1.000]  (?batchsize [1.000,100.000])
13.009: (go_to supplier4 workshop4)  [5.000]
18.010: (go_to workshop4 supplier3)  [5.000]
23.011: (supply_raw_material_s s supplier3)  [1.000]  (?batchsize [1.000,100.000])
24.012: (supply_raw_material_s s supplier3)  [1.000]  (?batchsize [1.000,100.000])
25.013: (supply_raw_material_p p supplier3)  [1.000]  (?batchsize [1.000,100.000])
26.014: (supply_raw_material_d d supplier3)  [1.000]  (?batchsize [1.000,100.000])
27.015: (go_to supplier3 workshop4)  [5.000]
32.016: (produce_m m r s workshop4)  [1.000]  (?batchsize [1.000,66.667])
33.017: (go_to workshop4 supplier2)  [5.000]
38.018: (supply_raw_material_o o supplier2)  [1.000]  (?batchsize [1.000,100.000])
39.019: (supply_raw_material_o o supplier2)  [1.000]  (?batchsize [1.000,100.000])
40.020: (go_to supplier2 workshop4)  [5.000]
45.021: (go_to workshop4 supplier1)  [5.000]
50.022: (supply_raw_material_g g supplier1)  [1.000]  (?batchsize [1.000,100.000])
51.023: (supply_raw_material_g g supplier1)  [1.000]  (?batchsize [1.000,100.000])
52.024: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
53.025: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
54.026: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
55.027: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
56.028: (go_to supplier1 workshop4)  [5.000]
61.029: (go_to workshop4 workshop0)  [5.000]
66.030: (produce_k k n o p workshop0)  [1.000]  (?batchsize [1.000,66.667])
67.031: (go_to workshop0 workshop4)  [5.000]
72.032: (go_to workshop4 supplier0)  [5.000]
77.033: (supply_raw_material_q q supplier0)  [1.000]  (?batchsize [1.000,100.000])
78.034: (supply_raw_material_q q supplier0)  [1.000]  (?batchsize [1.000,100.000])
79.035: (supply_raw_material_q q supplier0)  [1.000]  (?batchsize [1.000,100.000])
80.036: (supply_raw_material_q q supplier0)  [1.000]  (?batchsize [1.000,100.000])
81.037: (go_to supplier0 workshop2)  [5.000]
86.038: (produce_f f j k workshop2)  [1.000]  (?batchsize [1.000,66.667])
87.039: (go_to workshop2 workshop4)  [5.000]
92.040: (produce_b b e f g workshop4)  [1.000]  (?batchsize [1.000,66.667])
93.041: (go_to workshop4 workshop3)  [5.000]
98.042: (go_to workshop3 workshop0)  [5.000]
103.043: (produce_l l q workshop0)  [1.000]  (?batchsize [1.000,100.000])
104.044: (go_to workshop0 workshop3)  [5.000]
109.045: (go_to workshop3 customer3)  [5.000]
114.046: (deliver_to_customer customer3 l)  [1.000]
115.047: (go_to customer3 workshop3)  [5.000]
120.048: (go_to workshop3 workshop0)  [5.000]
125.049: (produce_l l q workshop0)  [1.000]  (?batchsize [1.000,100.000])
126.050: (produce_h h l m workshop0)  [1.000]  (?batchsize [1.000,33.000])
127.051: (go_to workshop0 workshop3)  [5.000]
132.052: (produce_c c h i workshop3)  [1.000]  (?batchsize [1.000,33.000])
133.053: (produce_a a b c d workshop3)  [2.000]  (?batchsize [1.000,33.000])
135.054: (go_to workshop3 customer1)  [5.000]
140.055: (deliver_to_customer customer1 a)  [1.000]

 Execution time of Domain\Problem 9 
00:00:08.3898622

 DOMAIN/PROBLEM 10 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile10.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 44.000
b (43.000 | 6.001)b (42.000 | 6.001)b (41.000 | 7.002)b (40.000 | 7.002)b (39.000 | 8.003)b (38.000 | 8.003)b (37.000 | 9.004)b (36.000 | 9.004)b (35.000 | 14.005)b (33.000 | 15.006)b (32.000 | 21.008)b (31.000 | 21.008)b (30.000 | 26.009)b (29.000 | 32.011)b (28.000 | 32.011)b (27.000 | 33.012)b (25.000 | 38.013)b (23.000 | 39.014)b (22.000 | 44.015)b (20.000 | 45.016)b (19.000 | 50.017)b (17.000 | 51.018)b (16.000 | 56.019)b (15.000 | 62.021)b (14.000 | 62.021)b (13.000 | 67.022)b (12.000 | 73.024)b (11.000 | 73.024)b (10.000 | 78.025)b (9.000 | 84.027)b (8.000 | 85.028)b (7.000 | 85.028)b (5.000 | 90.029)b (3.000 | 91.030)b (2.000 | 96.031)b (1.000 | 97.032);;;; Solution Found
; States evaluated: 119
; Cost: 97.032
0.000: (go_to workshop2 supplier4)  [5.000]
5.001: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
7.003: (supply_raw_material_o o supplier4)  [1.000]  (?batchsize [1.000,100.000])
8.004: (supply_raw_material_i i supplier4)  [1.000]  (?batchsize [1.000,100.000])
9.005: (go_to supplier4 workshop0)  [5.000]
14.006: (produce_l l q workshop0)  [1.000]  (?batchsize [1.000,100.000])
15.007: (go_to workshop0 supplier2)  [5.000]
20.008: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
21.009: (go_to supplier2 workshop4)  [5.000]
26.010: (go_to workshop4 supplier1)  [5.000]
31.011: (supply_raw_material_p p supplier1)  [1.000]  (?batchsize [1.000,100.000])
32.012: (supply_raw_material_r r supplier1)  [1.000]  (?batchsize [1.000,100.000])
33.013: (go_to supplier1 workshop4)  [5.000]
38.014: (produce_m m r s workshop4)  [1.000]  (?batchsize [1.000,33.333])
39.015: (go_to workshop4 workshop3)  [5.000]
44.016: (produce_h h l m workshop3)  [1.000]  (?batchsize [1.000,16.667])
45.017: (go_to workshop3 workshop2)  [5.000]
50.018: (produce_c c h i workshop2)  [1.000]  (?batchsize [1.000,16.667])
51.019: (go_to workshop2 workshop0)  [5.000]
56.020: (go_to workshop0 customer4)  [5.000]
61.021: (deliver_to_customer customer4 l)  [1.000]
62.022: (go_to customer4 workshop0)  [5.000]
67.023: (go_to workshop0 customer3)  [5.000]
72.024: (deliver_to_customer customer3 c)  [1.000]
73.025: (go_to customer3 workshop0)  [5.000]
78.026: (go_to workshop0 supplier0)  [5.000]
83.027: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
84.028: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
85.029: (go_to supplier0 workshop0)  [5.000]
90.030: (produce_k k n o p workshop0)  [1.000]  (?batchsize [1.000,33.333])
91.031: (go_to workshop0 customer0)  [5.000]
96.032: (deliver_to_customer customer0 k)  [1.000]

 Execution time of Domain\Problem 10 
00:00:03.2262296

 DOMAIN/PROBLEM 11 

File: procurement\domain.pddl
File: procurement\pfile11.pddl
Number of literals: 113
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 59.000
b (58.000 | 6.001)b (57.000 | 6.001)b (56.000 | 7.002)b (55.000 | 7.002)b (54.000 | 8.003)b (53.000 | 8.003)b (52.000 | 9.004)b (51.000 | 9.004)b (50.000 | 10.005)b (49.000 | 10.005)b (48.000 | 11.006)b (47.000 | 11.006)b (46.000 | 12.007)b (45.000 | 12.007)b (44.000 | 13.008)b (43.000 | 13.008)b (42.000 | 14.009)b (41.000 | 14.009)b (40.000 | 15.010)b (39.000 | 15.010)b (38.000 | 20.011)b (37.000 | 26.013)b (36.000 | 27.014)b (35.000 | 27.014)b (33.000 | 32.015)b (32.000 | 38.017)b (31.000 | 39.018)b (30.000 | 39.018)b (28.000 | 44.019)b (26.000 | 45.020)b (25.000 | 51.022)b (24.000 | 56.023)b (23.000 | 58.025)b (21.000 | 63.026)b (19.000 | 69.028)b (18.000 | 74.029)b (17.000 | 80.031)b (16.000 | 80.031)b (15.000 | 86.033)b (14.000 | 91.034)b (13.000 | 96.035)b (11.000 | 97.036)b (10.000 | 98.037)b (8.000 | 103.038)b (6.000 | 104.039)b (5.000 | 109.040)b (4.000 | 110.041)b (3.000 | 110.041)b (2.000 | 115.042)b (1.000 | 116.043);;;; Solution Found
; States evaluated: 189
; Cost: 116.043
0.000: (go_to workshop2 supplier4)  [5.000]
5.001: (supply_raw_material_p p supplier4)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_r r supplier4)  [1.000]  (?batchsize [1.000,100.000])
7.003: (supply_raw_material_r r supplier4)  [1.000]  (?batchsize [1.000,100.000])
8.004: (supply_raw_material_r r supplier4)  [1.000]  (?batchsize [1.000,100.000])
9.005: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
10.006: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
11.007: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
12.008: (supply_raw_material_q q supplier4)  [1.000]  (?batchsize [1.000,100.000])
13.009: (supply_raw_material_n n supplier4)  [1.000]  (?batchsize [1.000,100.000])
14.010: (supply_raw_material_n n supplier4)  [1.000]  (?batchsize [1.000,100.000])
15.011: (go_to supplier4 workshop5)  [5.000]
20.012: (go_to workshop5 supplier2)  [5.000]
25.013: (supply_raw_material_g g supplier2)  [1.000]  (?batchsize [1.000,100.000])
26.014: (supply_raw_material_e e supplier2)  [1.000]  (?batchsize [1.000,100.000])
27.015: (go_to supplier2 workshop5)  [5.000]
32.016: (go_to workshop5 supplier1)  [5.000]
37.017: (supply_raw_material_s s supplier1)  [1.000]  (?batchsize [1.000,100.000])
38.018: (supply_raw_material_s s supplier1)  [1.000]  (?batchsize [1.000,100.000])
39.019: (go_to supplier1 workshop5)  [5.000]
44.020: (produce_m m r s workshop5)  [1.000]  (?batchsize [1.000,100.000])
45.021: (go_to workshop5 supplier0)  [5.000]
50.022: (supply_raw_material_j j supplier0)  [1.000]  (?batchsize [1.000,100.000])
51.023: (go_to supplier0 workshop5)  [5.000]
56.024: (produce_l l q workshop5)  [1.000]  (?batchsize [1.000,100.000])
57.025: (produce_l l q workshop5)  [1.000]  (?batchsize [1.000,100.000])
58.026: (go_to workshop5 workshop3)  [5.000]
63.027: (go_to workshop3 workshop2)  [5.000]
68.028: (produce_h h l m workshop2)  [1.000]  (?batchsize [1.000,40.000])
69.029: (go_to workshop2 workshop3)  [5.000]
74.030: (go_to workshop3 customer4)  [5.000]
79.031: (deliver_to_customer customer4 h)  [1.000]
80.032: (go_to customer4 supplier0)  [5.000]
85.033: (supply_raw_material_o o supplier0)  [1.000]  (?batchsize [1.000,100.000])
86.034: (go_to supplier0 workshop3)  [5.000]
91.035: (go_to workshop3 workshop0)  [5.000]
96.036: (produce_k k n o p workshop0)  [1.000]  (?batchsize [1.000,33.333])
97.037: (produce_f f j k workshop0)  [1.000]  (?batchsize [1.000,25.000])
98.038: (go_to workshop0 workshop3)  [5.000]
103.039: (produce_b b e f g workshop3)  [1.000]  (?batchsize [1.000,25.000])
104.040: (go_to workshop3 customer4)  [5.000]
109.041: (deliver_to_customer customer4 k)  [1.000]
110.042: (go_to customer4 customer3)  [5.000]
115.043: (deliver_to_customer customer3 b)  [1.000]

 Execution time of Domain\Problem 11
00:00:03.8013244

 DOMAIN/PROBLEM 12 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile12.pddl
Number of literals: 132
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 58.000
b (57.000 | 6.001)b (56.000 | 6.001)b (55.000 | 11.002)b (54.000 | 17.004)b (53.000 | 17.004)b (52.000 | 18.005)b (51.000 | 18.005)b (50.000 | 23.006)b (49.000 | 29.008)b (48.000 | 29.008)b (47.000 | 30.009)b (46.000 | 30.009)b (45.000 | 31.010)b (44.000 | 31.010)b (43.000 | 37.012)b (42.000 | 37.012)b (41.000 | 38.013)b (40.000 | 38.013)b (39.000 | 39.014)b (38.000 | 39.014)b (37.000 | 40.015)b (36.000 | 45.016)b (35.000 | 50.017)b (33.000 | 51.018)b (32.000 | 57.020)b (31.000 | 57.020)b (30.000 | 58.021)b (29.000 | 58.021)b (28.000 | 59.022)b (27.000 | 60.023)b (26.000 | 60.023)b (25.000 | 65.024)b (23.000 | 71.026)b (22.000 | 76.027)b (20.000 | 77.028)b (18.000 | 83.030)b (17.000 | 88.031)b (16.000 | 94.033)b (15.000 | 94.033)b (14.000 | 99.034)b (11.000 | 101.036)b (9.000 | 102.037)b (8.000 | 107.038)b (6.000 | 108.039)b (5.000 | 113.040)b (3.000 | 126.045)b (2.000 | 131.046)b (1.000 | 132.047);;;; Solution Found
; States evaluated: 221
; Cost: 132.047
0.000: (go_to workshop2 supplier4)  [5.000]
5.001: (supply_raw_material_o o supplier4)  [1.000]  (?batchsize [1.000,100.000])
6.002: (go_to supplier4 workshop5)  [5.000]
11.003: (go_to workshop5 supplier3)  [5.000]
16.004: (supply_raw_material_p p supplier3)  [1.000]  (?batchsize [1.000,100.000])
17.005: (supply_raw_material_e e supplier3)  [1.000]  (?batchsize [1.000,100.000])
18.006: (go_to supplier3 workshop5)  [5.000]
23.007: (go_to workshop5 supplier2)  [5.000]
28.008: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
29.009: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
30.010: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
31.011: (go_to supplier2 supplier1)  [5.000]
36.012: (supply_raw_material_s s supplier1)  [1.000]  (?batchsize [1.000,100.000])
37.013: (supply_raw_material_s s supplier1)  [1.000]  (?batchsize [1.000,100.000])
38.014: (supply_raw_material_i i supplier1)  [1.000]  (?batchsize [1.000,100.000])
39.015: (supply_raw_material_n n supplier1)  [1.000]  (?batchsize [1.000,100.000])
40.016: (go_to supplier1 workshop5)  [5.000]
45.017: (go_to workshop5 workshop2)  [5.000]
50.018: (produce_k k n o p workshop2)  [1.000]  (?batchsize [1.000,16.667])
51.019: (go_to workshop2 supplier0)  [5.000]
56.020: (supply_raw_material_j j supplier0)  [1.000]  (?batchsize [1.000,100.000])
57.021: (supply_raw_material_g g supplier0)  [1.000]  (?batchsize [1.000,100.000])
58.022: (supply_raw_material_r r supplier0)  [1.000]  (?batchsize [1.000,100.000])
59.023: (supply_raw_material_r r supplier0)  [1.000]  (?batchsize [1.000,100.000])
60.024: (go_to supplier0 workshop5)  [5.000]
65.025: (go_to workshop5 workshop3)  [5.000]
70.026: (produce_f f j k workshop3)  [1.000]  (?batchsize [1.000,16.667])
71.027: (go_to workshop3 workshop2)  [5.000]
76.028: (produce_m m r s workshop2)  [1.000]  (?batchsize [1.000,66.667])
77.029: (go_to workshop2 workshop5)  [5.000]
82.030: (produce_b b e f g workshop5)  [1.000]  (?batchsize [1.000,16.667])
83.031: (go_to workshop5 workshop3)  [5.000]
88.032: (go_to workshop3 customer2)  [5.000]
93.033: (deliver_to_customer customer2 b)  [1.000]
94.034: (go_to customer2 workshop3)  [5.000]
99.035: (produce_l l q workshop3)  [1.000]  (?batchsize [1.000,100.000])
100.036: (produce_l l q workshop3)  [1.000]  (?batchsize [1.000,100.000])
101.037: (produce_h h l m workshop3)  [1.000]  (?batchsize [1.000,30.000])
102.038: (go_to workshop3 workshop2)  [5.000]
107.039: (produce_c c h i workshop2)  [1.000]  (?batchsize [1.000,30.000])
108.040: (go_to workshop2 customer3)  [5.000]
113.041: (deliver_to_customer customer3 c)  [1.000]
114.042: (go_to customer3 supplier2)  [5.000]
119.043: (supply_raw_material_q q supplier2)  [1.000]  (?batchsize [1.000,100.000])
120.044: (go_to supplier2 workshop3)  [5.000]
125.045: (produce_l l q workshop3)  [1.000]  (?batchsize [1.000,50.000])
126.046: (go_to workshop3 customer0)  [5.000]
131.047: (deliver_to_customer customer0 l)  [1.000]

 Execution time of Domain\Problem 12 
00:00:11.8013244

 DOMAIN/PROBLEM 13 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile13.pddl
Number of literals: 87
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

81% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 6.001)b (37.000 | 7.002)b (36.000 | 7.002)b (35.000 | 8.003)b (34.000 | 8.003)b (33.000 | 9.004)b (32.000 | 9.004)b (30.000 | 14.005)b (28.000 | 15.006)b (27.000 | 20.007)b (26.000 | 26.009)b (25.000 | 26.009)b (24.000 | 27.010)b (23.000 | 27.010)b (22.000 | 28.011)b (21.000 | 28.011)b (20.000 | 29.012)b (19.000 | 29.012)b (17.000 | 35.014)b (15.000 | 46.017)
Resorting to best-first search

 Execution time of Domain\Problem 13 
00:30:00.1593410

 DOMAIN/PROBLEM 14 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile14.pddl
Number of literals: 108
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 25.000
b (24.000 | 6.001)b (23.000 | 6.001)b (22.000 | 11.002)b (21.000 | 17.004)b (20.000 | 18.005)b (19.000 | 18.005)b (18.000 | 19.006)b (17.000 | 19.006)b (16.000 | 20.007)b (15.000 | 20.007)b (14.000 | 21.008)b (13.000 | 21.008)b (10.000 | 27.010)b (8.000 | 32.011)b (6.000 | 33.012)b (5.000 | 38.013)b (3.000 | 57.020)b (2.000 | 62.021)b (1.000 | 63.022);;;; Solution Found
; States evaluated: 285
; Cost: 63.022
0.000: (go_to workshop1 supplier2)  [5.000]
5.001: (supply_raw_material_p p supplier2)  [1.000]  (?batchsize [1.000,100.000])
6.002: (go_to supplier2 workshop1)  [5.000]
11.003: (go_to workshop1 supplier0)  [5.000]
16.004: (supply_raw_material_o o supplier0)  [1.000]  (?batchsize [1.000,100.000])
17.005: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
18.006: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
19.007: (supply_raw_material_j j supplier0)  [1.000]  (?batchsize [1.000,100.000])
20.008: (supply_raw_material_j j supplier0)  [1.000]  (?batchsize [1.000,100.000])
21.009: (go_to supplier0 workshop1)  [5.000]
26.010: (produce_k k n o p workshop1)  [1.000]  (?batchsize [1.000,33.500])
27.011: (go_to workshop1 workshop0)  [5.000]
32.012: (produce_f f j k workshop0)  [1.000]  (?batchsize [1.000,38.500])
33.013: (go_to workshop0 customer4)  [5.000]
38.014: (go_to customer4 customer3)  [5.000]
43.015: (deliver_to_customer customer3 f)  [1.000]
44.016: (go_to customer3 supplier0)  [5.000]
49.017: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
50.018: (supply_raw_material_n n supplier0)  [1.000]  (?batchsize [1.000,100.000])
51.019: (go_to supplier0 workshop1)  [5.000]
56.020: (produce_k k n o p workshop1)  [1.000]  (?batchsize [1.000,19.500])
57.021: (go_to workshop1 customer4)  [5.000]
62.022: (deliver_to_customer customer4 k)  [1.000]

 Execution time of Domain\Problem 14 
00:00:07.4721059

 DOMAIN/PROBLEM 15 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile15.pddl
Number of literals: 129
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 5.000)b (24.000 | 10.001)b (23.000 | 11.002)b (22.000 | 11.002)b (21.000 | 12.003)b (20.000 | 12.003)b (19.000 | 13.004)b (18.000 | 13.004)b (17.000 | 14.005)b (16.000 | 14.005)b (15.000 | 15.006)b (14.000 | 15.006)b (11.000 | 26.009)
Resorting to best-first search
b (28.000 | 5.000)b (24.000 | 5.000)b (23.000 | 6.001)b (22.000 | 6.001)b (21.000 | 7.002)b (20.000 | 7.002)b (19.000 | 8.003)b (18.000 | 8.003)b (17.000 | 9.004)b (16.000 | 9.004)b (15.000 | 10.005)b (14.000 | 10.005)b (12.000 | 22.009)b (11.000 | 22.009)b (10.000 | 23.010)b (9.000 | 28.011)b (8.000 | 24.011)b (6.000 | 25.012)b (5.000 | 30.013)b (3.000 | 43.018)b (2.000 | 48.019)b (1.000 | 49.020);;;; Solution Found
; States evaluated: 41436
; Cost: 49.020
0.000: (go_to workshop1 supplier3)  [5.000]
5.001: (supply_raw_material_p p supplier3)  [1.000]  (?batchsize [1.000,100.000])
6.002: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
7.003: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
8.004: (supply_raw_material_g g supplier3)  [1.000]  (?batchsize [1.000,100.000])
9.005: (supply_raw_material_e e supplier3)  [1.000]  (?batchsize [1.000,100.000])
10.006: (supply_raw_material_j j supplier3)  [1.000]  (?batchsize [1.000,100.000])
11.007: (go_to supplier3 supplier2)  [5.000]
16.008: (supply_raw_material_o o supplier2)  [1.000]  (?batchsize [1.000,100.000])
17.009: (go_to supplier2 workshop3)  [5.000]
22.010: (produce_k k n o p workshop3)  [1.000]  (?batchsize [1.000,33.667])
23.011: (produce_f f j k workshop3)  [1.000]  (?batchsize [1.000,25.250])
24.012: (produce_b b e f g workshop3)  [1.000]  (?batchsize [1.000,30.250])
25.013: (go_to workshop3 supplier3)  [5.000]
30.014: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
31.015: (go_to supplier3 customer2)  [5.000]
36.016: (deliver_to_customer customer2 b)  [1.000]
37.017: (go_to customer2 workshop3)  [5.000]
42.018: (produce_k k n o p workshop3)  [1.000]  (?batchsize [1.000,26.750])
43.019: (go_to workshop3 customer4)  [5.000]
48.020: (deliver_to_customer customer4 k)  [1.000]

 Execution time of Domain\Problem 15 
00:23:28.7995156

 DOMAIN/PROBLEM 16 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile16.pddl
Number of literals: 90
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 61.000

 Execution time of Domain\Problem 16 
00:30:00.1756508

 DOMAIN/PROBLEM 17 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile17.pddl
Number of literals: 151
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 70.000
b (69.000 | 5.000)b (68.000 | 10.001)b (62.000 | 15.002)b (61.000 | 16.003)b (60.000 | 16.003)b (59.000 | 17.004)b (58.000 | 17.004)b (57.000 | 18.005)b (56.000 | 18.005)b (54.000 | 24.007)b (53.000 | 30.009)b (52.000 | 35.010)b (51.000 | 40.011)b (50.000 | 41.012)b (49.000 | 41.012)b (37.000 | 47.014)b (36.000 | 63.022)b (35.000 | 68.023)b (34.000 | 74.025)b (33.000 | 75.026)b (32.000 | 75.026)b (31.000 | 76.027)b (30.000 | 76.027)b (29.000 | 77.028)b (28.000 | 77.028)b (27.000 | 82.029)b (26.000 | 88.031)b (25.000 | 95.034)b (24.000 | 100.035)b (23.000 | 107.038)b (22.000 | 112.039)b (21.000 | 117.040)b (19.000 | 118.041)b (16.000 | 130.045)b (15.000 | 141.048)
Resorting to best-first search

 Execution time of Domain\Problem 17 
00:30:00.1966096

 DOMAIN/PROBLEM 18 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile18.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 87.000
b (86.000 | 5.000)b (30.000 | 10.001)b (29.000 | 11.002)b (28.000 | 11.002)b (27.000 | 12.003)b (26.000 | 12.003)b (25.000 | 13.004)b (24.000 | 13.004)b (23.000 | 14.005)b (22.000 | 14.005)b (21.000 | 15.006)b (20.000 | 15.006)b (17.000 | 26.009)
Resorting to best-first search

 Execution time of Domain\Problem 18 
00:30:00.1556069

 DOMAIN/PROBLEM 19 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile19.pddl
Number of literals: 114
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

93% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 42.000

 Execution time of Domain\Problem 19 
00:30:00.1767653

 DOMAIN/PROBLEM 20 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile20.pddl
Number of literals: 134
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

93% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 53.000
b (51.000 | 5.000)b (47.000 | 6.001)b (46.000 | 14.005)b (45.000 | 14.005)b (44.000 | 15.006)b (43.000 | 15.006)b (42.000 | 16.007)b (41.000 | 16.007)b (40.000 | 21.008)b (39.000 | 27.010)b (38.000 | 28.011)b (37.000 | 28.011)b (36.000 | 33.012)b (34.000 | 34.013)b (33.000 | 39.014)b (32.000 | 45.016)b (31.000 | 46.017)b (30.000 | 46.017)b (29.000 | 47.018)b (28.000 | 47.018)b (27.000 | 48.019)b (26.000 | 48.019)b (24.000 | 53.020)b (22.000 | 54.021)b (21.000 | 60.023)b (20.000 | 65.024)b (18.000 | 71.026)b (17.000 | 76.027)b (16.000 | 81.028)b (14.000 | 83.030)b (13.000 | 88.031)b (11.000 | 89.032)b (10.000 | 94.033)b (9.000 | 95.034)b (8.000 | 95.034)b (7.000 | 100.035)b (6.000 | 106.037)b (5.000 | 106.037)b (4.000 | 111.038)b (3.000 | 124.043)b (2.000 | 129.044)b (1.000 | 130.045);;;; Solution Found
; States evaluated: 536
; Cost: 130.045
0.000: (go_to workshop3 workshop5)  [5.000]
5.001: (produce_m m r s workshop5)  [1.000]  (?batchsize [1.000,1.000])
6.002: (go_to workshop5 supplier3)  [5.000]
11.003: (supply_raw_material_p p supplier3)  [1.000]  (?batchsize [1.000,100.000])
12.004: (supply_raw_material_r r supplier3)  [1.000]  (?batchsize [1.000,100.000])
13.005: (supply_raw_material_r r supplier3)  [1.000]  (?batchsize [1.000,100.000])
14.006: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
15.007: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
16.008: (go_to supplier3 workshop1)  [5.000]
21.009: (go_to workshop1 supplier2)  [5.000]
26.010: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
27.011: (supply_raw_material_s s supplier2)  [1.000]  (?batchsize [1.000,100.000])
28.012: (go_to supplier2 workshop5)  [5.000]
33.013: (produce_m m r s workshop5)  [1.000]  (?batchsize [1.000,67.667])
34.014: (go_to workshop5 workshop1)  [5.000]
39.015: (go_to workshop1 supplier1)  [5.000]
44.016: (supply_raw_material_q q supplier1)  [1.000]  (?batchsize [1.000,100.000])
45.017: (supply_raw_material_q q supplier1)  [1.000]  (?batchsize [1.000,100.000])
46.018: (supply_raw_material_q q supplier1)  [1.000]  (?batchsize [1.000,100.000])
47.019: (supply_raw_material_o o supplier1)  [1.000]  (?batchsize [1.000,100.000])
48.020: (go_to supplier1 workshop1)  [5.000]
53.021: (produce_k k n o p workshop1)  [1.000]  (?batchsize [1.000,34.000])
54.022: (go_to workshop1 supplier0)  [5.000]
59.023: (supply_raw_material_j j supplier0)  [1.000]  (?batchsize [1.000,100.000])
60.024: (go_to supplier0 workshop5)  [5.000]
65.025: (go_to workshop5 workshop4)  [5.000]
70.026: (produce_f f j k workshop4)  [1.000]  (?batchsize [1.000,25.500])
71.027: (go_to workshop4 workshop5)  [5.000]
76.028: (go_to workshop5 workshop1)  [5.000]
81.029: (produce_l l q workshop1)  [1.000]  (?batchsize [1.000,100.000])
82.030: (produce_l l q workshop1)  [1.000]  (?batchsize [1.000,100.000])
83.031: (go_to workshop1 workshop5)  [5.000]
88.032: (produce_h h l m workshop5)  [1.000]  (?batchsize [1.000,31.300])
89.033: (go_to workshop5 customer4)  [5.000]
94.034: (deliver_to_customer customer4 m)  [1.000]
95.035: (go_to customer4 customer3)  [5.000]
100.036: (go_to customer3 customer1)  [5.000]
105.037: (deliver_to_customer customer1 h)  [1.000]
106.038: (go_to customer1 customer3)  [5.000]
111.039: (deliver_to_customer customer3 f)  [1.000]
112.040: (go_to customer3 supplier3)  [5.000]
117.041: (supply_raw_material_n n supplier3)  [1.000]  (?batchsize [1.000,100.000])
118.042: (go_to supplier3 workshop1)  [5.000]
123.043: (produce_k k n o p workshop1)  [1.000]  (?batchsize [1.000,26.167])
124.044: (go_to workshop1 customer3)  [5.000]
129.045: (deliver_to_customer customer3 k)  [1.000]

 Execution time of Domain\Problem 20 
00:00:27.0380812
