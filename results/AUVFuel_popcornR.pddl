the results of squirrel t1 tests in popcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile01.pddl
Number of literals: 2
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 4.000
b (3.000 | 0.001)b (2.000 | 0.001)b (1.000 | 6.001);;;; Solution Found
; States evaluated: 5
; Cost: 7.001
0.000: (glide_forward)  [5.000]  (?dx [0.000,10.000])
5.001: (take-sample a)  [2.000]

 Execution time of Domain\Problem 1 
00:00:03.4546226

 DOMAIN/PROBLEM 2 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile02.pddl
Number of literals: 3
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 14.000
b (13.000 | 0.050)b (12.000 | 0.050)b (11.000 | 0.101)b (7.000 | 0.152)b (6.000 | 0.152)b (5.000 | 0.154)b (4.000 | 0.154)b (3.000 | 37.004)b (2.000 | 37.004)
Resorting to best-first search
b (13.000 | 0.050)b (12.000 | 0.050)b (11.000 | 0.101)b (10.000 | 0.101)b (9.000 | 8.253)b (8.000 | 8.253)b (5.000 | 8.406)b (4.000 | 8.406)b (3.000 | 8.408)b (2.000 | 8.408)b (1.000 | 39.008);;;; Solution Found
; States evaluated: 113
; Cost: 53.008
0.000: (recharge)  [0.200]  (?deltarecharge [1.000,10.000])
0.201: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.202: (glide_forward)  [6.000]  (?dx [0.000,15.000])
16.203: (take-sample a)  [2.000]
18.204: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
18.255: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
28.256: (recharge)  [5.250]  (?deltarecharge [1.000,105.000])
33.507: (glide_forward)  [17.500]  (?dx [0.000,53.000])
51.008: (take-sample b)  [2.000]

 Execution time of Domain\Problem 2 
00:00:04.6588823

 DOMAIN/PROBLEM 3 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile03.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 20.000
b (19.000 | 0.050)b (18.000 | 0.050)b (17.000 | 0.101)b (16.000 | 0.101)b (15.000 | 0.152)b (13.000 | 0.203)b (12.000 | 0.203)b (11.000 | 37.005)b (10.000 | 37.005)b (9.000 | 37.056)b (8.000 | 37.056)b (7.000 | 37.257)b (6.000 | 37.257)b (5.000 | 37.259)b (4.000 | 37.259)b (3.000 | 69.009)b (2.000 | 69.009)
Resorting to best-first search
b (19.000 | 0.050)b (18.000 | 0.050)b (17.000 | 0.101)b (16.000 | 0.101)b (15.000 | 0.152)b (14.000 | 0.152)b (13.000 | 8.304)b (12.000 | 8.304)b (11.000 | 8.457)b (10.000 | 8.457)b (9.000 | 39.009)b (8.000 | 39.009)b (7.000 | 39.060)b (6.000 | 39.060)b (5.000 | 39.261)b (4.000 | 39.261)b (3.000 | 39.263)b (2.000 | 39.263)b (1.000 | 71.013);;;; Solution Found
; States evaluated: 163
; Cost: 95.013
0.000: (recharge)  [0.450]  (?deltarecharge [1.000,10.000])
0.451: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.452: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
20.453: (glide_forward)  [7.250]  (?dx [0.000,15.000])
27.704: (take-sample a)  [2.000]
29.705: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
29.756: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
39.757: (recharge)  [2.700]  (?deltarecharge [1.000,105.000])
42.458: (glide_forward)  [13.750]  (?dx [0.000,53.000])
56.209: (take-sample b)  [2.000]
58.210: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
68.211: (recharge)  [7.300]  (?deltarecharge [1.000,146.000])
75.512: (glide_forward)  [17.500]  (?dx [0.000,58.000])
93.013: (take-sample c)  [2.000]

 Execution time of Domain\Problem 3 
00:00:05.6887783

 DOMAIN/PROBLEM 4 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile04.pddl
Number of literals: 5
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 22.000
b (21.000 | 0.050)b (20.000 | 0.050)b (19.000 | 0.101)b (18.000 | 0.101)b (17.000 | 0.152)b (15.000 | 0.203)b (14.000 | 0.203)b (13.000 | 8.355)b (12.000 | 8.355)b (9.000 | 8.457)b (8.000 | 8.457)b (7.000 | 8.459)b (6.000 | 8.459)b (5.000 | 89.009)b (4.000 | 89.009)
Resorting to best-first search
b (21.000 | 0.050)b (20.000 | 0.050)b (19.000 | 0.101)b (18.000 | 0.101)b (17.000 | 0.152)b (16.000 | 0.152)b (15.000 | 8.304)b (14.000 | 8.304)b (13.000 | 8.457)b (12.000 | 8.457)b (11.000 | 39.060)b (10.000 | 39.060)b (5.000 | 39.263)b (4.000 | 39.263)b (3.000 | 91.013)b (2.000 | 91.013)b (1.000 | 108.015);;;; Solution Found
; States evaluated: 1528
; Cost: 116.015
0.000: (recharge)  [0.500]  (?deltarecharge [1.000,10.000])
0.501: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.502: (refuel_tank)  [9.950]  (?fuelamount [1.000,200.000])
20.453: (glide_forward)  [6.000]  (?dx [0.000,15.000])
26.454: (take-sample a)  [2.000]
28.455: (refuel_tank)  [0.050]  (?fuelamount [1.000,200.000])
28.506: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.507: (recharge)  [2.700]  (?deltarecharge [1.000,106.000])
41.208: (glide_forward)  [15.000]  (?dx [0.000,53.000])
56.209: (take-sample b)  [2.000]
58.210: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
68.211: (recharge)  [7.300]  (?deltarecharge [1.000,146.000])
75.512: (glide_forward)  [27.000]  (?dx [0.000,58.000])
102.513: (take-sample d)  [2.000]
104.514: (glide_backward)  [9.500]  (?dx [0.000,18.000])
114.015: (take-sample c)  [2.000]

 Execution time of Domain\Problem 4 
00:00:38.1329373

 DOMAIN/PROBLEM 5 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile05.pddl
Number of literals: 6
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 24.000
b (23.000 | 0.050)b (22.000 | 0.050)b (21.000 | 0.101)b (20.000 | 0.101)b (19.000 | 0.152)b (17.000 | 0.203)b (16.000 | 0.203)b (15.000 | 13.005)b (14.000 | 13.005)b (13.000 | 13.056)b (12.000 | 13.056)b (11.000 | 13.257)b (10.000 | 13.257)b (9.000 | 13.259)b (8.000 | 13.259)b (7.000 | 89.009)b (6.000 | 89.009)
Resorting to best-first search
b (23.000 | 0.050)b (22.000 | 0.050)b (21.000 | 0.101)b (20.000 | 0.101)b (19.000 | 0.152)b (18.000 | 0.152)b (17.000 | 8.304)b (16.000 | 8.304)b (15.000 | 8.457)b (14.000 | 8.457)b (13.000 | 15.009)b (12.000 | 15.009)b (11.000 | 15.060)b (10.000 | 15.060)b (9.000 | 15.261)b (8.000 | 15.261)b (7.000 | 15.263)b (6.000 | 15.263)b (5.000 | 91.013)b (4.000 | 91.013)b (3.000 | 108.015)b (2.000 | 108.015)b (1.000 | 140.020);;;; Solution Found
; States evaluated: 7660
; Cost: 156.520
0.000: (recharge)  [0.450]  (?deltarecharge [1.000,10.000])
0.451: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.452: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
20.453: (glide_forward)  [6.000]  (?dx [0.000,15.000])
26.454: (take-sample a)  [2.000]
28.455: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
28.506: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.507: (recharge)  [5.000]  (?deltarecharge [1.000,105.000])
43.508: (glide_forward)  [3.000]  (?dx [0.000,53.000])
46.509: (take-sample e)  [2.000]
48.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
58.511: (recharge)  [4.200]  (?deltarecharge [1.000,194.000])
62.712: (glide_forward)  [37.000]  (?dx [0.000,82.000])
99.713: (take-sample d)  [2.000]
101.714: (glide_backward)  [7.500]  (?dx [0.000,18.000])
109.215: (take-sample c)  [2.000]
111.216: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
121.217: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
131.218: (recharge)  [5.800]  (?deltarecharge [1.000,116.000])
137.019: (glide_backward)  [17.500]  (?dx [0.000,58.000])
154.520: (take-sample b)  [2.000]

 Execution time of Domain\Problem 5 
00:05:03.3740053

 DOMAIN/PROBLEM 6 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile06.pddl
Number of literals: 7
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 26.000
b (25.000 | 0.050)b (24.000 | 0.050)b (23.000 | 0.101)b (22.000 | 0.101)b (21.000 | 0.152)b (19.000 | 0.203)b (18.000 | 0.203)b (17.000 | 13.005)b (16.000 | 13.005)b (15.000 | 13.056)b (14.000 | 13.056)b (13.000 | 13.257)b (12.000 | 13.257)b (11.000 | 13.259)b (10.000 | 13.259)b (9.000 | 89.009)b (8.000 | 89.009)
Resorting to best-first search
b (25.000 | 0.050)b (24.000 | 0.050)b (23.000 | 0.101)b (22.000 | 0.101)b (21.000 | 0.152)b (20.000 | 0.152)b (19.000 | 8.304)b (18.000 | 8.304)b (17.000 | 8.457)b (16.000 | 8.457)b (15.000 | 15.009)b (14.000 | 15.009)b (13.000 | 15.060)b (12.000 | 15.060)b (11.000 | 15.261)b (10.000 | 15.261)b (9.000 | 15.263)b (8.000 | 15.263)b (7.000 | 91.013)b (6.000 | 91.013)b (5.000 | 108.015)b (4.000 | 108.015)b (3.000 | 136.020)b (2.000 | 136.020)b (1.000 | 142.022);;;; Solution Found
; States evaluated: 7689
; Cost: 158.522
0.000: (recharge)  [0.450]  (?deltarecharge [1.000,10.000])
0.451: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.452: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
20.453: (glide_forward)  [6.000]  (?dx [0.000,15.000])
26.454: (take-sample a)  [2.000]
28.455: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
28.506: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.507: (recharge)  [5.000]  (?deltarecharge [1.000,105.000])
43.508: (glide_forward)  [3.000]  (?dx [0.000,53.000])
46.509: (take-sample e)  [2.000]
48.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
58.511: (recharge)  [4.200]  (?deltarecharge [1.000,194.000])
62.712: (glide_forward)  [37.000]  (?dx [0.000,82.000])
99.713: (take-sample d)  [2.000]
101.714: (glide_backward)  [7.500]  (?dx [0.000,18.000])
109.215: (take-sample c)  [2.000]
111.216: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
121.217: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
131.218: (recharge)  [5.800]  (?deltarecharge [1.000,116.000])
137.019: (glide_backward)  [13.000]  (?dx [0.000,58.000])
150.020: (take-sample f)  [2.000]
152.021: (glide_backward)  [4.500]  (?dx [0.000,32.000])
156.522: (take-sample b)  [2.000]

 Execution time of Domain\Problem 6 
00:05:00.3581842

 DOMAIN/PROBLEM 7 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile07.pddl
Number of literals: 8
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 28.000
b (27.000 | 0.050)b (26.000 | 0.050)b (25.000 | 0.101)b (24.000 | 0.101)b (23.000 | 0.152)b (21.000 | 0.203)b (20.000 | 0.203)b (19.000 | 13.005)b (18.000 | 13.005)b (17.000 | 13.056)b (16.000 | 13.056)b (15.000 | 13.257)b (14.000 | 13.257)b (13.000 | 13.259)b (12.000 | 13.259)b (11.000 | 89.009)b (10.000 | 89.009)
Resorting to best-first search
b (27.000 | 0.050)b (26.000 | 0.050)b (25.000 | 0.101)b (24.000 | 0.101)b (23.000 | 0.152)b (22.000 | 0.152)b (21.000 | 8.304)b (20.000 | 8.304)b (19.000 | 8.457)b (18.000 | 8.457)b (17.000 | 15.009)b (16.000 | 15.009)b (15.000 | 15.060)b (14.000 | 15.060)b (13.000 | 15.261)b (12.000 | 15.261)b (11.000 | 15.263)b (10.000 | 15.263)b (9.000 | 91.013)b (8.000 | 91.013)b (7.000 | 98.015)b (6.000 | 98.015)b (5.000 | 110.017)b (4.000 | 110.017)b (3.000 | 138.022)b (2.000 | 138.022)b (1.000 | 144.024);;;; Solution Found
; States evaluated: 9257
; Cost: 160.524
0.000: (recharge)  [0.450]  (?deltarecharge [1.000,10.000])
0.451: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
10.452: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
20.453: (glide_forward)  [6.000]  (?dx [0.000,15.000])
26.454: (take-sample a)  [2.000]
28.455: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
28.506: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.507: (recharge)  [5.000]  (?deltarecharge [1.000,105.000])
43.508: (glide_forward)  [3.000]  (?dx [0.000,53.000])
46.509: (take-sample e)  [2.000]
48.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
58.511: (recharge)  [4.200]  (?deltarecharge [1.000,194.000])
62.712: (glide_forward)  [37.000]  (?dx [0.000,82.000])
99.713: (take-sample d)  [2.000]
101.714: (glide_backward)  [5.000]  (?dx [0.000,18.000])
106.715: (take-sample g)  [2.000]
108.716: (glide_backward)  [2.500]  (?dx [0.000,18.000])
111.217: (take-sample c)  [2.000]
113.218: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
123.219: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
133.220: (recharge)  [5.800]  (?deltarecharge [1.000,116.000])
139.021: (glide_backward)  [13.000]  (?dx [0.000,58.000])
152.022: (take-sample f)  [2.000]
154.023: (glide_backward)  [4.500]  (?dx [0.000,32.000])
158.524: (take-sample b)  [2.000]

 Execution time of Domain\Problem 7 
00:06:41.4443439

 DOMAIN/PROBLEM 8 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile08.pddl
Number of literals: 9
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 32.000
b (31.000 | 0.050)b (30.000 | 0.050)b (29.000 | 0.101)b (28.000 | 0.101)b (27.000 | 0.152)b (25.000 | 0.203)b (24.000 | 0.203)b (23.000 | 0.205)b (22.000 | 0.205)b (21.000 | 4.705)b (20.000 | 4.705)b (19.000 | 15.007)b (18.000 | 15.007)b (17.000 | 15.058)b (16.000 | 15.058)b (15.000 | 15.259)b (14.000 | 15.259)b (13.000 | 15.261)b (12.000 | 15.261)b (11.000 | 91.011)b (10.000 | 91.011)
Resorting to best-first search
b (31.000 | 0.001)b (30.000 | 0.001)b (29.000 | 4.501)b (29.000 | 0.052)b (28.000 | 0.052)b (27.000 | 4.552)b (27.000 | 0.103)b (26.000 | 0.103)b (25.000 | 4.603)b (25.000 | 0.154)b (24.000 | 0.154)b (23.000 | 4.654)b (22.000 | 4.654)b (21.000 | 10.306)b (20.000 | 10.306)b (19.000 | 10.459)b (18.000 | 10.459)b (17.000 | 17.011)b (16.000 | 17.011)b (15.000 | 17.062)b (14.000 | 17.062)b (13.000 | 17.263)b (12.000 | 17.263)b (11.000 | 17.265)b (10.000 | 17.265)b (9.000 | 93.015)b (8.000 | 93.015)b (7.000 | 100.017)b (6.000 | 100.017)b (5.000 | 112.019)b (4.000 | 112.019)b (3.000 | 140.024)b (2.000 | 140.024)b (1.000 | 146.026);;;; Solution Found
; States evaluated: 9293
; Cost: 162.526
0.000: (glide_forward)  [5.000]  (?dx [0.000,10.000])
5.001: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
15.002: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
25.003: (recharge)  [0.450]  (?deltarecharge [1.000,10.000])
25.454: (take-sample h)  [2.000]
27.455: (glide_forward)  [2.250]  (?dx [0.000,10.000])
29.706: (take-sample a)  [2.000]
31.707: (recharge)  [0.050]  (?deltarecharge [1.000,6.000])
31.758: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
41.759: (recharge)  [5.000]  (?deltarecharge [1.000,105.000])
46.760: (glide_forward)  [1.750]  (?dx [0.000,53.000])
48.511: (take-sample e)  [2.000]
50.512: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
60.513: (recharge)  [4.200]  (?deltarecharge [1.000,194.000])
64.714: (glide_forward)  [37.000]  (?dx [0.000,82.000])
101.715: (take-sample d)  [2.000]
103.716: (glide_backward)  [5.000]  (?dx [0.000,18.000])
108.717: (take-sample g)  [2.000]
110.718: (glide_backward)  [2.500]  (?dx [0.000,18.000])
113.219: (take-sample c)  [2.000]
115.220: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
125.221: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
135.222: (recharge)  [5.800]  (?deltarecharge [1.000,116.000])
141.023: (glide_backward)  [13.000]  (?dx [0.000,58.000])
154.024: (take-sample f)  [2.000]
156.025: (glide_backward)  [4.500]  (?dx [0.000,32.000])
160.526: (take-sample b)  [2.000]

 Execution time of Domain\Problem 8 
00:07:26.7117197

 DOMAIN/PROBLEM 9 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile09.pddl
Number of literals: 10
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 34.000
b (33.000 | 0.050)b (32.000 | 0.050)b (31.000 | 0.101)b (30.000 | 0.101)b (29.000 | 0.152)b (27.000 | 0.203)b (26.000 | 0.203)b (25.000 | 0.205)b (24.000 | 0.205)b (23.000 | 5.705)b (22.000 | 5.705)b (21.000 | 7.706)b (20.000 | 7.706)b (19.000 | 17.008)b (18.000 | 17.008)b (17.000 | 17.059)b (16.000 | 17.059)b (15.000 | 17.260)b (14.000 | 17.260)b (13.000 | 17.262)b (12.000 | 17.262)b (11.000 | 93.012)b (10.000 | 93.012)
Resorting to best-first search
b (33.000 | 0.001)b (32.000 | 0.001)b (31.000 | 5.501)b (31.000 | 4.501)b (31.000 | 0.052)b (30.000 | 0.052)b (29.000 | 5.552)b (29.000 | 4.552)b (29.000 | 0.103)b (28.000 | 0.103)b (27.000 | 5.603)b (27.000 | 4.603)b (27.000 | 0.154)b (26.000 | 0.154)b (25.000 | 5.654)b (25.000 | 4.654)b (24.000 | 5.654)b (23.000 | 7.655)b (22.000 | 7.655)b (21.000 | 7.757)b (20.000 | 7.757)b (19.000 | 17.009)b (19.000 | 12.359)b (18.000 | 17.009)b (17.000 | 17.060)b (16.000 | 17.060)b (15.000 | 17.261)b (14.000 | 17.261)b (13.000 | 17.263)b (12.000 | 17.263)b (11.000 | 93.013)b (10.000 | 93.013)b (9.000 | 100.015)b (8.000 | 100.015)b (7.000 | 112.017)b (6.000 | 112.017)b (5.000 | 140.022)b (4.000 | 140.022)b (3.000 | 146.024)b (2.000 | 146.024)b (1.000 | 178.029);;;; Solution Found
; States evaluated: 21329
; Cost: 204.529
0.000: (glide_forward)  [3.500]  (?dx [0.000,10.000])
3.501: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
13.502: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
23.503: (recharge)  [0.500]  (?deltarecharge [1.000,10.000])
24.004: (take-sample i)  [2.000]
26.005: (take-sample h)  [2.000]
28.006: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.007: (recharge)  [5.000]  (?deltarecharge [1.000,109.000])
43.008: (glide_forward)  [5.500]  (?dx [0.000,58.000])
48.509: (take-sample e)  [2.000]
50.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
60.511: (recharge)  [5.000]  (?deltarecharge [1.000,194.000])
65.512: (glide_forward)  [37.000]  (?dx [0.000,82.000])
102.513: (take-sample d)  [2.000]
104.514: (glide_backward)  [5.000]  (?dx [0.000,18.000])
109.515: (take-sample g)  [2.000]
111.516: (glide_backward)  [5.000]  (?dx [0.000,18.000])
116.517: (take-sample c)  [2.000]
118.518: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
128.519: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
138.520: (recharge)  [2.200]  (?deltarecharge [1.000,116.000])
140.721: (glide_backward)  [12.000]  (?dx [0.000,58.000])
152.722: (take-sample f)  [2.000]
154.723: (glide_backward)  [0.500]  (?dx [0.000,32.000])
155.224: (take-sample b)  [2.000]
157.225: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
167.226: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
177.227: (recharge)  [7.800]  (?deltarecharge [1.000,156.000])
185.028: (glide_backward)  [17.500]  (?dx [0.000,47.000])
202.529: (take-sample a)  [2.000]

 Execution time of Domain\Problem 9 
00:19:36.9054161

 DOMAIN/PROBLEM 10 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile10.pddl
Number of literals: 11
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 36.000
b (35.000 | 0.050)b (34.000 | 0.050)b (33.000 | 0.101)b (32.000 | 0.101)b (31.000 | 0.152)b (29.000 | 0.203)b (28.000 | 0.203)b (27.000 | 0.205)b (26.000 | 0.205)b (25.000 | 5.705)b (24.000 | 5.705)b (23.000 | 7.706)b (22.000 | 7.706)b (21.000 | 17.008)b (20.000 | 17.008)b (19.000 | 17.059)b (18.000 | 17.059)b (17.000 | 17.260)b (16.000 | 17.260)b (15.000 | 17.262)b (14.000 | 17.262)b (13.000 | 89.012)b (12.000 | 89.012)b (11.000 | 95.013)b (10.000 | 95.013)
Resorting to best-first search
b (35.000 | 0.001)b (34.000 | 0.001)b (33.000 | 5.501)b (33.000 | 4.501)b (33.000 | 0.052)b (32.000 | 0.052)b (31.000 | 5.552)b (31.000 | 4.552)b (31.000 | 0.103)b (30.000 | 0.103)b (29.000 | 5.603)b (29.000 | 4.603)b (29.000 | 0.154)b (28.000 | 0.154)b (27.000 | 5.654)b (27.000 | 4.654)b (26.000 | 5.654)b (25.000 | 7.655)b (24.000 | 7.655)b (23.000 | 7.757)b (22.000 | 7.757)b (21.000 | 17.009)b (21.000 | 12.359)b (20.000 | 17.009)b (19.000 | 17.060)b (18.000 | 17.060)b (17.000 | 17.261)b (16.000 | 17.261)b (15.000 | 17.263)b (14.000 | 17.263)b (13.000 | 89.013)b (12.000 | 89.013)b (11.000 | 95.014)b (10.000 | 95.014)b (9.000 | 102.016)b (8.000 | 102.016)b (7.000 | 114.018)b (6.000 | 114.018)b (5.000 | 142.023)b (4.000 | 142.023)b (3.000 | 148.025)b (2.000 | 148.025)b (1.000 | 180.030);;;; Solution Found
; States evaluated: 21351
; Cost: 206.530
0.000: (glide_forward)  [5.000]  (?dx [0.000,10.000])
5.001: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
15.002: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
25.003: (recharge)  [0.500]  (?deltarecharge [1.000,10.000])
25.504: (take-sample i)  [2.000]
27.505: (take-sample h)  [2.000]
29.506: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
39.507: (recharge)  [5.000]  (?deltarecharge [1.000,109.000])
44.508: (glide_forward)  [4.000]  (?dx [0.000,58.000])
48.509: (take-sample e)  [2.000]
50.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
60.511: (recharge)  [5.000]  (?deltarecharge [1.000,194.000])
65.512: (glide_forward)  [37.000]  (?dx [0.000,82.000])
102.513: (take-sample j)  [2.000]
104.514: (take-sample d)  [2.000]
106.515: (glide_backward)  [5.000]  (?dx [0.000,23.000])
111.516: (take-sample g)  [2.000]
113.517: (glide_backward)  [5.000]  (?dx [0.000,18.000])
118.518: (take-sample c)  [2.000]
120.519: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
130.520: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
140.521: (recharge)  [2.200]  (?deltarecharge [1.000,116.000])
142.722: (glide_backward)  [12.000]  (?dx [0.000,58.000])
154.723: (take-sample f)  [2.000]
156.724: (glide_backward)  [0.500]  (?dx [0.000,32.000])
157.225: (take-sample b)  [2.000]
159.226: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
169.227: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
179.228: (recharge)  [7.800]  (?deltarecharge [1.000,156.000])
187.029: (glide_backward)  [17.500]  (?dx [0.000,47.000])
204.530: (take-sample a)  [2.000]

 Execution time of Domain\Problem 10 
00:21:20.0504387

 DOMAIN/PROBLEM 11 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile11.pddl
Number of literals: 12
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 38.000
b (37.000 | 0.050)b (36.000 | 0.050)b (35.000 | 0.101)b (34.000 | 0.101)b (33.000 | 0.152)b (31.000 | 0.203)b (30.000 | 0.203)b (29.000 | 0.205)b (28.000 | 0.205)b (27.000 | 17.005)b (26.000 | 17.005)b (25.000 | 17.056)b (24.000 | 17.056)b (23.000 | 17.257)b (22.000 | 17.257)b (21.000 | 17.259)b (20.000 | 17.259)b (19.000 | 85.009)b (18.000 | 85.009)b (17.000 | 91.010)b (16.000 | 91.010)
Resorting to best-first search
b (37.000 | 0.001)b (36.000 | 0.001)b (35.000 | 5.501)b (35.000 | 4.501)b (35.000 | 0.052)b (34.000 | 0.052)b (33.000 | 5.552)b (33.000 | 4.552)b (33.000 | 0.103)b (32.000 | 0.103)b (31.000 | 5.603)b (31.000 | 4.603)b (31.000 | 0.154)b (30.000 | 0.154)b (29.000 | 5.654)b (29.000 | 4.654)b (28.000 | 5.654)b (27.000 | 7.655)b (26.000 | 7.655)b (25.000 | 7.757)b (24.000 | 7.757)b (23.000 | 21.009)b (23.000 | 17.009)b (23.000 | 12.359)b (22.000 | 21.009)b (21.000 | 21.060)b (20.000 | 21.060)b (19.000 | 21.261)b (18.000 | 21.261)b (17.000 | 21.263)b (16.000 | 21.263)b (15.000 | 89.013)b (14.000 | 89.013)b (13.000 | 95.014)b (12.000 | 95.014)b (11.000 | 102.016)b (10.000 | 102.016)b (9.000 | 114.018)b (8.000 | 114.018)b (7.000 | 142.023)b (6.000 | 142.023)b (5.000 | 148.025)b (4.000 | 148.025)b (3.000 | 176.027)b (2.000 | 176.027)b (1.000 | 182.032);;;; Solution Found
; States evaluated: 18243
; Cost: 208.532
0.000: (glide_forward)  [3.500]  (?dx [0.000,10.000])
3.501: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
13.502: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
23.503: (recharge)  [0.500]  (?deltarecharge [1.000,10.000])
24.004: (take-sample i)  [2.000]
26.005: (take-sample h)  [2.000]
28.006: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.007: (recharge)  [5.000]  (?deltarecharge [1.000,109.000])
43.008: (glide_forward)  [7.500]  (?dx [0.000,58.000])
50.509: (take-sample k)  [2.000]
52.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
62.511: (recharge)  [5.000]  (?deltarecharge [1.000,186.000])
67.512: (glide_forward)  [35.000]  (?dx [0.000,78.000])
102.513: (take-sample j)  [2.000]
104.514: (take-sample d)  [2.000]
106.515: (glide_backward)  [5.000]  (?dx [0.000,23.000])
111.516: (take-sample g)  [2.000]
113.517: (glide_backward)  [5.000]  (?dx [0.000,18.000])
118.518: (take-sample c)  [2.000]
120.519: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
130.520: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
140.521: (recharge)  [4.800]  (?deltarecharge [1.000,116.000])
145.322: (glide_backward)  [12.000]  (?dx [0.000,58.000])
157.323: (take-sample f)  [2.000]
159.324: (glide_backward)  [3.000]  (?dx [0.000,32.000])
162.325: (take-sample b)  [2.000]
164.326: (glide_backward)  [10.500]  (?dx [0.000,28.000])
174.827: (take-sample e)  [2.000]
176.828: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
186.829: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
196.830: (recharge)  [5.200]  (?deltarecharge [1.000,104.000])
202.031: (glide_backward)  [4.500]  (?dx [0.000,21.000])
206.532: (take-sample a)  [2.000]

 Execution time of Domain\Problem 11 
00:18:23.9055743

 DOMAIN/PROBLEM 12 

File: E:\scotty_tests\icaps18tests\domain.pddl
File: E:\scotty_tests\icaps18tests\pfile12.pddl
Number of literals: 13
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%] [140%] [150%] [160%] [170%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 40.000
b (39.000 | 0.050)b (38.000 | 0.050)b (37.000 | 0.101)b (36.000 | 0.101)b (35.000 | 0.152)b (33.000 | 0.203)b (32.000 | 0.203)b (31.000 | 0.205)b (30.000 | 0.205)b (29.000 | 17.005)b (28.000 | 17.005)b (27.000 | 17.056)b (26.000 | 17.056)b (25.000 | 17.257)b (24.000 | 17.257)b (23.000 | 17.259)b (22.000 | 17.259)b (21.000 | 85.009)b (20.000 | 85.009)b (19.000 | 91.010)b (18.000 | 91.010)
Resorting to best-first search
b (39.000 | 0.001)b (38.000 | 0.001)b (37.000 | 5.501)b (37.000 | 4.501)b (37.000 | 0.052)b (36.000 | 0.052)b (35.000 | 5.552)b (35.000 | 4.552)b (35.000 | 0.103)b (34.000 | 0.103)b (33.000 | 5.603)b (33.000 | 4.603)b (33.000 | 0.154)b (32.000 | 0.154)b (31.000 | 5.654)b (31.000 | 4.654)b (30.000 | 5.654)b (29.000 | 7.655)b (28.000 | 7.655)b (27.000 | 7.757)b (26.000 | 7.757)b (25.000 | 21.009)b (25.000 | 17.009)b (25.000 | 12.359)b (24.000 | 21.009)b (23.000 | 21.060)b (22.000 | 21.060)b (21.000 | 21.261)b (20.000 | 21.261)b (19.000 | 21.263)b (18.000 | 21.263)b (17.000 | 89.013)b (16.000 | 89.013)b (15.000 | 95.014)b (14.000 | 95.014)b (13.000 | 102.016)b (12.000 | 102.016)b (11.000 | 114.018)b (10.000 | 114.018)b (9.000 | 142.023)b (8.000 | 142.023)b (7.000 | 148.025)b (6.000 | 148.025)b (5.000 | 160.027)b (4.000 | 160.027)b (3.000 | 178.029)b (2.000 | 178.029)b (1.000 | 184.034);;;; Solution Found
; States evaluated: 18253
; Cost: 210.534
0.000: (glide_forward)  [3.500]  (?dx [0.000,10.000])
3.501: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
13.502: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
23.503: (recharge)  [0.500]  (?deltarecharge [1.000,10.000])
24.004: (take-sample i)  [2.000]
26.005: (take-sample h)  [2.000]
28.006: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
38.007: (recharge)  [5.000]  (?deltarecharge [1.000,109.000])
43.008: (glide_forward)  [7.500]  (?dx [0.000,58.000])
50.509: (take-sample k)  [2.000]
52.510: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
62.511: (recharge)  [5.000]  (?deltarecharge [1.000,186.000])
67.512: (glide_forward)  [35.000]  (?dx [0.000,78.000])
102.513: (take-sample j)  [2.000]
104.514: (take-sample d)  [2.000]
106.515: (glide_backward)  [5.000]  (?dx [0.000,23.000])
111.516: (take-sample g)  [2.000]
113.517: (glide_backward)  [5.000]  (?dx [0.000,18.000])
118.518: (take-sample c)  [2.000]
120.519: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
130.520: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
140.521: (recharge)  [4.800]  (?deltarecharge [1.000,116.000])
145.322: (glide_backward)  [12.000]  (?dx [0.000,58.000])
157.323: (take-sample f)  [2.000]
159.324: (glide_backward)  [3.000]  (?dx [0.000,32.000])
162.325: (take-sample b)  [2.000]
164.326: (glide_backward)  [5.000]  (?dx [0.000,28.000])
169.327: (take-sample l)  [2.000]
171.328: (glide_backward)  [5.500]  (?dx [0.000,18.000])
176.829: (take-sample e)  [2.000]
178.830: (turn_solar_panels_on)  [10.000]  (?solar [1.000,200.000])
188.831: (refuel_tank)  [10.000]  (?fuelamount [1.000,200.000])
198.832: (recharge)  [5.200]  (?deltarecharge [1.000,104.000])
204.033: (glide_backward)  [4.500]  (?dx [0.000,21.000])
208.534: (take-sample a)  [2.000]

 Execution time of Domain\Problem 12 
00:19:06.3521439
