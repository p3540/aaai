the results of rovers tests in popcornX 

 POPCORNX 


 DOMAIN/PROBLEM 1 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile01.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000
b (7.000 | 0.010)b (6.000 | 50.011)b (4.000 | 60.012)b (3.000 | 932.880)b (2.000 | 932.880)b (1.000 | 932.880);;;; Solution Found
; States evaluated: 36
; Cost: 964.858
0.000: (fill_tanker tanker0 refinery0)  [24.852]  (?amount [10.000,24852.000])
0.000: (load_airplane cargo1 plane0 airport0)  [10.000]
24.853: (drive_tanker tanker0 refinery0 airport1)  [50.000]
74.854: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]  (?amount [10.000,25000.000])
84.855: (fly_airplane_fast plane1 airport1 airport0)  [795.000]
84.855: (drive_tanker tanker0 airport1 airport0)  [50.000]
134.856: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]  (?amount [10.000,23533.000])
144.857: (fly_airplane_fast plane0 airport0 airport1)  [810.000]
954.858: (unload_airplane cargo1 plane0 airport1)  [10.000]

 Execution time of Domain\Problem 1 
00:00:00.7760768

 DOMAIN/PROBLEM 2 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile02.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000
b (8.000 | 10.000)b (6.000 | 10.000)b (5.000 | 50.011)b (3.000 | 60.012)b (2.000 | 696.144)b (1.000 | 696.144);;;; Solution Found
; States evaluated: 11
; Cost: 729.886
0.000: (load_airplane cargo0 plane0 airport1)  [10.000]
0.000: (fill_tanker tanker0 refinery1)  [24.882]  (?amount [10.000,24882.000])
24.883: (drive_tanker tanker0 refinery1 airport1)  [50.000]
74.884: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]  (?amount [10.000,25000.000])
84.885: (fly_airplane_fast plane0 airport1 airport0)  [635.000]
719.886: (unload_airplane cargo0 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 2 
00:00:00.4993579

 DOMAIN/PROBLEM 3 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile03.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

62% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 11.000
b (10.000 | 10.000)b (8.000 | 10.000)b (7.000 | 50.011)b (5.000 | 60.012)b (4.000 | 761.922)b (3.000 | 1724.143)b (2.000 | 1724.143)b (1.000 | 1724.143);;;; Solution Found
; States evaluated: 89
; Cost: 1754.937
0.000: (load_airplane cargo1 plane1 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [24.930]  (?amount [10.000,24930.000])
24.931: (drive_tanker tanker0 refinery0 airport2)  [50.000]
74.932: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]  (?amount [10.000,25000.000])
84.933: (fly_airplane_fast plane1 airport2 airport1)  [530.000]
84.933: (drive_tanker tanker0 airport2 airport0)  [50.000]
134.934: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]  (?amount [10.000,24171.000])
144.935: (fly_airplane_fast plane0 airport0 airport1)  [640.000]
614.934: (unload_airplane cargo1 plane1 airport1)  [10.000]
624.935: (load_airplane cargo2 plane1 airport1)  [10.000]
634.936: (fly_airplane_fast plane1 airport1 airport2)  [1110.000]
1744.937: (unload_airplane cargo2 plane1 airport2)  [10.000]

 Execution time of Domain\Problem 3 
00:00:01.3690350

 DOMAIN/PROBLEM 4 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile04.pddl
Number of literals: 23
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

70% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000
b (11.000 | 0.010)b (10.000 | 50.011)b (8.000 | 60.012)b (7.000 | 575.623)b (6.000 | 1312.075)b (5.000 | 2143.736)b (4.000 | 2143.736)b (3.000 | 2153.737)b (2.000 | 3316.058)b (1.000 | 3316.058);;;; Solution Found
; States evaluated: 28
; Cost: 3344.954
0.000: (fill_tanker tanker0 refinery0)  [24.944]  (?amount [10.000,24944.000])
24.945: (drive_tanker tanker0 refinery0 airport1)  [50.000]
74.946: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]  (?amount [10.000,25000.000])
84.947: (fly_airplane_fast plane0 airport1 airport2)  [505.000]
589.948: (load_airplane cargo0 plane0 airport2)  [10.000]
599.949: (fly_airplane_fast plane0 airport2 airport0)  [725.000]
1324.950: (load_airplane cargo1 plane0 airport0)  [10.000]
1334.951: (fly_airplane_fast plane0 airport0 airport2)  [830.000]
2164.952: (unload_airplane cargo1 plane0 airport2)  [10.000]
2174.953: (fly_airplane_fast plane0 airport2 airport1)  [1160.000]
3334.954: (unload_airplane cargo0 plane0 airport1)  [10.000]

 Execution time of Domain\Problem 4 
00:00:00.6739887

 DOMAIN/PROBLEM 5 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile05.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

72% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (13.000 | 0.010)b (12.000 | 50.011)b (10.000 | 60.012)b (9.000 | 726.114)b (8.000 | 736.115)b (7.000 | 1657.937)b (6.000 | 1667.938)b (5.000 | 2419.439)b (4.000 | 2419.439)b (3.000 | 2429.440)b (2.000 | 3636.851)b (1.000 | 3636.851);;;; Solution Found
; States evaluated: 34
; Cost: 3664.951
0.000: (fill_tanker tanker0 refinery1)  [24.939]  (?amount [10.000,24939.000])
24.940: (drive_tanker tanker0 refinery1 airport0)  [50.000]
74.941: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]  (?amount [10.000,25000.000])
84.942: (fly_airplane_fast plane0 airport0 airport2)  [655.000]
739.943: (load_airplane cargo2 plane0 airport2)  [10.000]
749.944: (load_airplane cargo0 plane0 airport2)  [10.000]
759.945: (fly_airplane_fast plane0 airport2 airport1)  [910.000]
1669.946: (unload_airplane cargo2 plane0 airport1)  [10.000]
1679.947: (load_airplane cargo3 plane0 airport1)  [10.000]
1689.948: (fly_airplane_fast plane0 airport1 airport2)  [750.000]
2439.949: (unload_airplane cargo3 plane0 airport2)  [10.000]
2449.950: (fly_airplane_fast plane0 airport2 airport0)  [1205.000]
3654.951: (unload_airplane cargo0 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 5 
00:00:00.7447904

 DOMAIN/PROBLEM 6 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile06.pddl
Number of literals: 40
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000
b (12.000 | 10.000)b (10.000 | 10.000)b (9.000 | 50.011)b (7.000 | 60.012)b (6.000 | 1154.016)b (5.000 | 1154.016)b (4.000 | 1154.016)b (3.000 | 1630.028)b (2.000 | 1630.028)b (1.000 | 1630.028);;;; Solution Found
; States evaluated: 56
; Cost: 1659.990
0.000: (load_airplane cargo1 plane0 airport1)  [10.000]
0.000: (fill_tanker tanker0 refinery2)  [24.983]  (?amount [10.000,24983.000])
24.984: (drive_tanker tanker0 refinery2 airport1)  [50.000]
74.985: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]  (?amount [10.000,25000.000])
84.986: (fly_airplane_fast plane0 airport1 airport0)  [1040.000]
84.986: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]  (?amount [10.000,10.000])
94.987: (fly_airplane_fast plane1 airport1 airport2)  [1080.000]
1124.987: (unload_airplane cargo1 plane0 airport0)  [10.000]
1134.988: (load_airplane cargo4 plane0 airport0)  [10.000]
1144.989: (fly_airplane_fast plane0 airport0 airport2)  [505.000]
1649.990: (unload_airplane cargo4 plane0 airport2)  [10.000]

 Execution time of Domain\Problem 6 
00:00:01.0114501

 DOMAIN/PROBLEM 7 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile07.pddl
Number of literals: 45
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

74% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 10.000)b (14.000 | 10.000)b (13.000 | 50.011)b (11.000 | 60.012)b (10.000 | 761.267)b (9.000 | 761.277)b (8.000 | 771.278)b (7.000 | 1634.649)b (6.000 | 1660.962)b (5.000 | 1660.962)b (4.000 | 1660.962)b (3.000 | 1670.963)b (2.000 | 2222.064)b (1.000 | 2222.064);;;; Solution Found
; States evaluated: 208
; Cost: 2249.953
0.000: (load_airplane cargo1 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [24.942]  (?amount [10.000,24942.000])
0.000: (load_airplane cargo3 plane1 airport1)  [10.000]
10.001: (load_airplane cargo0 plane1 airport1)  [10.000]
24.943: (drive_tanker tanker0 refinery0 airport2)  [50.000]
74.944: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]  (?amount [10.000,25000.000])
84.945: (fly_airplane_fast plane0 airport2 airport1)  [700.000]
84.945: (drive_tanker tanker0 airport2 airport1)  [50.000]
134.946: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]  (?amount [10.000,23678.000])
144.947: (fly_airplane_fast plane1 airport1 airport2)  [860.000]
784.946: (unload_airplane cargo1 plane0 airport1)  [10.000]
794.947: (fly_airplane_fast plane0 airport1 airport2)  [860.000]
1004.948: (unload_airplane cargo3 plane1 airport2)  [10.000]
1014.949: (unload_airplane cargo0 plane1 airport2)  [10.000]
1024.950: (fly_airplane_fast plane1 airport2 airport0)  [655.000]
1679.951: (load_airplane cargo4 plane1 airport0)  [10.000]
1689.952: (fly_airplane_fast plane1 airport0 airport1)  [550.000]
2239.953: (unload_airplane cargo4 plane1 airport1)  [10.000]

 Execution time of Domain\Problem 7 
00:00:02.8839367

 DOMAIN/PROBLEM 8 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile08.pddl
Number of literals: 57
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 10.000)b (15.000 | 20.001)b (14.000 | 20.001)b (13.000 | 20.001)b (12.000 | 50.012)b (10.000 | 60.012)b (9.000 | 1116.790)b (8.000 | 1116.790)b (7.000 | 1116.790)b (6.000 | 1116.790)b (5.000 | 1126.791)b (4.000 | 1126.791)b (3.000 | 1637.683)b (2.000 | 1637.683)b (1.000 | 1647.684);;;; Solution Found
; States evaluated: 233
; Cost: 1679.924
0.000: (load_airplane cargo5 plane0 airport0)  [10.000]
0.000: (fill_tanker tanker0 refinery1)  [24.916]  (?amount [10.000,24916.000])
0.001: (fill_tanker tanker1 refinery1)  [24.881]  (?amount [10.000,24881.000])
10.001: (load_airplane cargo2 plane0 airport0)  [10.000]
24.883: (drive_tanker tanker1 refinery1 airport0)  [50.000]
24.917: (drive_tanker tanker0 refinery1 airport0)  [50.000]
74.884: (fill_airplane_tank plane1 tanker1 airport0)  [10.000]  (?amount [10.000,25000.000])
74.918: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]  (?amount [10.000,25000.000])
84.885: (fly_airplane_fast plane1 airport0 airport1)  [510.000]
84.919: (fly_airplane_fast plane0 airport0 airport2)  [895.000]
594.886: (load_airplane cargo3 plane1 airport1)  [10.000]
604.887: (fly_airplane_fast plane1 airport1 airport0)  [535.000]
979.920: (unload_airplane cargo2 plane0 airport2)  [10.000]
989.921: (load_airplane cargo6 plane0 airport2)  [10.000]
999.922: (fly_airplane_fast plane0 airport2 airport1)  [660.000]
1139.888: (unload_airplane cargo3 plane1 airport0)  [10.000]
1659.923: (unload_airplane cargo6 plane0 airport1)  [10.000]
1669.924: (unload_airplane cargo5 plane0 airport1)  [10.000]

 Execution time of Domain\Problem 8 
00:00:04.0140712

 DOMAIN/PROBLEM 9 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile09.pddl
Number of literals: 52
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

67% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 19.000
b (18.000 | 0.010)b (17.000 | 0.010)b (16.000 | 50.011)b (14.000 | 60.012)b (13.000 | 1893.499)b (11.000 | 1893.499)b (10.000 | 3045.733)b (9.000 | 6257.126)b (8.000 | 6257.126)b (7.000 | 6257.126)b (6.000 | 6257.126)b (5.000 | 8301.253)b (4.000 | 8301.253)b (3.000 | 8301.253)b (2.000 | 8301.253)b (1.000 | 8311.254);;;; Solution Found
; States evaluated: 135
; Cost: 8329.942
0.000: (fill_tanker tanker0 refinery2)  [24.933]  (?amount [10.000,24933.000])
0.000: (fill_tanker tanker1 refinery0)  [24.971]  (?amount [10.000,24971.000])
0.000: (load_airplane cargo2 plane0 airport3)  [10.000]
24.934: (drive_tanker tanker0 refinery2 airport0)  [50.000]
24.972: (drive_tanker tanker1 refinery0 airport0)  [50.000]
74.935: (fill_airplane_tank plane1 tanker0 airport0)  [10.000]  (?amount [10.000,25000.000])
74.973: (drive_tanker tanker1 airport0 airport3)  [50.000]
84.936: (fly_airplane_fast plane1 airport0 airport3)  [1830.000]
84.936: (drive_tanker tanker0 airport0 airport3)  [50.000]
124.974: (fill_airplane_tank plane0 tanker1 airport3)  [10.000]  (?amount [10.000,25000.000])
134.975: (fly_airplane_fast plane0 airport3 airport2)  [2920.000]
1914.937: (fly_airplane_fast plane1 airport3 airport1)  [2915.000]
3054.976: (unload_airplane cargo2 plane0 airport2)  [10.000]
3064.977: (fly_airplane_fast plane0 airport2 airport1)  [1470.000]
4534.978: (load_airplane cargo3 plane0 airport1)  [10.000]
4544.979: (fly_airplane_fast plane0 airport1 airport3)  [1725.000]
4829.938: (load_airplane cargo1 plane1 airport1)  [10.000]
4839.939: (load_airplane cargo0 plane1 airport1)  [10.000]
4849.940: (fly_airplane_fast plane1 airport1 airport2)  [3460.000]
6269.980: (unload_airplane cargo3 plane0 airport3)  [10.000]
8309.941: (unload_airplane cargo1 plane1 airport2)  [10.000]
8319.942: (unload_airplane cargo0 plane1 airport2)  [10.000]

 Execution time of Domain\Problem 9 
00:00:02.9611505
File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile10.pddl
Number of literals: 54
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

69% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 19.000
b (18.000 | 10.000)b (17.000 | 20.001)b (16.000 | 20.001)b (15.000 | 20.001)b (14.000 | 50.012)b (12.000 | 60.012)b (11.000 | 3070.859)b (10.000 | 3070.859)b (9.000 | 5310.311)b (8.000 | 5310.311)b (7.000 | 5310.311)b (6.000 | 5310.311)b (5.000 | 5310.311)b (4.000 | 5896.440)b (3.000 | 5896.440)b (2.000 | 5896.440)b (1.000 | 5896.440);;;; Solution Found
; States evaluated: 106
; Cost: 5919.937
0.000: (load_airplane cargo1 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [24.928]  (?amount [10.000,24928.000])
0.001: (fill_tanker tanker1 refinery0)  [24.961]  (?amount [10.000,24961.000])
10.001: (load_airplane cargo0 plane0 airport2)  [10.000]
24.929: (drive_tanker tanker0 refinery0 airport2)  [50.000]
24.963: (drive_tanker tanker1 refinery0 airport2)  [50.000]
74.930: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]  (?amount [10.000,25000.000])
74.964: (fill_airplane_tank plane1 tanker1 airport2)  [10.000]  (?amount [10.000,25000.000])
84.931: (fly_airplane_fast plane0 airport2 airport0)  [1705.000]
84.965: (fly_airplane_fast plane1 airport2 airport3)  [3005.000]
1789.932: (fly_airplane_fast plane0 airport0 airport1)  [2300.000]
3089.966: (load_airplane cargo2 plane1 airport3)  [10.000]
3099.967: (fly_airplane_fast plane1 airport3 airport0)  [2225.000]
4089.933: (unload_airplane cargo1 plane0 airport1)  [10.000]
4099.934: (unload_airplane cargo0 plane0 airport1)  [10.000]
4109.935: (load_airplane cargo3 plane0 airport1)  [10.000]
4119.936: (fly_airplane_fast plane0 airport1 airport0)  [1790.000]
5324.968: (unload_airplane cargo2 plane1 airport0)  [10.000]
5909.937: (unload_airplane cargo3 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 10 
00:00:02.4821675

 DOMAIN/PROBLEM 11 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile11.pddl
Number of literals: 60
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 22.000
b (21.000 | 10.000)b (20.000 | 10.000)b (19.000 | 10.000)b (18.000 | 50.011)b (16.000 | 60.012)b (15.000 | 110.013)b (13.000 | 110.013)b (12.000 | 2178.866)b (11.000 | 2178.866)b (10.000 | 2178.866)b (9.000 | 2178.866)b (8.000 | 2188.867)b (7.000 | 3702.072)b (6.000 | 3712.073)b (5.000 | 6532.539)b (4.000 | 6532.539)b (3.000 | 8180.976)b (2.000 | 8180.976)b (1.000 | 8180.976);;;; Solution Found
; States evaluated: 145
; Cost: 8199.962
0.000: (load_airplane cargo0 plane1 airport0)  [10.000]
0.000: (fill_tanker tanker0 refinery2)  [24.952]  (?amount [10.000,24952.000])
0.000: (fill_tanker tanker1 refinery0)  [24.911]  (?amount [10.000,24911.000])
24.912: (drive_tanker tanker1 refinery0 airport1)  [50.000]
24.953: (drive_tanker tanker0 refinery2 airport1)  [50.000]
74.913: (drive_tanker tanker1 airport1 airport0)  [50.000]
74.954: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]  (?amount [10.000,25000.000])
84.955: (drive_tanker tanker0 airport1 airport0)  [50.000]
84.955: (fly_airplane_fast plane0 airport1 airport3)  [1295.000]
124.914: (fill_airplane_tank plane1 tanker1 airport0)  [10.000]  (?amount [10.000,25000.000])
134.915: (fly_airplane_fast plane1 airport0 airport1)  [2065.000]
1379.956: (fly_airplane_fast plane0 airport3 airport2)  [2330.000]
2199.916: (unload_airplane cargo0 plane1 airport1)  [10.000]
2209.917: (fly_airplane_fast plane1 airport1 airport3)  [1295.000]
3504.918: (fly_airplane_fast plane1 airport3 airport0)  [3040.000]
3709.957: (load_airplane cargo2 plane0 airport2)  [10.000]
3719.958: (load_airplane cargo1 plane0 airport2)  [10.000]
3729.959: (fly_airplane_fast plane0 airport2 airport3)  [1410.000]
5139.960: (unload_airplane cargo2 plane0 airport3)  [10.000]
5149.961: (fly_airplane_fast plane0 airport3 airport0)  [3040.000]
8189.962: (unload_airplane cargo1 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 11 
00:00:03.6156095

 DOMAIN/PROBLEM 12 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile12.pddl
Number of literals: 62
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

72% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 10.000)b (26.000 | 10.000)b (25.000 | 10.000)b (24.000 | 50.012)b (22.000 | 60.012)b (20.000 | 2249.126)b (19.000 | 2249.126)b (18.000 | 2249.126)b (17.000 | 2249.126)b (15.000 | 2249.126)b (14.000 | 2259.007)b (13.000 | 2269.008)b (12.000 | 2279.009)b (11.000 | 5645.888)b (10.000 | 5645.888)b (9.000 | 5645.888)b (8.000 | 5645.888)b (7.000 | 5655.889)b (6.000 | 8767.100)b (5.000 | 8767.100)b (4.000 | 10976.334)b (3.000 | 10976.334)b (2.000 | 10976.334)b (1.000 | 10986.335);;;; Solution Found
; States evaluated: 96
; Cost: 10999.900
0.000: (load_airplane cargo3 plane0 airport0)  [10.000]
0.000: (fill_tanker tanker0 refinery5)  [24.942]  (?amount [10.000,24942.000])
0.001: (fill_tanker tanker1 refinery5)  [24.885]  (?amount [10.000,24885.000])
24.887: (drive_tanker tanker1 refinery5 airport3)  [50.000]
24.943: (drive_tanker tanker0 refinery5 airport3)  [50.000]
74.888: (drive_tanker tanker1 airport3 airport0)  [50.000]
74.944: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]  (?amount [10.000,25000.000])
84.945: (fly_airplane_fast plane1 airport3 airport1)  [2185.000]
84.945: (drive_tanker tanker0 airport3 airport0)  [50.000]
124.889: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]  (?amount [10.000,25000.000])
134.890: (fly_airplane_fast plane0 airport0 airport2)  [2135.000]
2269.891: (load_airplane cargo2 plane0 airport2)  [10.000]
2269.946: (load_airplane cargo4 plane1 airport1)  [10.000]
2279.892: (load_airplane cargo1 plane0 airport2)  [10.000]
2279.947: (fly_airplane_fast plane1 airport1 airport3)  [3380.000]
2289.893: (load_airplane cargo0 plane0 airport2)  [10.000]
2299.894: (fly_airplane_fast plane0 airport2 airport1)  [1925.000]
4224.895: (unload_airplane cargo3 plane0 airport1)  [10.000]
4234.896: (fly_airplane_fast plane0 airport1 airport0)  [3270.000]
5659.948: (unload_airplane cargo4 plane1 airport3)  [10.000]
5669.949: (fly_airplane_fast plane1 airport3 airport0)  [3105.000]
7504.897: (unload_airplane cargo1 plane0 airport0)  [10.000]
7514.898: (fly_airplane_fast plane0 airport0 airport3)  [3465.000]
10979.899: (unload_airplane cargo2 plane0 airport3)  [10.000]
10989.900: (unload_airplane cargo0 plane0 airport3)  [10.000]

 Execution time of Domain\Problem 12 
00:00:02.7229572

 DOMAIN/PROBLEM 13 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile13.pddl
Number of literals: 81
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 26.000
b (25.000 | 10.000)b (24.000 | 20.001)b (23.000 | 20.001)b (22.000 | 20.001)b (21.000 | 50.011)b (19.000 | 60.012)b (18.000 | 110.013)b (16.000 | 110.013)b (15.000 | 2178.907)b (14.000 | 2178.907)b (13.000 | 2178.907)b (12.000 | 2178.907)b (11.000 | 2178.907)b (10.000 | 3328.773)b (9.000 | 3328.773)b (8.000 | 3338.774)b (7.000 | 3348.775)b (6.000 | 3662.940)b (5.000 | 3672.941)b (4.000 | 3682.942)b (3.000 | 6258.254)b (2.000 | 6258.254)b (1.000 | 6258.254);;;; Solution Found
; States evaluated: 572
; Cost: 6269.997
0.000: (load_airplane cargo5 plane1 airport3)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [24.986]  (?amount [10.000,24986.000])
0.000: (fill_tanker tanker1 refinery5)  [24.925]  (?amount [10.000,24925.000])
10.001: (load_airplane cargo3 plane1 airport3)  [10.000]
24.926: (drive_tanker tanker1 refinery5 airport3)  [50.000]
24.987: (drive_tanker tanker0 refinery0 airport3)  [50.000]
74.927: (drive_tanker tanker1 airport3 airport2)  [50.000]
74.988: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]  (?amount [10.000,25000.000])
84.989: (drive_tanker tanker0 airport3 airport2)  [50.000]
84.989: (fly_airplane_fast plane1 airport3 airport2)  [1485.000]
124.928: (fill_airplane_tank plane0 tanker1 airport2)  [10.000]  (?amount [10.000,25000.000])
134.929: (fly_airplane_fast plane0 airport2 airport0)  [2065.000]
134.929: (drive_tanker tanker1 airport2 airport3)  [50.000]
134.990: (drive_tanker tanker0 airport2 airport3)  [50.000]
184.991: (fill_airplane_tank plane2 tanker0 airport3)  [10.000]  (?amount [10.000,10.000])
194.992: (fly_airplane_fast plane2 airport3 airport1)  [1790.000]
1569.990: (load_airplane cargo2 plane1 airport2)  [10.000]
1579.991: (fly_airplane_fast plane1 airport2 airport1)  [2085.000]
1984.993: (load_airplane cargo4 plane2 airport1)  [10.000]
1994.994: (load_airplane cargo1 plane2 airport1)  [10.000]
2004.995: (fly_airplane_fast plane2 airport1 airport3)  [1340.000]
2199.930: (fly_airplane_fast plane0 airport0 airport1)  [1880.000]
3344.996: (unload_airplane cargo4 plane2 airport3)  [10.000]
3354.997: (unload_airplane cargo1 plane2 airport3)  [10.000]
3364.998: (fly_airplane_fast plane2 airport3 airport0)  [2600.000]
3664.992: (unload_airplane cargo5 plane1 airport1)  [10.000]
3674.993: (unload_airplane cargo3 plane1 airport1)  [10.000]
3684.994: (unload_airplane cargo2 plane1 airport1)  [10.000]
3694.995: (load_airplane cargo5 plane1 airport1)  [10.000]
3704.996: (fly_airplane_fast plane1 airport1 airport0)  [2555.000]
6259.997: (unload_airplane cargo5 plane1 airport0)  [10.000]

 Execution time of Domain\Problem 13 
00:00:20.9532526

 DOMAIN/PROBLEM 14 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile14.pddl
Number of literals: 82
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 10.000)b (13.000 | 20.001)b (12.000 | 50.000)b (11.000 | 50.000)b (10.000 | 100.001)b (8.000 | 100.001)b (7.000 | 2534.868)b (6.000 | 2544.869)b (5.000 | 2544.869)b (4.000 | 2544.869)b (3.000 | 4007.743)b (2.000 | 4007.743)b (1.000 | 4017.744);;;; Solution Found
; States evaluated: 74
; Cost: 4044.925
0.000: (load_airplane cargo4 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery4)  [24.985]  (?amount [10.000,24985.000])
0.000: (fill_tanker tanker1 refinery3)  [24.917]  (?amount [10.000,24917.000])
0.000: (drive_tanker tanker2 refinery6 airport2)  [50.000]
10.001: (load_airplane cargo3 plane0 airport2)  [10.000]
24.918: (drive_tanker tanker1 refinery3 airport2)  [50.000]
24.986: (drive_tanker tanker0 refinery4 airport2)  [50.000]
50.001: (drive_tanker tanker2 airport2 refinery0)  [50.000]
74.919: (fill_airplane_tank plane1 tanker1 airport2)  [10.000]  (?amount [10.000,25000.000])
74.987: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]  (?amount [10.000,25000.000])
84.920: (fly_airplane_fast plane1 airport2 airport1)  [2460.000]
84.988: (fly_airplane_fast plane0 airport2 airport1)  [2460.000]
2544.921: (load_airplane cargo2 plane1 airport1)  [10.000]
2544.989: (unload_airplane cargo4 plane0 airport1)  [10.000]
2554.922: (load_airplane cargo0 plane1 airport1)  [10.000]
2554.990: (unload_airplane cargo3 plane0 airport1)  [10.000]
2564.923: (fly_airplane_fast plane1 airport1 airport2)  [1460.000]
4024.924: (unload_airplane cargo2 plane1 airport2)  [10.000]
4034.925: (unload_airplane cargo0 plane1 airport2)  [10.000]

 Execution time of Domain\Problem 14 
00:00:02.8915780

 DOMAIN/PROBLEM 15 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile15.pddl
Number of literals: 100
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 10.000)b (26.000 | 10.000)b (25.000 | 50.000)b (24.000 | 50.000)b (23.000 | 100.001)b (21.000 | 100.001)b (19.000 | 1447.473)b (18.000 | 1447.473)b (17.000 | 1447.473)b (16.000 | 1447.473)b (15.000 | 1457.474)b (13.000 | 1457.474)b (12.000 | 1672.676)b (11.000 | 1672.676)b (10.000 | 4433.187)b (9.000 | 4433.187)b (8.000 | 4443.188)b (7.000 | 7103.480)b (6.000 | 7103.480)b (5.000 | 7103.480)b (4.000 | 7103.480)b (3.000 | 7103.480)b (2.000 | 7103.480)b (1.000 | 7103.480);;;; Solution Found
; States evaluated: 125
; Cost: 7124.900
0.000: (load_airplane cargo4 plane1 airport3)  [10.000]
0.000: (load_airplane cargo0 plane0 airport0)  [10.000]
0.000: (fill_tanker tanker0 refinery3)  [24.866]  (?amount [10.000,24866.000])
0.000: (fill_tanker tanker1 refinery1)  [24.891]  (?amount [10.000,24891.000])
0.000: (drive_tanker tanker2 refinery3 airport3)  [50.000]
24.867: (drive_tanker tanker0 refinery3 airport3)  [50.000]
24.892: (drive_tanker tanker1 refinery1 airport3)  [50.000]
50.001: (drive_tanker tanker2 airport3 refinery0)  [50.000]
74.868: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]  (?amount [10.000,25000.000])
74.893: (drive_tanker tanker1 airport3 airport0)  [50.000]
84.869: (fly_airplane_fast plane1 airport3 airport4)  [1385.000]
84.869: (drive_tanker tanker0 airport3 airport0)  [50.000]
124.894: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]  (?amount [10.000,25000.000])
134.895: (fly_airplane_fast plane0 airport0 airport3)  [1560.000]
1469.870: (unload_airplane cargo4 plane1 airport4)  [10.000]
1479.871: (fly_airplane_fast plane1 airport4 airport1)  [3170.000]
1694.896: (fly_airplane_fast plane0 airport3 airport1)  [2755.000]
4449.897: (unload_airplane cargo0 plane0 airport1)  [10.000]
4459.898: (load_airplane cargo6 plane0 airport1)  [10.000]
4469.899: (fly_airplane_fast plane0 airport1 airport4)  [2645.000]
4649.872: (load_airplane cargo2 plane1 airport1)  [10.000]
4659.873: (fly_airplane_fast plane1 airport1 airport2)  [1450.000]
6109.874: (unload_airplane cargo2 plane1 airport2)  [10.000]
7114.900: (unload_airplane cargo6 plane0 airport4)  [10.000]

 Execution time of Domain\Problem 15 
00:00:07.3301497

 DOMAIN/PROBLEM 16 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile16.pddl
Number of literals: 103
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 36.000
b (35.000 | 10.000)b (34.000 | 20.001)b (33.000 | 30.002)b (32.000 | 50.000)b (31.000 | 50.000)b (30.000 | 100.001)b (28.000 | 100.001)b (26.000 | 1357.262)b (25.000 | 1357.262)b (24.000 | 1357.262)b (23.000 | 1357.262)b (22.000 | 1357.262)b (21.000 | 1367.263)b (20.000 | 1377.264)b (19.000 | 1387.265)b (18.000 | 1387.265)b (17.000 | 3411.316)b (16.000 | 3411.316)b (15.000 | 3411.316)b (14.000 | 6193.080)b (13.000 | 6193.080)b (12.000 | 6193.080)b (11.000 | 6193.080)b (10.000 | 6481.538)b (9.000 | 6481.538)b (8.000 | 6501.550)b (7.000 | 6501.550)b (6.000 | 8731.552)b (5.000 | 8731.552)b (4.000 | 8731.552)b (3.000 | 8741.553)b (2.000 | 10126.554)b (1.000 | 10126.554);;;; Solution Found
; States evaluated: 829
; Cost: 10139.989
0.000: (load_airplane cargo5 plane1 airport3)  [10.000]
0.000: (fill_tanker tanker0 refinery3)  [24.966]  (?amount [10.000,24966.000])
0.000: (fill_tanker tanker1 refinery7)  [24.995]  (?amount [10.000,24995.000])
0.000: (drive_tanker tanker2 refinery0 airport3)  [50.000]
0.000: (load_airplane cargo6 plane0 airport2)  [10.000]
10.001: (load_airplane cargo4 plane1 airport3)  [10.000]
20.002: (load_airplane cargo3 plane1 airport3)  [10.000]
24.967: (drive_tanker tanker0 refinery3 airport3)  [50.000]
24.996: (drive_tanker tanker1 refinery7 airport3)  [50.000]
50.001: (drive_tanker tanker2 airport3 refinery0)  [50.000]
74.968: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]  (?amount [10.000,25000.000])
74.997: (drive_tanker tanker1 airport3 airport2)  [50.000]
84.969: (fly_airplane_fast plane1 airport3 airport2)  [1295.000]
84.969: (drive_tanker tanker0 airport3 airport2)  [50.000]
100.002: (drive_tanker tanker2 refinery0 airport1)  [50.000]
124.998: (drive_tanker tanker1 airport2 airport1)  [50.000]
1379.970: (unload_airplane cargo5 plane1 airport2)  [10.000]
1389.971: (unload_airplane cargo4 plane1 airport2)  [10.000]
1389.971: (load_airplane cargo5 plane0 airport2)  [10.000]
1399.972: (unload_airplane cargo3 plane1 airport2)  [10.000]
1399.972: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]  (?amount [10.000,22707.000])
1409.973: (fly_airplane_fast plane1 airport2 airport4)  [2020.000]
1409.973: (drive_tanker tanker0 airport2 refinery0)  [50.000]
1409.973: (load_airplane cargo4 plane0 airport2)  [10.000]
1419.974: (fly_airplane_fast plane0 airport2 airport0)  [3270.000]
1459.974: (drive_tanker tanker0 refinery0 airport1)  [50.000]
3429.974: (fly_airplane_fast plane1 airport4 airport1)  [2770.000]
4689.975: (unload_airplane cargo6 plane0 airport0)  [10.000]
4699.976: (unload_airplane cargo5 plane0 airport0)  [10.000]
4709.977: (load_airplane cargo1 plane0 airport0)  [10.000]
4719.978: (load_airplane cargo0 plane0 airport0)  [10.000]
4729.979: (fly_airplane_fast plane0 airport0 airport1)  [1725.000]
6199.975: (load_airplane cargo2 plane1 airport1)  [10.000]
6454.980: (unload_airplane cargo4 plane0 airport1)  [10.000]
6464.981: (unload_airplane cargo1 plane0 airport1)  [10.000]
6464.981: (load_airplane cargo4 plane1 airport1)  [10.000]
6474.982: (unload_airplane cargo0 plane0 airport1)  [10.000]
6474.982: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]  (?amount [10.000,3444.000])
6484.983: (unload_airplane cargo4 plane1 airport1)  [10.000]
6484.983: (fill_airplane_tank plane0 tanker1 airport1)  [10.000]  (?amount [10.000,25000.000])
6494.984: (unload_airplane cargo2 plane1 airport1)  [10.000]
6494.984: (load_airplane cargo4 plane0 airport1)  [10.000]
6504.985: (fill_airplane_tank plane1 tanker2 airport1)  [10.000]  (?amount [10.000,123.000])
6504.985: (load_airplane cargo2 plane0 airport1)  [10.000]
6514.986: (fly_airplane_fast plane0 airport1 airport4)  [2220.000]
8734.987: (unload_airplane cargo4 plane0 airport4)  [10.000]
8744.988: (fly_airplane_fast plane0 airport4 airport2)  [1385.000]
10129.989: (unload_airplane cargo2 plane0 airport2)  [10.000]

 Execution time of Domain\Problem 16 
00:00:58.5071420

 DOMAIN/PROBLEM 17 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile17.pddl
Number of literals: 112
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

73% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 10.000)b (26.000 | 20.001)b (25.000 | 20.001)b (24.000 | 20.001)b (23.000 | 50.011)b (21.000 | 60.012)b (20.000 | 110.013)b (18.000 | 110.013)b (13.000 | 2795.198)b (12.000 | 2795.198)b (11.000 | 2805.199)b (10.000 | 4348.280)b (9.000 | 4348.280)b (8.000 | 4348.280)b (7.000 | 4348.280)b (6.000 | 4358.281)b (5.000 | 4487.030)b (4.000 | 4497.031)b (3.000 | 6055.473)b (2.000 | 7673.704)b (1.000 | 7673.704);;;; Solution Found
; States evaluated: 570
; Cost: 7684.936
0.000: (load_airplane cargo6 plane1 airport4)  [10.000]
0.000: (fill_tanker tanker0 refinery8)  [24.926]  (?amount [10.000,24926.000])
0.000: (fill_tanker tanker1 refinery7)  [24.867]  (?amount [10.000,24867.000])
10.001: (load_airplane cargo2 plane1 airport4)  [10.000]
24.868: (drive_tanker tanker1 refinery7 airport4)  [50.000]
24.927: (drive_tanker tanker0 refinery8 airport4)  [50.000]
74.869: (drive_tanker tanker1 airport4 airport1)  [50.000]
74.928: (fill_airplane_tank plane1 tanker0 airport4)  [10.000]  (?amount [10.000,25000.000])
84.929: (drive_tanker tanker0 airport4 airport1)  [50.000]
84.929: (fly_airplane_fast plane1 airport4 airport1)  [1725.000]
124.870: (fill_airplane_tank plane0 tanker1 airport1)  [10.000]  (?amount [10.000,25000.000])
134.871: (fly_airplane_fast plane0 airport1 airport3)  [2680.000]
134.871: (drive_tanker tanker1 airport1 airport4)  [50.000]
134.930: (drive_tanker tanker0 airport1 airport4)  [50.000]
184.931: (fill_airplane_tank plane2 tanker0 airport4)  [10.000]  (?amount [10.000,10.000])
194.932: (fly_airplane_fast plane2 airport4 airport1)  [1725.000]
1809.930: (fly_airplane_fast plane1 airport1 airport3)  [2680.000]
1919.933: (fly_airplane_fast plane2 airport1 airport3)  [2680.000]
2814.872: (load_airplane cargo5 plane0 airport3)  [10.000]
2824.873: (fly_airplane_fast plane0 airport3 airport2)  [1540.000]
4364.874: (unload_airplane cargo5 plane0 airport2)  [10.000]
4374.875: (fly_airplane_fast plane0 airport2 airport1)  [1615.000]
4489.931: (unload_airplane cargo6 plane1 airport3)  [10.000]
4499.932: (unload_airplane cargo2 plane1 airport3)  [10.000]
4509.933: (load_airplane cargo0 plane1 airport3)  [10.000]
4519.934: (fly_airplane_fast plane1 airport3 airport2)  [1540.000]
6059.935: (fly_airplane_fast plane1 airport2 airport1)  [1615.000]
7674.936: (unload_airplane cargo0 plane1 airport1)  [10.000]

 Execution time of Domain\Problem 17 
00:00:38.6245380

 DOMAIN/PROBLEM 18 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile18.pddl
Number of literals: 123
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

69% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 40.000

 Execution time of Domain\Problem 18 
00:30:00.1693781

 DOMAIN/PROBLEM 19 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile19.pddl
Number of literals: 134
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 43.000

 Execution time of Domain\Problem 19 
00:30:00.1936003

 DOMAIN/PROBLEM 20 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile20.pddl
Number of literals: 150
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

73% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 51.000

 Execution time of Domain\Problem 20 
00:30:00.1814969
