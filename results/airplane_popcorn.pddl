the results of rovers tests in popcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile01.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000
b (7.000 | 50.000)b (6.000 | 60.001)b (4.000 | 270.025)b (3.000 | 1142.893)b (2.000 | 1142.893)b (1.000 | 1142.893);;;; Solution Found
; States evaluated: 6179
; Cost: 1152.894
0.000: (drive_tanker tanker0 refinery0 airport1)  [50.000]
0.000: (load_airplane cargo1 plane0 airport0)  [10.000]
50.001: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
60.002: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
70.003: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
80.004: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
90.005: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
100.006: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
110.007: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
120.008: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
130.009: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
140.010: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
150.011: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
160.012: (drive_tanker tanker0 airport1 refinery0)  [50.000]
210.013: (fill_tanker tanker0 refinery0)  [2.875]
212.889: (drive_tanker tanker0 refinery0 airport1)  [50.000]
262.890: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
272.891: (fly_airplane_fast plane1 airport1 airport0)  [795.000]
272.891: (drive_tanker tanker0 airport1 airport0)  [50.000]
322.892: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
332.893: (fly_airplane_fast plane0 airport0 airport1)  [810.000]
1142.894: (unload_airplane cargo1 plane0 airport1)  [10.000]

 Execution time of Domain\Problem 1 
00:01:17.4993902

 DOMAIN/PROBLEM 2 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile02.pddl
Number of literals: 19
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
65% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000
b (7.000 | 10.000)b (6.000 | 50.000)b (5.000 | 60.001)b (3.000 | 240.022)b (2.000 | 876.154)b (1.000 | 876.154);;;; Solution Found
; States evaluated: 778
; Cost: 886.155
0.000: (load_airplane cargo0 plane0 airport1)  [10.000]
0.000: (drive_tanker tanker0 refinery1 airport1)  [50.000]
50.001: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
60.002: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
70.003: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
80.004: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
90.005: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
100.006: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
110.007: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
120.008: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
130.009: (drive_tanker tanker0 airport1 refinery0)  [50.000]
180.010: (fill_tanker tanker0 refinery0)  [1.141]
181.152: (drive_tanker tanker0 refinery0 airport1)  [50.000]
231.153: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
241.154: (fly_airplane_fast plane0 airport1 airport0)  [635.000]
876.155: (unload_airplane cargo0 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 2 
00:00:07.9046836

 DOMAIN/PROBLEM 3 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile03.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
62% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 10.000
b (9.000 | 10.000)b (8.000 | 50.000)b (7.000 | 60.001)b (5.000 | 180.016)b (4.000 | 881.926)b (3.000 | 1844.147)b (2.000 | 1844.147)b (1.000 | 1844.147);;;; Solution Found
; States evaluated: 114
; Cost: 1854.148
0.000: (load_airplane cargo1 plane1 airport2)  [10.000]
0.000: (drive_tanker tanker0 refinery0 airport2)  [50.000]
50.001: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]
60.002: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]
70.003: (drive_tanker tanker0 airport2 refinery0)  [50.000]
120.004: (fill_tanker tanker0 refinery0)  [4.137]
124.142: (drive_tanker tanker0 refinery0 airport2)  [50.000]
174.143: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]
184.144: (fly_airplane_fast plane1 airport2 airport1)  [530.000]
184.144: (drive_tanker tanker0 airport2 airport0)  [50.000]
234.145: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
244.146: (fly_airplane_fast plane0 airport0 airport1)  [640.000]
714.145: (unload_airplane cargo1 plane1 airport1)  [10.000]
724.146: (load_airplane cargo2 plane1 airport1)  [10.000]
734.147: (fly_airplane_fast plane1 airport1 airport2)  [1110.000]
1844.148: (unload_airplane cargo2 plane1 airport2)  [10.000]

 Execution time of Domain\Problem 3 
00:00:01.6447517

 DOMAIN/PROBLEM 4 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile04.pddl
Number of literals: 23
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
70% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 12.000
b (11.000 | 50.000)b (10.000 | 60.001)b (8.000 | 190.017)b (7.000 | 705.628)b (6.000 | 1442.080)b (5.000 | 2273.741)b (4.000 | 2273.741)b (3.000 | 2283.742)b (2.000 | 3446.063)b (1.000 | 3446.063);;;; Solution Found
; States evaluated: 51
; Cost: 3456.064
0.000: (drive_tanker tanker0 refinery0 airport1)  [50.000]
50.001: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
60.002: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
70.003: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
80.004: (drive_tanker tanker0 airport1 refinery0)  [50.000]
130.005: (fill_tanker tanker0 refinery0)  [6.049]
136.055: (drive_tanker tanker0 refinery0 airport1)  [50.000]
186.056: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
196.057: (fly_airplane_fast plane0 airport1 airport2)  [505.000]
701.058: (load_airplane cargo0 plane0 airport2)  [10.000]
711.059: (fly_airplane_fast plane0 airport2 airport0)  [725.000]
1436.060: (load_airplane cargo1 plane0 airport0)  [10.000]
1446.061: (fly_airplane_fast plane0 airport0 airport2)  [830.000]
2276.062: (unload_airplane cargo1 plane0 airport2)  [10.000]
2286.063: (fly_airplane_fast plane0 airport2 airport1)  [1160.000]
3446.064: (unload_airplane cargo0 plane0 airport1)  [10.000]

 Execution time of Domain\Problem 4 
00:00:00.8799331

 DOMAIN/PROBLEM 5 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile05.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
72% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 14.000
b (13.000 | 50.000)b (12.000 | 60.001)b (10.000 | 200.018)b (9.000 | 866.120)b (8.000 | 876.121)b (7.000 | 1797.943)b (6.000 | 1807.944)b (5.000 | 2559.445)b (4.000 | 2559.445)b (3.000 | 2569.446)b (2.000 | 3776.857)b (1.000 | 3776.857);;;; Solution Found
; States evaluated: 81
; Cost: 3786.858
0.000: (drive_tanker tanker0 refinery1 airport0)  [50.000]
50.001: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
60.002: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
70.003: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
80.004: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
90.005: (drive_tanker tanker0 airport0 refinery0)  [50.000]
140.006: (fill_tanker tanker0 refinery0)  [6.840]
146.847: (drive_tanker tanker0 refinery0 airport0)  [50.000]
196.848: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
206.849: (fly_airplane_fast plane0 airport0 airport2)  [655.000]
861.850: (load_airplane cargo2 plane0 airport2)  [10.000]
871.851: (load_airplane cargo0 plane0 airport2)  [10.000]
881.852: (fly_airplane_fast plane0 airport2 airport1)  [910.000]
1791.853: (unload_airplane cargo2 plane0 airport1)  [10.000]
1801.854: (load_airplane cargo3 plane0 airport1)  [10.000]
1811.855: (fly_airplane_fast plane0 airport1 airport2)  [750.000]
2561.856: (unload_airplane cargo3 plane0 airport2)  [10.000]
2571.857: (fly_airplane_fast plane0 airport2 airport0)  [1205.000]
3776.858: (unload_airplane cargo0 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 5 
00:00:01.1881143

 DOMAIN/PROBLEM 6 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile06.pddl
Number of literals: 40
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 10.000)b (13.000 | 10.000)b (11.000 | 10.000)b (10.000 | 50.011)b (9.000 | 60.012)b (7.000 | 60.012)b (6.000 | 1154.016)b (5.000 | 1154.016)b (4.000 | 1154.016)b (3.000 | 1630.028)b (2.000 | 1630.028)b (1.000 | 1630.028);;;; Solution Found
; States evaluated: 54
; Cost: 1640.029
0.000: (load_airplane cargo1 plane0 airport1)  [10.000]
0.000: (fill_tanker tanker0 refinery2)  [5.022]
5.023: (drive_tanker tanker0 refinery2 airport1)  [50.000]
55.024: (fill_airplane_tank plane0 tanker0 airport1)  [10.000]
65.025: (fly_airplane_fast plane0 airport1 airport0)  [1040.000]
65.025: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
75.026: (fly_airplane_fast plane1 airport1 airport2)  [1080.000]
1105.026: (unload_airplane cargo1 plane0 airport0)  [10.000]
1115.027: (load_airplane cargo4 plane0 airport0)  [10.000]
1125.028: (fly_airplane_fast plane0 airport0 airport2)  [505.000]
1630.029: (unload_airplane cargo4 plane0 airport2)  [10.000]

 Execution time of Domain\Problem 6 
00:00:00.8660729

 DOMAIN/PROBLEM 7 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile07.pddl
Number of literals: 45
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
74% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 19.000
b (18.000 | 10.000)b (17.000 | 10.000)b (15.000 | 10.000)b (14.000 | 50.011)b (13.000 | 60.012)b (11.000 | 60.012)b (10.000 | 761.267)b (9.000 | 761.277)b (8.000 | 771.278)b (7.000 | 1634.649)b (6.000 | 1660.962)b (5.000 | 1660.962)b (4.000 | 1660.962)b (3.000 | 1670.963)b (2.000 | 2222.064)b (1.000 | 2222.064);;;; Solution Found
; States evaluated: 205
; Cost: 2232.065
0.000: (load_airplane cargo1 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [7.054]
0.000: (load_airplane cargo3 plane1 airport1)  [10.000]
7.055: (drive_tanker tanker0 refinery0 airport2)  [50.000]
10.001: (load_airplane cargo0 plane1 airport1)  [10.000]
57.056: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]
67.057: (fly_airplane_fast plane0 airport2 airport1)  [700.000]
67.057: (drive_tanker tanker0 airport2 airport1)  [50.000]
117.058: (fill_airplane_tank plane1 tanker0 airport1)  [10.000]
127.059: (fly_airplane_fast plane1 airport1 airport2)  [860.000]
767.058: (unload_airplane cargo1 plane0 airport1)  [10.000]
777.059: (fly_airplane_fast plane0 airport1 airport2)  [860.000]
987.060: (unload_airplane cargo3 plane1 airport2)  [10.000]
997.061: (unload_airplane cargo0 plane1 airport2)  [10.000]
1007.062: (fly_airplane_fast plane1 airport2 airport0)  [655.000]
1662.063: (load_airplane cargo4 plane1 airport0)  [10.000]
1672.064: (fly_airplane_fast plane1 airport0 airport1)  [550.000]
2222.065: (unload_airplane cargo4 plane1 airport1)  [10.000]

 Execution time of Domain\Problem 7 
00:00:02.2400648

 DOMAIN/PROBLEM 8 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile08.pddl
Number of literals: 57
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000
b (15.000 | 10.000)b (14.000 | 50.000)b (13.000 | 50.000)b (12.000 | 60.001)b (10.000 | 190.017)b (9.000 | 711.310)b (8.000 | 1217.321)b (7.000 | 1889.653)b (6.000 | 1889.653)b (5.000 | 1889.653)b (4.000 | 1889.653)b (3.000 | 1889.653)b (2.000 | 1889.653)b (1.000 | 1890.733);;;; Solution Found
; States evaluated: 65954
; Cost: 1900.734
0.000: (load_airplane cargo5 plane0 airport0)  [10.000]
0.000: (drive_tanker tanker0 refinery1 airport0)  [50.000]
0.000: (drive_tanker tanker1 refinery1 airport0)  [50.000]
10.001: (load_airplane cargo2 plane0 airport0)  [10.000]
50.001: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
50.001: (fill_airplane_tank plane1 tanker1 airport0)  [10.000]
60.002: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
60.002: (fill_airplane_tank plane1 tanker1 airport0)  [10.000]
70.003: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
70.003: (fill_airplane_tank plane1 tanker1 airport0)  [10.000]
80.004: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]
80.004: (drive_tanker tanker0 airport0 refinery0)  [50.000]
90.005: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]
100.006: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]
110.007: (drive_tanker tanker1 airport0 refinery0)  [50.000]
130.005: (fill_tanker tanker0 refinery0)  [5.721]
135.727: (drive_tanker tanker0 refinery0 airport0)  [50.000]
185.728: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
195.729: (fly_airplane_fast plane0 airport0 airport1)  [510.000]
195.729: (fill_airplane_tank plane1 tanker0 airport0)  [10.000]
205.730: (fly_airplane_fast plane1 airport0 airport1)  [510.000]
705.730: (unload_airplane cargo5 plane0 airport1)  [10.000]
715.731: (load_airplane cargo3 plane0 airport1)  [10.000]
715.731: (fly_airplane_fast plane1 airport1 airport2)  [505.000]
725.732: (fly_airplane_fast plane0 airport1 airport2)  [505.000]
1220.732: (load_airplane cargo6 plane1 airport2)  [10.000]
1230.733: (fly_airplane_fast plane1 airport2 airport1)  [660.000]
1230.733: (unload_airplane cargo2 plane0 airport2)  [10.000]
1240.734: (fly_airplane_fast plane0 airport2 airport0)  [540.000]
1780.735: (unload_airplane cargo3 plane0 airport0)  [10.000]
1890.734: (unload_airplane cargo6 plane1 airport1)  [10.000]

 Execution time of Domain\Problem 8 
00:16:10.3515304

 DOMAIN/PROBLEM 9 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile09.pddl
Number of literals: 52
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
67% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 21.000

 Execution time of Domain\Problem 9 
00:30:00.1592987

 DOMAIN/PROBLEM 10 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile10.pddl
Number of literals: 54
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
69% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 21.000
b (20.000 | 10.000)b (19.000 | 20.001)b (18.000 | 50.000)b (16.000 | 50.000)b (15.000 | 50.011)b (14.000 | 60.012)b (12.000 | 60.012)b (11.000 | 3084.150)b (10.000 | 3084.150)b (9.000 | 5328.202)b (8.000 | 5328.202)b (7.000 | 5328.202)b (6.000 | 5328.202)b (5.000 | 5328.202)b (4.000 | 5906.784)b (3.000 | 5906.784)b (2.000 | 5906.784)b (1.000 | 5906.784);;;; Solution Found
; States evaluated: 116
; Cost: 5916.785
0.000: (load_airplane cargo1 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery0)  [21.776]
0.000: (drive_tanker tanker1 refinery0 airport2)  [50.000]
10.001: (load_airplane cargo0 plane0 airport2)  [10.000]
21.777: (drive_tanker tanker0 refinery0 airport2)  [50.000]
71.778: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]
81.779: (fly_airplane_fast plane0 airport2 airport0)  [1705.000]
81.779: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]
91.780: (fly_airplane_fast plane1 airport2 airport3)  [3005.000]
1786.780: (fly_airplane_fast plane0 airport0 airport1)  [2300.000]
3096.781: (load_airplane cargo2 plane1 airport3)  [10.000]
3106.782: (fly_airplane_fast plane1 airport3 airport0)  [2225.000]
4086.781: (unload_airplane cargo1 plane0 airport1)  [10.000]
4096.782: (unload_airplane cargo0 plane0 airport1)  [10.000]
4106.783: (load_airplane cargo3 plane0 airport1)  [10.000]
4116.784: (fly_airplane_fast plane0 airport1 airport0)  [1790.000]
5331.783: (unload_airplane cargo2 plane1 airport0)  [10.000]
5906.785: (unload_airplane cargo3 plane0 airport0)  [10.000]

 Execution time of Domain\Problem 10 
00:00:01.5077741

 DOMAIN/PROBLEM 11 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile11.pddl
Number of literals: 60
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 25.000

 Execution time of Domain\Problem 11 
00:30:00.1748738

 DOMAIN/PROBLEM 12 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile12.pddl
Number of literals: 62
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
72% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000

 Execution time of Domain\Problem 12 
00:30:00.2058682

 DOMAIN/PROBLEM 13 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile13.pddl
Number of literals: 81
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 29.000

 Execution time of Domain\Problem 13 
00:30:00.1651059

 DOMAIN/PROBLEM 14 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile14.pddl
Number of literals: 82
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 10.000)b (14.000 | 10.000)b (13.000 | 50.000)b (12.000 | 50.000)b (11.000 | 60.001)b (10.000 | 60.001)b (9.000 | 90.004)b (8.000 | 100.005)b (7.000 | 2570.007)b (6.000 | 2580.008)b (5.000 | 2580.008)b (4.000 | 4050.010)b (3.000 | 4050.010)b (2.000 | 4050.010)b (1.000 | 4050.010);;;; Solution Found
; States evaluated: 591
; Cost: 4060.011
0.000: (load_airplane cargo4 plane0 airport2)  [10.000]
0.000: (fill_tanker tanker0 refinery4)  [15.521]
0.000: (drive_tanker tanker1 refinery3 airport2)  [50.000]
10.001: (load_airplane cargo3 plane0 airport2)  [10.000]
15.522: (drive_tanker tanker0 refinery4 airport2)  [50.000]
50.001: (fill_airplane_tank plane0 tanker1 airport2)  [10.000]
60.002: (unload_airplane cargo4 plane0 airport2)  [10.000]
70.003: (load_airplane cargo4 plane1 airport2)  [10.000]
70.003: (fill_airplane_tank plane0 tanker0 airport2)  [10.000]
80.004: (unload_airplane cargo4 plane1 airport2)  [10.000]
90.005: (load_airplane cargo4 plane0 airport2)  [10.000]
90.005: (fill_airplane_tank plane1 tanker0 airport2)  [10.000]
100.006: (fly_airplane_fast plane1 airport2 airport1)  [2460.000]
100.006: (fly_airplane_fast plane0 airport2 airport1)  [2460.000]
2560.007: (unload_airplane cargo4 plane0 airport1)  [10.000]
2560.007: (load_airplane cargo2 plane1 airport1)  [10.000]
2570.008: (unload_airplane cargo3 plane0 airport1)  [10.000]
2570.008: (fly_airplane_fast plane1 airport1 airport2)  [1460.000]
2580.009: (load_airplane cargo0 plane0 airport1)  [10.000]
2590.010: (fly_airplane_fast plane0 airport1 airport2)  [1460.000]
4030.009: (unload_airplane cargo2 plane1 airport2)  [10.000]
4050.011: (unload_airplane cargo0 plane0 airport2)  [10.000]

 Execution time of Domain\Problem 14 
00:00:06.2278642

 DOMAIN/PROBLEM 15 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile15.pddl
Number of literals: 100
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
75% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 10.000)b (26.000 | 10.000)b (25.000 | 50.000)b (24.000 | 50.000)b (23.000 | 60.001)b (22.000 | 60.001)b (20.000 | 170.015)b (17.000 | 1557.387)b (16.000 | 1557.387)b (15.000 | 1567.388)b (13.000 | 1567.388)b (12.000 | 1742.446)b (11.000 | 1742.446)b (10.000 | 4502.957)b (9.000 | 4502.957)b (8.000 | 4512.958)b (7.000 | 7173.250)b (6.000 | 7173.250)b (5.000 | 7173.250)b (4.000 | 7173.250)b (3.000 | 7173.250)b (2.000 | 7173.250)b (1.000 | 7175.084);;;; Solution Found
; States evaluated: 7476
; Cost: 7185.085
0.000: (load_airplane cargo4 plane1 airport3)  [10.000]
0.000: (load_airplane cargo0 plane0 airport0)  [10.000]
0.000: (drive_tanker tanker0 refinery3 airport3)  [50.000]
0.000: (drive_tanker tanker1 refinery1 airport0)  [50.000]
0.000: (drive_tanker tanker2 refinery3 airport0)  [50.000]
50.001: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]
50.001: (fill_airplane_tank plane0 tanker1 airport0)  [10.000]
60.002: (drive_tanker tanker0 airport3 airport0)  [50.000]
60.002: (drive_tanker tanker1 airport0 refinery0)  [50.000]
60.002: (fill_airplane_tank plane0 tanker2 airport0)  [10.000]
70.003: (drive_tanker tanker2 airport0 refinery0)  [50.000]
110.003: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
110.003: (fill_tanker tanker1 refinery0)  [11.834]
120.004: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
121.838: (drive_tanker tanker1 refinery0 airport3)  [50.000]
121.838: (fill_tanker tanker2 refinery0)  [13.239]
130.005: (fill_airplane_tank plane0 tanker0 airport0)  [10.000]
135.078: (drive_tanker tanker2 refinery0 airport0)  [50.000]
171.839: (fill_airplane_tank plane1 tanker1 airport3)  [10.000]
181.840: (fly_airplane_fast plane1 airport3 airport4)  [1385.000]
185.079: (fill_airplane_tank plane0 tanker2 airport0)  [10.000]
195.080: (fly_airplane_fast plane0 airport0 airport3)  [1560.000]
1566.841: (unload_airplane cargo4 plane1 airport4)  [10.000]
1576.842: (fly_airplane_fast plane1 airport4 airport1)  [3170.000]
1755.081: (fly_airplane_fast plane0 airport3 airport1)  [2755.000]
4510.082: (unload_airplane cargo0 plane0 airport1)  [10.000]
4520.083: (load_airplane cargo6 plane0 airport1)  [10.000]
4530.084: (fly_airplane_fast plane0 airport1 airport4)  [2645.000]
4746.843: (load_airplane cargo2 plane1 airport1)  [10.000]
4756.844: (fly_airplane_fast plane1 airport1 airport2)  [1450.000]
6206.845: (unload_airplane cargo2 plane1 airport2)  [10.000]
7175.085: (unload_airplane cargo6 plane0 airport4)  [10.000]

 Execution time of Domain\Problem 15 
00:01:25.3856522

 DOMAIN/PROBLEM 16 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile16.pddl
Number of literals: 103
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 10.000)b (37.000 | 20.001)b (36.000 | 30.002)b (35.000 | 30.002)b (34.000 | 30.002)b (33.000 | 50.000)b (32.000 | 50.011)b (31.000 | 60.001)b (30.000 | 60.012)b (29.000 | 60.012)b (27.000 | 60.012)b (22.000 | 2083.731)b (21.000 | 2083.731)b (19.000 | 2083.731)b (18.000 | 3471.502)b (17.000 | 3471.502)b (16.000 | 3471.502)b (15.000 | 3471.502)b (13.000 | 5921.194)b (12.000 | 5921.194)b (11.000 | 5931.195)b (10.000 | 5941.196)b (9.000 | 6056.663)b (8.000 | 6056.663)b (7.000 | 8291.105)b (6.000 | 8291.105)b (5.000 | 8291.105)b (4.000 | 8291.105)b (3.000 | 8291.105)b (2.000 | 9678.876)b (1.000 | 9678.876);;;; Solution Found
; States evaluated: 155
; Cost: 9688.877
0.000: (load_airplane cargo5 plane1 airport3)  [10.000]
0.000: (fill_tanker tanker0 refinery3)  [14.636]
0.000: (fill_tanker tanker1 refinery7)  [18.868]
0.000: (drive_tanker tanker2 refinery0 airport3)  [50.000]
10.001: (load_airplane cargo4 plane1 airport3)  [10.000]
14.637: (drive_tanker tanker0 refinery3 airport3)  [50.000]
18.869: (drive_tanker tanker1 refinery7 airport2)  [50.000]
20.002: (load_airplane cargo3 plane1 airport3)  [10.000]
50.001: (fill_airplane_tank plane1 tanker2 airport3)  [10.000]
64.638: (fill_airplane_tank plane1 tanker0 airport3)  [10.000]
68.870: (fill_airplane_tank plane0 tanker1 airport2)  [10.000]
74.639: (fly_airplane_fast plane1 airport3 airport2)  [1295.000]
78.871: (fly_airplane_fast plane0 airport2 airport4)  [2020.000]
1369.640: (unload_airplane cargo3 plane1 airport2)  [10.000]
1379.641: (load_airplane cargo6 plane1 airport2)  [10.000]
1389.642: (fly_airplane_fast plane1 airport2 airport4)  [2020.000]
2098.872: (fly_airplane_fast plane0 airport4 airport2)  [1385.000]
3409.643: (unload_airplane cargo4 plane1 airport4)  [10.000]
3419.644: (fly_airplane_fast plane1 airport4 airport0)  [2505.000]
3483.873: (fly_airplane_fast plane0 airport2 airport1)  [2580.000]
5924.645: (unload_airplane cargo6 plane1 airport0)  [10.000]
5934.646: (unload_airplane cargo5 plane1 airport0)  [10.000]
5944.647: (load_airplane cargo1 plane1 airport0)  [10.000]
5954.648: (load_airplane cargo0 plane1 airport0)  [10.000]
5964.649: (fly_airplane_fast plane1 airport0 airport1)  [1725.000]
6063.874: (load_airplane cargo2 plane0 airport1)  [10.000]
6073.875: (fly_airplane_fast plane0 airport1 airport4)  [2220.000]
7689.650: (unload_airplane cargo1 plane1 airport1)  [10.000]
7699.651: (unload_airplane cargo0 plane1 airport1)  [10.000]
8293.876: (fly_airplane_fast plane0 airport4 airport2)  [1385.000]
9678.877: (unload_airplane cargo2 plane0 airport2)  [10.000]

 Execution time of Domain\Problem 16 
00:00:02.7596640

 DOMAIN/PROBLEM 17 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile17.pddl
Number of literals: 112
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
73% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000

 Execution time of Domain\Problem 17 
00:30:00.2192941

 DOMAIN/PROBLEM 18 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile18.pddl
Number of literals: 123
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
69% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 43.000
b (42.000 | 10.000)b (41.000 | 20.001)b (40.000 | 30.002)b (39.000 | 50.000)b (37.000 | 50.000)b (36.000 | 100.001)b (35.000 | 100.001)b (33.000 | 100.001)b (32.000 | 110.013)b (31.000 | 120.014)b (29.000 | 120.014)b (24.000 | 3346.088)b (23.000 | 3346.088)b (22.000 | 3346.088)b (21.000 | 3356.089)b (20.000 | 3366.090)b (19.000 | 3376.091)b (18.000 | 3376.091)b (17.000 | 3376.091)b (16.000 | 3376.091)b (15.000 | 4053.864)

 Execution time of Domain\Problem 18 
00:20:49.4587495

 DOMAIN/PROBLEM 19 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile19.pddl
Number of literals: 134
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
71% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 47.000
b (46.000 | 10.000)b (45.000 | 10.000)b (44.000 | 50.000)b (42.000 | 50.000)b (36.000 | 50.011)b (33.000 | 100.001)b (32.000 | 100.001)b (30.000 | 100.001)b (29.000 | 110.013)b (28.000 | 120.014)b (26.000 | 120.014)b (22.000 | 2018.519)b (21.000 | 2030.731)b (20.000 | 4131.300)b (19.000 | 4131.300)b (18.000 | 4141.301)b (17.000 | 4141.301)b (16.000 | 4141.301)b (15.000 | 4141.301)b (14.000 | 4834.518)

 Execution time of Domain\Problem 19 
00:28:40.1757655

 DOMAIN/PROBLEM 20 

File: ..\airplane_cargo\domain.pddl
File: ..\airplane_cargo\pfile20.pddl
Number of literals: 150
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (refinery_tank_level refinery0) are preferable
Have identified that bigger values of (refinery_tank_level refinery1) are preferable
Have identified that bigger values of (refinery_tank_level refinery2) are preferable
Have identified that bigger values of (refinery_tank_level refinery3) are preferable
Have identified that bigger values of (refinery_tank_level refinery4) are preferable
Have identified that bigger values of (refinery_tank_level refinery5) are preferable
Have identified that bigger values of (refinery_tank_level refinery6) are preferable
Have identified that bigger values of (refinery_tank_level refinery7) are preferable
Have identified that bigger values of (refinery_tank_level refinery8) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

73% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 55.000

 Execution time of Domain\Problem 20 
00:30:00.7205812
