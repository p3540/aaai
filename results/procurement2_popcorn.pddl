the results of procurement2 tests using popcorn 

 popcorn 


 DOMAIN/PROBLEM 1 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile01.pddl
Number of literals: 67
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
78% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 18.000
b (17.000 | 5.000)b (15.000 | 11.002)b (14.000 | 11.002)b (13.000 | 16.003)b (11.000 | 22.005)b (10.000 | 22.005)b (8.000 | 23.006)b (7.000 | 23.006)b (6.000 | 28.007)b (5.000 | 29.008)b (3.000 | 29.008)b (2.000 | 34.009)b (1.000 | 35.010);;;; Solution Found
; States evaluated: 21
; Cost: 35.010
0.000: (go_to workshop1 workshop2)  [5.000]
5.001: (go_to workshop2 supplier2)  [5.000]
10.002: (supply_raw_material_p p supplier2)  [1.000]
11.003: (go_to supplier2 workshop2)  [5.000]
16.004: (go_to workshop2 supplier1)  [5.000]
21.005: (supply_raw_material_o o supplier1)  [1.000]
22.006: (supply_raw_material_n n supplier1)  [1.000]
23.007: (go_to supplier1 workshop2)  [5.000]
28.008: (produce_k k n o p workshop2)  [1.000]
29.009: (go_to workshop2 customer0)  [5.000]
34.010: (deliver_to_customer customer0 k)  [1.000]

 Execution time of Domain\Problem 1 
00:00:00.7853605

 DOMAIN/PROBLEM 2 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile02.pddl
Number of literals: 89
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000
b (29.000 | 6.001)b (28.000 | 6.001)b (26.000 | 7.002)b (25.000 | 7.002)b (24.000 | 12.003)b (23.000 | 13.004)b (21.000 | 13.004)b (19.000 | 19.006)b (18.000 | 19.006)b (17.000 | 24.007)b (15.000 | 30.009)b (14.000 | 30.009)b (13.000 | 35.010)b (12.000 | 36.011)b (10.000 | 36.011)b (9.000 | 41.012)b (8.000 | 42.013)b (6.000 | 42.013)b (5.000 | 43.014)b (3.000 | 71.030)b (2.000 | 76.031)b (1.000 | 77.032);;;; Solution Found
; States evaluated: 3140
; Cost: 77.032
0.000: (go_to workshop1 supplier3)  [5.000]
5.001: (supply_raw_material_q q supplier3)  [1.000]
6.002: (supply_raw_material_i i supplier3)  [1.000]
7.003: (go_to supplier3 workshop3)  [5.000]
12.004: (produce_l l q workshop3)  [1.000]
13.005: (go_to workshop3 supplier2)  [5.000]
18.006: (supply_raw_material_s s supplier2)  [1.000]
19.007: (go_to supplier2 workshop3)  [5.000]
24.008: (go_to workshop3 supplier1)  [5.000]
29.009: (supply_raw_material_r r supplier1)  [1.000]
30.010: (go_to supplier1 workshop3)  [5.000]
35.011: (produce_m m r s workshop3)  [1.000]
36.012: (go_to workshop3 workshop1)  [5.000]
41.013: (produce_h h l m workshop1)  [1.000]
42.014: (produce_c c h i workshop1)  [1.000]
43.015: (produce_c c h i workshop1)  [1.000]
44.016: (produce_c c h i workshop1)  [1.000]
45.017: (produce_c c h i workshop1)  [1.000]
46.018: (produce_c c h i workshop1)  [1.000]
47.019: (produce_c c h i workshop1)  [1.000]
48.020: (produce_c c h i workshop1)  [1.000]
49.021: (produce_c c h i workshop1)  [1.000]
50.022: (produce_c c h i workshop1)  [1.000]
51.023: (produce_c c h i workshop1)  [1.000]
52.024: (go_to workshop1 supplier3)  [5.000]
57.025: (supply_raw_material_q q supplier3)  [1.000]
58.026: (go_to supplier3 workshop3)  [5.000]
63.027: (produce_l l q workshop3)  [1.000]
64.028: (go_to workshop3 workshop1)  [5.000]
69.029: (produce_h h l m workshop1)  [1.000]
70.030: (produce_c c h i workshop1)  [1.000]
71.031: (go_to workshop1 customer3)  [5.000]
76.032: (deliver_to_customer customer3 c)  [1.000]

 Execution time of Domain\Problem 2 
00:02:02.6382340

 DOMAIN/PROBLEM 3 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile03.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
91% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 5.000)b (12.000 | 11.002)b (11.000 | 11.002)b (10.000 | 16.003)b (8.000 | 22.005)b (7.000 | 22.005)b (6.000 | 27.006)b (5.000 | 28.007)b (3.000 | 28.007)b (2.000 | 33.008)b (1.000 | 34.009);;;; Solution Found
; States evaluated: 19
; Cost: 34.009
0.000: (go_to workshop2 workshop3)  [5.000]
5.001: (go_to workshop3 supplier3)  [5.000]
10.002: (supply_raw_material_r r supplier3)  [1.000]
11.003: (go_to supplier3 workshop3)  [5.000]
16.004: (go_to workshop3 supplier2)  [5.000]
21.005: (supply_raw_material_s s supplier2)  [1.000]
22.006: (go_to supplier2 workshop3)  [5.000]
27.007: (produce_m m r s workshop3)  [1.000]
28.008: (go_to workshop3 customer4)  [5.000]
33.009: (deliver_to_customer customer4 m)  [1.000]

 Execution time of Domain\Problem 3 
00:00:00.6030766

 DOMAIN/PROBLEM 4 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile04.pddl
Number of literals: 132
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 49.000
b (48.000 | 5.000)b (46.000 | 11.002)b (45.000 | 11.002)b (44.000 | 16.003)b (42.000 | 22.005)b (41.000 | 22.005)b (39.000 | 23.006)b (38.000 | 23.006)b (37.000 | 28.007)b (36.000 | 34.009)b (34.000 | 34.009)b (32.000 | 40.011)b (31.000 | 40.011)b (29.000 | 41.012)b (28.000 | 41.012)b (26.000 | 42.013)b (25.000 | 42.013)b (24.000 | 47.014)b (23.000 | 48.015)b (21.000 | 48.015)b (20.000 | 54.017)b (18.000 | 54.017)b (17.000 | 59.018)b (15.000 | 65.020)b (14.000 | 65.020)b (13.000 | 70.021)b (12.000 | 71.022)b (10.000 | 71.022)b (9.000 | 76.023)b (8.000 | 77.024)b (7.000 | 89.028)b (6.000 | 94.029)b (5.000 | 117.044)

 Execution time of Domain\Problem 4 
00:02:31.5178846

 DOMAIN/PROBLEM 5 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile05.pddl
Number of literals: 153
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
94% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 55.000
b (53.000 | 6.001)b (52.000 | 6.001)b (50.000 | 7.002)b (49.000 | 7.002)b (48.000 | 12.003)b (46.000 | 18.005)b (45.000 | 18.005)b (44.000 | 23.006)b (42.000 | 29.008)b (41.000 | 29.008)b (39.000 | 30.009)b (38.000 | 30.009)b (36.000 | 31.010)b (35.000 | 31.010)b (34.000 | 36.011)b (33.000 | 37.012)b (31.000 | 37.012)b (30.000 | 43.014)b (28.000 | 43.014)b (26.000 | 49.016)b (25.000 | 49.016)b (23.000 | 50.017)b (22.000 | 50.017)b (21.000 | 55.018)b (20.000 | 56.019)b (18.000 | 56.019)b (17.000 | 61.020)b (16.000 | 67.022)b (14.000 | 67.022)b (13.000 | 72.023)b (12.000 | 78.025)b (10.000 | 78.025)b (9.000 | 83.026)b (8.000 | 84.027)b (6.000 | 84.027)b (5.000 | 89.028)b (4.000 | 90.029)b (3.000 | 90.029)b (2.000 | 95.030)b (1.000 | 96.031);;;; Solution Found
; States evaluated: 98
; Cost: 96.031
0.000: (go_to workshop2 supplier5)  [5.000]
5.001: (supply_raw_material_p p supplier5)  [1.000]
6.002: (supply_raw_material_j j supplier5)  [1.000]
7.003: (go_to supplier5 workshop4)  [5.000]
12.004: (go_to workshop4 supplier4)  [5.000]
17.005: (supply_raw_material_o o supplier4)  [1.000]
18.006: (go_to supplier4 workshop4)  [5.000]
23.007: (go_to workshop4 supplier3)  [5.000]
28.008: (supply_raw_material_r r supplier3)  [1.000]
29.009: (supply_raw_material_n n supplier3)  [1.000]
30.010: (supply_raw_material_i i supplier3)  [1.000]
31.011: (go_to supplier3 workshop4)  [5.000]
36.012: (produce_k k n o p workshop4)  [1.000]
37.013: (go_to workshop4 workshop2)  [5.000]
42.014: (produce_f f j k workshop2)  [1.000]
43.015: (go_to workshop2 supplier1)  [5.000]
48.016: (supply_raw_material_s s supplier1)  [1.000]
49.017: (supply_raw_material_q q supplier1)  [1.000]
50.018: (go_to supplier1 workshop4)  [5.000]
55.019: (produce_m m r s workshop4)  [1.000]
56.020: (go_to workshop4 workshop3)  [5.000]
61.021: (go_to workshop3 workshop2)  [5.000]
66.022: (produce_l l q workshop2)  [1.000]
67.023: (go_to workshop2 workshop3)  [5.000]
72.024: (go_to workshop3 workshop1)  [5.000]
77.025: (produce_h h l m workshop1)  [1.000]
78.026: (go_to workshop1 workshop3)  [5.000]
83.027: (produce_c c h i workshop3)  [1.000]
84.028: (go_to workshop3 customer3)  [5.000]
89.029: (deliver_to_customer customer3 c)  [1.000]
90.030: (go_to customer3 customer0)  [5.000]
95.031: (deliver_to_customer customer0 f)  [1.000]

 Execution time of Domain\Problem 5 
00:00:01.7897900

 DOMAIN/PROBLEM 6 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile06.pddl
Number of literals: 175
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
95% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 31.000
b (30.000 | 5.000)b (28.000 | 11.002)b (27.000 | 11.002)b (25.000 | 12.003)b (24.000 | 12.003)b (23.000 | 17.004)b (21.000 | 23.006)b (20.000 | 23.006)b (18.000 | 24.007)b (17.000 | 24.007)b (16.000 | 29.008)b (15.000 | 30.009)b (13.000 | 30.009)b (11.000 | 36.011)b (10.000 | 36.011)b (9.000 | 41.012)b (8.000 | 42.013)b (6.000 | 42.013)b (5.000 | 47.014)b (4.000 | 48.015)b (3.000 | 48.015)b (2.000 | 53.016)b (1.000 | 54.017);;;; Solution Found
; States evaluated: 42
; Cost: 54.017
0.000: (go_to workshop2 workshop5)  [5.000]
5.001: (go_to workshop5 supplier6)  [5.000]
10.002: (supply_raw_material_r r supplier6)  [1.000]
11.003: (supply_raw_material_o o supplier6)  [1.000]
12.004: (go_to supplier6 workshop5)  [5.000]
17.005: (go_to workshop5 supplier1)  [5.000]
22.006: (supply_raw_material_p p supplier1)  [1.000]
23.007: (supply_raw_material_n n supplier1)  [1.000]
24.008: (go_to supplier1 workshop5)  [5.000]
29.009: (produce_k k n o p workshop5)  [1.000]
30.010: (go_to workshop5 supplier0)  [5.000]
35.011: (supply_raw_material_s s supplier0)  [1.000]
36.012: (go_to supplier0 workshop5)  [5.000]
41.013: (produce_m m r s workshop5)  [1.000]
42.014: (go_to workshop5 customer4)  [5.000]
47.015: (deliver_to_customer customer4 k)  [1.000]
48.016: (go_to customer4 customer2)  [5.000]
53.017: (deliver_to_customer customer2 m)  [1.000]

 Execution time of Domain\Problem 6 
00:00:00.8220154

 DOMAIN/PROBLEM 7 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile07.pddl
Number of literals: 196
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
96% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 46.000
b (44.000 | 6.001)b (43.000 | 6.001)b (41.000 | 7.002)b (40.000 | 7.002)b (39.000 | 12.003)b (37.000 | 18.005)b (36.000 | 18.005)b (34.000 | 19.006)b (33.000 | 19.006)b (32.000 | 24.007)b (30.000 | 30.009)b (29.000 | 30.009)b (28.000 | 35.010)b (27.000 | 36.011)b (25.000 | 36.011)b (24.000 | 41.012)b (22.000 | 47.014)b (21.000 | 47.014)b (20.000 | 52.015)b (19.000 | 58.017)b (17.000 | 58.017)b (16.000 | 63.018)b (15.000 | 69.020)b (14.000 | 69.020)b (13.000 | 74.021)b (10.000 | 80.023)b (9.000 | 85.024)b (8.000 | 86.025)b (6.000 | 86.025)b (5.000 | 91.026)b (4.000 | 92.027)b (3.000 | 92.027)b (2.000 | 97.028)b (1.000 | 98.029);;;; Solution Found
; States evaluated: 95
; Cost: 98.029
0.000: (go_to workshop2 supplier8)  [5.000]
5.001: (supply_raw_material_s s supplier8)  [1.000]
6.002: (supply_raw_material_n n supplier8)  [1.000]
7.003: (go_to supplier8 workshop5)  [5.000]
12.004: (go_to workshop5 supplier6)  [5.000]
17.005: (supply_raw_material_q q supplier6)  [1.000]
18.006: (supply_raw_material_p p supplier6)  [1.000]
19.007: (go_to supplier6 workshop5)  [5.000]
24.008: (go_to workshop5 supplier1)  [5.000]
29.009: (supply_raw_material_r r supplier1)  [1.000]
30.010: (go_to supplier1 workshop5)  [5.000]
35.011: (produce_m m r s workshop5)  [1.000]
36.012: (go_to workshop5 workshop4)  [5.000]
41.013: (go_to workshop4 supplier0)  [5.000]
46.014: (supply_raw_material_o o supplier0)  [1.000]
47.015: (go_to supplier0 workshop4)  [5.000]
52.016: (go_to workshop4 workshop0)  [5.000]
57.017: (produce_k k n o p workshop0)  [1.000]
58.018: (go_to workshop0 workshop4)  [5.000]
63.019: (go_to workshop4 customer8)  [5.000]
68.020: (deliver_to_customer customer8 k)  [1.000]
69.021: (go_to customer8 workshop4)  [5.000]
74.022: (go_to workshop4 workshop2)  [5.000]
79.023: (produce_l l q workshop2)  [1.000]
80.024: (go_to workshop2 workshop4)  [5.000]
85.025: (produce_h h l m workshop4)  [1.000]
86.026: (go_to workshop4 customer2)  [5.000]
91.027: (deliver_to_customer customer2 h)  [1.000]
92.028: (go_to customer2 customer1)  [5.000]
97.029: (deliver_to_customer customer1 l)  [1.000]

 Execution time of Domain\Problem 7 
00:00:01.6251166

 DOMAIN/PROBLEM 8 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile08.pddl
Number of literals: 89
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 48.000

 Execution time of Domain\Problem 8 
00:30:00.1706031

 DOMAIN/PROBLEM 9 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile09.pddl
Number of literals: 91
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 71.000

 Execution time of Domain\Problem 9 
00:30:00.1818833

 DOMAIN/PROBLEM 10 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile10.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 52.000

 Execution time of Domain\Problem 10 
00:30:00.1516568

 DOMAIN/PROBLEM 11 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile11.pddl
Number of literals: 113
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 60.000

 Execution time of Domain\Problem 11 
00:30:00.1700398

 DOMAIN/PROBLEM 12 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile12.pddl
Number of literals: 132
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 67.000
b (65.000 | 6.001)b (64.000 | 6.001)b (63.000 | 11.002)b (61.000 | 17.004)b (60.000 | 17.004)b (58.000 | 18.005)b (57.000 | 18.005)b (56.000 | 23.006)b (54.000 | 29.008)b (53.000 | 29.008)b (52.000 | 34.009)b (50.000 | 40.011)b (49.000 | 40.011)b (47.000 | 41.012)b (46.000 | 41.012)b (44.000 | 42.013)b (43.000 | 42.013)b (42.000 | 47.014)b (41.000 | 53.016)b (39.000 | 53.016)b (37.000 | 59.018)b (36.000 | 59.018)b (34.000 | 60.019)b (33.000 | 60.019)b (31.000 | 61.020)b (30.000 | 61.020)b (29.000 | 66.021)b (28.000 | 72.023)b (26.000 | 72.023)b (23.000 | 73.024)b (22.000 | 79.026)b (20.000 | 79.026)b (19.000 | 84.027)b (18.000 | 90.029)b (16.000 | 90.029)b (15.000 | 96.031)b (13.000 | 96.031)b (12.000 | 101.032)b (11.000 | 102.033)b (10.000 | 108.035)b (9.000 | 113.036)b (8.000 | 114.037)b (7.000 | 120.039)b (6.000 | 125.040)b (5.000 | 126.041)

 Execution time of Domain\Problem 12 
00:27:06.1725302

 DOMAIN/PROBLEM 13 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile13.pddl
Number of literals: 87
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
81% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 13 
00:30:00.1810058

 DOMAIN/PROBLEM 14 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile14.pddl
Number of literals: 108
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000

 Execution time of Domain\Problem 14 
00:30:00.1925978

 DOMAIN/PROBLEM 15 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile15.pddl
Number of literals: 129
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 18.000

 Execution time of Domain\Problem 15 
00:30:00.1753882

 DOMAIN/PROBLEM 16 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile16.pddl
Number of literals: 90
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000

 Execution time of Domain\Problem 16 
00:30:00.1681052

 DOMAIN/PROBLEM 17 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile17.pddl
Number of literals: 151
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
92% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 19.000

 Execution time of Domain\Problem 17 
00:30:00.1766946

 DOMAIN/PROBLEM 18 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile18.pddl
Number of literals: 111
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 19.000

 Execution time of Domain\Problem 18 
00:30:00.1641837

 DOMAIN/PROBLEM 19 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile19.pddl
Number of literals: 114
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
93% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 32.000

 Execution time of Domain\Problem 19 
00:30:00.1887866

 DOMAIN/PROBLEM 20 

File: ..\procurement\domain.pddl
File: ..\procurement\pfile20.pddl
Number of literals: 134
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have identified that bigger values of (stock a) are preferable
Have identified that bigger values of (stock b) are preferable
Have identified that bigger values of (stock c) are preferable
Have identified that bigger values of (stock f) are preferable
Have identified that bigger values of (stock h) are preferable
Have identified that bigger values of (stock k) are preferable
Have identified that bigger values of (stock l) are preferable
Have identified that bigger values of (stock m) are preferable
Have identified that bigger values of (stock d) are preferable
Have identified that bigger values of (stock e) are preferable
Have identified that bigger values of (stock g) are preferable
Have identified that bigger values of (stock i) are preferable
Have identified that bigger values of (stock j) are preferable
Have identified that bigger values of (stock n) are preferable
Have identified that bigger values of (stock o) are preferable
Have identified that bigger values of (stock q) are preferable
Have identified that bigger values of (stock r) are preferable
Have identified that bigger values of (stock p) are preferable
Have identified that bigger values of (stock s) are preferable
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
93% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 30.000

 Execution time of Domain\Problem 20 
00:30:00.1870847
