the results of rovers tests in popcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile01.pddl
Number of literals: 16
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 4.000
b (3.000 | 5.000)b (2.000 | 10.001)b (1.000 | 17.002);;;; Solution Found
; States evaluated: 5
; Cost: 32.003
0.000: (navigate rover0 waypoint1 waypoint0)  [5.000]
5.001: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
10.002: (take_image rover0 waypoint0 objective0 camera0 colour)  [7.000]
17.003: (communicate_image_data rover0 general objective0 colour waypoint0 waypoint2)  [15.000]

 Execution time of Domain\Problem 1 
00:00:00.4338816

 DOMAIN/PROBLEM 2 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile02.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
Have a metric tracking fluent: (power_at_station waypoint2)
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000
b (5.000 | 5.000)b (4.000 | 8.001)b (3.000 | 8.001)b (2.000 | 18.002)b (1.000 | 33.003);;;; Solution Found
; States evaluated: 12
; Cost: 33.003
0.000: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
0.001: (sample_rock rover0 rover0store waypoint0)  [8.000]
5.001: (take_image rover0 waypoint0 objective1 camera0 high_res)  [7.000]
5.002: (recharge rover0 waypoint0)  [14.167]
8.002: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint2)  [10.000]
18.003: (communicate_image_data rover0 general objective1 high_res waypoint0 waypoint2)  [15.000]

 Execution time of Domain\Problem 2 
00:00:00.5253538

 DOMAIN/PROBLEM 3 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile03.pddl
Number of literals: 32
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
83% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000
b (7.000 | 5.000)b (6.000 | 12.001)b (5.000 | 17.002)b (4.000 | 32.002)b (3.000 | 32.002)b (2.000 | 32.834)b (1.000 | 32.834);;;; Solution Found
; States evaluated: 15
; Cost: 52.835
0.000: (calibrate rover0 camera1 objective1 waypoint2)  [5.000]
5.001: (take_image rover0 waypoint2 objective1 camera1 high_res)  [7.000]
12.001: (navigate rover0 waypoint2 waypoint1)  [5.000]
17.001: (recharge rover0 waypoint1)  [25.833]
17.002: (communicate_image_data rover0 general objective1 high_res waypoint1 waypoint2)  [15.000]
17.003: (sample_soil rover0 rover0store waypoint1)  [10.000]
42.835: (communicate_soil_data rover0 general waypoint1 waypoint1 waypoint2)  [10.000]

 Execution time of Domain\Problem 3 
00:00:00.5596105

 DOMAIN/PROBLEM 4 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile04.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have a metric tracking fluent: (power_at_station waypoint2)
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
78% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000

 Execution time of Domain\Problem 4 
00:30:00.2182542

 DOMAIN/PROBLEM 5 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile05.pddl
Number of literals: 21
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000

 Execution time of Domain\Problem 5 
00:30:00.2050638

 DOMAIN/PROBLEM 6 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile06.pddl
Number of literals: 33
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 7.000

 Execution time of Domain\Problem 6 
00:30:00.1902068

 DOMAIN/PROBLEM 7 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile07.pddl
Number of literals: 35
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000

 Execution time of Domain\Problem 7 
00:30:00.1809237

 DOMAIN/PROBLEM 8 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile08.pddl
Number of literals: 44
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
74% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000

 Execution time of Domain\Problem 8 
00:30:00.1857824

 DOMAIN/PROBLEM 9 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile09.pddl
Number of literals: 43
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 9 
00:30:00.1800727

 DOMAIN/PROBLEM 10 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile10.pddl
Number of literals: 48
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 10 
00:30:00.1757652

 DOMAIN/PROBLEM 11 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile11.pddl
Number of literals: 39
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000
b (8.000 | 5.000)b (7.000 | 5.000)b (6.000 | 10.001)b (5.000 | 32.002)b (4.000 | 32.002)b (3.000 | 32.002)b (2.000 | 47.003)b (1.000 | 62.004);;;; Solution Found
; States evaluated: 46
; Cost: 62.004
0.000: (navigate rover1 waypoint2 waypoint1)  [5.000]
0.000: (calibrate rover0 camera2 objective1 waypoint1)  [5.000]
5.001: (calibrate rover1 camera0 objective2 waypoint1)  [5.000]
5.001: (take_image rover0 waypoint1 objective2 camera2 low_res)  [7.000]
7.002: (calibrate rover0 camera2 objective1 waypoint1)  [5.000]
10.001: (take_image rover1 waypoint1 objective0 camera0 high_res)  [7.000]
10.002: (recharge rover1 waypoint1)  [7.001]
12.002: (take_image rover0 waypoint1 objective0 camera2 low_res)  [7.000]
17.002: (communicate_image_data rover1 general objective0 high_res waypoint1 waypoint2)  [15.000]
17.004: (recharge rover0 waypoint1)  [30.001]
32.003: (communicate_image_data rover0 general objective2 low_res waypoint1 waypoint2)  [15.000]
47.004: (communicate_image_data rover0 general objective0 low_res waypoint1 waypoint2)  [15.000]

 Execution time of Domain\Problem 11 
00:00:00.9277031

 DOMAIN/PROBLEM 12 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile12.pddl
Number of literals: 65
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000

 Execution time of Domain\Problem 12 
00:30:00.2360878

 DOMAIN/PROBLEM 13 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile13.pddl
Number of literals: 66
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 27.000

 Execution time of Domain\Problem 13 
00:30:00.1899019

 DOMAIN/PROBLEM 14 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile14.pddl
Number of literals: 58
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 5.000)b (13.000 | 10.001)b (12.000 | 17.002)b (11.000 | 32.504)b (10.000 | 34.503)b (9.000 | 65.005)b (7.000 | 65.005)b (6.000 | 65.005)b (5.000 | 65.005)b (4.000 | 75.006)b (3.000 | 88.007)b (2.000 | 88.007)b (1.000 | 103.008);;;; Solution Found
; States evaluated: 170
; Cost: 113.228
0.000: (navigate rover0 waypoint2 waypoint0)  [5.000]
0.000: (navigate rover1 waypoint3 waypoint4)  [5.000]
5.001: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
5.001: (sample_rock rover1 rover1store waypoint4)  [8.000]
10.001: (take_image rover0 waypoint0 objective3 camera0 low_res)  [7.000]
10.002: (recharge rover0 waypoint0)  [35.220]
13.001: (navigate rover1 waypoint4 waypoint3)  [5.000]
13.002: (drop rover1 rover1store)  [1.000]
18.001: (recharge rover1 waypoint3)  [40.556]
45.223: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
45.224: (communicate_image_data rover0 general objective3 low_res waypoint0 waypoint1)  [15.000]
50.223: (take_image rover0 waypoint0 objective2 camera0 low_res)  [7.000]
58.558: (navigate rover1 waypoint3 waypoint0)  [5.000]
60.225: (communicate_image_data rover0 general objective2 low_res waypoint0 waypoint1)  [15.000]
75.226: (communicate_rock_data rover1 general waypoint4 waypoint0 waypoint1)  [10.000]
85.227: (navigate rover1 waypoint0 waypoint1)  [5.000]
90.227: (sample_rock rover1 rover1store waypoint1)  [8.000]
90.228: (recharge rover1 waypoint1)  [7.998]
98.227: (navigate rover1 waypoint1 waypoint0)  [5.000]
103.227: (recharge rover1 waypoint0)  [6.446]
103.228: (communicate_rock_data rover1 general waypoint1 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 14 
00:00:01.5744952

 DOMAIN/PROBLEM 15 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile15.pddl
Number of literals: 102
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
83% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 24.000

 Execution time of Domain\Problem 15 
00:30:00.1805660

 DOMAIN/PROBLEM 16 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile16.pddl
Number of literals: 82
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000

 Execution time of Domain\Problem 16 
00:30:00.1969135

 DOMAIN/PROBLEM 17 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile17.pddl
Number of literals: 102
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 17 
00:30:00.1946647

 DOMAIN/PROBLEM 18 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile18.pddl
Number of literals: 123
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 34.000
b (33.000 | 5.000)b (32.000 | 5.000)b (31.000 | 10.000)b (30.000 | 10.001)b (29.000 | 17.002)b (28.000 | 20.000)b (27.000 | 20.000)b (26.000 | 20.000)alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed

 Execution time of Domain\Problem 18 
00:29:54.9248279

 DOMAIN/PROBLEM 19 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile19.pddl
Number of literals: 139
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 40.000

 Execution time of Domain\Problem 19 
00:30:00.1965779

 DOMAIN/PROBLEM 20 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile20.pddl
Number of literals: 163
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
91% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 47.000
b (46.000 | 5.000)b (45.000 | 5.000)b (44.000 | 5.001)b (43.000 | 12.001)b (42.000 | 12.001)b (41.000 | 15.001)b (40.000 | 15.001)b (39.000 | 15.001)b (38.000 | 15.001)b (37.000 | 16.003)b (36.000 | 16.003)alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed
alloc of 10000 'REAL' failed

 Execution time of Domain\Problem 20 
00:28:38.5687562
