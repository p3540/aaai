Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain02.pddl, problem: ICAPS18tests/pfile02.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 6

.| 5 b| 4 b| 3 b
Queue is empty and no solution was found. FAIL.
Plan before FAIL was: 

(#SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-RECHARGE #SNAP-ACTION:END-RECHARGE
 #SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-TAKE-SAMPLEA #SNAP-ACTION:END-TAKE-SAMPLEA
 #SNAP-ACTION:START-GLIDE_FORWARD)

Method EHC failed to find a solution!.

Trying search method 2: A*

Starting A* Search!----------
Initial heuristic: 6

.| 5 b| 4 b| 3 b| 2 b| 1 b| 0 b
A* found a plan!!

Search method A* found a solution!



Statistics
Total time: 6.366 (s)
Num states explored: 374
Num states queued: 532
Num feasible optimization problems: 4249
Total time solver calls (feasible): 1.0670000488171354d0 (s)
Num infeasible optimization problems: 0
Total time solver calls (infeasible): 0.0d0 (s)
Total time optimization: 4.7700002181809396d0 (s)
Solver: gurobi 7.0.2
Exploring state feasible actions when no descenent: T
Exploring all feasible actions: NIL


Short plan:
Grounded-Plan with 14 steps:
   0:   0.0000 (s) TURN_SOLAR_PANELS_ON [end:   5.4000]
   2:   5.4100 (s) REFUEL_TANK     [end:  10.8100]
   4:  10.8200 (s) RECHARGE        [end:  14.0200]
   6:  14.0300 (s) GLIDE_FORWARD   [end:  20.0300]
   8:  20.0400 (s) TAKE-SAMPLEA    [end:  22.0400]
  10:  22.0500 (s) GLIDE_FORWARD   [end:  37.0500]
  12:  37.0600 (s) TAKE-SAMPLEB    [end:  39.0600]
Objective: 39.0600  Makespan: 39.0600


Long plan:
Grounded-Plan with 14 steps:
 i   t              Duration             Event                Variables (bf event)
   0   0.0000 (s)   [  5.4000 (s) :  1]  START-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=20.000, FUEL=20.000
   1   5.4000 (s)   [  0.0000 (s) :  0]    END-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=128.000, FUEL=20.000
   2   5.4100 (s)   [  5.4000 (s) :  3]  START-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=128.000, FUEL=20.000
   3  10.8100 (s)   [  0.0000 (s) :  2]    END-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=128.000, FUEL=128.000
   4  10.8200 (s)   [  3.2000 (s) :  5]  START-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=128.000, FUEL=128.000
   5  14.0200 (s)   [  0.0000 (s) :  4]    END-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=84.000, SOLAR-POWER=0.000, FUEL=0.000
   6  14.0300 (s)   [  6.0000 (s) :  7]  START-GLIDE_FORWARD   VARS: X=0.000, FUEL-CELL-BATTERY=84.000, SOLAR-POWER=0.000, FUEL=0.000
   7  20.0300 (s)   [  0.0000 (s) :  6]    END-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
   8  20.0400 (s)   [  2.0000 (s) :  9]  START-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
   9  22.0400 (s)   [  0.0000 (s) :  8]    END-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  10  22.0500 (s)   [ 15.0000 (s) : 11]  START-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  11  37.0500 (s)   [  0.0000 (s) : 10]    END-GLIDE_FORWARD   VARS: X=42.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  12  37.0600 (s)   [  2.0000 (s) : 13]  START-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  13  39.0600 (s)   [  0.0000 (s) : 12]    END-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
Objective: 39.0600  Makespan: 39.0600

Results saved to scotty_results.json
