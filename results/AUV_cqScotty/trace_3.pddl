Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain03.pddl, problem: ICAPS18tests/pfile03.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 8

.| 7 b| 6 b| 5 b
Queue is empty and no solution was found. FAIL.
Plan before FAIL was: 

(#SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-RECHARGE #SNAP-ACTION:END-RECHARGE
 #SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-TAKE-SAMPLEA #SNAP-ACTION:END-TAKE-SAMPLEA
 #SNAP-ACTION:START-GLIDE_FORWARD)

Method EHC failed to find a solution!.

Trying search method 2: A*

Starting A* Search!----------
Initial heuristic: 8

.| 7 b| 6 b| 5 b| 4 b| 3 b| 2 b| 1 b| 0 b
A* found a plan!!

Search method A* found a solution!



Statistics
Total time: 45.37 (s)
Num states explored: 2088
Num states queued: 2779
Num feasible optimization problems: 22225
Total time solver calls (feasible): 7.147000336321071d0 (s)
Num infeasible optimization problems: 0
Total time solver calls (infeasible): 0.0d0 (s)
Total time optimization: 34.962001533363946d0 (s)
Solver: gurobi 7.0.2
Exploring state feasible actions when no descenent: T
Exploring all feasible actions: NIL


Short plan:
Grounded-Plan with 22 steps:
   0:   0.0000 (s) TURN_SOLAR_PANELS_ON [end:  10.0000]
   2:  10.0100 (s) TURN_SOLAR_PANELS_ON [end:  11.4100]
   4:  11.4200 (s) REFUEL_TANK     [end:  21.4200]
   6:  21.4300 (s) REFUEL_TANK     [end:  22.8300]
   8:  22.8400 (s) RECHARGE        [end:  29.0400]
  10:  29.0500 (s) GLIDE_FORWARD   [end:  35.0500]
  12:  35.0600 (s) TAKE-SAMPLEA    [end:  37.0600]
  14:  37.0700 (s) GLIDE_FORWARD   [end:  52.0700]
  16:  52.0800 (s) TAKE-SAMPLEB    [end:  54.0800]
  18:  54.0900 (s) GLIDE_FORWARD   [end:  69.0900]
  20:  69.1000 (s) TAKE-SAMPLEC    [end:  71.1000]
Objective: 71.1000  Makespan: 71.1000


Long plan:
Grounded-Plan with 22 steps:
 i   t              Duration             Event                Variables (bf event)
   0   0.0000 (s)   [ 10.0000 (s) :  1]  START-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=20.000, FUEL=20.000
   1  10.0000 (s)   [  0.0000 (s) :  0]    END-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=220.000, FUEL=20.000
   2  10.0100 (s)   [  1.4000 (s) :  3]  START-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=220.000, FUEL=20.000
   3  11.4100 (s)   [  0.0000 (s) :  2]    END-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=20.000
   4  11.4200 (s)   [ 10.0000 (s) :  5]  START-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=20.000
   5  21.4200 (s)   [  0.0000 (s) :  4]    END-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=220.000
   6  21.4300 (s)   [  1.4000 (s) :  7]  START-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=220.000
   7  22.8300 (s)   [  0.0000 (s) :  6]    END-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=248.000
   8  22.8400 (s)   [  6.2000 (s) :  9]  START-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=248.000, FUEL=248.000
   9  29.0400 (s)   [  0.0000 (s) :  8]    END-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=144.000, SOLAR-POWER=0.000, FUEL=0.000
  10  29.0500 (s)   [  6.0000 (s) : 11]  START-GLIDE_FORWARD   VARS: X=0.000, FUEL-CELL-BATTERY=144.000, SOLAR-POWER=0.000, FUEL=0.000
  11  35.0500 (s)   [  0.0000 (s) : 10]    END-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=120.000, SOLAR-POWER=0.000, FUEL=0.000
  12  35.0600 (s)   [  2.0000 (s) : 13]  START-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=120.000, SOLAR-POWER=0.000, FUEL=0.000
  13  37.0600 (s)   [  0.0000 (s) : 12]    END-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=120.000, SOLAR-POWER=0.000, FUEL=0.000
  14  37.0700 (s)   [ 15.0000 (s) : 15]  START-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=120.000, SOLAR-POWER=0.000, FUEL=0.000
  15  52.0700 (s)   [  0.0000 (s) : 14]    END-GLIDE_FORWARD   VARS: X=42.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  16  52.0800 (s)   [  2.0000 (s) : 17]  START-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  17  54.0800 (s)   [  0.0000 (s) : 16]    END-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  18  54.0900 (s)   [ 15.0000 (s) : 19]  START-GLIDE_FORWARD   VARS: X=42.000, FUEL-CELL-BATTERY=60.000, SOLAR-POWER=0.000, FUEL=0.000
  19  69.0900 (s)   [  0.0000 (s) : 18]    END-GLIDE_FORWARD   VARS: X=72.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  20  69.1000 (s)   [  2.0000 (s) : 21]  START-TAKE-SAMPLEC    VARS: X=72.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  21  71.1000 (s)   [  0.0000 (s) : 20]    END-TAKE-SAMPLEC    VARS: X=72.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
Objective: 71.1000  Makespan: 71.1000

Results saved to scotty_results.json
