Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain06.pddl, problem: ICAPS18tests/pfile06.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 14

.| 13 b| 12 b| 11 b
Queue is empty and no solution was found. FAIL.
Plan before FAIL was: 

(#SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-RECHARGE #SNAP-ACTION:END-RECHARGE
 #SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-TAKE-SAMPLEA #SNAP-ACTION:END-TAKE-SAMPLEA
 #SNAP-ACTION:START-GLIDE_FORWARD)

Method EHC failed to find a solution!.

Trying search method 2: A*

Starting A* Search!----------
Initial heuristic: 14

.| 13 b| 12 b| 11 b| 10 b| 9 b| 8 b| 7 b| 6 b| 5 b| 4 bWelcome to LDB, a low-level debugger for the Lisp runtime environment.
ldb> 