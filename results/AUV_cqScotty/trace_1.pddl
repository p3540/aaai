Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain01.pddl, problem: ICAPS18tests/pfile01.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 4

.| 3 b| 2 b| 1 b
Goal found!!!

Search method EHC found a solution!



Statistics
Total time: 0.412 (s)
Num states explored: 4
Num states queued: 4
Num feasible optimization problems: 33
Total time solver calls (feasible): 0.006000000284984708d0 (s)
Num infeasible optimization problems: 0
Total time solver calls (infeasible): 0.0d0 (s)
Total time optimization: 0.04200000106357038d0 (s)
Solver: gurobi 7.0.2
Exploring state feasible actions when no descenent: T
Exploring all feasible actions: NIL


Short plan:
Grounded-Plan with 4 steps:
   0:   0.0000 (s) GLIDE_FORWARD   [end:   4.0000]
   2:   4.0100 (s) TAKE-SAMPLEA    [end:   6.0100]
Objective: 6.0100  Makespan: 6.0100


Long plan:
Grounded-Plan with 4 steps:
 i   t              Duration             Event                Variables (bf event)
   0   0.0000 (s)   [  4.0000 (s) :  1]  START-GLIDE_FORWARD   VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=20.000, FUEL=20.000
   1   4.0000 (s)   [  0.0000 (s) :  0]    END-GLIDE_FORWARD   VARS: X=8.000, FUEL-CELL-BATTERY=4.000, SOLAR-POWER=20.000, FUEL=20.000
   2   4.0100 (s)   [  2.0000 (s) :  3]  START-TAKE-SAMPLEA    VARS: X=8.000, FUEL-CELL-BATTERY=4.000, SOLAR-POWER=20.000, FUEL=20.000
   3   6.0100 (s)   [  0.0000 (s) :  2]    END-TAKE-SAMPLEA    VARS: X=8.000, FUEL-CELL-BATTERY=4.000, SOLAR-POWER=20.000, FUEL=20.000
Objective: 6.0100  Makespan: 6.0100

Results saved to scotty_results.json
