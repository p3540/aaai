Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain12.pddl, problem: ICAPS18tests/pfile12.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 26

.| 25 b| 24 b| 23 b| 22 b| 21 b
Queue is empty and no solution was found. FAIL.
Plan before FAIL was: 

(#SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-TAKE-SAMPLEI #SNAP-ACTION:END-TAKE-SAMPLEI
 #SNAP-ACTION:START-TAKE-SAMPLEH #SNAP-ACTION:END-TAKE-SAMPLEH
 #SNAP-ACTION:START-GLIDE_FORWARD)

Method EHC failed to find a solution!.

Trying search method 2: A*

Starting A* Search!----------
Initial heuristic: 26

.| 25 b| 24 b| 23 b| 22 b| 21 b| 20 b| 19 b| 18 b| 17 b| 16 b| 15 bWelcome to LDB, a low-level debugger for the Lisp runtime environment.
ldb> 