Convex-quadratic Scotty (cqScotty) Hybrid Planner 2016
2016-11-14 20:38:28 -0500, c9baa48e11778efcb11234f8ec56f7784493b41b
Enrique Fernandez
MERS Lab - MIT CSAIL 
Scotty will plan with domain: ICAPS18tests/domain04.pddl, problem: ICAPS18tests/pfile04.pddl and search method: EHC-ASTAR.
Loading domain file...
Loading problem file...
Starting to plan...
Preprocessing actions....
Looking for threats...
Preprocessing completed!
Settings:
 Selected solver: GUROBI-SOLVER
 Explore feasible if no descendents: T
 Explore all feasible actions: NIL
 Debugging search: NIL


Trying search method 1: EHC

Starting Enforced Hill Climbing Search!----------
Initial heuristic: 10

.| 9 b| 8 b| 7 b
Queue is empty and no solution was found. FAIL.
Plan before FAIL was: 

(#SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-RECHARGE #SNAP-ACTION:END-RECHARGE
 #SNAP-ACTION:START-GLIDE_FORWARD #SNAP-ACTION:END-GLIDE_FORWARD
 #SNAP-ACTION:START-TAKE-SAMPLEA #SNAP-ACTION:END-TAKE-SAMPLEA
 #SNAP-ACTION:START-GLIDE_FORWARD)

Method EHC failed to find a solution!.

Trying search method 2: A*

Starting A* Search!----------
Initial heuristic: 10

.| 9 b| 8 b| 7 b| 6 b| 5 b| 4 b| 3 b| 2 b| 1 b| 0 b
A* found a plan!!

Search method A* found a solution!



Statistics
Total time: 96.842 (s)
Num states explored: 3661
Num states queued: 4971
Num feasible optimization problems: 39761
Total time solver calls (feasible): 14.271000675973482d0 (s)
Num infeasible optimization problems: 0
Total time solver calls (infeasible): 0.0d0 (s)
Total time optimization: 72.99700296903029d0 (s)
Solver: gurobi 7.0.2
Exploring state feasible actions when no descenent: T
Exploring all feasible actions: NIL


Short plan:
Grounded-Plan with 26 steps:
   0:   0.0000 (s) TURN_SOLAR_PANELS_ON [end:  10.0000]
   2:  10.0100 (s) TURN_SOLAR_PANELS_ON [end:  15.4100]
   4:  15.4200 (s) REFUEL_TANK     [end:  25.4200]
   6:  25.4300 (s) REFUEL_TANK     [end:  30.8300]
   8:  30.8400 (s) RECHARGE        [end:  39.0400]
  10:  39.0500 (s) GLIDE_FORWARD   [end:  45.0500]
  12:  45.0600 (s) TAKE-SAMPLEA    [end:  47.0600]
  14:  47.0700 (s) GLIDE_FORWARD   [end:  62.0700]
  16:  62.0800 (s) TAKE-SAMPLEB    [end:  64.0800]
  18:  64.0900 (s) GLIDE_FORWARD   [end:  79.0900]
  20:  79.1000 (s) TAKE-SAMPLEC    [end:  81.1000]
  22:  81.1100 (s) GLIDE_FORWARD   [end:  91.1100]
  24:  91.1200 (s) TAKE-SAMPLED    [end:  93.1200]
Objective: 93.1200  Makespan: 93.1200


Long plan:
Grounded-Plan with 26 steps:
 i   t              Duration             Event                Variables (bf event)
   0   0.0000 (s)   [ 10.0000 (s) :  1]  START-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=20.000, FUEL=20.000
   1  10.0000 (s)   [  0.0000 (s) :  0]    END-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=220.000, FUEL=20.000
   2  10.0100 (s)   [  5.4000 (s) :  3]  START-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=220.000, FUEL=20.000
   3  15.4100 (s)   [  0.0000 (s) :  2]    END-TURN_SOLAR_PANELS_ON VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=20.000
   4  15.4200 (s)   [ 10.0000 (s) :  5]  START-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=20.000
   5  25.4200 (s)   [  0.0000 (s) :  4]    END-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=220.000
   6  25.4300 (s)   [  5.4000 (s) :  7]  START-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=220.000
   7  30.8300 (s)   [  0.0000 (s) :  6]    END-REFUEL_TANK     VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=328.000
   8  30.8400 (s)   [  8.2000 (s) :  9]  START-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=20.000, SOLAR-POWER=328.000, FUEL=328.000
   9  39.0400 (s)   [  0.0000 (s) :  8]    END-RECHARGE        VARS: X=0.000, FUEL-CELL-BATTERY=184.000, SOLAR-POWER=0.000, FUEL=0.000
  10  39.0500 (s)   [  6.0000 (s) : 11]  START-GLIDE_FORWARD   VARS: X=0.000, FUEL-CELL-BATTERY=184.000, SOLAR-POWER=0.000, FUEL=0.000
  11  45.0500 (s)   [  0.0000 (s) : 10]    END-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=160.000, SOLAR-POWER=0.000, FUEL=0.000
  12  45.0600 (s)   [  2.0000 (s) : 13]  START-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=160.000, SOLAR-POWER=0.000, FUEL=0.000
  13  47.0600 (s)   [  0.0000 (s) : 12]    END-TAKE-SAMPLEA    VARS: X=12.000, FUEL-CELL-BATTERY=160.000, SOLAR-POWER=0.000, FUEL=0.000
  14  47.0700 (s)   [ 15.0000 (s) : 15]  START-GLIDE_FORWARD   VARS: X=12.000, FUEL-CELL-BATTERY=160.000, SOLAR-POWER=0.000, FUEL=0.000
  15  62.0700 (s)   [  0.0000 (s) : 14]    END-GLIDE_FORWARD   VARS: X=42.000, FUEL-CELL-BATTERY=100.000, SOLAR-POWER=0.000, FUEL=0.000
  16  62.0800 (s)   [  2.0000 (s) : 17]  START-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=100.000, SOLAR-POWER=0.000, FUEL=0.000
  17  64.0800 (s)   [  0.0000 (s) : 16]    END-TAKE-SAMPLEB    VARS: X=42.000, FUEL-CELL-BATTERY=100.000, SOLAR-POWER=0.000, FUEL=0.000
  18  64.0900 (s)   [ 15.0000 (s) : 19]  START-GLIDE_FORWARD   VARS: X=42.000, FUEL-CELL-BATTERY=100.000, SOLAR-POWER=0.000, FUEL=0.000
  19  79.0900 (s)   [  0.0000 (s) : 18]    END-GLIDE_FORWARD   VARS: X=72.000, FUEL-CELL-BATTERY=40.000, SOLAR-POWER=0.000, FUEL=0.000
  20  79.1000 (s)   [  2.0000 (s) : 21]  START-TAKE-SAMPLEC    VARS: X=72.000, FUEL-CELL-BATTERY=40.000, SOLAR-POWER=0.000, FUEL=0.000
  21  81.1000 (s)   [  0.0000 (s) : 20]    END-TAKE-SAMPLEC    VARS: X=72.000, FUEL-CELL-BATTERY=40.000, SOLAR-POWER=0.000, FUEL=0.000
  22  81.1100 (s)   [ 10.0000 (s) : 23]  START-GLIDE_FORWARD   VARS: X=72.000, FUEL-CELL-BATTERY=40.000, SOLAR-POWER=0.000, FUEL=0.000
  23  91.1100 (s)   [  0.0000 (s) : 22]    END-GLIDE_FORWARD   VARS: X=92.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  24  91.1200 (s)   [  2.0000 (s) : 25]  START-TAKE-SAMPLED    VARS: X=92.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
  25  93.1200 (s)   [  0.0000 (s) : 24]    END-TAKE-SAMPLED    VARS: X=92.000, FUEL-CELL-BATTERY=0.000, SOLAR-POWER=0.000, FUEL=0.000
Objective: 93.1200  Makespan: 93.1200

Results saved to scotty_results.json
