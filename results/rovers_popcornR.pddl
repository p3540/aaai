the results of rovers tests in POPF 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile01.pddl
Number of literals: 16
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 4.000
b (3.000 | 5.000)b (2.000 | 10.001)b (1.000 | 17.002);;;; Solution Found
; States evaluated: 5
; Cost: 32.003
0.000: (navigate rover0 waypoint1 waypoint0)  [5.000]
5.001: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
10.002: (take_image rover0 waypoint0 objective0 camera0 colour)  [7.000]
17.003: (communicate_image_data rover0 general objective0 colour waypoint0 waypoint2)  [15.000]

 Execution time of Domain\Problem 1 
00:00:00.4433917

 DOMAIN/PROBLEM 2 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile02.pddl
Number of literals: 29
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%] [120%] [130%]
Have a metric tracking fluent: (power_at_station waypoint2)
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

82% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000
b (5.000 | 5.000)b (4.000 | 8.001)b (3.000 | 8.001)b (2.000 | 18.002)b (1.000 | 33.003);;;; Solution Found
; States evaluated: 12
; Cost: 33.003
0.000: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
0.001: (sample_rock rover0 rover0store waypoint0)  [8.000]
5.001: (take_image rover0 waypoint0 objective1 camera0 high_res)  [7.000]
5.002: (recharge rover0 waypoint0)  [20.000]  (?recharge_amount [0.000,36.000])
8.002: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint2)  [10.000]
18.003: (communicate_image_data rover0 general objective1 high_res waypoint0 waypoint2)  [15.000]

 Execution time of Domain\Problem 2 
00:00:00.5319274

 DOMAIN/PROBLEM 3 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile03.pddl
Number of literals: 32
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

83% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000
b (7.000 | 5.000)b (6.000 | 12.001)b (5.000 | 17.002)b (4.000 | 32.002)b (3.000 | 32.002)b (2.000 | 32.834)b (1.000 | 32.834);;;; Solution Found
; States evaluated: 15
; Cost: 68.669
0.000: (calibrate rover0 camera1 objective1 waypoint2)  [5.000]
5.001: (take_image rover0 waypoint2 objective1 camera1 high_res)  [7.000]
12.001: (navigate rover0 waypoint2 waypoint1)  [5.000]
17.001: (recharge rover0 waypoint1)  [41.667]  (?recharge_amount [0.000,50.000])
17.002: (communicate_image_data rover0 general objective1 high_res waypoint1 waypoint2)  [15.000]
17.003: (sample_soil rover0 rover0store waypoint1)  [10.000]
58.669: (communicate_soil_data rover0 general waypoint1 waypoint1 waypoint2)  [10.000]

 Execution time of Domain\Problem 3 
00:00:00.5141922

 DOMAIN/PROBLEM 4 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile04.pddl
Number of literals: 27
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Have a metric tracking fluent: (power_at_station waypoint2)
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

78% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000
b (8.000 | 5.000)b (7.000 | 12.002)b (6.000 | 19.002)b (5.000 | 43.672)b (4.000 | 43.672)b (3.000 | 67.230)b (2.000 | 67.230)b (1.000 | 93.199);;;; Solution Found
; States evaluated: 163
; Cost: 120.327
0.000: (calibrate rover0 camera1 objective0 waypoint0)  [5.000]
5.001: (take_image rover0 waypoint0 objective1 camera1 high_res)  [7.000]
7.002: (calibrate rover0 camera1 objective0 waypoint0)  [5.000]
12.002: (take_image rover0 waypoint0 objective1 camera1 colour)  [7.000]
12.003: (recharge rover0 waypoint0)  [1.667]  (?recharge_amount [0.000,17.000])
13.671: (turn_on_solar_panel rover0 waypoint0)  [34.653]  (?illumination [0.000,35.000])
13.671: (communicate_image_data rover0 general objective1 high_res waypoint0 waypoint1)  [15.000]
13.672: (recharge rover0 waypoint0)  [25.654]  (?recharge_amount [0.000,32.436])
33.323: (communicate_image_data rover0 general objective1 colour waypoint0 waypoint1)  [15.000]
48.324: (navigate rover0 waypoint0 waypoint1)  [5.000]
53.324: (recharge rover0 waypoint1)  [0.000]  (?recharge_amount [0.000,10.000])
53.325: (turn_on_solar_panel rover0 waypoint1)  [24.823]  (?illumination [0.000,35.000])
53.326: (recharge rover0 waypoint1)  [19.346]  (?recharge_amount [0.000,23.215])
72.673: (sample_rock rover0 rover0store waypoint1)  [8.000]
80.673: (navigate rover0 waypoint1 waypoint0)  [5.000]
85.673: (turn_on_solar_panel rover0 waypoint0)  [34.653]  (?illumination [0.000,35.000])
85.674: (recharge rover0 waypoint0)  [20.628]  (?recharge_amount [0.000,24.753])
85.675: (communicate_rock_data rover0 general waypoint1 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 4 
00:00:02.6599592

 DOMAIN/PROBLEM 5 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile05.pddl
Number of literals: 21
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%] [110%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 6.000
b (5.000 | 5.000)b (4.000 | 10.001)b (3.000 | 24.003)b (2.000 | 24.003)b (1.000 | 51.173);;;; Solution Found
; States evaluated: 160
; Cost: 65.648
0.000: (navigate rover0 waypoint2 waypoint0)  [5.000]
5.001: (turn_on_solar_panel rover0 waypoint0)  [24.648]  (?illumination [0.000,35.000])
5.001: (calibrate rover0 camera0 objective0 waypoint0)  [5.000]
10.002: (take_image rover0 waypoint0 objective0 camera0 high_res)  [7.000]
12.003: (calibrate rover0 camera0 objective0 waypoint0)  [5.000]
12.004: (recharge rover0 waypoint0)  [17.645]  (?recharge_amount [0.000,19.765])
17.003: (take_image rover0 waypoint0 objective1 camera0 colour)  [7.000]
29.650: (turn_on_solar_panel rover0 waypoint0)  [24.648]  (?illumination [0.000,35.000])
29.650: (communicate_image_data rover0 general objective0 high_res waypoint0 waypoint1)  [15.000]
29.651: (recharge rover0 waypoint0)  [20.998]  (?recharge_amount [0.000,25.197])
50.648: (communicate_image_data rover0 general objective1 colour waypoint0 waypoint1)  [15.000]

 Execution time of Domain\Problem 5 
00:00:01.8271978

 DOMAIN/PROBLEM 6 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile06.pddl
Number of literals: 33
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 7.000
b (5.000 | 5.000)b (4.000 | 10.001)b (3.000 | 12.001)b (2.000 | 12.001)b (1.000 | 35.003);;;; Solution Found
; States evaluated: 27
; Cost: 35.003
0.000: (calibrate rover0 camera0 objective1 waypoint1)  [5.000]
0.000: (turn_on_solar_panel rover0 waypoint1)  [27.344]  (?illumination [0.000,35.000])
0.001: (sample_soil rover0 rover0store waypoint1)  [10.000]
5.001: (take_image rover0 waypoint1 objective0 camera0 low_res)  [7.000]
10.002: (communicate_soil_data rover0 general waypoint1 waypoint1 waypoint0)  [10.000]
10.003: (recharge rover0 waypoint1)  [22.413]  (?recharge_amount [0.000,26.896])
20.003: (communicate_image_data rover0 general objective0 low_res waypoint1 waypoint0)  [15.000]

 Execution time of Domain\Problem 6 
00:00:00.6285681

 DOMAIN/PROBLEM 7 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile07.pddl
Number of literals: 35
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

80% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000

 Execution time of Domain\Problem 7 
00:30:00.1564866

 DOMAIN/PROBLEM 8 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile08.pddl
Number of literals: 44
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

74% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 8.000

 Execution time of Domain\Problem 8 
00:30:00.1847640

 DOMAIN/PROBLEM 9 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile09.pddl
Number of literals: 43
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

76% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 9 
00:30:00.1830074

 DOMAIN/PROBLEM 10 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile10.pddl
Number of literals: 48
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 10 
00:30:00.2039596

 DOMAIN/PROBLEM 11 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile11.pddl
Number of literals: 39
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

86% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 9.000
b (8.000 | 5.000)b (7.000 | 5.000)b (6.000 | 10.001)b (5.000 | 32.002)b (4.000 | 32.002)b (3.000 | 32.002)b (2.000 | 47.003)b (1.000 | 62.004);;;; Solution Found
; States evaluated: 46
; Cost: 62.004
0.000: (navigate rover1 waypoint2 waypoint1)  [5.000]
0.000: (calibrate rover0 camera2 objective1 waypoint1)  [5.000]
5.001: (calibrate rover1 camera0 objective2 waypoint1)  [5.000]
5.001: (take_image rover0 waypoint1 objective2 camera2 low_res)  [7.000]
7.002: (calibrate rover0 camera2 objective1 waypoint1)  [5.000]
10.001: (take_image rover1 waypoint1 objective0 camera0 high_res)  [7.000]
10.002: (recharge rover1 waypoint1)  [7.001]  (?recharge_amount [0.000,31.000])
12.002: (take_image rover0 waypoint1 objective0 camera2 low_res)  [7.000]
17.002: (communicate_image_data rover1 general objective0 high_res waypoint1 waypoint2)  [15.000]
17.004: (recharge rover0 waypoint1)  [30.001]  (?recharge_amount [0.000,28.000])
32.003: (communicate_image_data rover0 general objective2 low_res waypoint1 waypoint2)  [15.000]
47.004: (communicate_image_data rover0 general objective0 low_res waypoint1 waypoint2)  [15.000]

 Execution time of Domain\Problem 11 
00:00:00.7906096

 DOMAIN/PROBLEM 12 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile12.pddl
Number of literals: 65
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 13.000
b (12.000 | 5.000)b (11.000 | 10.001)b (10.000 | 17.002)b (9.000 | 17.002)b (8.000 | 32.003)b (7.000 | 52.006)b (5.000 | 71.888)b (4.000 | 95.888)b (3.000 | 95.888)b (2.000 | 95.888)b (1.000 | 118.390);;;; Solution Found
; States evaluated: 359
; Cost: 138.302
0.000: (navigate rover0 waypoint2 waypoint0)  [5.000]
0.000: (navigate rover1 waypoint4 waypoint0)  [5.000]
5.001: (turn_on_solar_panel rover0 waypoint0)  [29.915]  (?illumination [0.000,35.000])
5.001: (calibrate rover0 camera0 objective0 waypoint0)  [5.000]
5.001: (calibrate rover1 camera1 objective0 waypoint0)  [5.000]
5.002: (recharge rover0 waypoint0)  [18.286]  (?recharge_amount [0.000,21.943])
10.002: (take_image rover1 waypoint0 objective0 camera1 colour)  [7.000]
17.003: (communicate_image_data rover1 general objective0 colour waypoint0 waypoint4)  [15.000]
23.289: (take_image rover0 waypoint0 objective2 camera0 high_res)  [7.000]
32.004: (communicate_image_data rover0 general objective2 high_res waypoint0 waypoint4)  [15.000]
34.917: (turn_on_solar_panel rover1 waypoint0)  [29.915]  (?illumination [0.000,35.000])
47.005: (navigate rover0 waypoint0 waypoint4)  [5.000]
52.005: (recharge rover0 waypoint4)  [27.778]  (?recharge_amount [0.000,50.000])
79.784: (sample_rock rover0 rover0store waypoint4)  [8.000]
87.784: (navigate rover0 waypoint4 waypoint0)  [5.000]
87.785: (drop rover0 rover0store)  [1.000]
92.784: (recharge rover0 waypoint0)  [15.603]  (?recharge_amount [0.000,32.829])
92.785: (communicate_rock_data rover0 general waypoint4 waypoint0 waypoint4)  [10.000]
92.786: (sample_rock rover0 rover0store waypoint0)  [8.000]
108.388: (turn_on_solar_panel rover0 waypoint0)  [29.915]  (?illumination [0.000,35.000])
108.389: (recharge rover0 waypoint0)  [17.635]  (?recharge_amount [0.000,21.162])
108.390: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint4)  [10.000]

 Execution time of Domain\Problem 12 
00:00:07.0546370

 DOMAIN/PROBLEM 13 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile13.pddl
Number of literals: 66
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

87% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 27.000
b (26.000 | 5.000)b (25.000 | 10.002)b (24.000 | 17.002)b (23.000 | 17.002)b (22.000 | 19.002)b (21.000 | 19.002)b (20.000 | 25.002)b (19.000 | 25.002)b (18.000 | 26.338)b (17.000 | 26.338)b (16.000 | 35.004)b (15.000 | 35.004)b (14.000 | 55.958)b (13.000 | 71.162)b (12.000 | 81.488)b (11.000 | 108.442)b (10.000 | 125.992)b (9.000 | 129.490)b (8.000 | 155.057)b (7.000 | 155.059)b (6.000 | 177.466)b (5.000 | 177.466)b (4.000 | 191.938)b (3.000 | 220.226)b (2.000 | 220.226)b (1.000 | 244.894);;;; Solution Found
; States evaluated: 22971
; Cost: 254.982
0.000: (calibrate rover1 camera2 objective0 waypoint2)  [5.000]
0.000: (navigate rover0 waypoint4 waypoint0)  [5.000]
5.001: (recharge rover0 waypoint0)  [10.000]  (?recharge_amount [0.000,24.000])
5.001: (take_image rover1 waypoint2 objective2 camera2 low_res)  [7.000]
5.002: (calibrate rover0 camera1 objective0 waypoint0)  [5.000]
7.002: (calibrate rover1 camera2 objective0 waypoint2)  [5.000]
7.003: (recharge rover1 waypoint2)  [8.333]  (?recharge_amount [0.000,25.000])
10.002: (take_image rover0 waypoint0 objective0 camera1 colour)  [7.000]
12.002: (take_image rover1 waypoint2 objective0 camera2 low_res)  [7.000]
15.002: (sample_soil rover0 rover0store waypoint0)  [10.000]
15.002: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
15.003: (recharge rover0 waypoint0)  [10.000]  (?recharge_amount [0.000,16.821])
15.337: (sample_soil rover1 rover1store waypoint2)  [10.000]
15.337: (turn_on_solar_panel rover1 waypoint2)  [18.519]  (?illumination [0.000,35.000])
15.338: (recharge rover1 waypoint2)  [15.844]  (?recharge_amount [0.000,19.012])
25.338: (drop rover1 rover1store)  [1.000]
33.856: (navigate rover1 waypoint2 waypoint0)  [5.000]
34.234: (communicate_soil_data rover0 general waypoint0 waypoint0 waypoint2)  [10.000]
34.234: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
34.235: (recharge rover0 waypoint0)  [17.489]  (?recharge_amount [0.000,17.641])
51.723: (recharge rover1 waypoint0)  [1.742]  (?recharge_amount [0.000,0.000])
51.725: (communicate_image_data rover0 general objective0 colour waypoint0 waypoint2)  [15.000]
53.466: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
53.467: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,12.821])
72.697: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
72.697: (communicate_image_data rover1 general objective2 low_res waypoint0 waypoint2)  [15.000]
72.698: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,17.653])
87.698: (communicate_soil_data rover1 general waypoint2 waypoint0 waypoint2)  [10.000]
91.929: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
91.930: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,18.474])
102.615: (communicate_image_data rover1 general objective0 low_res waypoint0 waypoint2)  [15.000]
111.161: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
111.162: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,16.294])
121.847: (navigate rover1 waypoint0 waypoint3)  [5.000]
126.847: (recharge rover1 waypoint3)  [21.111]  (?recharge_amount [0.000,38.000])
126.848: (sample_rock rover1 rover1store waypoint3)  [8.000]
130.393: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
134.849: (drop rover1 rover1store)  [1.000]
147.959: (navigate rover1 waypoint3 waypoint0)  [5.000]
152.959: (recharge rover1 waypoint0)  [6.068]  (?recharge_amount [0.000,27.134])
152.960: (communicate_rock_data rover1 general waypoint3 waypoint0 waypoint2)  [10.000]
159.028: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
159.029: (recharge rover1 waypoint0)  [15.299]  (?recharge_amount [0.000,16.269])
174.329: (sample_rock rover1 rover1store waypoint0)  [8.000]
178.260: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
178.261: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,14.089])
182.330: (drop rover1 rover1store)  [1.000]
188.946: (communicate_rock_data rover1 general waypoint0 waypoint0 waypoint2)  [10.000]
197.492: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
197.493: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,14.910])
208.177: (navigate rover1 waypoint0 waypoint1)  [5.000]
213.177: (recharge rover1 waypoint1)  [8.333]  (?recharge_amount [0.000,15.000])
213.178: (turn_on_solar_panel rover1 waypoint1)  [26.119]  (?illumination [0.000,35.000])
213.178: (sample_rock rover1 rover1store waypoint1)  [8.000]
213.179: (recharge rover1 waypoint1)  [14.511]  (?recharge_amount [0.000,17.413])
216.723: (turn_on_solar_panel rover0 waypoint0)  [19.231]  (?illumination [0.000,35.000])
239.298: (navigate rover1 waypoint1 waypoint0)  [5.000]
244.298: (recharge rover1 waypoint0)  [10.684]  (?recharge_amount [0.000,20.057])
244.299: (communicate_rock_data rover1 general waypoint1 waypoint0 waypoint2)  [10.000]

 Execution time of Domain\Problem 13 
00:22:37.3152033

 DOMAIN/PROBLEM 14 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile14.pddl
Number of literals: 58
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 15.000
b (14.000 | 5.000)b (13.000 | 10.001)b (12.000 | 17.002)b (11.000 | 32.504)b (10.000 | 34.503)b (9.000 | 65.005)b (7.000 | 65.005)b (6.000 | 65.005)b (5.000 | 65.005)b (4.000 | 75.006)b (3.000 | 88.007)b (2.000 | 88.007)b (1.000 | 103.008);;;; Solution Found
; States evaluated: 170
; Cost: 118.343
0.000: (navigate rover0 waypoint2 waypoint0)  [5.000]
0.000: (navigate rover1 waypoint3 waypoint4)  [5.000]
5.001: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
5.001: (sample_rock rover1 rover1store waypoint4)  [8.000]
10.001: (take_image rover0 waypoint0 objective3 camera0 low_res)  [7.000]
10.002: (recharge rover0 waypoint0)  [37.778]  (?recharge_amount [0.000,50.000])
13.001: (navigate rover1 waypoint4 waypoint3)  [5.000]
13.002: (drop rover1 rover1store)  [1.000]
18.001: (recharge rover1 waypoint3)  [40.556]  (?recharge_amount [0.000,50.000])
47.781: (calibrate rover0 camera0 objective1 waypoint0)  [5.000]
47.782: (communicate_image_data rover0 general objective3 low_res waypoint0 waypoint1)  [15.000]
52.781: (take_image rover0 waypoint0 objective2 camera0 low_res)  [7.000]
58.558: (navigate rover1 waypoint3 waypoint0)  [5.000]
62.783: (communicate_image_data rover0 general objective2 low_res waypoint0 waypoint1)  [15.000]
77.784: (communicate_rock_data rover1 general waypoint4 waypoint0 waypoint1)  [10.000]
87.785: (navigate rover1 waypoint0 waypoint1)  [5.000]
92.785: (sample_rock rover1 rover1store waypoint1)  [8.000]
92.786: (recharge rover1 waypoint1)  [10.556]  (?recharge_amount [0.000,19.000])
103.342: (navigate rover1 waypoint1 waypoint0)  [5.000]
108.342: (recharge rover1 waypoint0)  [3.889]  (?recharge_amount [0.000,30.000])
108.343: (communicate_rock_data rover1 general waypoint1 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 14 
00:00:03.5647222

 DOMAIN/PROBLEM 15 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile15.pddl
Number of literals: 102
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

83% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 24.000

 Execution time of Domain\Problem 15 
00:30:00.2043879

 DOMAIN/PROBLEM 16 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile16.pddl
Number of literals: 82
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 10.000)b (15.000 | 10.000)b (14.000 | 10.001)b (13.000 | 25.001)b (12.000 | 25.001)b (11.000 | 27.002)b (10.000 | 40.002)b (9.000 | 55.003)b (8.000 | 55.003)b (7.000 | 55.003)b (6.000 | 55.003)b (5.000 | 55.003)b (4.000 | 55.003)b (3.000 | 55.003)b (2.000 | 55.003)b (1.000 | 80.672);;;; Solution Found
; States evaluated: 317
; Cost: 108.819
0.000: (sample_soil rover0 rover0store waypoint1)  [10.000]
0.000: (navigate rover2 waypoint3 waypoint2)  [5.000]
0.000: (navigate rover1 waypoint3 waypoint0)  [5.000]
5.001: (calibrate rover1 camera4 objective0 waypoint0)  [5.000]
5.001: (sample_rock rover2 rover2store waypoint2)  [8.000]
5.002: (recharge rover2 waypoint2)  [11.111]  (?recharge_amount [0.000,20.000])
10.000: (navigate rover0 waypoint1 waypoint0)  [5.000]
10.002: (take_image rover1 waypoint0 objective4 camera4 low_res)  [7.000]
12.003: (calibrate rover1 camera4 objective0 waypoint0)  [5.000]
13.002: (drop rover2 rover2store)  [1.000]
15.000: (recharge rover0 waypoint0)  [6.111]  (?recharge_amount [0.000,50.000])
15.001: (communicate_soil_data rover0 general waypoint1 waypoint0 waypoint2)  [10.000]
16.114: (navigate rover2 waypoint2 waypoint3)  [5.000]
21.112: (recharge rover1 waypoint0)  [21.667]  (?recharge_amount [0.000,41.000])
21.113: (take_image rover1 waypoint0 objective4 camera4 high_res)  [7.000]
21.114: (recharge rover2 waypoint3)  [8.333]  (?recharge_amount [0.000,15.000])
25.002: (communicate_image_data rover1 general objective4 low_res waypoint0 waypoint2)  [15.000]
29.448: (navigate rover2 waypoint3 waypoint0)  [5.000]
42.778: (communicate_image_data rover1 general objective4 high_res waypoint0 waypoint2)  [15.000]
42.780: (turn_on_solar_panel rover0 waypoint0)  [33.019]  (?illumination [0.000,35.000])
42.781: (recharge rover2 waypoint0)  [17.222]  (?recharge_amount [0.000,22.013])
42.782: (sample_rock rover2 rover2store waypoint0)  [8.000]
60.004: (communicate_rock_data rover2 general waypoint2 waypoint0 waypoint2)  [10.000]
75.800: (turn_on_solar_panel rover0 waypoint0)  [33.019]  (?illumination [0.000,35.000])
75.801: (recharge rover2 waypoint0)  [19.465]  (?recharge_amount [0.000,23.358])
75.802: (communicate_rock_data rover2 general waypoint0 waypoint0 waypoint2)  [10.000]

 Execution time of Domain\Problem 16 
00:00:08.0718862

 DOMAIN/PROBLEM 17 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile17.pddl
Number of literals: 102
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

90% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 16.000

 Execution time of Domain\Problem 17 
00:30:00.1813961

 DOMAIN/PROBLEM 18 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile18.pddl
Number of literals: 123
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

88% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 34.000

 Execution time of Domain\Problem 18 
00:30:00.2225334

 DOMAIN/PROBLEM 19 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile19.pddl
Number of literals: 139
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

89% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 40.000

 Execution time of Domain\Problem 19 
00:30:00.1895702

 DOMAIN/PROBLEM 20 

File: ..\Rovers\Time\domain.pddl
File: ..\Rovers\Time\pfile20.pddl
Number of literals: 163
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

91% of the ground temporal actions in this problem are compression-safe
Initial heuristic = 47.000

 Execution time of Domain\Problem 20 
00:30:00.1990987
