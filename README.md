this repository includes the suplementary materials for ICAPS-18 submission 40. Here we added:	

	-the experimental results obtained, 	
	-the PDDL domain models and problems instances used in the experiments
	-random problem generators used 
	-random generator parameters used

We commit to make the source code of our implementation (POPCORN-R) available to the public if the work is accepted for proceedings.