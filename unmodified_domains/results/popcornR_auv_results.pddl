the results of 2d-auv-power tests in POPcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: .\2D-auv-power\domain1.pddl
File: .\2D-auv-power\p1.pddl
Number of literals: 2
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 6.000
b (3.000 | 1.202)b (2.000 | 1.202)
Resorting to best-first search
b (3.000 | 1.202)b (2.000 | 1.202)b (1.000 | 20.005);;;; Solution Found
; States evaluated: 28
; Cost: 20.672
0.000: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
3.430: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
6.859: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
10.289: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
13.718: (glide)  [4.952]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
18.672: (take-sample)  [2.000]

 Execution time of Domain\Problem 1 
00:00:02.5400413

 DOMAIN/PROBLEM 2 

File: .\2D-auv-power\domain2.pddl
File: .\2D-auv-power\p2.pddl
Number of literals: 3
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 8.000
b (5.000 | 1.202)b (4.000 | 1.202)
Resorting to best-first search
b (5.000 | 1.202)b (4.000 | 1.202)b (3.000 | 20.005)b (2.000 | 20.005)b (1.000 | 24.407);;;; Solution Found
; States evaluated: 37
; Cost: 26.674
0.000: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
3.430: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
6.859: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
10.289: (glide)  [3.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
13.718: (glide)  [4.952]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
18.672: (take-samplea)  [2.000]
20.673: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
24.674: (take-sampleb)  [2.000]

 Execution time of Domain\Problem 2 
00:00:02.6461795

 DOMAIN/PROBLEM 3 

File: .\2D-auv-power\domain3.pddl
File: .\2D-auv-power\p3.pddl
Number of literals: 3
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 8.000
b (7.000 | 1.202)b (6.000 | 1.202)b (5.000 | 1.603)b (4.000 | 1.603)
Resorting to best-first search
b (7.000 | 1.202)b (6.000 | 1.202)b (5.000 | 1.603)b (4.000 | 1.603)b (3.000 | 18.406)b (2.000 | 18.406)b (1.000 | 22.808);;;; Solution Found
; States evaluated: 41
; Cost: 24.808
0.000: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
2.572: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
5.145: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
7.717: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
10.290: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
12.862: (glide)  [3.943]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
16.806: (take-samplea)  [2.000]
18.807: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
22.808: (take-sampleb)  [2.000]

 Execution time of Domain\Problem 3 
00:00:02.6810609

 DOMAIN/PROBLEM 4 

File: .\2D-auv-power\domain4.pddl
File: .\2D-auv-power\p4.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 10.000
b (7.000 | 1.603)b (6.000 | 1.603)b (5.000 | 10.406)b (4.000 | 10.406)b (3.000 | 20.808)b (2.000 | 20.808)
Resorting to best-first search
b (7.000 | 1.603)b (6.000 | 1.603)b (5.000 | 20.807)b (4.000 | 20.807)b (3.000 | 25.209)b (2.000 | 25.209)b (1.000 | 34.012);;;; Solution Found
; States evaluated: 89
; Cost: 35.212
0.000: (glide)  [2.229]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
2.230: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.802: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
7.374: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
9.947: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
12.519: (glide)  [2.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
15.092: (glide)  [3.714]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
18.807: (take-samplea)  [2.000]
20.808: (glide)  [2.400]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
23.209: (take-sampleb)  [2.000]
25.210: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
29.211: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
33.212: (take-samplec)  [2.000]

 Execution time of Domain\Problem 4 
00:00:03.2202356

 DOMAIN/PROBLEM 5 

File: .\2D-auv-power\domain5.pddl
File: .\2D-auv-power\p5.pddl
Number of literals: 4
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 10.000
b (7.000 | 2.670)b (6.000 | 2.670)b (5.000 | 16.006)b (4.000 | 16.006)b (3.000 | 32.008)b (2.000 | 32.008)
Resorting to best-first search
b (7.000 | 2.670)b (6.000 | 2.670)b (5.000 | 30.007)b (4.000 | 30.007)b (3.000 | 35.341)b (2.000 | 35.341)b (1.000 | 52.679);;;; Solution Found
; States evaluated: 101
; Cost: 56.012
0.000: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.001: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
8.288: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
12.574: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
16.861: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
21.148: (glide)  [6.190]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
27.339: (take-sampleb)  [2.000]
29.340: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
33.341: (take-samplea)  [2.000]
35.342: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
42.010: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
48.678: (glide)  [5.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
54.012: (take-samplec)  [2.000]

 Execution time of Domain\Problem 5 
00:00:03.4512267

 DOMAIN/PROBLEM 6 

File: .\2D-auv-power\domain6.pddl
File: .\2D-auv-power\p6.pddl
Number of literals: 5
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 12.000
b (11.000 | 6.001)b (10.000 | 6.001)b (9.000 | 7.336)b (8.000 | 7.336)b (7.000 | 8.004)b (6.000 | 8.004)b (5.000 | 24.674)b (4.000 | 24.674)b (3.000 | 30.508)b (2.000 | 30.508)
Resorting to best-first search
b (11.000 | 6.001)b (10.000 | 6.001)b (9.000 | 7.336)b (8.000 | 7.336)b (7.000 | 8.004)b (6.000 | 8.004)b (5.000 | 24.674)b (4.000 | 24.674)b (3.000 | 30.508)b (2.000 | 30.508)b (1.000 | 54.512);;;; Solution Found
; States evaluated: 154
; Cost: 58.179
0.000: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.287: (take-samplec)  [2.000]
6.288: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
10.574: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
14.861: (glide)  [6.476]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
21.338: (take-sampleb)  [2.000]
23.339: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
27.840: (take-samplea)  [2.000]
29.841: (glide)  [6.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
35.842: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
42.510: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
49.178: (glide)  [7.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
56.179: (take-sampled)  [2.000]

 Execution time of Domain\Problem 6 
00:00:04.2196925

 DOMAIN/PROBLEM 7 

File: .\2D-auv-power\domain7.pddl
File: .\2D-auv-power\p7.pddl
Number of literals: 6
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 14.000
b (13.000 | 0.667)b (11.000 | 1.334)b (10.000 | 1.334)b (9.000 | 10.670)b (8.000 | 11.337)b (7.000 | 16.672)b (6.000 | 16.672)
Resorting to best-first search
b (13.000 | 0.667)b (11.000 | 1.334)b (10.000 | 1.334)b (9.000 | 10.670)b (8.000 | 11.337)b (7.000 | 16.672)b (6.000 | 16.672)b (5.000 | 34.677)b (4.000 | 42.677)b (3.000 | 44.012)b (2.000 | 44.680)b (1.000 | 60.014);;;; Solution Found
; States evaluated: 213
; Cost: 62.966
0.000: (glide)  [0.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
0.668: (glide)  [7.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
8.002: (take-sampleb)  [2.000]
10.003: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
14.004: (take-samplea)  [2.000]
16.005: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
22.673: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
29.340: (glide)  [5.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
34.675: (take-samplec)  [2.000]
36.676: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
40.677: (take-samplee)  [2.000]
42.678: (glide)  [7.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
49.679: (glide)  [7.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
56.680: (glide)  [4.286]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
60.966: (take-sampled)  [2.000]

 Execution time of Domain\Problem 7 
00:00:04.7864617

 DOMAIN/PROBLEM 8 

File: .\2D-auv-power\domain8.pddl
File: .\2D-auv-power\p8.pddl
Number of literals: 6
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 14.000
b (11.000 | 1.334)b (10.000 | 1.334)b (9.000 | 11.337)b (8.000 | 11.337)b (7.000 | 16.672)b (6.000 | 16.672)
Resorting to best-first search
b (11.000 | 1.334)b (10.000 | 1.334)b (9.000 | 14.003)b (8.000 | 14.003)b (7.000 | 21.340)b (6.000 | 21.340)b (5.000 | 42.677)b (4.000 | 42.677)b (3.000 | 44.680)b (2.000 | 44.680)b (1.000 | 60.014);;;; Solution Found
; States evaluated: 134
; Cost: 61.931
0.000: (glide)  [1.048]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
1.049: (glide)  [3.571]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.621: (glide)  [7.381]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
12.003: (take-samplea)  [2.000]
14.004: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
18.005: (take-sampleb)  [2.000]
20.006: (glide)  [5.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
25.340: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
32.008: (take-samplec)  [2.000]
34.009: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
40.677: (take-samplee)  [2.000]
42.678: (glide)  [5.750]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
48.429: (glide)  [5.750]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
54.180: (glide)  [5.750]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
59.931: (take-sampled)  [2.000]

 Execution time of Domain\Problem 8 
00:00:03.7019375

 DOMAIN/PROBLEM 9 

File: .\2D-auv-power\domain9.pddl
File: .\2D-auv-power\p9.pddl
Number of literals: 7
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 16.000
b (15.000 | 2.000)b (14.000 | 2.000)b (13.000 | 8.002)b (12.000 | 8.002)b (9.000 | 10.005)b (8.000 | 10.005)b (7.000 | 20.008)b (6.000 | 20.008)b (5.000 | 36.010)b (4.000 | 36.010)b (3.000 | 42.680)b (2.000 | 42.680)
Resorting to best-first search
b (15.000 | 2.000)b (14.000 | 2.000)b (13.000 | 8.002)b (12.000 | 8.002)b (9.000 | 10.005)b (8.000 | 10.005)b (7.000 | 34.675)b (6.000 | 34.675)b (5.000 | 36.010)b (4.000 | 36.010)b (3.000 | 51.344)b (2.000 | 51.344)b (1.000 | 57.346);;;; Solution Found
; States evaluated: 120
; Cost: 60.013
0.000: (take-samplec)  [2.000]
2.001: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
6.002: (take-samplee)  [2.000]
8.003: (glide)  [5.750]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
13.754: (glide)  [5.750]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
19.505: (glide)  [3.833]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
23.339: (take-sampled)  [2.000]
25.340: (glide)  [7.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
32.675: (take-samplef)  [2.000]
34.676: (glide)  [7.222]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
41.899: (glide)  [7.444]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
49.344: (take-samplea)  [2.000]
51.345: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
58.013: (take-sampleb)  [2.000]

 Execution time of Domain\Problem 9 
00:00:03.5932277

 DOMAIN/PROBLEM 10 

File: .\2D-auv-power\domain10.pddl
File: .\2D-auv-power\p10.pddl
Number of literals: 7
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 16.000
b (15.000 | 2.000)b (14.000 | 2.000)b (13.000 | 8.002)b (12.000 | 8.002)b (9.000 | 10.673)b (8.000 | 10.673)b (7.000 | 20.676)b (6.000 | 20.676)b (5.000 | 34.012)b (4.000 | 34.012)b (3.000 | 39.346)b (2.000 | 39.346)
Resorting to best-first search
b (15.000 | 2.000)b (14.000 | 2.000)b (13.000 | 8.002)b (12.000 | 8.002)b (9.000 | 10.673)b (8.000 | 10.673)b (7.000 | 36.010)b (6.000 | 36.010)b (5.000 | 37.345)b (4.000 | 37.345)b (3.000 | 52.680)b (2.000 | 52.680)b (1.000 | 58.682);;;; Solution Found
; States evaluated: 169
; Cost: 61.348
0.000: (take-samplec)  [2.000]
2.001: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
6.002: (take-samplee)  [2.000]
8.003: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
12.504: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
17.005: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
19.863: (glide)  [4.143]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
24.007: (take-sampled)  [2.000]
26.008: (glide)  [5.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
31.342: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
37.121: (take-samplef)  [2.000]
39.122: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
44.901: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
50.680: (take-samplea)  [2.000]
52.681: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
59.348: (take-sampleb)  [2.000]

 Execution time of Domain\Problem 10 
00:00:04.2343300

 DOMAIN/PROBLEM 11 

File: .\2D-auv-power\domain11.pddl
File: .\2D-auv-power\p11.pddl
Number of literals: 7
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 16.000
b (15.000 | 14.671)b (14.000 | 14.671)b (13.000 | 20.673)b (12.000 | 20.673)b (11.000 | 22.676)b (10.000 | 22.676)b (9.000 | 23.343)b (8.000 | 23.343)b (7.000 | 35.346)b (6.000 | 35.346)b (5.000 | 45.349)b (4.000 | 45.349)b (3.000 | 50.684)b (2.000 | 50.684)
Resorting to best-first search
b (15.000 | 14.671)b (14.000 | 14.671)b (13.000 | 20.673)b (12.000 | 20.673)b (11.000 | 22.676)b (10.000 | 22.676)b (9.000 | 23.343)b (8.000 | 23.343)b (7.000 | 50.681)b (6.000 | 50.681)b (5.000 | 52.016)b (4.000 | 52.016)b (3.000 | 67.350)b (2.000 | 67.350)b (1.000 | 73.352);;;; Solution Found
; States evaluated: 218
; Cost: 76.019
0.000: (glide)  [4.429]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.430: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
7.288: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
10.146: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
13.004: (take-samplee)  [2.000]
15.005: (glide)  [4.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
19.006: (take-samplec)  [2.000]
21.007: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
25.508: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
30.009: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
34.510: (glide)  [4.167]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
38.678: (take-sampled)  [2.000]
40.679: (glide)  [5.333]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
46.013: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
51.792: (take-samplef)  [2.000]
53.793: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
59.572: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
65.350: (take-samplea)  [2.000]
67.351: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
74.019: (take-sampleb)  [2.000]

 Execution time of Domain\Problem 11 

00:00:05.6643841

 DOMAIN/PROBLEM 12 

File: .\2D-auv-power\domain12.pddl
File: .\2D-auv-power\p12.pddl
Number of literals: 8
Constructing lookup tables:
Post filtering unreachable actions: 
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

None of the ground temporal actions in this problem have been recognised as compression-safe
Initial heuristic = 18.000
b (17.000 | 10.002)b (16.000 | 10.002)b (15.000 | 18.672)b (14.000 | 18.672)b (13.000 | 20.675)b (12.000 | 20.675)b (11.000 | 21.342)b (10.000 | 21.342)b (9.000 | 31.345)b (8.000 | 31.345)b (7.000 | 47.181)b (6.000 | 47.181)b (5.000 | 53.850)b (4.000 | 53.850)b (3.000 | 69.190)b (2.000 | 69.190)b (1.000 | 91.191);;;; Solution Found
; States evaluated: 104
; Cost: 94.524
0.000: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
4.501: (glide)  [3.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
8.002: (take-samplee)  [2.000]
10.003: (glide)  [3.810]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
13.814: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
16.672: (take-samplec)  [2.000]
18.673: (glide)  [3.929]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
22.602: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
25.460: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
28.319: (glide)  [2.857]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
31.177: (take-samplef)  [2.000]
33.178: (glide)  [5.556]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
38.734: (glide)  [5.778]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
44.513: (take-samplea)  [2.000]
46.514: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
53.182: (take-sampleb)  [2.000]
55.183: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
61.850: (glide)  [2.000]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
63.851: (take-sampleg)  [2.000]
65.852: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
70.353: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
74.854: (glide)  [6.667]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
81.522: (glide)  [4.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
86.023: (glide)  [6.500]  (?x_pos [2.000,1000000000.000])  (?y_pos [2.000,1000000000.000])  (?x_neg [2.000,20.000])  (?y_neg [2.000,20.000])
92.524: (take-sampled)  [2.000]

 Execution time of Domain\Problem 12 

00:00:04.0249302

 Execution time of Domain\Problem 1 

00:00:02.5400413

 Execution time of Domain\Problem 2  

00:00:02.6461795

 Execution time of Domain\Problem 3 

00:00:02.6810609

 Execution time of Domain\Problem 4  

00:00:03.2202356

 Execution time of Domain\Problem 5 
 
00:00:03.4512267

 Execution time of Domain\Problem 6 
 
00:00:04.2196925

 Execution time of Domain\Problem 7 
 
00:00:04.7864617

 Execution time of Domain\Problem 8 
 
00:00:03.7019375

 Execution time of Domain\Problem 9 

00:00:03.5932277

 Execution time of Domain\Problem 10 

00:00:04.2343300

 Execution time of Domain\Problem 11 
 
00:00:05.6643841

 Execution time of Domain\Problem 12 
 
00:00:04.0249302
