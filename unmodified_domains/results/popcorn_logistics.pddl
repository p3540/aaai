the results of squirrel t1_pt1 tests in popcorn 

 POPCORN 


 DOMAIN/PROBLEM 1 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile01.pddl
Number of literals: 204
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 26.000
b (24.000 | 0.000)b (23.000 | 0.000)b (22.000 | 0.001)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.002)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.002)b (13.000 | 0.002)b (12.000 | 0.003)b (11.000 | 0.005)b (10.000 | 0.005)b (9.000 | 0.006)b (8.000 | 0.007)b (7.000 | 0.007)b (6.000 | 0.007)b (5.000 | 0.007)b (4.000 | 0.008)b (3.000 | 0.008)b (2.000 | 0.009)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 105
; Cost: 0.011
0.000: (fly-airplane plane2 city4-2 city6-2) 
0.000: (load-airplane package5 plane1 city4-2) 
0.000: (load-truck package6 truck3 city3-1) 
0.000: (load-truck package4 truck1 city1-1) 
0.000: (load-truck package3 truck1 city1-1) 
0.000: (drive-truck truck6 city6-1 city6-2 city6) 
0.001: (fly-airplane plane1 city4-2 city6-2) 
0.001: (drive-truck truck3 city3-1 city3-2 city3) 
0.001: (drive-truck truck1 city1-1 city1-2 city1) 
0.001: (fly-airplane plane2 city6-2 city3-2) 
0.002: (unload-airplane package5 plane1 city6-2) 
0.002: (unload-truck package6 truck3 city3-2) 
0.002: (unload-truck package4 truck1 city1-2) 
0.002: (unload-truck package3 truck1 city1-2) 
0.003: (fly-airplane plane1 city6-2 city3-2) 
0.003: (load-airplane package6 plane2 city3-2) 
0.004: (fly-airplane plane2 city3-2 city1-2) 
0.004: (fly-airplane plane1 city3-2 city6-2) 
0.005: (unload-airplane package6 plane2 city1-2) 
0.005: (load-airplane package4 plane2 city1-2) 
0.005: (fly-airplane plane1 city6-2 city1-2) 
0.006: (fly-airplane plane2 city1-2 city3-2) 
0.006: (load-airplane package3 plane1 city1-2) 
0.006: (load-airplane package2 plane1 city1-2) 
0.007: (unload-airplane package4 plane2 city3-2) 
0.007: (fly-airplane plane1 city1-2 city6-2) 
0.008: (unload-airplane package3 plane1 city6-2) 
0.008: (unload-airplane package2 plane1 city6-2) 
0.009: (load-truck package3 truck6 city6-2) 
0.010: (drive-truck truck6 city6-2 city6-1 city6) 
0.011: (unload-truck package3 truck6 city6-1) 

 Execution time of Domain\Problem 1 
00:00:04.7712519

 DOMAIN/PROBLEM 2 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile02.pddl
Number of literals: 410
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 29.000
b (28.000 | 0.000)b (27.000 | 0.000)b (26.000 | 0.000)b (25.000 | 0.000)b (24.000 | 0.001)b (23.000 | 0.002)b (22.000 | 0.002)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.003)b (18.000 | 0.003)b (17.000 | 0.003)b (16.000 | 0.004)b (15.000 | 0.005)b (14.000 | 0.006)b (13.000 | 0.006)b (12.000 | 0.006)b (11.000 | 0.006)b (10.000 | 0.006)b (9.000 | 0.006)b (8.000 | 0.006)b (7.000 | 0.006)b (6.000 | 0.006)b (5.000 | 0.007)b (4.000 | 0.008)b (3.000 | 0.009)b (2.000 | 0.009)b (1.000 | 0.009);;;; Solution Found
; States evaluated: 143
; Cost: 0.009
0.000: (fly-airplane plane1 city6-2 city9-2) 
0.000: (fly-airplane plane2 city3-2 city1-2) 
0.000: (load-airplane package3 plane4 city3-2) 
0.000: (load-airplane package4 plane3 city7-2) 
0.000: (load-truck package2 truck10 city10-1) 
0.000: (drive-truck truck6 city6-1 city6-2 city6) 
0.000: (drive-truck truck3 city3-1 city3-2 city3) 
0.000: (drive-truck truck1 city1-1 city1-2 city1) 
0.001: (fly-airplane plane3 city7-2 city6-2) 
0.001: (load-airplane package5 plane2 city1-2) 
0.001: (fly-airplane plane4 city3-2 city2-2) 
0.001: (fly-airplane plane1 city9-2 city10-2) 
0.001: (drive-truck truck10 city10-1 city10-2 city10) 
0.002: (unload-airplane package4 plane3 city6-2) 
0.002: (fly-airplane plane2 city1-2 city4-2) 
0.002: (load-airplane package1 plane4 city2-2) 
0.002: (unload-truck package2 truck10 city10-2) 
0.003: (unload-airplane package5 plane2 city4-2) 
0.003: (fly-airplane plane4 city2-2 city1-2) 
0.003: (load-airplane package2 plane1 city10-2) 
0.003: (load-truck package4 truck6 city6-2) 
0.004: (unload-airplane package3 plane4 city1-2) 
0.004: (fly-airplane plane1 city10-2 city9-2) 
0.004: (drive-truck truck6 city6-2 city6-1 city6) 
0.005: (fly-airplane plane4 city1-2 city3-2) 
0.005: (unload-airplane package2 plane1 city9-2) 
0.005: (unload-truck package4 truck6 city6-1) 
0.005: (load-truck package3 truck1 city1-2) 
0.006: (unload-airplane package1 plane4 city3-2) 
0.006: (drive-truck truck1 city1-2 city1-1 city1) 
0.007: (load-truck package1 truck3 city3-2) 
0.007: (unload-truck package3 truck1 city1-1) 
0.008: (drive-truck truck3 city3-2 city3-1 city3) 
0.009: (unload-truck package1 truck3 city3-1) 

 Execution time of Domain\Problem 2 
00:00:04.7122088

 DOMAIN/PROBLEM 3 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile03.pddl
Number of literals: 1184
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 53.000
b (52.000 | 0.000)b (51.000 | 0.000)b (50.000 | 0.000)b (49.000 | 0.000)b (48.000 | 0.000)b (47.000 | 0.000)b (46.000 | 0.000)b (45.000 | 0.001)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.002)b (40.000 | 0.003)b (39.000 | 0.004)b (38.000 | 0.005)b (37.000 | 0.006)b (36.000 | 0.007)b (35.000 | 0.008)b (34.000 | 0.008)b (33.000 | 0.008)b (32.000 | 0.008)b (31.000 | 0.008)b (30.000 | 0.008)b (29.000 | 0.008)b (28.000 | 0.008)b (27.000 | 0.008)b (26.000 | 0.008)b (25.000 | 0.009)b (24.000 | 0.009)b (23.000 | 0.009)b (22.000 | 0.009)b (21.000 | 0.009)b (19.000 | 0.009)b (18.000 | 0.009)b (17.000 | 0.009)b (16.000 | 0.009)b (15.000 | 0.010)b (14.000 | 0.010)b (13.000 | 0.010)b (12.000 | 0.010)b (11.000 | 0.010)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.011);;;; Solution Found
; States evaluated: 434
; Cost: 0.012
0.000: (fly-airplane plane4 city10-3 city12-3) 
0.000: (fly-airplane plane1 city11-3 city12-3) 
0.000: (drive-truck truck9 city9-2 city9-3 city9) 
0.000: (drive-truck truck8 city8-2 city8-3 city8) 
0.000: (drive-truck truck12 city12-1 city12-3 city12) 
0.000: (drive-truck truck11 city11-1 city11-3 city11) 
0.000: (drive-truck truck1 city1-1 city1-3 city1) 
0.000: (load-truck package9 truck4 city4-2) 
0.000: (fly-airplane plane2 city11-3 city4-3) 
0.000: (fly-airplane plane3 city1-3 city8-3) 
0.000: (drive-truck truck13 city13-1 city13-3 city13) 
0.000: (drive-truck truck10 city10-1 city10-3 city10) 
0.001: (load-airplane package3 plane1 city12-3) 
0.001: (drive-truck truck4 city4-2 city4-3 city4) 
0.001: (drive-truck truck9 city9-3 city9-1 city9) 
0.001: (drive-truck truck8 city8-3 city8-1 city8) 
0.001: (drive-truck truck12 city12-3 city12-2 city12) 
0.001: (drive-truck truck1 city1-3 city1-2 city1) 
0.002: (fly-airplane plane1 city12-3 city9-3) 
0.002: (unload-truck package9 truck4 city4-3) 
0.002: (load-truck package8 truck9 city9-1) 
0.002: (load-truck package6 truck9 city9-1) 
0.002: (load-truck package4 truck8 city8-1) 
0.002: (load-truck package5 truck12 city12-2) 
0.002: (load-truck package7 truck1 city1-2) 
0.003: (load-airplane package9 plane2 city4-3) 
0.003: (drive-truck truck9 city9-1 city9-3 city9) 
0.003: (drive-truck truck8 city8-1 city8-3 city8) 
0.003: (drive-truck truck12 city12-2 city12-3 city12) 
0.003: (drive-truck truck1 city1-2 city1-3 city1) 
0.004: (fly-airplane plane2 city4-3 city11-3) 
0.004: (unload-truck package8 truck9 city9-3) 
0.004: (unload-truck package6 truck9 city9-3) 
0.004: (unload-truck package4 truck8 city8-3) 
0.004: (unload-truck package5 truck12 city12-3) 
0.004: (unload-truck package7 truck1 city1-3) 
0.005: (unload-airplane package9 plane2 city11-3) 
0.005: (load-airplane package8 plane1 city9-3) 
0.005: (load-airplane package4 plane3 city8-3) 
0.005: (load-airplane package5 plane4 city12-3) 
0.006: (load-truck package9 truck11 city11-3) 
0.006: (fly-airplane plane1 city9-3 city5-3) 
0.006: (fly-airplane plane3 city8-3 city13-3) 
0.006: (fly-airplane plane4 city12-3 city1-3) 
0.006: (fly-airplane plane2 city11-3 city10-3) 
0.007: (drive-truck truck11 city11-3 city11-2 city11) 
0.007: (unload-airplane package8 plane1 city5-3) 
0.007: (unload-airplane package4 plane3 city13-3) 
0.007: (load-airplane package7 plane4 city1-3) 
0.008: (unload-truck package9 truck11 city11-2) 
0.008: (fly-airplane plane1 city5-3 city1-3) 
0.008: (load-truck package4 truck13 city13-3) 
0.008: (fly-airplane plane4 city1-3 city10-3) 
0.009: (unload-airplane package3 plane1 city1-3) 
0.009: (drive-truck truck13 city13-3 city13-1 city13) 
0.009: (unload-airplane package7 plane4 city10-3) 
0.009: (unload-airplane package5 plane4 city10-3) 
0.010: (unload-truck package4 truck13 city13-1) 
0.010: (load-truck package5 truck10 city10-3) 
0.011: (drive-truck truck10 city10-3 city10-1 city10) 
0.012: (unload-truck package5 truck10 city10-1) 

 Execution time of Domain\Problem 3 
00:00:05.2461970

 DOMAIN/PROBLEM 4 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile04.pddl
Number of literals: 1821
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 58.000
b (57.000 | 0.000)b (56.000 | 0.000)b (55.000 | 0.000)b (54.000 | 0.000)b (53.000 | 0.000)b (52.000 | 0.000)b (51.000 | 0.001)b (50.000 | 0.001)b (49.000 | 0.001)b (48.000 | 0.002)b (47.000 | 0.002)b (46.000 | 0.002)b (45.000 | 0.002)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.003)b (40.000 | 0.003)b (39.000 | 0.004)b (37.000 | 0.005)b (36.000 | 0.006)b (35.000 | 0.007)b (34.000 | 0.008)b (33.000 | 0.009)b (32.000 | 0.009)b (31.000 | 0.009)b (30.000 | 0.009)b (29.000 | 0.009)b (28.000 | 0.009)b (27.000 | 0.009)b (26.000 | 0.009)b (25.000 | 0.009)b (24.000 | 0.009)b (23.000 | 0.009)b (22.000 | 0.009)b (21.000 | 0.009)b (20.000 | 0.009)b (19.000 | 0.009)b (18.000 | 0.009)b (17.000 | 0.009)b (16.000 | 0.009)b (15.000 | 0.009)b (14.000 | 0.009)b (13.000 | 0.009)b (12.000 | 0.009)b (11.000 | 0.009)b (10.000 | 0.009)b (9.000 | 0.009)b (8.000 | 0.009)b (7.000 | 0.009)b (6.000 | 0.009)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 590
; Cost: 0.010
0.000: (fly-airplane plane5 city12-4 city1-4) 
0.000: (drive-truck truck19 city9-1 city9-4 city9) 
0.000: (drive-truck truck17 city7-3 city7-4 city7) 
0.000: (drive-truck truck14 city4-1 city4-4 city4) 
0.000: (drive-truck truck12 city2-1 city2-3 city2) 
0.000: (drive-truck truck23 city13-2 city13-4 city13) 
0.000: (load-truck package2 truck21 city11-3) 
0.000: (load-truck package5 truck20 city10-1) 
0.000: (fly-airplane plane4 city8-4 city5-4) 
0.000: (fly-airplane plane2 city13-4 city11-4) 
0.000: (fly-airplane plane3 city6-4 city10-4) 
0.000: (drive-truck truck22 city12-3 city12-4 city12) 
0.001: (load-airplane package7 plane5 city1-4) 
0.001: (drive-truck truck21 city11-3 city11-4 city11) 
0.001: (drive-truck truck20 city10-1 city10-4 city10) 
0.001: (load-truck package6 truck12 city2-3) 
0.001: (load-airplane package4 plane4 city5-4) 
0.001: (drive-truck truck14 city4-4 city4-2 city4) 
0.002: (unload-truck package2 truck21 city11-4) 
0.002: (unload-truck package5 truck20 city10-4) 
0.002: (drive-truck truck12 city2-3 city2-4 city2) 
0.002: (fly-airplane plane5 city1-4 city12-4) 
0.002: (fly-airplane plane4 city5-4 city8-4) 
0.002: (load-truck package1 truck14 city4-2) 
0.003: (unload-truck package6 truck12 city2-4) 
0.003: (unload-airplane package7 plane5 city12-4) 
0.003: (unload-airplane package4 plane4 city8-4) 
0.003: (load-airplane package5 plane3 city10-4) 
0.003: (load-airplane package2 plane2 city11-4) 
0.003: (drive-truck truck14 city4-2 city4-4 city4) 
0.003: (drive-truck truck21 city11-4 city11-1 city11) 
0.004: (load-airplane package6 plane1 city2-4) 
0.004: (fly-airplane plane3 city10-4 city12-4) 
0.004: (fly-airplane plane2 city11-4 city7-4) 
0.004: (unload-truck package1 truck14 city4-4) 
0.004: (load-truck package7 truck22 city12-4) 
0.004: (load-truck package3 truck21 city11-1) 
0.004: (fly-airplane plane4 city8-4 city4-4) 
0.004: (fly-airplane plane5 city12-4 city11-4) 
0.005: (fly-airplane plane1 city2-4 city9-4) 
0.005: (unload-airplane package5 plane3 city12-4) 
0.005: (unload-airplane package2 plane2 city7-4) 
0.005: (load-airplane package1 plane4 city4-4) 
0.005: (drive-truck truck21 city11-1 city11-4 city11) 
0.006: (unload-airplane package6 plane1 city9-4) 
0.006: (load-truck package2 truck17 city7-4) 
0.006: (load-truck package5 truck22 city12-4) 
0.006: (fly-airplane plane4 city4-4 city13-4) 
0.006: (unload-truck package3 truck21 city11-4) 
0.006: (fly-airplane plane2 city7-4 city11-4) 
0.006: (fly-airplane plane3 city12-4 city13-4) 
0.007: (load-truck package6 truck19 city9-4) 
0.007: (drive-truck truck17 city7-4 city7-1 city7) 
0.007: (drive-truck truck22 city12-4 city12-3 city12) 
0.007: (unload-airplane package1 plane4 city13-4) 
0.007: (fly-airplane plane1 city9-4 city13-4) 
0.007: (load-airplane package3 plane5 city11-4) 
0.008: (drive-truck truck19 city9-4 city9-3 city9) 
0.008: (unload-truck package2 truck17 city7-1) 
0.008: (unload-truck package7 truck22 city12-3) 
0.008: (unload-truck package5 truck22 city12-3) 
0.008: (load-truck package1 truck23 city13-4) 
0.008: (fly-airplane plane5 city11-4 city13-4) 
0.009: (unload-truck package6 truck19 city9-3) 
0.009: (drive-truck truck23 city13-4 city13-1 city13) 
0.009: (unload-airplane package3 plane5 city13-4) 
0.010: (unload-truck package1 truck23 city13-1) 

 Execution time of Domain\Problem 4 
00:00:06.3480681

 DOMAIN/PROBLEM 5 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile05.pddl
Number of literals: 327
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 22.000
b (21.000 | 0.000)b (20.000 | 0.000)b (19.000 | 0.001)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.003)b (13.000 | 0.004)b (12.000 | 0.005)b (11.000 | 0.007)b (10.000 | 0.008)b (9.000 | 0.010)b (8.000 | 0.011)b (7.000 | 0.012)b (6.000 | 0.013)b (5.000 | 0.014)b (4.000 | 0.015)b (3.000 | 0.015)b (2.000 | 0.016)b (1.000 | 0.016);;;; Solution Found
; States evaluated: 106
; Cost: 0.017
0.000: (load-airplane package2 plane1 city8-2) 
0.000: (load-truck package3 truck11 city9-1) 
0.000: (load-truck package4 truck10 city8-1) 
0.001: (drive-truck truck11 city9-1 city9-2 city9) 
0.001: (drive-truck truck10 city8-1 city8-2 city8) 
0.002: (unload-truck package3 truck11 city9-2) 
0.002: (unload-truck package4 truck10 city8-2) 
0.003: (load-airplane package4 plane1 city8-2) 
0.004: (fly-airplane plane1 city8-2 city9-2) 
0.005: (load-airplane package3 plane1 city9-2) 
0.006: (fly-airplane plane1 city9-2 city7-2) 
0.007: (unload-airplane package3 plane1 city7-2) 
0.008: (fly-airplane plane1 city7-2 city9-2) 
0.009: (fly-airplane plane1 city9-2 city6-2) 
0.010: (load-airplane package1 plane1 city6-2) 
0.011: (fly-airplane plane1 city6-2 city9-2) 
0.012: (unload-airplane package1 plane1 city9-2) 
0.013: (fly-airplane plane1 city9-2 city3-2) 
0.014: (unload-airplane package2 plane1 city3-2) 
0.015: (fly-airplane plane1 city3-2 city2-2) 
0.015: (load-truck package2 truck1 city3-2) 
0.016: (drive-truck truck1 city3-2 city3-1 city3) 
0.016: (unload-airplane package4 plane1 city2-2) 
0.017: (unload-truck package2 truck1 city3-1) 

 Execution time of Domain\Problem 5 
00:00:03.9744237

 DOMAIN/PROBLEM 6 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile06.pddl
Number of literals: 3508
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 69.000
b (67.000 | 0.000)b (66.000 | 0.000)b (65.000 | 0.000)b (64.000 | 0.000)b (63.000 | 0.000)b (62.000 | 0.000)b (61.000 | 0.000)b (60.000 | 0.001)b (59.000 | 0.002)b (58.000 | 0.002)b (57.000 | 0.002)b (56.000 | 0.002)b (55.000 | 0.002)b (54.000 | 0.002)b (53.000 | 0.002)b (52.000 | 0.002)b (51.000 | 0.002)b (50.000 | 0.002)b (49.000 | 0.002)b (48.000 | 0.003)b (47.000 | 0.004)b (46.000 | 0.004)b (45.000 | 0.004)b (44.000 | 0.005)b (43.000 | 0.006)b (42.000 | 0.006)b (41.000 | 0.006)b (40.000 | 0.006)b (39.000 | 0.006)b (38.000 | 0.006)b (37.000 | 0.006)b (36.000 | 0.006)b (35.000 | 0.006)b (34.000 | 0.006)b (33.000 | 0.006)b (32.000 | 0.006)b (31.000 | 0.006)b (30.000 | 0.006)b (29.000 | 0.007)b (28.000 | 0.008)b (27.000 | 0.009)b (26.000 | 0.010)b (25.000 | 0.010)b (24.000 | 0.010)b (23.000 | 0.010)b (22.000 | 0.010)b (21.000 | 0.010)b (20.000 | 0.010)b (19.000 | 0.010)b (18.000 | 0.011)b (17.000 | 0.012)b (16.000 | 0.013)b (15.000 | 0.014)b (14.000 | 0.014)b (13.000 | 0.014)b (12.000 | 0.014)b (11.000 | 0.014)b (10.000 | 0.014)b (9.000 | 0.014)b (8.000 | 0.014)b (7.000 | 0.014)b (6.000 | 0.014)b (5.000 | 0.014)b (4.000 | 0.015)b (3.000 | 0.015)b (2.000 | 0.016)b (1.000 | 0.017);;;; Solution Found
; States evaluated: 947
; Cost: 0.018
0.000: (fly-airplane plane2 city10-4 city9-4) 
0.000: (fly-airplane plane1 city18-4 city8-4) 
0.000: (drive-truck truck12 city4-2 city4-4 city4) 
0.000: (drive-truck truck11 city3-2 city3-4 city3) 
0.000: (drive-truck truck25 city17-1 city17-4 city17) 
0.000: (drive-truck truck24 city16-1 city16-4 city16) 
0.000: (drive-truck truck20 city12-3 city12-4 city12) 
0.000: (load-truck package14 truck5 city14-1) 
0.000: (load-truck package8 truck23 city15-2) 
0.000: (load-truck package12 truck14 city6-1) 
0.000: (load-truck package15 truck1 city14-3) 
0.000: (drive-truck truck16 city8-2 city8-4 city8) 
0.000: (drive-truck truck7 city3-4 city3-1 city3) 
0.001: (load-airplane package10 plane2 city9-4) 
0.001: (drive-truck truck5 city14-1 city14-4 city14) 
0.001: (drive-truck truck23 city15-2 city15-4 city15) 
0.001: (drive-truck truck14 city6-1 city6-4 city6) 
0.001: (fly-airplane plane1 city8-4 city6-4) 
0.001: (load-truck package7 truck16 city8-4) 
0.001: (load-truck package11 truck7 city3-1) 
0.001: (drive-truck truck25 city17-4 city17-3 city17) 
0.001: (drive-truck truck1 city14-3 city14-2 city14) 
0.002: (fly-airplane plane2 city9-4 city16-4) 
0.002: (unload-truck package14 truck5 city14-4) 
0.002: (unload-truck package8 truck23 city15-4) 
0.002: (unload-truck package12 truck14 city6-4) 
0.002: (drive-truck truck7 city3-1 city3-4 city3) 
0.002: (load-truck package9 truck25 city17-3) 
0.002: (unload-truck package15 truck1 city14-2) 
0.002: (drive-truck truck16 city8-4 city8-2 city8) 
0.003: (unload-airplane package10 plane2 city16-4) 
0.003: (load-airplane package12 plane1 city6-4) 
0.003: (unload-truck package11 truck7 city3-4) 
0.003: (drive-truck truck25 city17-3 city17-4 city17) 
0.003: (drive-truck truck1 city14-2 city14-4 city14) 
0.003: (unload-truck package7 truck16 city8-2) 
0.004: (load-truck package10 truck24 city16-4) 
0.004: (fly-airplane plane1 city6-4 city8-4) 
0.004: (unload-truck package9 truck25 city17-4) 
0.004: (drive-truck truck16 city8-2 city8-4 city8) 
0.004: (fly-airplane plane2 city16-4 city3-4) 
0.005: (fly-airplane plane1 city8-4 city13-4) 
0.005: (load-airplane package11 plane2 city3-4) 
0.005: (drive-truck truck24 city16-4 city16-2 city16) 
0.006: (unload-airplane package12 plane1 city13-4) 
0.006: (fly-airplane plane2 city3-4 city12-4) 
0.006: (unload-truck package10 truck24 city16-2) 
0.006: (load-truck package16 truck24 city16-2) 
0.007: (unload-airplane package11 plane2 city12-4) 
0.007: (drive-truck truck24 city16-2 city16-4 city16) 
0.007: (fly-airplane plane1 city13-4 city8-4) 
0.008: (load-truck package11 truck20 city12-4) 
0.008: (unload-truck package16 truck24 city16-4) 
0.008: (fly-airplane plane2 city12-4 city8-4) 
0.008: (fly-airplane plane1 city8-4 city16-4) 
0.009: (drive-truck truck20 city12-4 city12-2 city12) 
0.009: (load-airplane package16 plane1 city16-4) 
0.009: (fly-airplane plane2 city8-4 city15-4) 
0.010: (unload-truck package11 truck20 city12-2) 
0.010: (fly-airplane plane1 city16-4 city14-4) 
0.010: (load-airplane package8 plane2 city15-4) 
0.011: (unload-airplane package16 plane1 city14-4) 
0.011: (fly-airplane plane2 city15-4 city8-4) 
0.011: (load-airplane package14 plane1 city14-4) 
0.011: (load-airplane package13 plane1 city14-4) 
0.012: (load-truck package16 truck1 city14-4) 
0.012: (fly-airplane plane1 city14-4 city8-4) 
0.012: (unload-airplane package8 plane2 city8-4) 
0.013: (drive-truck truck1 city14-4 city14-3 city14) 
0.013: (unload-airplane package14 plane1 city8-4) 
0.013: (load-truck package8 truck16 city8-4) 
0.014: (unload-truck package16 truck1 city14-3) 
0.014: (fly-airplane plane1 city8-4 city4-4) 
0.014: (drive-truck truck16 city8-4 city8-1 city8) 
0.015: (unload-truck package8 truck16 city8-1) 
0.015: (unload-airplane package13 plane1 city4-4) 
0.016: (load-truck package13 truck12 city4-4) 
0.017: (drive-truck truck12 city4-4 city4-3 city4) 
0.018: (unload-truck package13 truck12 city4-3) 

 Execution time of Domain\Problem 6 
00:00:32.6858804

 DOMAIN/PROBLEM 7 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile07.pddl
Number of literals: 698
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 32.000
b (30.000 | 0.000)b (29.000 | 0.000)b (28.000 | 0.001)b (27.000 | 0.002)b (26.000 | 0.002)b (25.000 | 0.002)b (24.000 | 0.002)b (23.000 | 0.002)b (22.000 | 0.003)b (21.000 | 0.003)b (20.000 | 0.004)b (19.000 | 0.004)b (18.000 | 0.004)b (17.000 | 0.004)b (16.000 | 0.004)b (15.000 | 0.005)b (14.000 | 0.005)b (13.000 | 0.005)b (12.000 | 0.005)b (11.000 | 0.006)b (10.000 | 0.007)b (9.000 | 0.008)b (8.000 | 0.008)b (7.000 | 0.008)b (6.000 | 0.008)b (5.000 | 0.008)b (4.000 | 0.008)b (3.000 | 0.008)b (2.000 | 0.008)b (1.000 | 0.008);;;; Solution Found
; States evaluated: 195
; Cost: 0.008
0.000: (fly-airplane plane1 city1-2 city7-2) 
0.000: (load-airplane package9 plane5 city5-2) 
0.000: (load-airplane package7 plane4 city6-2) 
0.000: (load-airplane package10 plane4 city6-2) 
0.000: (load-truck package5 truck4 city4-1) 
0.000: (drive-truck truck9 city9-1 city9-2 city9) 
0.000: (drive-truck truck7 city7-1 city7-2 city7) 
0.000: (drive-truck truck5 city5-1 city5-2 city5) 
0.000: (fly-airplane plane6 city2-2 city10-2) 
0.000: (fly-airplane plane3 city9-2 city10-2) 
0.000: (fly-airplane plane2 city8-2 city10-2) 
0.001: (fly-airplane plane5 city5-2 city9-2) 
0.001: (fly-airplane plane4 city6-2 city9-2) 
0.001: (fly-airplane plane1 city7-2 city4-2) 
0.001: (drive-truck truck4 city4-1 city4-2 city4) 
0.001: (load-airplane package8 plane2 city10-2) 
0.002: (unload-airplane package9 plane5 city9-2) 
0.002: (unload-airplane package10 plane4 city9-2) 
0.002: (unload-truck package5 truck4 city4-2) 
0.002: (fly-airplane plane2 city10-2 city1-2) 
0.003: (fly-airplane plane4 city9-2 city5-2) 
0.003: (load-airplane package5 plane1 city4-2) 
0.003: (load-truck package10 truck9 city9-2) 
0.003: (unload-airplane package8 plane2 city1-2) 
0.003: (load-airplane package6 plane2 city1-2) 
0.004: (unload-airplane package7 plane4 city5-2) 
0.004: (fly-airplane plane1 city4-2 city7-2) 
0.004: (drive-truck truck9 city9-2 city9-1 city9) 
0.004: (fly-airplane plane2 city1-2 city10-2) 
0.005: (unload-airplane package5 plane1 city7-2) 
0.005: (unload-truck package10 truck9 city9-1) 
0.005: (load-truck package7 truck5 city5-2) 
0.005: (unload-airplane package6 plane2 city10-2) 
0.006: (load-truck package5 truck7 city7-2) 
0.006: (drive-truck truck5 city5-2 city5-1 city5) 
0.007: (drive-truck truck7 city7-2 city7-1 city7) 
0.007: (unload-truck package7 truck5 city5-1) 
0.008: (unload-truck package5 truck7 city7-1) 

 Execution time of Domain\Problem 7 
00:00:04.4464103

 DOMAIN/PROBLEM 8 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile08.pddl
Number of literals: 4388
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 42.000
b (41.000 | 0.000)b (40.000 | 0.000)b (39.000 | 0.001)b (38.000 | 0.001)b (37.000 | 0.001)b (36.000 | 0.001)b (35.000 | 0.002)b (34.000 | 0.002)b (33.000 | 0.003)b (32.000 | 0.003)b (31.000 | 0.003)b (30.000 | 0.004)b (29.000 | 0.004)b (28.000 | 0.004)b (27.000 | 0.004)b (26.000 | 0.004)b (25.000 | 0.004)b (24.000 | 0.005)b (23.000 | 0.005)b (22.000 | 0.006)b (21.000 | 0.007)b (20.000 | 0.008)b (19.000 | 0.008)b (18.000 | 0.008)b (17.000 | 0.008)b (16.000 | 0.008)b (15.000 | 0.008)b (14.000 | 0.008)b (13.000 | 0.008)b (12.000 | 0.008)b (11.000 | 0.009)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.011);;;; Solution Found
; States evaluated: 305
; Cost: 0.012
0.000: (fly-airplane plane1 city5-3 city7-3) 
0.000: (fly-airplane plane4 city7-3 city6-3) 
0.000: (drive-truck truck20 city7-2 city7-3 city7) 
0.000: (drive-truck truck33 city20-1 city20-3 city20) 
0.000: (drive-truck truck15 city2-2 city2-3 city2) 
0.000: (load-truck package24 truck26 city13-1) 
0.000: (fly-airplane plane2 city16-3 city7-3) 
0.000: (fly-airplane plane3 city2-3 city3-3) 
0.000: (drive-truck truck10 city20-3 city20-2 city20) 
0.001: (fly-airplane plane1 city7-3 city20-3) 
0.001: (drive-truck truck26 city13-1 city13-3 city13) 
0.001: (load-airplane package19 plane3 city3-3) 
0.001: (fly-airplane plane2 city7-3 city20-3) 
0.001: (fly-airplane plane4 city6-3 city20-3) 
0.001: (load-truck package20 truck10 city20-2) 
0.002: (load-airplane package21 plane1 city20-3) 
0.002: (unload-truck package24 truck26 city13-3) 
0.002: (fly-airplane plane3 city3-3 city15-3) 
0.002: (fly-airplane plane2 city20-3 city13-3) 
0.002: (drive-truck truck10 city20-2 city20-3 city20) 
0.003: (fly-airplane plane1 city20-3 city6-3) 
0.003: (load-airplane package22 plane3 city15-3) 
0.003: (load-airplane package24 plane2 city13-3) 
0.003: (load-airplane package23 plane2 city13-3) 
0.003: (unload-truck package20 truck10 city20-3) 
0.004: (unload-airplane package21 plane1 city6-3) 
0.004: (fly-airplane plane3 city15-3 city1-3) 
0.004: (fly-airplane plane2 city13-3 city7-3) 
0.004: (load-airplane package20 plane4 city20-3) 
0.005: (unload-airplane package22 plane3 city1-3) 
0.005: (unload-airplane package19 plane3 city1-3) 
0.005: (unload-airplane package24 plane2 city7-3) 
0.005: (fly-airplane plane4 city20-3 city7-3) 
0.006: (load-truck package19 truck13 city1-3) 
0.006: (fly-airplane plane2 city7-3 city2-3) 
0.006: (load-truck package24 truck20 city7-3) 
0.006: (unload-airplane package20 plane4 city7-3) 
0.007: (drive-truck truck13 city1-3 city1-2 city1) 
0.007: (unload-airplane package23 plane2 city2-3) 
0.007: (drive-truck truck20 city7-3 city7-1 city7) 
0.008: (unload-truck package19 truck13 city1-2) 
0.008: (load-truck package23 truck15 city2-3) 
0.008: (unload-truck package24 truck20 city7-1) 
0.009: (drive-truck truck15 city2-3 city2-1 city2) 
0.009: (drive-truck truck20 city7-1 city7-3 city7) 
0.010: (unload-truck package23 truck15 city2-1) 
0.010: (load-truck package20 truck20 city7-3) 
0.011: (drive-truck truck20 city7-3 city7-1 city7) 
0.012: (unload-truck package20 truck20 city7-1) 

 Execution time of Domain\Problem 8 
00:01:54.6245703

 DOMAIN/PROBLEM 9 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile09.pddl
Number of literals: 3068
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 84.000
b (83.000 | 0.000)b (82.000 | 0.000)b (80.000 | 0.001)b (79.000 | 0.001)b (78.000 | 0.001)b (77.000 | 0.001)b (76.000 | 0.001)b (75.000 | 0.001)b (74.000 | 0.001)b (73.000 | 0.001)b (72.000 | 0.001)b (71.000 | 0.002)b (70.000 | 0.002)b (69.000 | 0.002)b (68.000 | 0.002)b (67.000 | 0.002)b (66.000 | 0.002)b (65.000 | 0.002)b (64.000 | 0.002)b (63.000 | 0.002)b (62.000 | 0.003)b (61.000 | 0.004)b (60.000 | 0.004)b (59.000 | 0.004)b (58.000 | 0.004)b (57.000 | 0.004)b (56.000 | 0.004)b (55.000 | 0.004)b (54.000 | 0.004)b (53.000 | 0.004)b (52.000 | 0.004)b (51.000 | 0.005)b (50.000 | 0.006)b (49.000 | 0.007)b (48.000 | 0.007)b (47.000 | 0.007)b (46.000 | 0.007)b (45.000 | 0.007)b (44.000 | 0.007)b (43.000 | 0.007)b (42.000 | 0.007)b (41.000 | 0.007)b (40.000 | 0.007)b (39.000 | 0.007)b (38.000 | 0.007)b (37.000 | 0.007)b (36.000 | 0.008)b (35.000 | 0.008)b (34.000 | 0.009)b (33.000 | 0.009)b (32.000 | 0.010)b (31.000 | 0.011)b (30.000 | 0.011)b (29.000 | 0.011)b (28.000 | 0.011)b (27.000 | 0.011)b (26.000 | 0.011)b (25.000 | 0.011)b (24.000 | 0.011)b (23.000 | 0.011)b (22.000 | 0.011)b (21.000 | 0.011)b (20.000 | 0.011)b (19.000 | 0.011)b (18.000 | 0.011)b (17.000 | 0.011)b (16.000 | 0.012)b (15.000 | 0.012)b (14.000 | 0.012)b (13.000 | 0.012)b (12.000 | 0.012)b (11.000 | 0.012)b (10.000 | 0.012)b (9.000 | 0.012)b (8.000 | 0.012)b (7.000 | 0.012)b (6.000 | 0.012)b (5.000 | 0.012)b (4.000 | 0.012)b (3.000 | 0.012)b (2.000 | 0.012)b (1.000 | 0.012);;;; Solution Found
; States evaluated: 2053
; Cost: 0.012
0.000: (fly-airplane plane4 city5-6 city9-6) 
0.000: (fly-airplane plane1 city10-6 city9-6) 
0.000: (drive-truck truck16 city6-3 city6-6 city6) 
0.000: (drive-truck truck22 city5-2 city5-6 city5) 
0.000: (drive-truck truck21 city4-2 city4-5 city4) 
0.000: (drive-truck truck19 city2-1 city2-4 city2) 
0.000: (drive-truck truck28 city11-2 city11-6 city11) 
0.000: (drive-truck truck27 city10-2 city10-6 city10) 
0.000: (drive-truck truck18 city1-1 city1-2 city1) 
0.000: (load-airplane package1 plane3 city6-6) 
0.000: (load-truck package6 truck3 city4-1) 
0.000: (load-truck package11 truck26 city9-1) 
0.000: (fly-airplane plane2 city7-6 city6-6) 
0.000: (drive-truck truck14 city11-6 city11-5 city11) 
0.001: (fly-airplane plane1 city9-6 city2-6) 
0.001: (load-truck package3 truck21 city4-5) 
0.001: (load-truck package7 truck19 city2-4) 
0.001: (load-truck package12 truck18 city1-2) 
0.001: (fly-airplane plane3 city6-6 city5-6) 
0.001: (drive-truck truck26 city9-1 city9-6 city9) 
0.001: (fly-airplane plane4 city9-6 city4-6) 
0.001: (drive-truck truck3 city4-1 city4-6 city4) 
0.001: (load-truck package8 truck14 city11-5) 
0.001: (fly-airplane plane2 city6-6 city5-6) 
0.002: (load-airplane package5 plane1 city2-6) 
0.002: (load-airplane package2 plane1 city2-6) 
0.002: (drive-truck truck21 city4-5 city4-6 city4) 
0.002: (drive-truck truck18 city1-2 city1-6 city1) 
0.002: (unload-airplane package1 plane3 city5-6) 
0.002: (unload-truck package11 truck26 city9-6) 
0.002: (load-airplane package10 plane4 city4-6) 
0.002: (unload-truck package6 truck3 city4-6) 
0.002: (drive-truck truck19 city2-4 city2-6 city2) 
0.002: (drive-truck truck14 city11-5 city11-6 city11) 
0.002: (fly-airplane plane2 city5-6 city9-6) 
0.003: (unload-truck package3 truck21 city4-6) 
0.003: (unload-truck package12 truck18 city1-6) 
0.003: (load-truck package1 truck22 city5-6) 
0.003: (fly-airplane plane4 city4-6 city6-6) 
0.003: (unload-truck package7 truck19 city2-6) 
0.003: (unload-truck package8 truck14 city11-6) 
0.003: (fly-airplane plane3 city5-6 city9-6) 
0.003: (load-airplane package11 plane2 city9-6) 
0.004: (drive-truck truck21 city4-6 city4-1 city4) 
0.004: (unload-airplane package10 plane4 city6-6) 
0.004: (drive-truck truck22 city5-6 city5-1 city5) 
0.004: (load-airplane package7 plane1 city2-6) 
0.004: (fly-airplane plane2 city9-6 city4-6) 
0.004: (fly-airplane plane3 city9-6 city8-6) 
0.005: (load-truck package10 truck16 city6-6) 
0.005: (unload-truck package1 truck22 city5-1) 
0.005: (fly-airplane plane1 city2-6 city9-6) 
0.005: (unload-airplane package11 plane2 city4-6) 
0.005: (fly-airplane plane4 city6-6 city8-6) 
0.005: (load-airplane package6 plane2 city4-6) 
0.005: (load-airplane package3 plane2 city4-6) 
0.005: (fly-airplane plane3 city8-6 city5-6) 
0.005: (drive-truck truck21 city4-1 city4-6 city4) 
0.006: (drive-truck truck16 city6-6 city6-2 city6) 
0.006: (drive-truck truck22 city5-1 city5-6 city5) 
0.006: (unload-airplane package7 plane1 city9-6) 
0.006: (unload-airplane package2 plane1 city9-6) 
0.006: (fly-airplane plane2 city4-6 city8-6) 
0.006: (load-truck package11 truck3 city4-6) 
0.006: (fly-airplane plane3 city5-6 city11-6) 
0.006: (fly-airplane plane4 city8-6 city1-6) 
0.007: (unload-truck package10 truck16 city6-2) 
0.007: (fly-airplane plane1 city9-6 city10-6) 
0.007: (load-truck package2 truck13 city9-6) 
0.007: (unload-airplane package3 plane2 city8-6) 
0.007: (drive-truck truck3 city4-6 city4-1 city4) 
0.007: (load-airplane package12 plane4 city1-6) 
0.007: (load-airplane package8 plane3 city11-6) 
0.008: (drive-truck truck13 city9-6 city9-2 city9) 
0.008: (unload-airplane package5 plane1 city10-6) 
0.008: (fly-airplane plane2 city8-6 city2-6) 
0.008: (load-truck package3 truck11 city8-6) 
0.008: (unload-truck package11 truck3 city4-1) 
0.008: (fly-airplane plane3 city11-6 city2-6) 
0.008: (fly-airplane plane4 city1-6 city5-6) 
0.009: (unload-truck package2 truck13 city9-2) 
0.009: (load-truck package5 truck27 city10-6) 
0.009: (drive-truck truck11 city8-6 city8-1 city8) 
0.009: (unload-airplane package6 plane2 city2-6) 
0.009: (unload-airplane package12 plane4 city5-6) 
0.009: (unload-airplane package8 plane3 city2-6) 
0.010: (drive-truck truck27 city10-6 city10-3 city10) 
0.010: (unload-truck package3 truck11 city8-1) 
0.010: (load-truck package6 truck19 city2-6) 
0.010: (load-truck package8 truck6 city2-6) 
0.010: (load-truck package12 truck22 city5-6) 
0.011: (unload-truck package5 truck27 city10-3) 
0.011: (drive-truck truck19 city2-6 city2-4 city2) 
0.011: (drive-truck truck6 city2-6 city2-3 city2) 
0.011: (drive-truck truck22 city5-6 city5-4 city5) 
0.012: (unload-truck package6 truck19 city2-4) 
0.012: (unload-truck package12 truck22 city5-4) 
0.012: (unload-truck package8 truck6 city2-3) 

 Execution time of Domain\Problem 9 
00:00:24.3142412

 DOMAIN/PROBLEM 10 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile10.pddl
Number of literals: 2391
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 98.000
b (97.000 | 0.000)b (96.000 | 0.000)b (95.000 | 0.001)b (94.000 | 0.002)b (93.000 | 0.002)b (92.000 | 0.002)b (91.000 | 0.002)b (90.000 | 0.002)b (89.000 | 0.002)b (88.000 | 0.002)b (87.000 | 0.002)b (86.000 | 0.002)b (85.000 | 0.002)b (84.000 | 0.002)b (83.000 | 0.002)b (82.000 | 0.002)b (81.000 | 0.002)b (80.000 | 0.002)b (79.000 | 0.002)b (78.000 | 0.002)b (77.000 | 0.002)b (76.000 | 0.002)b (75.000 | 0.002)b (74.000 | 0.002)b (73.000 | 0.002)b (72.000 | 0.002)b (71.000 | 0.002)b (70.000 | 0.002)b (69.000 | 0.002)b (68.000 | 0.002)b (67.000 | 0.002)b (66.000 | 0.002)b (65.000 | 0.002)b (64.000 | 0.003)b (63.000 | 0.003)b (62.000 | 0.004)b (61.000 | 0.005)b (60.000 | 0.005)b (59.000 | 0.005)b (58.000 | 0.006)b (57.000 | 0.007)b (56.000 | 0.008)b (55.000 | 0.008)b (54.000 | 0.008)b (53.000 | 0.008)b (52.000 | 0.008)b (51.000 | 0.008)b (50.000 | 0.008)b (49.000 | 0.008)b (48.000 | 0.008)b (47.000 | 0.008)b (46.000 | 0.008)b (45.000 | 0.008)b (44.000 | 0.008)b (43.000 | 0.008)b (42.000 | 0.008)b (41.000 | 0.008)b (40.000 | 0.008)b (39.000 | 0.008)b (38.000 | 0.008)b (37.000 | 0.008)b (36.000 | 0.009)b (35.000 | 0.009)b (34.000 | 0.009)b (33.000 | 0.009)b (32.000 | 0.009)b (31.000 | 0.010)b (30.000 | 0.011)b (29.000 | 0.012)b (28.000 | 0.012)b (27.000 | 0.012)b (26.000 | 0.012)b (25.000 | 0.012)b (24.000 | 0.012)b (23.000 | 0.012)b (22.000 | 0.012)b (21.000 | 0.013)b (20.000 | 0.013)b (19.000 | 0.013)b (18.000 | 0.013)b (17.000 | 0.013)b (16.000 | 0.013)b (15.000 | 0.013)b (14.000 | 0.014)b (13.000 | 0.015)b (12.000 | 0.015)b (11.000 | 0.015)b (10.000 | 0.015)b (9.000 | 0.015)b (8.000 | 0.015)b (7.000 | 0.015)b (6.000 | 0.015)b (5.000 | 0.015)b (4.000 | 0.015)b (3.000 | 0.015)b (2.000 | 0.015)b (1.000 | 0.015);;;; Solution Found
; States evaluated: 4326
; Cost: 0.015
0.000: (fly-airplane plane1 city16-2 city8-2) 
0.000: (fly-airplane plane4 city11-2 city15-2) 
0.000: (load-airplane package16 plane3 city13-2) 
0.000: (fly-airplane plane2 city18-2 city3-2) 
0.000: (load-truck package8 truck8 city8-1) 
0.000: (load-truck package4 truck7 city7-1) 
0.000: (load-truck package5 truck6 city6-1) 
0.000: (load-truck package12 truck5 city5-1) 
0.000: (load-truck package2 truck22 city22-1) 
0.000: (load-truck package9 truck21 city21-1) 
0.000: (load-truck package3 truck19 city19-1) 
0.000: (load-truck package11 truck16 city16-1) 
0.000: (load-truck package7 truck11 city11-1) 
0.000: (drive-truck truck2 city2-1 city2-2 city2) 
0.000: (drive-truck truck9 city9-1 city9-2 city9) 
0.000: (drive-truck truck3 city3-1 city3-2 city3) 
0.000: (drive-truck truck1 city1-1 city1-2 city1) 
0.001: (load-airplane package15 plane4 city15-2) 
0.001: (fly-airplane plane3 city13-2 city4-2) 
0.001: (fly-airplane plane1 city8-2 city3-2) 
0.001: (load-airplane package14 plane2 city3-2) 
0.001: (load-airplane package10 plane2 city3-2) 
0.001: (drive-truck truck7 city7-1 city7-2 city7) 
0.001: (drive-truck truck6 city6-1 city6-2 city6) 
0.001: (drive-truck truck5 city5-1 city5-2 city5) 
0.001: (drive-truck truck22 city22-1 city22-2 city22) 
0.001: (drive-truck truck16 city16-1 city16-2 city16) 
0.001: (drive-truck truck11 city11-1 city11-2 city11) 
0.001: (drive-truck truck8 city8-1 city8-2 city8) 
0.001: (drive-truck truck21 city21-1 city21-2 city21) 
0.001: (drive-truck truck19 city19-1 city19-2 city19) 
0.002: (fly-airplane plane4 city15-2 city19-2) 
0.002: (fly-airplane plane1 city3-2 city22-2) 
0.002: (unload-airplane package16 plane3 city4-2) 
0.002: (unload-truck package4 truck7 city7-2) 
0.002: (unload-truck package5 truck6 city6-2) 
0.002: (unload-truck package12 truck5 city5-2) 
0.002: (unload-truck package2 truck22 city22-2) 
0.002: (unload-truck package11 truck16 city16-2) 
0.002: (unload-truck package7 truck11 city11-2) 
0.002: (unload-truck package8 truck8 city8-2) 
0.002: (unload-truck package9 truck21 city21-2) 
0.002: (unload-truck package3 truck19 city19-2) 
0.002: (fly-airplane plane2 city3-2 city9-2) 
0.003: (unload-airplane package15 plane4 city19-2) 
0.003: (load-airplane package2 plane1 city22-2) 
0.003: (load-airplane package3 plane4 city19-2) 
0.003: (unload-airplane package14 plane2 city9-2) 
0.003: (unload-airplane package10 plane2 city9-2) 
0.003: (fly-airplane plane3 city4-2 city8-2) 
0.004: (fly-airplane plane1 city22-2 city2-2) 
0.004: (fly-airplane plane4 city19-2 city9-2) 
0.004: (load-truck package15 truck19 city19-2) 
0.004: (fly-airplane plane2 city9-2 city8-2) 
0.004: (load-airplane package8 plane3 city8-2) 
0.004: (load-airplane package17 plane3 city8-2) 
0.004: (load-truck package14 truck9 city9-2) 
0.005: (unload-airplane package2 plane1 city2-2) 
0.005: (drive-truck truck19 city19-2 city19-1 city19) 
0.005: (unload-airplane package3 plane4 city9-2) 
0.005: (drive-truck truck9 city9-2 city9-1 city9) 
0.005: (fly-airplane plane3 city8-2 city3-2) 
0.005: (fly-airplane plane2 city8-2 city7-2) 
0.006: (load-truck package2 truck2 city2-2) 
0.006: (unload-truck package15 truck19 city19-1) 
0.006: (unload-truck package14 truck9 city9-1) 
0.006: (fly-airplane plane1 city2-2 city8-2) 
0.006: (unload-airplane package17 plane3 city3-2) 
0.006: (fly-airplane plane4 city9-2 city8-2) 
0.006: (load-airplane package4 plane2 city7-2) 
0.007: (drive-truck truck2 city2-2 city2-1 city2) 
0.007: (fly-airplane plane3 city3-2 city13-2) 
0.007: (load-truck package17 truck3 city3-2) 
0.007: (fly-airplane plane2 city7-2 city8-2) 
0.007: (fly-airplane plane1 city8-2 city6-2) 
0.007: (fly-airplane plane4 city8-2 city11-2) 
0.008: (unload-truck package2 truck2 city2-1) 
0.008: (unload-airplane package8 plane3 city13-2) 
0.008: (drive-truck truck3 city3-2 city3-1 city3) 
0.008: (fly-airplane plane2 city8-2 city1-2) 
0.008: (load-airplane package7 plane4 city11-2) 
0.008: (load-airplane package5 plane1 city6-2) 
0.009: (unload-truck package17 truck3 city3-1) 
0.009: (unload-airplane package4 plane2 city1-2) 
0.009: (fly-airplane plane1 city6-2 city8-2) 
0.009: (fly-airplane plane4 city11-2 city3-2) 
0.009: (fly-airplane plane3 city13-2 city5-2) 
0.010: (load-truck package4 truck1 city1-2) 
0.010: (unload-airplane package5 plane1 city8-2) 
0.010: (unload-airplane package7 plane4 city3-2) 
0.010: (fly-airplane plane2 city1-2 city5-2) 
0.010: (load-airplane package12 plane3 city5-2) 
0.010: (drive-truck truck3 city3-1 city3-2 city3) 
0.011: (drive-truck truck1 city1-2 city1-1 city1) 
0.011: (load-truck package5 truck8 city8-2) 
0.011: (fly-airplane plane3 city5-2 city21-2) 
0.011: (load-truck package7 truck3 city3-2) 
0.011: (fly-airplane plane4 city3-2 city21-2) 
0.011: (fly-airplane plane1 city8-2 city14-2) 
0.011: (fly-airplane plane2 city5-2 city16-2) 
0.012: (unload-truck package4 truck1 city1-1) 
0.012: (drive-truck truck8 city8-2 city8-1 city8) 
0.012: (unload-airplane package12 plane3 city21-2) 
0.012: (drive-truck truck3 city3-2 city3-1 city3) 
0.012: (load-airplane package9 plane3 city21-2) 
0.012: (load-airplane package11 plane2 city16-2) 
0.012: (load-airplane package13 plane1 city14-2) 
0.013: (unload-truck package5 truck8 city8-1) 
0.013: (load-truck package12 truck21 city21-2) 
0.013: (unload-truck package7 truck3 city3-1) 
0.013: (fly-airplane plane3 city21-2 city20-2) 
0.013: (fly-airplane plane2 city16-2 city21-2) 
0.013: (fly-airplane plane1 city14-2 city3-2) 
0.014: (drive-truck truck21 city21-2 city21-1 city21) 
0.014: (unload-airplane package13 plane1 city3-2) 
0.014: (unload-airplane package11 plane2 city21-2) 
0.014: (unload-airplane package9 plane3 city20-2) 
0.015: (unload-truck package12 truck21 city21-1) 

 Execution time of Domain\Problem 10 
00:00:39.5481966

 DOMAIN/PROBLEM 11 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile11.pddl
Number of literals: 512
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 28.000
b (27.000 | 0.000)b (26.000 | 0.000)b (25.000 | 0.001)b (24.000 | 0.002)b (23.000 | 0.002)b (22.000 | 0.002)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.002)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.003)b (13.000 | 0.003)b (12.000 | 0.003)b (11.000 | 0.004)b (10.000 | 0.004)b (9.000 | 0.005)b (8.000 | 0.005)b (7.000 | 0.006)b (6.000 | 0.007)b (5.000 | 0.008)b (4.000 | 0.009)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 129
; Cost: 0.010
0.000: (drive-truck truck20 city1-2 city1-3 city1) 
0.000: (load-truck package2 truck22 city3-2) 
0.000: (load-truck package7 truck16 city2-1) 
0.000: (load-truck package8 truck13 city1-2) 
0.000: (load-truck package3 truck12 city2-2) 
0.000: (fly-airplane plane3 city2-3 city3-3) 
0.001: (drive-truck truck22 city3-2 city3-3 city3) 
0.001: (load-truck package1 truck20 city1-3) 
0.001: (drive-truck truck16 city2-1 city2-3 city2) 
0.001: (drive-truck truck12 city2-2 city2-3 city2) 
0.001: (drive-truck truck13 city1-2 city1-3 city1) 
0.002: (unload-truck package2 truck22 city3-3) 
0.002: (drive-truck truck20 city1-3 city1-1 city1) 
0.002: (unload-truck package7 truck16 city2-3) 
0.002: (unload-truck package3 truck12 city2-3) 
0.002: (unload-truck package8 truck13 city1-3) 
0.003: (unload-truck package1 truck20 city1-1) 
0.003: (load-airplane package8 plane1 city1-3) 
0.003: (load-airplane package7 plane2 city2-3) 
0.003: (load-airplane package3 plane2 city2-3) 
0.003: (load-airplane package2 plane3 city3-3) 
0.004: (fly-airplane plane1 city1-3 city3-3) 
0.004: (fly-airplane plane2 city2-3 city3-3) 
0.004: (fly-airplane plane3 city3-3 city2-3) 
0.005: (unload-airplane package8 plane1 city3-3) 
0.005: (unload-airplane package7 plane2 city3-3) 
0.005: (unload-airplane package2 plane3 city2-3) 
0.006: (fly-airplane plane2 city3-3 city1-3) 
0.007: (unload-airplane package3 plane2 city1-3) 
0.008: (load-truck package3 truck13 city1-3) 
0.009: (drive-truck truck13 city1-3 city1-2 city1) 
0.010: (unload-truck package3 truck13 city1-2) 

 Execution time of Domain\Problem 11 
00:00:04.0670344

 DOMAIN/PROBLEM 12 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile12.pddl
Number of literals: 7043
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 42.000
b (41.000 | 0.000)b (40.000 | 0.000)b (39.000 | 0.000)b (38.000 | 0.000)b (37.000 | 0.001)b (36.000 | 0.002)b (35.000 | 0.002)b (34.000 | 0.002)b (33.000 | 0.002)b (32.000 | 0.002)b (31.000 | 0.002)b (30.000 | 0.002)b (29.000 | 0.002)b (28.000 | 0.002)b (27.000 | 0.002)b (26.000 | 0.003)b (25.000 | 0.004)b (24.000 | 0.004)b (23.000 | 0.004)b (22.000 | 0.004)b (21.000 | 0.004)b (20.000 | 0.004)b (19.000 | 0.004)b (18.000 | 0.004)b (17.000 | 0.005)b (16.000 | 0.005)b (15.000 | 0.005)b (14.000 | 0.006)b (13.000 | 0.007)b (12.000 | 0.007)b (11.000 | 0.007)b (10.000 | 0.008)b (9.000 | 0.008)b (8.000 | 0.008)b (7.000 | 0.008)b (6.000 | 0.008)b (5.000 | 0.008)b (4.000 | 0.008)b (3.000 | 0.008)b (2.000 | 0.008)b (1.000 | 0.008);;;; Solution Found
; States evaluated: 290
; Cost: 0.008
0.000: (drive-truck truck11 city14-2 city14-4 city14) 
0.000: (drive-truck truck40 city12-3 city12-4 city12) 
0.000: (drive-truck truck20 city10-1 city10-4 city10) 
0.000: (load-airplane package1 plane2 city1-4) 
0.000: (load-truck package2 truck5 city23-3) 
0.000: (load-truck package3 truck36 city8-1) 
0.000: (load-truck package4 truck35 city7-2) 
0.000: (fly-airplane plane3 city13-4 city8-4) 
0.000: (fly-airplane plane4 city2-4 city7-4) 
0.000: (drive-truck truck37 city9-2 city9-4 city9) 
0.000: (drive-truck truck26 city12-4 city12-1 city12) 
0.000: (fly-airplane plane5 city23-4 city18-4) 
0.001: (fly-airplane plane2 city1-4 city9-4) 
0.001: (drive-truck truck5 city23-3 city23-4 city23) 
0.001: (drive-truck truck36 city8-1 city8-4 city8) 
0.001: (drive-truck truck35 city7-2 city7-4 city7) 
0.001: (load-truck package5 truck26 city12-1) 
0.001: (fly-airplane plane5 city18-4 city12-4) 
0.002: (unload-airplane package1 plane2 city9-4) 
0.002: (unload-truck package2 truck5 city23-4) 
0.002: (unload-truck package3 truck36 city8-4) 
0.002: (unload-truck package4 truck35 city7-4) 
0.002: (drive-truck truck26 city12-1 city12-4 city12) 
0.003: (load-airplane package2 plane1 city23-4) 
0.003: (fly-airplane plane2 city9-4 city12-4) 
0.003: (load-airplane package4 plane4 city7-4) 
0.003: (load-airplane package3 plane3 city8-4) 
0.003: (load-truck package1 truck37 city9-4) 
0.003: (unload-truck package5 truck26 city12-4) 
0.004: (fly-airplane plane1 city23-4 city14-4) 
0.004: (fly-airplane plane4 city7-4 city10-4) 
0.004: (fly-airplane plane3 city8-4 city1-4) 
0.004: (drive-truck truck37 city9-4 city9-2 city9) 
0.004: (load-airplane package5 plane5 city12-4) 
0.005: (unload-airplane package2 plane1 city14-4) 
0.005: (unload-airplane package4 plane4 city10-4) 
0.005: (unload-airplane package3 plane3 city1-4) 
0.005: (unload-truck package1 truck37 city9-2) 
0.005: (fly-airplane plane5 city12-4 city18-4) 
0.006: (load-truck package4 truck20 city10-4) 
0.006: (load-truck package2 truck11 city14-4) 
0.006: (unload-airplane package5 plane5 city18-4) 
0.007: (drive-truck truck20 city10-4 city10-3 city10) 
0.007: (drive-truck truck11 city14-4 city14-3 city14) 
0.008: (unload-truck package2 truck11 city14-3) 
0.008: (unload-truck package4 truck20 city10-3) 

 Execution time of Domain\Problem 12 
00:00:10.6716770

 DOMAIN/PROBLEM 13 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile13.pddl
Number of literals: 7167
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 68.000
b (67.000 | 0.000)b (66.000 | 0.000)b (65.000 | 0.001)b (64.000 | 0.001)b (63.000 | 0.001)b (62.000 | 0.001)b (61.000 | 0.001)b (60.000 | 0.001)b (59.000 | 0.001)b (58.000 | 0.001)b (57.000 | 0.002)b (56.000 | 0.002)b (55.000 | 0.002)b (54.000 | 0.002)b (53.000 | 0.002)b (52.000 | 0.002)b (51.000 | 0.002)b (50.000 | 0.002)b (49.000 | 0.002)b (48.000 | 0.003)b (47.000 | 0.004)b (46.000 | 0.005)b (45.000 | 0.006)b (44.000 | 0.007)b (43.000 | 0.008)b (42.000 | 0.009)b (41.000 | 0.010)b (40.000 | 0.010)b (39.000 | 0.010)b (38.000 | 0.010)b (37.000 | 0.010)b (36.000 | 0.010)b (35.000 | 0.010)b (34.000 | 0.010)b (33.000 | 0.010)b (32.000 | 0.010)b (31.000 | 0.010)b (30.000 | 0.010)b (28.000 | 0.010)b (27.000 | 0.010)b (26.000 | 0.010)b (25.000 | 0.010)b (24.000 | 0.010)b (23.000 | 0.010)b (22.000 | 0.010)b (21.000 | 0.010)b (20.000 | 0.010)b (19.000 | 0.010)b (18.000 | 0.010)b (17.000 | 0.010)b (16.000 | 0.010)b (15.000 | 0.010)b (14.000 | 0.010)b (13.000 | 0.010)b (12.000 | 0.010)b (11.000 | 0.010)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 1007
; Cost: 0.010
0.000: (fly-airplane plane1 city13-4 city7-4) 
0.000: (fly-airplane plane9 city28-4 city1-4) 
0.000: (drive-truck truck3 city3-2 city3-4 city3) 
0.000: (drive-truck truck26 city26-1 city26-4 city26) 
0.000: (drive-truck truck22 city22-3 city22-4 city22) 
0.000: (drive-truck truck17 city17-3 city17-4 city17) 
0.000: (drive-truck truck11 city11-1 city11-4 city11) 
0.000: (load-airplane package16 plane6 city29-4) 
0.000: (load-truck package18 truck21 city21-1) 
0.000: (load-truck package20 truck18 city18-2) 
0.000: (load-truck package22 truck1 city1-1) 
0.000: (fly-airplane plane2 city14-4 city7-4) 
0.000: (fly-airplane plane3 city14-4 city7-4) 
0.000: (fly-airplane plane4 city4-4 city21-4) 
0.000: (fly-airplane plane5 city13-4 city4-4) 
0.000: (drive-truck truck14 city14-3 city14-4 city14) 
0.000: (fly-airplane plane7 city27-4 city21-4) 
0.001: (fly-airplane plane1 city7-4 city1-4) 
0.001: (fly-airplane plane6 city29-4 city14-4) 
0.001: (drive-truck truck21 city21-1 city21-2 city21) 
0.001: (drive-truck truck18 city18-2 city18-3 city18) 
0.001: (drive-truck truck1 city1-1 city1-4 city1) 
0.001: (drive-truck truck3 city3-4 city3-1 city3) 
0.001: (fly-airplane plane2 city7-4 city3-4) 
0.001: (drive-truck truck26 city26-4 city26-2 city26) 
0.001: (fly-airplane plane3 city7-4 city26-4) 
0.001: (fly-airplane plane5 city4-4 city18-4) 
0.001: (drive-truck truck17 city17-4 city17-2 city17) 
0.002: (unload-airplane package16 plane6 city14-4) 
0.002: (load-airplane package17 plane1 city1-4) 
0.002: (unload-truck package22 truck1 city1-4) 
0.002: (load-truck package21 truck3 city3-1) 
0.002: (load-truck package23 truck26 city26-2) 
0.002: (drive-truck truck21 city21-2 city21-4 city21) 
0.002: (drive-truck truck18 city18-3 city18-4 city18) 
0.002: (load-truck package19 truck17 city17-2) 
0.003: (load-airplane package22 plane1 city1-4) 
0.003: (drive-truck truck3 city3-1 city3-4 city3) 
0.003: (drive-truck truck26 city26-2 city26-4 city26) 
0.003: (unload-truck package18 truck21 city21-4) 
0.003: (unload-truck package20 truck18 city18-4) 
0.003: (fly-airplane plane6 city14-4 city17-4) 
0.003: (load-truck package16 truck14 city14-4) 
0.003: (drive-truck truck17 city17-2 city17-4 city17) 
0.004: (fly-airplane plane1 city1-4 city18-4) 
0.004: (unload-truck package21 truck3 city3-4) 
0.004: (unload-truck package23 truck26 city26-4) 
0.004: (load-airplane package18 plane4 city21-4) 
0.004: (load-airplane package20 plane5 city18-4) 
0.004: (drive-truck truck14 city14-4 city14-3 city14) 
0.004: (unload-truck package19 truck17 city17-4) 
0.005: (unload-airplane package17 plane1 city18-4) 
0.005: (load-airplane package21 plane2 city3-4) 
0.005: (load-airplane package23 plane3 city26-4) 
0.005: (fly-airplane plane4 city21-4 city22-4) 
0.005: (fly-airplane plane5 city18-4 city4-4) 
0.005: (unload-truck package16 truck14 city14-3) 
0.005: (load-airplane package19 plane6 city17-4) 
0.006: (fly-airplane plane1 city18-4 city11-4) 
0.006: (fly-airplane plane2 city3-4 city16-4) 
0.006: (fly-airplane plane3 city26-4 city7-4) 
0.006: (unload-airplane package18 plane4 city22-4) 
0.006: (load-truck package17 truck18 city18-4) 
0.006: (unload-airplane package20 plane5 city4-4) 
0.006: (fly-airplane plane6 city17-4 city21-4) 
0.007: (unload-airplane package22 plane1 city11-4) 
0.007: (unload-airplane package21 plane2 city16-4) 
0.007: (unload-airplane package23 plane3 city7-4) 
0.007: (load-truck package18 truck22 city22-4) 
0.007: (drive-truck truck18 city18-4 city18-3 city18) 
0.007: (unload-airplane package19 plane6 city21-4) 
0.008: (load-truck package22 truck11 city11-4) 
0.008: (drive-truck truck22 city22-4 city22-1 city22) 
0.008: (unload-truck package17 truck18 city18-3) 
0.008: (load-truck package19 truck21 city21-4) 
0.009: (drive-truck truck11 city11-4 city11-3 city11) 
0.009: (unload-truck package18 truck22 city22-1) 
0.009: (drive-truck truck21 city21-4 city21-2 city21) 
0.010: (unload-truck package22 truck11 city11-3) 
0.010: (unload-truck package19 truck21 city21-2) 

 Execution time of Domain\Problem 13 
00:07:56.3302240

 DOMAIN/PROBLEM 14 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile14.pddl
Number of literals: 3355
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 88.000
b (87.000 | 0.000)b (86.000 | 0.000)b (85.000 | 0.000)b (84.000 | 0.001)b (83.000 | 0.002)b (82.000 | 0.003)b (81.000 | 0.003)b (80.000 | 0.003)b (79.000 | 0.003)b (78.000 | 0.003)b (77.000 | 0.003)b (76.000 | 0.003)b (75.000 | 0.003)b (74.000 | 0.003)b (73.000 | 0.003)b (72.000 | 0.003)b (71.000 | 0.003)b (70.000 | 0.003)b (69.000 | 0.003)b (68.000 | 0.003)b (67.000 | 0.003)b (66.000 | 0.003)b (65.000 | 0.003)b (64.000 | 0.003)b (63.000 | 0.003)b (62.000 | 0.003)b (61.000 | 0.003)b (60.000 | 0.003)b (59.000 | 0.003)b (58.000 | 0.003)b (57.000 | 0.003)b (56.000 | 0.003)b (55.000 | 0.003)b (54.000 | 0.003)b (53.000 | 0.003)b (52.000 | 0.003)b (51.000 | 0.003)b (50.000 | 0.004)b (49.000 | 0.005)b (48.000 | 0.005)b (47.000 | 0.005)b (46.000 | 0.005)b (45.000 | 0.005)b (44.000 | 0.005)b (43.000 | 0.005)b (42.000 | 0.006)b (41.000 | 0.007)b (40.000 | 0.007)b (39.000 | 0.007)b (38.000 | 0.007)b (37.000 | 0.007)b (36.000 | 0.007)b (35.000 | 0.007)b (34.000 | 0.007)b (33.000 | 0.007)b (32.000 | 0.007)b (31.000 | 0.007)b (30.000 | 0.007)b (29.000 | 0.007)b (28.000 | 0.008)b (27.000 | 0.008)b (26.000 | 0.008)b (25.000 | 0.008)b (24.000 | 0.009)b (23.000 | 0.010)b (22.000 | 0.010)b (21.000 | 0.010)b (20.000 | 0.011)b (19.000 | 0.011)b (18.000 | 0.012)b (17.000 | 0.012)b (16.000 | 0.012)b (15.000 | 0.012)b (14.000 | 0.012)b (13.000 | 0.012)b (12.000 | 0.012)b (11.000 | 0.012)b (10.000 | 0.012)b (9.000 | 0.012)b (8.000 | 0.012)b (7.000 | 0.012)b (6.000 | 0.012)b (5.000 | 0.012)b (4.000 | 0.012)b (3.000 | 0.012)b (2.000 | 0.012)b (1.000 | 0.012);;;; Solution Found
; States evaluated: 1831
; Cost: 0.012
0.000: (drive-truck truck45 city9-1 city9-2 city9) 
0.000: (load-airplane package21 plane8 city9-2) 
0.000: (load-airplane package12 plane8 city9-2) 
0.000: (load-airplane package15 plane6 city1-2) 
0.000: (load-airplane package28 plane4 city4-2) 
0.000: (load-airplane package18 plane2 city2-2) 
0.000: (load-airplane package16 plane5 city2-2) 
0.000: (load-airplane package25 plane1 city6-2) 
0.000: (load-airplane package20 plane1 city6-2) 
0.000: (load-airplane package11 plane1 city6-2) 
0.000: (load-truck package13 truck33 city7-1) 
0.000: (load-truck package29 truck25 city10-1) 
0.000: (load-truck package26 truck24 city6-1) 
0.000: (load-truck package23 truck2 city1-1) 
0.000: (load-truck package17 truck18 city4-2) 
0.000: (load-truck package27 truck12 city10-2) 
0.000: (drive-truck truck47 city11-1 city11-2 city11) 
0.000: (fly-airplane plane3 city11-2 city7-2) 
0.000: (fly-airplane plane7 city4-2 city10-2) 
0.001: (fly-airplane plane8 city9-2 city11-2) 
0.001: (fly-airplane plane6 city1-2 city2-2) 
0.001: (fly-airplane plane4 city4-2 city11-2) 
0.001: (fly-airplane plane2 city2-2 city9-2) 
0.001: (fly-airplane plane5 city2-2 city11-2) 
0.001: (drive-truck truck33 city7-1 city7-2 city7) 
0.001: (drive-truck truck25 city10-1 city10-2 city10) 
0.001: (drive-truck truck24 city6-1 city6-2 city6) 
0.001: (drive-truck truck12 city10-2 city10-1 city10) 
0.001: (drive-truck truck18 city4-2 city4-1 city4) 
0.001: (drive-truck truck2 city1-1 city1-2 city1) 
0.001: (drive-truck truck45 city9-2 city9-1 city9) 
0.002: (unload-airplane package12 plane8 city11-2) 
0.002: (unload-airplane package15 plane6 city2-2) 
0.002: (unload-airplane package28 plane4 city11-2) 
0.002: (unload-airplane package18 plane2 city9-2) 
0.002: (unload-airplane package16 plane5 city11-2) 
0.002: (unload-truck package13 truck33 city7-2) 
0.002: (unload-truck package29 truck25 city10-2) 
0.002: (unload-truck package26 truck24 city6-2) 
0.002: (unload-truck package27 truck12 city10-1) 
0.002: (unload-truck package17 truck18 city4-1) 
0.002: (unload-truck package23 truck2 city1-2) 
0.002: (load-truck package22 truck45 city9-1) 
0.002: (load-truck package19 truck45 city9-1) 
0.002: (load-truck package14 truck45 city9-1) 
0.003: (fly-airplane plane8 city11-2 city1-2) 
0.003: (load-airplane package26 plane1 city6-2) 
0.003: (load-truck package18 truck22 city9-2) 
0.003: (load-truck package16 truck47 city11-2) 
0.003: (drive-truck truck45 city9-1 city9-2 city9) 
0.003: (load-airplane package29 plane7 city10-2) 
0.003: (load-airplane package13 plane3 city7-2) 
0.003: (drive-truck truck18 city4-1 city4-2 city4) 
0.003: (fly-airplane plane4 city11-2 city10-2) 
0.003: (fly-airplane plane5 city11-2 city10-2) 
0.003: (fly-airplane plane6 city2-2 city10-2) 
0.004: (unload-airplane package21 plane8 city1-2) 
0.004: (drive-truck truck22 city9-2 city9-1 city9) 
0.004: (drive-truck truck47 city11-2 city11-1 city11) 
0.004: (unload-truck package22 truck45 city9-2) 
0.004: (unload-truck package19 truck45 city9-2) 
0.004: (unload-truck package14 truck45 city9-2) 
0.004: (fly-airplane plane1 city6-2 city9-2) 
0.004: (fly-airplane plane3 city7-2 city10-2) 
0.004: (fly-airplane plane7 city10-2 city5-2) 
0.004: (load-airplane package23 plane8 city1-2) 
0.005: (unload-truck package18 truck22 city9-1) 
0.005: (unload-truck package16 truck47 city11-1) 
0.005: (load-truck package21 truck2 city1-2) 
0.005: (unload-airplane package26 plane1 city9-2) 
0.005: (load-airplane package22 plane2 city9-2) 
0.005: (load-airplane package19 plane2 city9-2) 
0.005: (load-airplane package14 plane2 city9-2) 
0.005: (unload-airplane package13 plane3 city10-2) 
0.005: (unload-airplane package29 plane7 city5-2) 
0.005: (fly-airplane plane8 city1-2 city10-2) 
0.006: (drive-truck truck2 city1-2 city1-1 city1) 
0.006: (fly-airplane plane1 city9-2 city4-2) 
0.006: (fly-airplane plane2 city9-2 city8-2) 
0.006: (drive-truck truck22 city9-1 city9-2 city9) 
0.006: (load-truck package26 truck45 city9-2) 
0.006: (unload-airplane package23 plane8 city10-2) 
0.007: (unload-truck package21 truck2 city1-1) 
0.007: (unload-airplane package14 plane2 city8-2) 
0.007: (unload-airplane package25 plane1 city4-2) 
0.007: (unload-airplane package20 plane1 city4-2) 
0.007: (drive-truck truck45 city9-2 city9-1 city9) 
0.008: (fly-airplane plane2 city8-2 city6-2) 
0.008: (fly-airplane plane1 city4-2 city2-2) 
0.008: (load-truck package25 truck18 city4-2) 
0.008: (load-truck package20 truck18 city4-2) 
0.008: (unload-truck package26 truck45 city9-1) 
0.009: (unload-airplane package19 plane2 city6-2) 
0.009: (unload-airplane package11 plane1 city2-2) 
0.009: (drive-truck truck18 city4-2 city4-1 city4) 
0.010: (fly-airplane plane2 city6-2 city1-2) 
0.010: (load-truck package19 truck14 city6-2) 
0.010: (unload-truck package25 truck18 city4-1) 
0.010: (unload-truck package20 truck18 city4-1) 
0.011: (drive-truck truck14 city6-2 city6-1 city6) 
0.011: (unload-airplane package22 plane2 city1-2) 
0.012: (unload-truck package19 truck14 city6-1) 

 Execution time of Domain\Problem 14 
00:00:45.7070208

 DOMAIN/PROBLEM 15 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile15.pddl
Number of literals: 801
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 85.000
b (84.000 | 0.000)b (83.000 | 0.001)b (82.000 | 0.002)b (81.000 | 0.002)b (80.000 | 0.002)b (79.000 | 0.002)b (78.000 | 0.002)b (77.000 | 0.002)b (76.000 | 0.002)b (75.000 | 0.002)b (74.000 | 0.002)b (73.000 | 0.002)b (72.000 | 0.003)b (71.000 | 0.005)b (70.000 | 0.006)b (69.000 | 0.007)b (68.000 | 0.007)b (67.000 | 0.007)b (66.000 | 0.007)b (65.000 | 0.007)b (64.000 | 0.007)b (63.000 | 0.007)b (62.000 | 0.007)b (61.000 | 0.007)b (60.000 | 0.007)b (59.000 | 0.007)b (58.000 | 0.007)b (57.000 | 0.008)b (56.000 | 0.009)b (55.000 | 0.010)b (54.000 | 0.011)b (53.000 | 0.011)b (52.000 | 0.011)b (51.000 | 0.011)b (50.000 | 0.011)b (49.000 | 0.012)b (48.000 | 0.013)b (47.000 | 0.014)b (46.000 | 0.015)b (45.000 | 0.015)b (44.000 | 0.015)b (43.000 | 0.015)b (42.000 | 0.015)b (41.000 | 0.016)b (40.000 | 0.016)b (39.000 | 0.016)b (38.000 | 0.016)b (37.000 | 0.016)b (36.000 | 0.017)b (35.000 | 0.018)b (34.000 | 0.018)b (33.000 | 0.019)b (32.000 | 0.019)b (31.000 | 0.019)b (30.000 | 0.019)b (29.000 | 0.019)b (28.000 | 0.020)b (27.000 | 0.020)b (26.000 | 0.020)b (25.000 | 0.020)b (24.000 | 0.020)b (23.000 | 0.020)b (22.000 | 0.020)b (21.000 | 0.020)b (20.000 | 0.020)b (19.000 | 0.020)b (18.000 | 0.020)b (17.000 | 0.020)b (16.000 | 0.020)b (15.000 | 0.020)b (14.000 | 0.020)b (13.000 | 0.020)b (12.000 | 0.020)b (11.000 | 0.020)b (10.000 | 0.020)b (9.000 | 0.021)b (8.000 | 0.021)b (7.000 | 0.021)b (6.000 | 0.021)b (5.000 | 0.022)b (4.000 | 0.022)b (3.000 | 0.022)b (2.000 | 0.023)b (1.000 | 0.024);;;; Solution Found
; States evaluated: 3349
; Cost: 0.024
0.000: (load-airplane package12 plane2 city1-6) 
0.000: (load-truck package13 truck5 city3-4) 
0.000: (load-truck package20 truck4 city2-3) 
0.000: (load-truck package17 truck4 city2-3) 
0.000: (load-truck package23 truck3 city1-5) 
0.000: (drive-truck truck2 city1-3 city1-6 city1) 
0.000: (drive-truck truck1 city1-6 city1-4 city1) 
0.001: (fly-airplane plane2 city1-6 city3-6) 
0.001: (drive-truck truck5 city3-4 city3-6 city3) 
0.001: (drive-truck truck3 city1-5 city1-6 city1) 
0.001: (drive-truck truck4 city2-3 city2-6 city2) 
0.001: (load-truck package11 truck1 city1-4) 
0.002: (unload-airplane package12 plane2 city3-6) 
0.002: (unload-truck package13 truck5 city3-6) 
0.002: (unload-truck package23 truck3 city1-6) 
0.002: (unload-truck package20 truck4 city2-6) 
0.002: (unload-truck package17 truck4 city2-6) 
0.002: (drive-truck truck1 city1-4 city1-2 city1) 
0.003: (load-truck package12 truck5 city3-6) 
0.003: (load-airplane package20 plane1 city2-6) 
0.003: (load-airplane package17 plane1 city2-6) 
0.003: (drive-truck truck4 city2-6 city2-4 city2) 
0.003: (load-truck package19 truck1 city1-2) 
0.003: (load-airplane package13 plane3 city3-6) 
0.003: (load-airplane package23 plane4 city1-6) 
0.004: (drive-truck truck5 city3-6 city3-5 city3) 
0.004: (load-truck package21 truck4 city2-4) 
0.004: (drive-truck truck1 city1-2 city1-3 city1) 
0.005: (load-truck package16 truck5 city3-5) 
0.005: (drive-truck truck4 city2-4 city2-6 city2) 
0.005: (unload-truck package19 truck1 city1-3) 
0.006: (drive-truck truck5 city3-5 city3-6 city3) 
0.006: (unload-truck package21 truck4 city2-6) 
0.006: (drive-truck truck1 city1-3 city1-6 city1) 
0.007: (unload-truck package16 truck5 city3-6) 
0.007: (load-airplane package21 plane1 city2-6) 
0.007: (drive-truck truck4 city2-6 city2-2 city2) 
0.007: (unload-truck package11 truck1 city1-6) 
0.008: (load-truck package15 truck4 city2-2) 
0.008: (drive-truck truck5 city3-6 city3-3 city3) 
0.008: (drive-truck truck1 city1-6 city1-5 city1) 
0.008: (load-airplane package16 plane2 city3-6) 
0.008: (load-airplane package11 plane4 city1-6) 
0.009: (drive-truck truck4 city2-2 city2-6 city2) 
0.009: (load-truck package18 truck5 city3-3) 
0.009: (load-truck package10 truck5 city3-3) 
0.009: (fly-airplane plane2 city3-6 city1-6) 
0.009: (fly-airplane plane4 city1-6 city2-6) 
0.009: (drive-truck truck1 city1-5 city1-6 city1) 
0.010: (unload-truck package15 truck4 city2-6) 
0.010: (drive-truck truck5 city3-3 city3-6 city3) 
0.010: (unload-airplane package16 plane2 city1-6) 
0.010: (unload-airplane package23 plane4 city2-6) 
0.010: (unload-airplane package11 plane4 city2-6) 
0.011: (load-airplane package15 plane1 city2-6) 
0.011: (unload-truck package18 truck5 city3-6) 
0.011: (unload-truck package10 truck5 city3-6) 
0.011: (drive-truck truck4 city2-6 city2-1 city2) 
0.011: (fly-airplane plane2 city1-6 city3-6) 
0.011: (load-truck package16 truck1 city1-6) 
0.012: (load-truck package14 truck4 city2-1) 
0.012: (drive-truck truck5 city3-6 city3-2 city3) 
0.012: (load-airplane package18 plane3 city3-6) 
0.012: (load-airplane package10 plane3 city3-6) 
0.013: (drive-truck truck4 city2-1 city2-6 city2) 
0.013: (unload-truck package12 truck5 city3-2) 
0.013: (fly-airplane plane3 city3-6 city2-6) 
0.014: (unload-truck package14 truck4 city2-6) 
0.014: (drive-truck truck5 city3-2 city3-6 city3) 
0.014: (unload-airplane package13 plane3 city2-6) 
0.014: (load-truck package23 truck4 city2-6) 
0.015: (load-airplane package14 plane1 city2-6) 
0.015: (fly-airplane plane3 city2-6 city1-6) 
0.015: (load-truck package13 truck4 city2-6) 
0.016: (fly-airplane plane1 city2-6 city3-6) 
0.016: (drive-truck truck4 city2-6 city2-5 city2) 
0.016: (unload-airplane package18 plane3 city1-6) 
0.016: (unload-airplane package10 plane3 city1-6) 
0.017: (unload-airplane package15 plane1 city3-6) 
0.017: (unload-truck package13 truck4 city2-5) 
0.017: (load-truck package18 truck1 city1-6) 
0.018: (fly-airplane plane1 city3-6 city1-6) 
0.018: (load-truck package15 truck5 city3-6) 
0.018: (drive-truck truck4 city2-5 city2-3 city2) 
0.019: (drive-truck truck5 city3-6 city3-3 city3) 
0.019: (unload-airplane package21 plane1 city1-6) 
0.019: (unload-airplane package20 plane1 city1-6) 
0.019: (unload-airplane package17 plane1 city1-6) 
0.019: (unload-airplane package14 plane1 city1-6) 
0.019: (unload-truck package23 truck4 city2-3) 
0.020: (unload-truck package15 truck5 city3-3) 
0.020: (load-truck package20 truck1 city1-6) 
0.020: (load-truck package17 truck2 city1-6) 
0.020: (load-truck package14 truck2 city1-6) 
0.021: (drive-truck truck1 city1-6 city1-3 city1) 
0.021: (drive-truck truck2 city1-6 city1-5 city1) 
0.022: (unload-truck package17 truck2 city1-5) 
0.022: (unload-truck package14 truck2 city1-5) 
0.022: (unload-truck package18 truck1 city1-3) 
0.023: (drive-truck truck1 city1-3 city1-1 city1) 
0.024: (unload-truck package20 truck1 city1-1) 
0.024: (unload-truck package16 truck1 city1-1) 

 Execution time of Domain\Problem 15 
00:00:10.6151822

 DOMAIN/PROBLEM 16 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile16.pddl
Number of literals: 2545
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 55.000
b (54.000 | 0.000)b (53.000 | 0.000)b (52.000 | 0.000)b (51.000 | 0.000)b (50.000 | 0.001)b (49.000 | 0.001)b (48.000 | 0.001)b (47.000 | 0.001)b (46.000 | 0.001)b (45.000 | 0.002)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.002)b (40.000 | 0.002)b (39.000 | 0.003)b (38.000 | 0.003)b (37.000 | 0.003)b (36.000 | 0.003)b (35.000 | 0.004)b (34.000 | 0.004)b (33.000 | 0.004)b (32.000 | 0.004)b (31.000 | 0.004)b (30.000 | 0.004)b (29.000 | 0.004)b (28.000 | 0.005)b (27.000 | 0.006)b (26.000 | 0.007)b (25.000 | 0.008)b (24.000 | 0.008)b (23.000 | 0.008)b (22.000 | 0.008)b (21.000 | 0.008)b (20.000 | 0.008)b (19.000 | 0.008)b (18.000 | 0.008)b (17.000 | 0.008)b (16.000 | 0.009)b (15.000 | 0.010)b (14.000 | 0.010)b (13.000 | 0.010)b (12.000 | 0.010)b (11.000 | 0.010)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.011)b (1.000 | 0.012);;;; Solution Found
; States evaluated: 471
; Cost: 0.012
0.000: (fly-airplane plane1 city6-3 city7-3) 
0.000: (drive-truck truck45 city4-1 city4-3 city4) 
0.000: (load-truck package5 truck52 city11-2) 
0.000: (load-truck package4 truck31 city2-2) 
0.000: (load-truck package3 truck26 city7-2) 
0.000: (load-truck package2 truck26 city7-2) 
0.000: (load-truck package1 truck26 city7-2) 
0.000: (load-truck package6 truck24 city4-1) 
0.000: (fly-airplane plane10 city1-3 city2-3) 
0.000: (fly-airplane plane12 city5-3 city4-3) 
0.000: (drive-truck truck1 city5-2 city5-3 city5) 
0.001: (drive-truck truck31 city2-2 city2-1 city2) 
0.001: (drive-truck truck26 city7-2 city7-3 city7) 
0.001: (drive-truck truck24 city4-1 city4-2 city4) 
0.001: (drive-truck truck52 city11-2 city11-3 city11) 
0.002: (unload-truck package3 truck26 city7-3) 
0.002: (unload-truck package2 truck26 city7-3) 
0.002: (unload-truck package1 truck26 city7-3) 
0.002: (load-truck package7 truck24 city4-2) 
0.002: (drive-truck truck31 city2-1 city2-3 city2) 
0.002: (unload-truck package5 truck52 city11-3) 
0.003: (drive-truck truck24 city4-2 city4-3 city4) 
0.003: (load-airplane package3 plane1 city7-3) 
0.003: (load-airplane package2 plane1 city7-3) 
0.003: (load-airplane package1 plane1 city7-3) 
0.003: (unload-truck package4 truck31 city2-3) 
0.003: (load-airplane package5 plane11 city11-3) 
0.004: (unload-truck package7 truck24 city4-3) 
0.004: (unload-truck package6 truck24 city4-3) 
0.004: (fly-airplane plane11 city11-3 city2-3) 
0.004: (fly-airplane plane1 city7-3 city5-3) 
0.004: (load-airplane package4 plane10 city2-3) 
0.005: (unload-airplane package5 plane11 city2-3) 
0.005: (fly-airplane plane10 city2-3 city5-3) 
0.005: (unload-airplane package3 plane1 city5-3) 
0.005: (unload-airplane package2 plane1 city5-3) 
0.005: (load-airplane package7 plane12 city4-3) 
0.005: (load-airplane package6 plane12 city4-3) 
0.006: (load-truck package5 truck31 city2-3) 
0.006: (fly-airplane plane1 city5-3 city4-3) 
0.006: (unload-airplane package4 plane10 city5-3) 
0.006: (fly-airplane plane12 city4-3 city5-3) 
0.006: (load-truck package3 truck1 city5-3) 
0.006: (load-truck package2 truck1 city5-3) 
0.007: (drive-truck truck31 city2-3 city2-1 city2) 
0.007: (unload-airplane package1 plane1 city4-3) 
0.007: (unload-airplane package7 plane12 city5-3) 
0.007: (unload-airplane package6 plane12 city5-3) 
0.008: (unload-truck package5 truck31 city2-1) 
0.008: (load-truck package1 truck17 city4-3) 
0.008: (load-truck package7 truck1 city5-3) 
0.008: (load-truck package6 truck1 city5-3) 
0.009: (drive-truck truck17 city4-3 city4-1 city4) 
0.009: (drive-truck truck1 city5-3 city5-1 city5) 
0.010: (unload-truck package1 truck17 city4-1) 
0.010: (unload-truck package7 truck1 city5-1) 
0.010: (unload-truck package3 truck1 city5-1) 
0.011: (drive-truck truck1 city5-1 city5-2 city5) 
0.012: (unload-truck package6 truck1 city5-2) 
0.012: (unload-truck package2 truck1 city5-2) 

 Execution time of Domain\Problem 16 
00:00:07.5685220

 DOMAIN/PROBLEM 17 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile17.pddl
Number of literals: 1490
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 43.000
b (42.000 | 0.000)b (41.000 | 0.001)b (40.000 | 0.002)b (39.000 | 0.002)b (38.000 | 0.002)b (37.000 | 0.002)b (36.000 | 0.002)b (35.000 | 0.002)b (34.000 | 0.002)b (33.000 | 0.002)b (32.000 | 0.002)b (31.000 | 0.002)b (30.000 | 0.002)b (29.000 | 0.002)b (28.000 | 0.002)b (27.000 | 0.002)b (26.000 | 0.002)b (25.000 | 0.003)b (24.000 | 0.003)b (23.000 | 0.004)b (22.000 | 0.005)b (21.000 | 0.005)b (20.000 | 0.005)b (19.000 | 0.005)b (18.000 | 0.005)b (17.000 | 0.005)b (16.000 | 0.005)b (15.000 | 0.006)b (14.000 | 0.007)b (13.000 | 0.007)b (12.000 | 0.007)b (11.000 | 0.007)b (10.000 | 0.008)b (9.000 | 0.009)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 255
; Cost: 0.010
0.000: (load-airplane package11 plane2 city3-3) 
0.000: (load-truck package14 truck16 city3-1) 
0.000: (load-truck package9 truck15 city3-2) 
0.000: (load-truck package13 truck11 city1-1) 
0.000: (load-truck package12 truck11 city1-1) 
0.000: (load-truck package15 truck10 city1-2) 
0.000: (fly-airplane plane1 city4-3 city3-3) 
0.000: (fly-airplane plane3 city4-3 city1-3) 
0.000: (drive-truck truck1 city4-2 city4-3 city4) 
0.000: (drive-truck truck12 city2-1 city2-3 city2) 
0.001: (fly-airplane plane2 city3-3 city2-3) 
0.001: (drive-truck truck16 city3-1 city3-3 city3) 
0.001: (drive-truck truck15 city3-2 city3-3 city3) 
0.001: (drive-truck truck11 city1-1 city1-3 city1) 
0.001: (drive-truck truck10 city1-2 city1-3 city1) 
0.002: (unload-airplane package11 plane2 city2-3) 
0.002: (unload-truck package14 truck16 city3-3) 
0.002: (unload-truck package9 truck15 city3-3) 
0.002: (unload-truck package13 truck11 city1-3) 
0.002: (unload-truck package12 truck11 city1-3) 
0.002: (unload-truck package15 truck10 city1-3) 
0.003: (load-airplane package9 plane1 city3-3) 
0.003: (load-airplane package14 plane1 city3-3) 
0.003: (load-airplane package15 plane3 city1-3) 
0.003: (load-airplane package13 plane3 city1-3) 
0.003: (load-airplane package12 plane3 city1-3) 
0.003: (load-truck package11 truck12 city2-3) 
0.004: (fly-airplane plane1 city3-3 city4-3) 
0.004: (fly-airplane plane3 city1-3 city2-3) 
0.005: (unload-airplane package9 plane1 city4-3) 
0.005: (unload-airplane package14 plane1 city4-3) 
0.005: (unload-airplane package12 plane3 city2-3) 
0.006: (fly-airplane plane3 city2-3 city4-3) 
0.006: (load-truck package9 truck1 city4-3) 
0.006: (load-truck package14 truck1 city4-3) 
0.006: (load-truck package12 truck12 city2-3) 
0.007: (unload-airplane package15 plane3 city4-3) 
0.007: (unload-airplane package13 plane3 city4-3) 
0.007: (drive-truck truck12 city2-3 city2-2 city2) 
0.008: (load-truck package15 truck1 city4-3) 
0.008: (unload-truck package12 truck12 city2-2) 
0.009: (drive-truck truck1 city4-3 city4-2 city4) 
0.009: (drive-truck truck12 city2-2 city2-1 city2) 
0.010: (unload-truck package9 truck1 city4-2) 
0.010: (unload-truck package15 truck1 city4-2) 
0.010: (unload-truck package14 truck1 city4-2) 
0.010: (unload-truck package11 truck12 city2-1) 

 Execution time of Domain\Problem 17 
00:00:05.6832478

 DOMAIN/PROBLEM 18 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile18.pddl
Number of literals: 5600
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 157.000
b (156.000 | 0.000)b (155.000 | 0.000)b (154.000 | 0.000)b (153.000 | 0.000)b (152.000 | 0.000)b (151.000 | 0.000)b (150.000 | 0.000)b (149.000 | 0.000)b (148.000 | 0.000)b (147.000 | 0.001)b (146.000 | 0.002)b (145.000 | 0.003)b (144.000 | 0.003)b (143.000 | 0.003)b (142.000 | 0.003)b (141.000 | 0.003)b (140.000 | 0.003)b (139.000 | 0.003)b (138.000 | 0.003)b (137.000 | 0.003)b (136.000 | 0.003)b (135.000 | 0.003)b (134.000 | 0.003)b (133.000 | 0.003)b (132.000 | 0.003)b (131.000 | 0.003)b (130.000 | 0.003)b (129.000 | 0.003)b (128.000 | 0.003)b (127.000 | 0.003)b (126.000 | 0.004)b (125.000 | 0.004)b (124.000 | 0.004)b (123.000 | 0.004)b (122.000 | 0.004)b (121.000 | 0.004)b (120.000 | 0.004)b (119.000 | 0.004)b (118.000 | 0.004)b (117.000 | 0.005)b (116.000 | 0.006)b (115.000 | 0.007)b (114.000 | 0.007)b (113.000 | 0.007)b (112.000 | 0.007)b (111.000 | 0.007)b (110.000 | 0.007)b (109.000 | 0.007)b (108.000 | 0.007)b (107.000 | 0.007)b (106.000 | 0.007)b (105.000 | 0.007)b (104.000 | 0.007)b (103.000 | 0.007)b (102.000 | 0.007)b (101.000 | 0.007)b (100.000 | 0.007)b (99.000 | 0.007)b (98.000 | 0.007)b (97.000 | 0.007)b (96.000 | 0.008)b (95.000 | 0.009)b (94.000 | 0.009)b (93.000 | 0.009)b (92.000 | 0.009)b (91.000 | 0.009)b (90.000 | 0.009)b (89.000 | 0.009)b (88.000 | 0.009)b (87.000 | 0.009)b (86.000 | 0.009)b (85.000 | 0.009)b (84.000 | 0.009)b (83.000 | 0.009)b (82.000 | 0.009)b (81.000 | 0.009)b (80.000 | 0.009)b (79.000 | 0.009)b (78.000 | 0.009)b (77.000 | 0.009)b (76.000 | 0.009)b (75.000 | 0.009)b (74.000 | 0.009)b (73.000 | 0.009)b (72.000 | 0.009)b (71.000 | 0.009)b (70.000 | 0.009)b (69.000 | 0.009)b (68.000 | 0.009)b (67.000 | 0.009)b (66.000 | 0.009)b (65.000 | 0.010)b (64.000 | 0.010)b (63.000 | 0.011)b (62.000 | 0.012)b (61.000 | 0.012)b (60.000 | 0.012)b (59.000 | 0.012)b (58.000 | 0.012)b (57.000 | 0.012)b (56.000 | 0.012)b (55.000 | 0.012)b (54.000 | 0.012)b (53.000 | 0.012)b (52.000 | 0.012)b (51.000 | 0.012)b (50.000 | 0.012)b (49.000 | 0.012)b (48.000 | 0.012)b (47.000 | 0.012)b (46.000 | 0.012)b (45.000 | 0.012)b (44.000 | 0.012)b (43.000 | 0.012)b (42.000 | 0.012)b (41.000 | 0.012)b (40.000 | 0.013)b (39.000 | 0.013)b (38.000 | 0.013)b (37.000 | 0.013)b (36.000 | 0.013)b (35.000 | 0.013)b (34.000 | 0.013)b (33.000 | 0.013)b (32.000 | 0.014)b (31.000 | 0.015)b (30.000 | 0.015)

 Execution time of Domain\Problem 18 
00:15:57.2094942

 DOMAIN/PROBLEM 19 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile19.pddl
Number of literals: 4003
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 139.000
b (138.000 | 0.000)b (137.000 | 0.000)b (135.000 | 0.000)b (134.000 | 0.000)b (133.000 | 0.000)b (132.000 | 0.000)b (131.000 | 0.000)b (129.000 | 0.000)b (128.000 | 0.001)b (127.000 | 0.001)b (126.000 | 0.001)b (125.000 | 0.002)b (124.000 | 0.002)b (123.000 | 0.002)b (122.000 | 0.002)b (121.000 | 0.002)b (120.000 | 0.002)b (119.000 | 0.002)b (118.000 | 0.002)b (117.000 | 0.003)b (116.000 | 0.003)b (115.000 | 0.003)b (114.000 | 0.003)b (113.000 | 0.004)b (112.000 | 0.005)b (111.000 | 0.006)b (110.000 | 0.007)b (109.000 | 0.008)b (108.000 | 0.008)b (107.000 | 0.008)b (106.000 | 0.008)b (105.000 | 0.008)b (104.000 | 0.008)b (103.000 | 0.008)b (102.000 | 0.008)b (101.000 | 0.008)b (100.000 | 0.008)b (99.000 | 0.008)b (98.000 | 0.008)b (97.000 | 0.008)b (96.000 | 0.008)b (95.000 | 0.008)b (94.000 | 0.008)b (93.000 | 0.008)b (92.000 | 0.008)b (91.000 | 0.008)b (90.000 | 0.008)b (89.000 | 0.008)b (88.000 | 0.008)b (87.000 | 0.008)b (86.000 | 0.008)b (85.000 | 0.008)b (84.000 | 0.008)b (83.000 | 0.008)b (82.000 | 0.008)b (81.000 | 0.008)b (80.000 | 0.008)b (79.000 | 0.008)b (78.000 | 0.008)b (77.000 | 0.008)b (76.000 | 0.008)b (75.000 | 0.008)b (74.000 | 0.008)b (73.000 | 0.008)b (72.000 | 0.008)b (71.000 | 0.008)b (70.000 | 0.008)

 Execution time of Domain\Problem 19 
00:08:37.8219413

 DOMAIN/PROBLEM 20 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile20.pddl
Number of literals: 7069
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m
Initial heuristic = 130.000
b (129.000 | 0.000)b (128.000 | 0.000)b (127.000 | 0.000)b (126.000 | 0.000)b (125.000 | 0.000)b (124.000 | 0.000)b (123.000 | 0.000)b (122.000 | 0.000)b (121.000 | 0.000)b (120.000 | 0.001)b (119.000 | 0.002)b (118.000 | 0.002)b (117.000 | 0.002)b (116.000 | 0.002)b (115.000 | 0.002)b (114.000 | 0.002)b (113.000 | 0.002)b (112.000 | 0.002)b (111.000 | 0.002)b (110.000 | 0.002)b (109.000 | 0.002)b (108.000 | 0.002)b (107.000 | 0.002)b (106.000 | 0.002)b (105.000 | 0.003)b (104.000 | 0.003)b (103.000 | 0.003)b (102.000 | 0.003)b (101.000 | 0.003)b (100.000 | 0.003)b (99.000 | 0.004)b (98.000 | 0.004)b (97.000 | 0.005)b (96.000 | 0.005)b (95.000 | 0.006)b (94.000 | 0.006)b (93.000 | 0.006)b (92.000 | 0.006)b (91.000 | 0.006)b (90.000 | 0.006)b (89.000 | 0.006)b (88.000 | 0.007)b (87.000 | 0.008)b (86.000 | 0.009)b (85.000 | 0.009)b (84.000 | 0.009)b (83.000 | 0.009)b (82.000 | 0.009)b (81.000 | 0.009)b (80.000 | 0.009)b (79.000 | 0.009)b (78.000 | 0.009)b (77.000 | 0.009)b (76.000 | 0.009)b (75.000 | 0.009)b (74.000 | 0.009)b (73.000 | 0.009)b (72.000 | 0.009)b (71.000 | 0.009)b (70.000 | 0.009)b (69.000 | 0.009)b (68.000 | 0.009)b (67.000 | 0.009)b (66.000 | 0.009)b (65.000 | 0.009)b (64.000 | 0.009)b (63.000 | 0.009)b (62.000 | 0.009)b (61.000 | 0.009)b (60.000 | 0.009)b (59.000 | 0.009)b (58.000 | 0.009)b (57.000 | 0.009)b (56.000 | 0.009)b (55.000 | 0.009)b (54.000 | 0.009)b (53.000 | 0.010)b (52.000 | 0.010)b (51.000 | 0.011)b (50.000 | 0.011)b (49.000 | 0.011)b (48.000 | 0.011)b (47.000 | 0.011)b (46.000 | 0.011)b (45.000 | 0.011)b (44.000 | 0.011)b (43.000 | 0.011)b (42.000 | 0.011)b (41.000 | 0.011)b (40.000 | 0.011)b (39.000 | 0.011)b (38.000 | 0.011)b (37.000 | 0.011)b (36.000 | 0.011)b (35.000 | 0.011)b (34.000 | 0.011)b (33.000 | 0.011)b (32.000 | 0.011)b (31.000 | 0.011)b (30.000 | 0.011)b (29.000 | 0.011)b (28.000 | 0.011)b (27.000 | 0.011)b (26.000 | 0.013)b (25.000 | 0.015)b (24.000 | 0.016)b (23.000 | 0.016)b (22.000 | 0.016)b (21.000 | 0.016)b (20.000 | 0.016)b (19.000 | 0.016)b (18.000 | 0.016)b (17.000 | 0.016)b (16.000 | 0.016)b (15.000 | 0.016)b (14.000 | 0.016)b (13.000 | 0.016)b (12.000 | 0.016)b (11.000 | 0.018)b (10.000 | 0.019)b (9.000 | 0.020)b (8.000 | 0.020)b (7.000 | 0.020)b (6.000 | 0.020)b (5.000 | 0.020)b (4.000 | 0.020)b (3.000 | 0.020)b (2.000 | 0.020)b (1.000 | 0.020);;;; Solution Found
; States evaluated: 15694
; Cost: 0.020
0.000: (fly-airplane plane15 city5-12 city1-12) 
0.000: (drive-truck truck13 city9-3 city9-8 city9) 
0.000: (drive-truck truck12 city8-7 city8-5 city8) 
0.000: (drive-truck truck9 city5-11 city5-2 city5) 
0.000: (drive-truck truck7 city3-8 city3-7 city3) 
0.000: (drive-truck truck6 city2-11 city2-3 city2) 
0.000: (drive-truck truck17 city13-6 city13-3 city13) 
0.000: (drive-truck truck16 city12-10 city12-5 city12) 
0.000: (drive-truck truck14 city10-11 city10-1 city10) 
0.000: (load-truck package8 truck4 city4-9) 
0.000: (fly-airplane plane10 city3-12 city5-12) 
0.000: (drive-truck truck11 city7-4 city7-5 city7) 
0.000: (drive-truck truck19 city15-6 city15-5 city15) 
0.000: (fly-airplane plane11 city10-12 city2-12) 
0.000: (fly-airplane plane13 city4-12 city3-12) 
0.000: (fly-airplane plane14 city6-12 city4-12) 
0.000: (fly-airplane plane2 city14-12 city4-12) 
0.000: (fly-airplane plane3 city11-12 city4-12) 
0.000: (fly-airplane plane4 city11-12 city4-12) 
0.000: (drive-truck truck8 city4-9 city4-12 city4) 
0.000: (drive-truck truck2 city10-5 city10-4 city10) 
0.001: (load-airplane package16 plane15 city1-12) 
0.001: (load-truck package13 truck9 city5-2) 
0.001: (load-truck package10 truck6 city2-3) 
0.001: (load-truck package11 truck17 city13-3) 
0.001: (load-truck package3 truck16 city12-5) 
0.001: (load-truck package9 truck14 city10-1) 
0.001: (load-truck package14 truck13 city9-8) 
0.001: (drive-truck truck12 city8-5 city8-1 city8) 
0.001: (load-truck package4 truck11 city7-5) 
0.001: (drive-truck truck7 city3-7 city3-4 city3) 
0.001: (load-truck package12 truck19 city15-5) 
0.001: (fly-airplane plane14 city4-12 city15-12) 
0.001: (fly-airplane plane2 city4-12 city12-12) 
0.001: (fly-airplane plane3 city4-12 city10-12) 
0.001: (drive-truck truck4 city4-9 city4-12 city4) 
0.001: (drive-truck truck2 city10-4 city10-12 city10) 
0.002: (fly-airplane plane15 city1-12 city9-12) 
0.002: (drive-truck truck9 city5-2 city5-12 city5) 
0.002: (drive-truck truck6 city2-3 city2-12 city2) 
0.002: (drive-truck truck17 city13-3 city13-12 city13) 
0.002: (drive-truck truck16 city12-5 city12-12 city12) 
0.002: (drive-truck truck14 city10-1 city10-4 city10) 
0.002: (drive-truck truck13 city9-8 city9-7 city9) 
0.002: (load-truck package5 truck12 city8-1) 
0.002: (drive-truck truck11 city7-5 city7-12 city7) 
0.002: (load-truck package7 truck7 city3-4) 
0.002: (drive-truck truck19 city15-5 city15-12 city15) 
0.002: (unload-truck package8 truck4 city4-12) 
0.003: (unload-airplane package16 plane15 city9-12) 
0.003: (unload-truck package13 truck9 city5-12) 
0.003: (unload-truck package10 truck6 city2-12) 
0.003: (unload-truck package11 truck17 city13-12) 
0.003: (unload-truck package3 truck16 city12-12) 
0.003: (load-truck package2 truck13 city9-7) 
0.003: (drive-truck truck12 city8-1 city8-5 city8) 
0.003: (unload-truck package4 truck11 city7-12) 
0.003: (drive-truck truck7 city3-4 city3-12 city3) 
0.003: (unload-truck package12 truck19 city15-12) 
0.003: (drive-truck truck14 city10-4 city10-12 city10) 
0.003: (load-airplane package8 plane4 city4-12) 
0.004: (drive-truck truck13 city9-7 city9-2 city9) 
0.004: (load-airplane package11 plane1 city13-12) 
0.004: (load-airplane package13 plane10 city5-12) 
0.004: (unload-truck package7 truck7 city3-12) 
0.004: (drive-truck truck16 city12-12 city12-11 city12) 
0.004: (unload-truck package9 truck14 city10-12) 
0.004: (drive-truck truck12 city8-5 city8-10 city8) 
0.004: (load-airplane package10 plane11 city2-12) 
0.004: (load-airplane package4 plane12 city7-12) 
0.004: (load-airplane package12 plane14 city15-12) 
0.004: (fly-airplane plane15 city9-12 city4-12) 
0.004: (load-airplane package3 plane2 city12-12) 
0.004: (fly-airplane plane4 city4-12 city15-12) 
0.005: (fly-airplane plane1 city13-12 city7-12) 
0.005: (load-truck package6 truck13 city9-2) 
0.005: (load-truck package1 truck16 city12-11) 
0.005: (unload-truck package5 truck12 city8-10) 
0.005: (drive-truck truck7 city3-12 city3-10 city3) 
0.005: (fly-airplane plane10 city5-12 city9-12) 
0.005: (fly-airplane plane11 city2-12 city8-12) 
0.005: (fly-airplane plane12 city7-12 city3-12) 
0.005: (load-airplane package7 plane13 city3-12) 
0.005: (fly-airplane plane14 city15-12 city10-12) 
0.005: (fly-airplane plane2 city12-12 city3-12) 
0.005: (load-airplane package9 plane3 city10-12) 
0.005: (unload-airplane package8 plane4 city15-12) 
0.005: (fly-airplane plane15 city4-12 city12-12) 
0.006: (unload-airplane package11 plane1 city7-12) 
0.006: (drive-truck truck12 city8-10 city8-5 city8) 
0.006: (drive-truck truck13 city9-2 city9-12 city9) 
0.006: (drive-truck truck16 city12-11 city12-12 city12) 
0.006: (load-truck package15 truck7 city3-10) 
0.006: (unload-airplane package10 plane11 city8-12) 
0.006: (unload-airplane package4 plane12 city3-12) 
0.006: (fly-airplane plane13 city3-12 city4-12) 
0.006: (unload-airplane package12 plane14 city10-12) 
0.006: (unload-airplane package3 plane2 city3-12) 
0.006: (fly-airplane plane3 city10-12 city4-12) 
0.006: (load-truck package8 truck19 city15-12) 
0.007: (load-truck package11 truck11 city7-12) 
0.007: (fly-airplane plane1 city7-12 city9-12) 
0.007: (unload-truck package6 truck13 city9-12) 
0.007: (unload-truck package2 truck13 city9-12) 
0.007: (unload-truck package14 truck13 city9-12) 
0.007: (unload-truck package1 truck16 city12-12) 
0.007: (load-truck package16 truck13 city9-12) 
0.007: (drive-truck truck7 city3-10 city3-12 city3) 
0.007: (drive-truck truck12 city8-5 city8-12 city8) 
0.007: (unload-airplane package7 plane13 city4-12) 
0.007: (load-truck package12 truck14 city10-12) 
0.007: (unload-airplane package9 plane3 city4-12) 
0.007: (drive-truck truck19 city15-12 city15-6 city15) 
0.007: (fly-airplane plane11 city8-12 city5-12) 
0.007: (fly-airplane plane12 city3-12 city12-12) 
0.007: (fly-airplane plane14 city10-12 city12-12) 
0.008: (drive-truck truck11 city7-12 city7-4 city7) 
0.008: (drive-truck truck13 city9-12 city9-10 city9) 
0.008: (unload-truck package15 truck7 city3-12) 
0.008: (fly-airplane plane1 city9-12 city5-12) 
0.008: (load-airplane package6 plane10 city9-12) 
0.008: (load-airplane package14 plane10 city9-12) 
0.008: (load-truck package4 truck7 city3-12) 
0.008: (load-truck package10 truck12 city8-12) 
0.008: (unload-truck package8 truck19 city15-6) 
0.008: (load-truck package9 truck4 city4-12) 
0.008: (load-truck package7 truck8 city4-12) 
0.008: (drive-truck truck14 city10-12 city10-10 city10) 
0.008: (fly-airplane plane13 city4-12 city12-12) 
0.008: (load-airplane package1 plane15 city12-12) 
0.008: (fly-airplane plane11 city5-12 city3-12) 
0.008: (fly-airplane plane12 city12-12 city5-12) 
0.008: (fly-airplane plane14 city12-12 city5-12) 
0.009: (unload-truck package11 truck11 city7-4) 
0.009: (unload-truck package16 truck13 city9-10) 
0.009: (drive-truck truck12 city8-12 city8-5 city8) 
0.009: (drive-truck truck7 city3-12 city3-11 city3) 
0.009: (drive-truck truck4 city4-12 city4-2 city4) 
0.009: (drive-truck truck8 city4-12 city4-9 city4) 
0.009: (unload-truck package12 truck14 city10-10) 
0.009: (fly-airplane plane10 city9-12 city5-12) 
0.009: (fly-airplane plane15 city12-12 city10-12) 
0.009: (load-airplane package15 plane2 city3-12) 
0.010: (unload-truck package10 truck12 city8-5) 
0.010: (unload-truck package4 truck7 city3-11) 
0.010: (unload-truck package9 truck4 city4-2) 
0.010: (unload-truck package7 truck8 city4-9) 
0.010: (drive-truck truck14 city10-10 city10-4 city10) 
0.010: (unload-airplane package6 plane10 city5-12) 
0.010: (unload-airplane package1 plane15 city10-12) 
0.010: (fly-airplane plane2 city3-12 city5-12) 
0.011: (drive-truck truck7 city3-11 city3-7 city3) 
0.011: (load-truck package6 truck9 city5-12) 
0.011: (fly-airplane plane10 city5-12 city10-12) 
0.011: (drive-truck truck14 city10-4 city10-12 city10) 
0.011: (load-truck package1 truck2 city10-12) 
0.011: (unload-airplane package15 plane2 city5-12) 
0.012: (drive-truck truck7 city3-7 city3-12 city3) 
0.012: (drive-truck truck9 city5-12 city5-10 city5) 
0.012: (unload-airplane package14 plane10 city10-12) 
0.012: (drive-truck truck2 city10-12 city10-10 city10) 
0.013: (load-truck package3 truck7 city3-12) 
0.013: (unload-truck package6 truck9 city5-10) 
0.013: (fly-airplane plane10 city10-12 city3-12) 
0.013: (load-truck package14 truck14 city10-12) 
0.013: (unload-truck package1 truck2 city10-10) 
0.014: (drive-truck truck7 city3-12 city3-7 city3) 
0.014: (unload-airplane package13 plane10 city3-12) 
0.014: (drive-truck truck14 city10-12 city10-4 city10) 
0.015: (unload-truck package3 truck7 city3-7) 
0.015: (unload-truck package14 truck14 city10-4) 
0.016: (drive-truck truck7 city3-7 city3-4 city3) 
0.017: (drive-truck truck7 city3-4 city3-12 city3) 
0.018: (load-truck package13 truck7 city3-12) 
0.019: (drive-truck truck7 city3-12 city3-4 city3) 
0.020: (unload-truck package13 truck7 city3-4) 

 Execution time of Domain\Problem 20 
00:10:13.6302223
