the results of squirrel t1_pt1 tests in popcorn 

 POPCORN-R 


 DOMAIN/PROBLEM 1 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile01.pddl
Number of literals: 36
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 10.000
b (8.000 | 5.000)b (7.000 | 12.001)b (6.000 | 17.001)b (5.000 | 17.001)b (4.000 | 32.001)b (3.000 | 42.002)b (2.000 | 57.004)b (1.000 | 62.004);;;; Solution Found
; States evaluated: 15
; Cost: 72.004
0.000: (calibrate rover0 camera0 objective1 waypoint3)  [5.000]
0.000: (sample_rock rover0 rover0store waypoint3)  [8.000]
5.001: (take_image rover0 waypoint3 objective1 camera0 high_res)  [7.000]
8.001: (drop rover0 rover0store)  [1.000]
12.001: (navigate rover0 waypoint3 waypoint1)  [5.000]
17.001: (communicate_image_data rover0 general objective1 high_res waypoint1 waypoint0)  [15.000]
32.002: (communicate_rock_data rover0 general waypoint3 waypoint1 waypoint0)  [10.000]
42.003: (navigate rover0 waypoint1 waypoint2)  [5.000]
47.004: (sample_soil rover0 rover0store waypoint2)  [10.000]
57.004: (navigate rover0 waypoint2 waypoint1)  [5.000]
62.004: (communicate_soil_data rover0 general waypoint2 waypoint1 waypoint0)  [10.000]

 Execution time of Domain\Problem 1 
00:00:03.0504104

 DOMAIN/PROBLEM 2 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile02.pddl
Number of literals: 30
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 7.000
b (6.000 | 5.000)b (5.000 | 12.001)b (4.000 | 27.002)b (3.000 | 27.002)b (2.000 | 27.002)b (1.000 | 37.003);;;; Solution Found
; States evaluated: 14
; Cost: 47.004
0.000: (calibrate rover0 camera0 objective0 waypoint0)  [5.000]
0.000: (sample_rock rover0 rover0store waypoint0)  [8.000]
5.001: (take_image rover0 waypoint0 objective1 camera0 low_res)  [7.000]
8.001: (drop rover0 rover0store)  [1.000]
9.002: (sample_soil rover0 rover0store waypoint0)  [10.000]
12.002: (communicate_image_data rover0 general objective1 low_res waypoint0 waypoint1)  [15.000]
27.003: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint1)  [10.000]
37.004: (communicate_soil_data rover0 general waypoint0 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 2 
00:00:02.7679031

 DOMAIN/PROBLEM 3 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile03.pddl
Number of literals: 46
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 11.000
b (10.000 | 5.000)b (9.000 | 10.001)b (8.000 | 17.002)b (7.000 | 17.002)b (6.000 | 37.002)b (5.000 | 47.003)b (2.000 | 52.004)b (1.000 | 62.005);;;; Solution Found
; States evaluated: 21
; Cost: 72.006
0.000: (navigate rover1 waypoint3 waypoint0)  [5.000]
5.001: (calibrate rover1 camera1 objective0 waypoint0)  [5.000]
5.001: (sample_rock rover1 rover1store waypoint0)  [8.000]
10.002: (take_image rover1 waypoint0 objective0 camera1 colour)  [7.000]
13.002: (drop rover1 rover1store)  [1.000]
17.002: (navigate rover1 waypoint0 waypoint3)  [5.000]
22.002: (communicate_image_data rover1 general objective0 colour waypoint3 waypoint0)  [15.000]
37.003: (communicate_rock_data rover1 general waypoint0 waypoint3 waypoint0)  [10.000]
47.004: (navigate rover1 waypoint3 waypoint2)  [5.000]
52.005: (sample_soil rover1 rover1store waypoint2)  [10.000]
62.006: (communicate_soil_data rover1 general waypoint2 waypoint2 waypoint0)  [10.000]

 Execution time of Domain\Problem 3 
00:00:02.9559022

 DOMAIN/PROBLEM 4 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile04.pddl
Number of literals: 53
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 9.000
b (8.000 | 5.000)b (7.000 | 10.000)b (6.000 | 15.000)b (5.000 | 15.000)b (4.000 | 17.000)b (3.000 | 18.001)b (2.000 | 25.000)b (1.000 | 40.001);;;; Solution Found
; States evaluated: 10
; Cost: 50.002
0.000: (calibrate rover1 camera0 objective0 waypoint2)  [5.000]
0.000: (sample_soil rover0 rover0store waypoint3)  [10.000]
5.000: (navigate rover1 waypoint2 waypoint1)  [5.000]
10.000: (navigate rover0 waypoint3 waypoint1)  [5.000]
10.000: (take_image rover1 waypoint1 objective0 camera0 high_res)  [7.000]
10.001: (sample_rock rover1 rover1store waypoint1)  [8.000]
15.000: (communicate_soil_data rover0 general waypoint3 waypoint1 waypoint2)  [10.000]
25.001: (communicate_image_data rover1 general objective0 high_res waypoint1 waypoint2)  [15.000]
40.002: (communicate_rock_data rover1 general waypoint1 waypoint1 waypoint2)  [10.000]

 Execution time of Domain\Problem 4 
00:00:03.1683496

 DOMAIN/PROBLEM 5 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile05.pddl
Number of literals: 62
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 19.000
b (17.000 | 5.000)b (16.000 | 9.001)b (15.000 | 12.001)b (14.000 | 27.002)b (13.000 | 37.003)b (12.000 | 37.003)b (11.000 | 37.003)b (10.000 | 52.004)b (9.000 | 67.005)b (8.000 | 80.007)b (7.000 | 85.007)b (6.000 | 95.007)b (5.000 | 95.007)b (4.000 | 95.007)b (3.000 | 95.007)b (2.000 | 95.007)b (1.000 | 105.008);;;; Solution Found
; States evaluated: 75
; Cost: 115.009
0.000: (calibrate rover1 camera1 objective1 waypoint0)  [5.000]
0.000: (sample_rock rover0 rover0store waypoint0)  [8.000]
0.000: (calibrate rover0 camera2 objective1 waypoint0)  [5.000]
5.001: (take_image rover1 waypoint0 objective2 camera1 high_res)  [7.000]
5.001: (take_image rover0 waypoint0 objective0 camera2 high_res)  [7.000]
8.001: (drop rover0 rover0store)  [1.000]
12.002: (communicate_image_data rover1 general objective2 high_res waypoint0 waypoint3)  [15.000]
12.002: (calibrate rover0 camera2 objective1 waypoint0)  [5.000]
17.003: (take_image rover0 waypoint0 objective0 camera2 colour)  [7.000]
27.003: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint3)  [10.000]
27.003: (navigate rover1 waypoint0 waypoint1)  [5.000]
32.004: (sample_soil rover1 rover1store waypoint1)  [10.000]
37.004: (communicate_image_data rover0 general objective0 high_res waypoint0 waypoint3)  [15.000]
42.004: (navigate rover1 waypoint1 waypoint2)  [5.000]
42.005: (drop rover1 rover1store)  [1.000]
47.005: (sample_soil rover1 rover1store waypoint2)  [10.000]
52.005: (communicate_image_data rover0 general objective0 colour waypoint0 waypoint3)  [15.000]
57.005: (navigate rover1 waypoint2 waypoint1)  [5.000]
62.006: (navigate rover1 waypoint1 waypoint0)  [5.000]
67.006: (navigate rover0 waypoint0 waypoint1)  [5.000]
72.007: (sample_rock rover0 rover0store waypoint1)  [8.000]
80.007: (navigate rover0 waypoint1 waypoint0)  [5.000]
85.007: (communicate_rock_data rover0 general waypoint1 waypoint0 waypoint3)  [10.000]
95.008: (communicate_soil_data rover1 general waypoint2 waypoint0 waypoint3)  [10.000]
105.009: (communicate_soil_data rover1 general waypoint1 waypoint0 waypoint3)  [10.000]

 Execution time of Domain\Problem 5 
00:00:02.8791874

 DOMAIN/PROBLEM 6 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile06.pddl
Number of literals: 66
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 28.000
b (27.000 | 5.000)b (26.000 | 10.000)b (25.000 | 16.002)b (24.000 | 32.001)b (23.000 | 32.001)b (22.000 | 42.002)b (21.000 | 42.002)b (20.000 | 52.003)b (19.000 | 52.003)b (18.000 | 52.003)b (17.000 | 67.004)b (16.000 | 82.005)b (15.000 | 82.005)b (14.000 | 82.005)b (13.000 | 82.005)b (12.000 | 82.005)b (11.000 | 82.005)b (10.000 | 82.005)b (9.000 | 82.006)b (8.000 | 92.006)b (7.000 | 102.007)b (6.000 | 112.008)b (5.000 | 112.008)b (4.000 | 112.008)b (3.000 | 122.009)b (2.000 | 135.011)b (1.000 | 140.011);;;; Solution Found
; States evaluated: 149
; Cost: 150.011
0.000: (calibrate rover0 camera0 objective0 waypoint1)  [5.000]
0.000: (navigate rover1 waypoint4 waypoint5)  [5.000]
5.000: (navigate rover0 waypoint1 waypoint0)  [5.000]
5.001: (sample_soil rover1 rover1store waypoint5)  [10.000]
10.000: (take_image rover0 waypoint0 objective1 camera0 low_res)  [7.000]
10.000: (calibrate rover0 camera1 objective1 waypoint0)  [5.000]
10.001: (sample_rock rover0 rover0store waypoint0)  [8.000]
15.001: (take_image rover0 waypoint0 objective0 camera1 low_res)  [7.000]
15.001: (navigate rover1 waypoint5 waypoint2)  [5.000]
15.002: (drop rover1 rover1store)  [1.000]
17.001: (calibrate rover0 camera0 objective0 waypoint0)  [5.000]
17.001: (communicate_image_data rover0 general objective1 low_res waypoint0 waypoint3)  [15.000]
18.002: (drop rover0 rover0store)  [1.000]
20.002: (sample_soil rover1 rover1store waypoint2)  [10.000]
22.002: (take_image rover0 waypoint0 objective0 camera0 colour)  [7.000]
30.003: (drop rover1 rover1store)  [1.000]
32.002: (communicate_soil_data rover1 general waypoint5 waypoint2 waypoint3)  [10.000]
42.003: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint3)  [10.000]
42.003: (navigate rover1 waypoint2 waypoint5)  [5.000]
47.004: (navigate rover1 waypoint5 waypoint4)  [5.000]
52.004: (communicate_image_data rover0 general objective0 low_res waypoint0 waypoint3)  [15.000]
52.005: (sample_soil rover1 rover1store waypoint4)  [10.000]
62.005: (navigate rover1 waypoint4 waypoint1)  [5.000]
62.006: (drop rover1 rover1store)  [1.000]
67.005: (communicate_image_data rover0 general objective0 colour waypoint0 waypoint3)  [15.000]
67.006: (sample_soil rover1 rover1store waypoint1)  [10.000]
77.006: (navigate rover1 waypoint1 waypoint0)  [5.000]
82.006: (communicate_soil_data rover1 general waypoint4 waypoint0 waypoint3)  [10.000]
82.006: (navigate rover0 waypoint0 waypoint3)  [5.000]
87.007: (sample_rock rover0 rover0store waypoint3)  [8.000]
92.007: (communicate_soil_data rover1 general waypoint2 waypoint0 waypoint3)  [10.000]
95.007: (navigate rover0 waypoint3 waypoint0)  [5.000]
95.008: (drop rover0 rover0store)  [1.000]
102.008: (communicate_soil_data rover1 general waypoint1 waypoint0 waypoint3)  [10.000]
112.009: (communicate_rock_data rover0 general waypoint3 waypoint0 waypoint3)  [10.000]
122.010: (navigate rover0 waypoint0 waypoint2)  [5.000]
127.011: (sample_rock rover0 rover0store waypoint2)  [8.000]
135.011: (navigate rover0 waypoint2 waypoint0)  [5.000]
140.011: (communicate_rock_data rover0 general waypoint2 waypoint0 waypoint3)  [10.000]

 Execution time of Domain\Problem 6 
00:00:02.9989317

 DOMAIN/PROBLEM 7 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile07.pddl
Number of literals: 70
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 17.000
b (16.000 | 5.000)b (15.000 | 8.000)b (14.000 | 13.000)b (13.000 | 13.000)b (12.000 | 23.000)b (10.000 | 23.000)b (9.000 | 38.001)b (8.000 | 38.001)b (7.000 | 48.002)b (6.000 | 48.002)b (5.000 | 48.002)b (4.000 | 48.002)b (3.000 | 48.002)b (2.000 | 58.003)b (1.000 | 68.004);;;; Solution Found
; States evaluated: 56
; Cost: 78.005
0.000: (calibrate rover0 camera0 objective0 waypoint2)  [5.000]
0.000: (sample_rock rover1 rover1store waypoint3)  [8.000]
0.000: (sample_rock rover0 rover0store waypoint2)  [8.000]
0.000: (sample_rock rover2 rover2store waypoint4)  [8.000]
5.001: (take_image rover0 waypoint2 objective0 camera0 high_res)  [7.000]
8.000: (navigate rover1 waypoint3 waypoint0)  [5.000]
8.001: (drop rover2 rover2store)  [1.000]
9.002: (sample_soil rover2 rover2store waypoint4)  [10.000]
12.001: (navigate rover0 waypoint2 waypoint0)  [5.000]
13.000: (communicate_rock_data rover1 general waypoint3 waypoint0 waypoint3)  [10.000]
19.002: (navigate rover2 waypoint4 waypoint0)  [5.000]
19.003: (drop rover2 rover2store)  [1.000]
23.001: (communicate_image_data rover0 general objective0 high_res waypoint0 waypoint3)  [15.000]
24.003: (navigate rover2 waypoint0 waypoint1)  [5.000]
29.004: (sample_soil rover2 rover2store waypoint1)  [10.000]
38.002: (communicate_rock_data rover0 general waypoint2 waypoint0 waypoint3)  [10.000]
39.004: (navigate rover2 waypoint1 waypoint0)  [5.000]
48.003: (communicate_rock_data rover2 general waypoint4 waypoint0 waypoint3)  [10.000]
58.004: (communicate_soil_data rover2 general waypoint4 waypoint0 waypoint3)  [10.000]
68.005: (communicate_soil_data rover2 general waypoint1 waypoint0 waypoint3)  [10.000]

 Execution time of Domain\Problem 7 
00:00:02.8361975

 DOMAIN/PROBLEM 8 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile08.pddl
Number of literals: 114
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 24.000
b (23.000 | 5.000)b (22.000 | 10.000)b (21.000 | 15.000)b (20.000 | 15.000)b (19.000 | 15.000)b (18.000 | 18.001)b (17.000 | 18.001)b (16.000 | 25.000)b (15.000 | 40.001)b (14.000 | 40.001)b (13.000 | 40.001)b (12.000 | 40.001)b (11.000 | 40.001)b (10.000 | 40.001)b (9.000 | 40.001)b (8.000 | 40.001)b (7.000 | 40.001)b (6.000 | 49.006)b (5.000 | 55.002)b (4.000 | 70.003)b (3.000 | 80.004)b (2.000 | 90.005)b (1.000 | 100.006);;;; Solution Found
; States evaluated: 191
; Cost: 110.007
0.000: (calibrate rover1 camera2 objective0 waypoint2)  [5.000]
0.000: (sample_soil rover3 rover3store waypoint3)  [10.000]
0.000: (navigate rover2 waypoint2 waypoint5)  [5.000]
0.000: (navigate rover0 waypoint2 waypoint4)  [5.000]
5.000: (navigate rover1 waypoint2 waypoint5)  [5.000]
5.001: (sample_rock rover2 rover2store waypoint5)  [8.000]
5.001: (sample_rock rover0 rover0store waypoint4)  [8.000]
10.000: (navigate rover3 waypoint3 waypoint2)  [5.000]
10.001: (navigate rover1 waypoint5 waypoint2)  [5.000]
13.001: (navigate rover2 waypoint5 waypoint2)  [5.000]
13.002: (drop rover0 rover0store)  [1.000]
14.003: (sample_soil rover0 rover0store waypoint4)  [10.000]
15.000: (communicate_soil_data rover3 general waypoint3 waypoint2 waypoint0)  [10.000]
15.000: (calibrate rover3 camera1 objective0 waypoint2)  [5.000]
15.001: (take_image rover1 waypoint2 objective2 camera2 low_res)  [7.000]
18.002: (navigate rover2 waypoint2 waypoint1)  [5.000]
20.001: (take_image rover3 waypoint2 objective0 camera1 high_res)  [7.000]
22.002: (calibrate rover1 camera2 objective0 waypoint2)  [5.000]
24.003: (navigate rover0 waypoint4 waypoint2)  [5.000]
24.004: (drop rover0 rover0store)  [1.000]
25.001: (communicate_image_data rover1 general objective2 low_res waypoint2 waypoint0)  [15.000]
27.003: (take_image rover1 waypoint2 objective0 camera2 low_res)  [7.000]
29.004: (navigate rover0 waypoint2 waypoint0)  [5.000]
34.005: (navigate rover0 waypoint0 waypoint1)  [5.000]
39.006: (sample_soil rover0 rover0store waypoint1)  [10.000]
40.002: (communicate_image_data rover3 general objective0 high_res waypoint2 waypoint0)  [15.000]
40.002: (navigate rover1 waypoint2 waypoint0)  [5.000]
45.003: (navigate rover1 waypoint0 waypoint1)  [5.000]
55.003: (communicate_image_data rover1 general objective0 low_res waypoint1 waypoint0)  [15.000]
70.004: (communicate_rock_data rover2 general waypoint5 waypoint1 waypoint0)  [10.000]
80.005: (communicate_rock_data rover0 general waypoint4 waypoint1 waypoint0)  [10.000]
90.006: (communicate_soil_data rover0 general waypoint4 waypoint1 waypoint0)  [10.000]
100.007: (communicate_soil_data rover0 general waypoint1 waypoint1 waypoint0)  [10.000]

 Execution time of Domain\Problem 8 
00:00:03.1606036

 DOMAIN/PROBLEM 9 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile09.pddl
Number of literals: 121
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 27.000
b (25.000 | 5.000)b (23.000 | 10.000)b (22.000 | 15.000)b (21.000 | 15.000)b (20.000 | 15.000)b (19.000 | 26.003)b (18.000 | 27.002)b (16.000 | 30.003)b (15.000 | 40.004)b (14.000 | 45.004)b (13.000 | 45.004)b (12.000 | 45.004)b (11.000 | 45.004)b (10.000 | 55.004)b (9.000 | 65.005)b (8.000 | 80.006)b (7.000 | 90.007)b (6.000 | 108.009)b (5.000 | 108.009)b (4.000 | 118.009)b (3.000 | 136.012)b (2.000 | 141.012)b (1.000 | 146.013);;;; Solution Found
; States evaluated: 631
; Cost: 156.013
0.000: (calibrate rover3 camera0 objective2 waypoint2)  [5.000]
0.000: (sample_soil rover2 rover2store waypoint0)  [10.000]
0.000: (calibrate rover1 camera4 objective1 waypoint2)  [5.000]
0.000: (navigate rover0 waypoint5 waypoint1)  [5.000]
5.001: (take_image rover3 waypoint2 objective2 camera0 colour)  [7.000]
5.001: (take_image rover1 waypoint2 objective2 camera4 low_res)  [7.000]
5.001: (calibrate rover0 camera2 objective0 waypoint1)  [5.000]
10.000: (navigate rover2 waypoint0 waypoint1)  [5.000]
10.001: (drop rover2 rover2store)  [1.000]
10.002: (take_image rover0 waypoint1 objective2 camera2 low_res)  [7.000]
12.001: (navigate rover1 waypoint2 waypoint6)  [5.000]
12.001: (navigate rover3 waypoint2 waypoint6)  [5.000]
15.001: (navigate rover2 waypoint1 waypoint0)  [5.000]
17.001: (communicate_image_data rover3 general objective2 colour waypoint6 waypoint4)  [15.000]
17.002: (sample_rock rover1 rover1store waypoint6)  [8.000]
17.002: (sample_soil rover3 rover3store waypoint6)  [10.000]
20.002: (navigate rover2 waypoint0 waypoint3)  [5.000]
25.002: (navigate rover1 waypoint6 waypoint2)  [5.000]
25.003: (navigate rover2 waypoint3 waypoint4)  [5.000]
25.003: (drop rover1 rover1store)  [1.000]
30.003: (navigate rover1 waypoint2 waypoint1)  [5.000]
30.004: (sample_soil rover2 rover2store waypoint4)  [10.000]
32.002: (communicate_soil_data rover3 general waypoint6 waypoint6 waypoint4)  [10.000]
40.004: (navigate rover2 waypoint4 waypoint3)  [5.000]
45.004: (communicate_soil_data rover2 general waypoint4 waypoint3 waypoint4)  [10.000]
55.005: (communicate_soil_data rover2 general waypoint0 waypoint3 waypoint4)  [10.000]
65.006: (communicate_image_data rover0 general objective2 low_res waypoint1 waypoint4)  [15.000]
80.007: (communicate_rock_data rover1 general waypoint6 waypoint1 waypoint4)  [10.000]
90.008: (navigate rover1 waypoint1 waypoint3)  [5.000]
95.009: (sample_rock rover1 rover1store waypoint3)  [8.000]
103.009: (navigate rover1 waypoint3 waypoint1)  [5.000]
103.010: (drop rover1 rover1store)  [1.000]
108.009: (communicate_rock_data rover1 general waypoint3 waypoint1 waypoint4)  [10.000]
118.010: (navigate rover1 waypoint1 waypoint2)  [5.000]
123.011: (navigate rover1 waypoint2 waypoint0)  [5.000]
128.012: (sample_rock rover1 rover1store waypoint0)  [8.000]
136.012: (navigate rover1 waypoint0 waypoint2)  [5.000]
141.013: (navigate rover1 waypoint2 waypoint1)  [5.000]
146.013: (communicate_rock_data rover1 general waypoint0 waypoint1 waypoint4)  [10.000]

 Execution time of Domain\Problem 9 
00:00:04.4093703

 DOMAIN/PROBLEM 10 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile10.pddl
Number of literals: 131
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 29.000
b (28.000 | 5.000)b (27.000 | 5.000)b (26.000 | 8.000)b (25.000 | 8.000)b (24.000 | 10.000)b (23.000 | 13.000)b (22.000 | 13.000)b (21.000 | 13.000)b (20.000 | 23.000)b (19.000 | 38.001)b (18.000 | 48.002)b (17.000 | 58.003)b (16.000 | 58.003)b (15.000 | 58.003)b (14.000 | 58.003)b (13.000 | 73.004)b (12.000 | 88.005)b (11.000 | 88.005)b (10.000 | 88.005)b (9.000 | 88.005)b (8.000 | 88.005)b (7.000 | 88.005)b (6.000 | 88.005)b (5.000 | 88.005)b (4.000 | 98.006)b (3.000 | 108.007)b (2.000 | 118.008)b (1.000 | 128.009);;;; Solution Found
; States evaluated: 142
; Cost: 138.010
0.000: (calibrate rover1 camera1 objective3 waypoint0)  [5.000]
0.000: (calibrate rover1 camera0 objective2 waypoint0)  [5.000]
0.000: (sample_rock rover2 rover2store waypoint3)  [8.000]
0.000: (sample_rock rover3 rover3store waypoint1)  [8.000]
0.000: (sample_soil rover1 rover1store waypoint0)  [10.000]
0.000: (sample_rock rover0 rover0store waypoint4)  [8.000]
0.000: (calibrate rover1 camera2 objective1 waypoint0)  [5.000]
5.001: (take_image rover1 waypoint0 objective3 camera0 low_res)  [7.000]
5.001: (take_image rover1 waypoint0 objective3 camera1 colour)  [7.000]
5.001: (take_image rover1 waypoint0 objective2 camera2 colour)  [7.000]
8.000: (navigate rover2 waypoint3 waypoint4)  [5.000]
8.000: (navigate rover3 waypoint1 waypoint0)  [5.000]
8.001: (drop rover0 rover0store)  [1.000]
9.002: (sample_soil rover0 rover0store waypoint4)  [10.000]
13.000: (communicate_rock_data rover2 general waypoint3 waypoint4 waypoint1)  [10.000]
19.002: (navigate rover0 waypoint4 waypoint6)  [5.000]
19.003: (drop rover0 rover0store)  [1.000]
23.001: (communicate_image_data rover1 general objective3 low_res waypoint0 waypoint1)  [15.000]
24.003: (sample_soil rover0 rover0store waypoint6)  [10.000]
34.003: (navigate rover0 waypoint6 waypoint4)  [5.000]
34.004: (drop rover0 rover0store)  [1.000]
38.002: (communicate_rock_data rover3 general waypoint1 waypoint0 waypoint1)  [10.000]
39.004: (navigate rover0 waypoint4 waypoint3)  [5.000]
44.005: (sample_soil rover0 rover0store waypoint3)  [10.000]
48.003: (communicate_soil_data rover1 general waypoint0 waypoint0 waypoint1)  [10.000]
54.005: (navigate rover0 waypoint3 waypoint4)  [5.000]
54.006: (drop rover0 rover0store)  [1.000]
58.004: (communicate_image_data rover1 general objective3 colour waypoint0 waypoint1)  [15.000]
59.006: (navigate rover0 waypoint4 waypoint0)  [5.000]
64.007: (sample_rock rover0 rover0store waypoint0)  [8.000]
73.005: (communicate_image_data rover1 general objective2 colour waypoint0 waypoint1)  [15.000]
88.006: (communicate_rock_data rover0 general waypoint4 waypoint0 waypoint1)  [10.000]
98.007: (communicate_rock_data rover0 general waypoint0 waypoint0 waypoint1)  [10.000]
108.008: (communicate_soil_data rover0 general waypoint6 waypoint0 waypoint1)  [10.000]
118.009: (communicate_soil_data rover0 general waypoint4 waypoint0 waypoint1)  [10.000]
128.010: (communicate_soil_data rover0 general waypoint3 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 10 
00:00:03.1546322

 DOMAIN/PROBLEM 11 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile11.pddl
Number of literals: 122
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 26.000
b (25.000 | 5.000)b (24.000 | 10.000)b (22.000 | 10.000)b (21.000 | 15.000)b (20.000 | 17.000)b (19.000 | 42.002)b (18.000 | 42.002)b (17.000 | 42.002)b (16.000 | 52.003)b (15.000 | 52.003)b (14.000 | 52.003)b (13.000 | 52.003)b (12.000 | 52.003)b (11.000 | 52.003)b (10.000 | 63.004)b (9.000 | 73.005)b (8.000 | 83.006)b (7.000 | 83.006)b (6.000 | 93.007)b (5.000 | 98.008)b (4.000 | 111.009)b (3.000 | 111.009)b (2.000 | 121.009)b (1.000 | 121.009);;;; Solution Found
; States evaluated: 135
; Cost: 131.010
0.000: (calibrate rover2 camera1 objective0 waypoint3)  [5.000]
0.000: (sample_soil rover0 rover0store waypoint1)  [10.000]
0.000: (sample_soil rover1 rover1store waypoint3)  [10.000]
5.000: (navigate rover2 waypoint3 waypoint0)  [5.000]
10.000: (navigate rover0 waypoint1 waypoint0)  [5.000]
10.000: (take_image rover2 waypoint0 objective1 camera1 high_res)  [7.000]
10.000: (navigate rover1 waypoint3 waypoint7)  [5.000]
10.001: (drop rover0 rover0store)  [1.000]
10.001: (drop rover1 rover1store)  [1.000]
15.001: (sample_soil rover0 rover0store waypoint0)  [10.000]
15.001: (sample_rock rover1 rover1store waypoint7)  [8.000]
17.001: (communicate_image_data rover2 general objective1 high_res waypoint0 waypoint1)  [15.000]
23.001: (navigate rover1 waypoint7 waypoint3)  [5.000]
23.002: (drop rover1 rover1store)  [1.000]
28.002: (navigate rover1 waypoint3 waypoint0)  [5.000]
32.002: (communicate_soil_data rover0 general waypoint1 waypoint0 waypoint1)  [10.000]
33.003: (navigate rover1 waypoint0 waypoint6)  [5.000]
38.004: (sample_soil rover1 rover1store waypoint6)  [10.000]
42.003: (communicate_soil_data rover0 general waypoint0 waypoint0 waypoint1)  [10.000]
48.004: (navigate rover1 waypoint6 waypoint4)  [5.000]
48.005: (drop rover1 rover1store)  [1.000]
53.004: (communicate_rock_data rover1 general waypoint7 waypoint4 waypoint1)  [10.000]
53.005: (sample_rock rover1 rover1store waypoint4)  [8.000]
61.006: (drop rover1 rover1store)  [1.000]
63.005: (communicate_soil_data rover1 general waypoint6 waypoint4 waypoint1)  [10.000]
73.006: (communicate_soil_data rover1 general waypoint3 waypoint4 waypoint1)  [10.000]
83.007: (communicate_rock_data rover1 general waypoint4 waypoint4 waypoint1)  [10.000]
93.008: (navigate rover1 waypoint4 waypoint6)  [5.000]
98.009: (sample_rock rover1 rover1store waypoint6)  [8.000]
106.009: (navigate rover1 waypoint6 waypoint0)  [5.000]
106.010: (drop rover1 rover1store)  [1.000]
111.009: (communicate_rock_data rover1 general waypoint6 waypoint0 waypoint1)  [10.000]
111.010: (sample_rock rover1 rover1store waypoint0)  [8.000]
121.010: (communicate_rock_data rover1 general waypoint0 waypoint0 waypoint1)  [10.000]

 Execution time of Domain\Problem 11 
00:00:03.1582282

 DOMAIN/PROBLEM 12 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile12.pddl
Number of literals: 112
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 23.000
b (21.000 | 5.000)b (18.000 | 5.000)b (17.000 | 5.000)b (16.000 | 10.001)b (15.000 | 10.001)b (14.000 | 13.001)b (13.000 | 18.001)b (12.000 | 18.001)b (11.000 | 18.001)b (10.000 | 18.001)b (9.000 | 22.003)b (8.000 | 32.003)b (6.000 | 32.003)b (5.000 | 36.004)b (4.000 | 42.004)b (3.000 | 52.005)b (2.000 | 67.006)b (1.000 | 82.007);;;; Solution Found
; States evaluated: 173
; Cost: 92.008
0.000: (navigate rover1 waypoint4 waypoint6)  [5.000]
0.000: (navigate rover0 waypoint4 waypoint5)  [5.000]
0.000: (navigate rover3 waypoint7 waypoint0)  [5.000]
5.001: (calibrate rover3 camera3 objective3 waypoint0)  [5.000]
5.001: (calibrate rover3 camera0 objective2 waypoint0)  [5.000]
5.001: (sample_rock rover1 rover1store waypoint6)  [8.000]
5.001: (sample_soil rover3 rover3store waypoint0)  [10.000]
5.001: (navigate rover0 waypoint5 waypoint0)  [5.000]
10.002: (calibrate rover0 camera2 objective0 waypoint0)  [5.000]
10.002: (take_image rover3 waypoint0 objective1 camera0 high_res)  [7.000]
10.002: (take_image rover3 waypoint0 objective3 camera3 low_res)  [7.000]
13.001: (navigate rover1 waypoint6 waypoint4)  [5.000]
13.002: (drop rover1 rover1store)  [1.000]
15.002: (drop rover3 rover3store)  [1.000]
15.003: (take_image rover0 waypoint0 objective2 camera2 low_res)  [7.000]
17.003: (communicate_image_data rover3 general objective1 high_res waypoint0 waypoint2)  [15.000]
18.002: (navigate rover1 waypoint4 waypoint2)  [5.000]
22.004: (calibrate rover0 camera2 objective0 waypoint0)  [5.000]
23.003: (navigate rover1 waypoint2 waypoint3)  [5.000]
27.005: (take_image rover0 waypoint0 objective3 camera2 low_res)  [7.000]
28.004: (sample_rock rover1 rover1store waypoint3)  [8.000]
32.004: (communicate_rock_data rover1 general waypoint6 waypoint3 waypoint2)  [10.000]
42.005: (communicate_rock_data rover1 general waypoint3 waypoint3 waypoint2)  [10.000]
52.006: (communicate_image_data rover0 general objective3 low_res waypoint0 waypoint2)  [15.000]
67.007: (communicate_image_data rover0 general objective2 low_res waypoint0 waypoint2)  [15.000]
82.008: (communicate_soil_data rover3 general waypoint0 waypoint0 waypoint2)  [10.000]

 Execution time of Domain\Problem 12 
00:00:03.1721814

 DOMAIN/PROBLEM 13 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile13.pddl
Number of literals: 153
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 35.000
b (34.000 | 5.000)b (32.000 | 10.000)b (30.000 | 10.000)b (29.000 | 15.000)b (28.000 | 15.000)b (27.000 | 15.000)b (26.000 | 23.002)b (25.000 | 23.002)b (24.000 | 25.002)b (23.000 | 33.003)b (22.000 | 33.003)b (21.000 | 43.004)b (20.000 | 43.004)b (19.000 | 43.004)b (18.000 | 43.004)b (17.000 | 43.004)b (16.000 | 43.004)b (15.000 | 43.004)b (14.000 | 43.005)b (13.000 | 53.006)b (12.000 | 53.006)b (11.000 | 68.007)b (10.000 | 83.008)b (9.000 | 98.009)b (8.000 | 98.009)b (7.000 | 98.009)b (6.000 | 98.009)b (5.000 | 98.009)b (4.000 | 108.010)b (3.000 | 118.011)b (2.000 | 128.012)b (1.000 | 138.013);;;; Solution Found
; States evaluated: 488
; Cost: 148.014
0.000: (calibrate rover2 camera0 objective1 waypoint6)  [5.000]
0.000: (sample_soil rover0 rover0store waypoint7)  [10.000]
0.000: (navigate rover3 waypoint3 waypoint8)  [5.000]
0.000: (sample_rock rover2 rover2store waypoint6)  [8.000]
0.000: (navigate rover1 waypoint6 waypoint0)  [5.000]
5.001: (sample_rock rover3 rover3store waypoint8)  [8.000]
5.001: (navigate rover1 waypoint0 waypoint2)  [5.000]
5.001: (take_image rover2 waypoint6 objective1 camera0 high_res)  [7.000]
8.001: (drop rover2 rover2store)  [1.000]
10.000: (navigate rover0 waypoint7 waypoint0)  [5.000]
10.002: (sample_soil rover1 rover1store waypoint2)  [10.000]
12.001: (navigate rover2 waypoint6 waypoint7)  [5.000]
13.002: (drop rover3 rover3store)  [1.000]
13.002: (communicate_rock_data rover3 general waypoint8 waypoint8 waypoint2)  [10.000]
15.000: (calibrate rover0 camera4 objective1 waypoint0)  [5.000]
17.002: (sample_rock rover2 rover2store waypoint7)  [8.000]
20.001: (take_image rover0 waypoint0 objective3 camera4 high_res)  [7.000]
20.002: (navigate rover1 waypoint2 waypoint0)  [5.000]
23.003: (communicate_soil_data rover0 general waypoint7 waypoint0 waypoint2)  [10.000]
23.003: (navigate rover3 waypoint8 waypoint3)  [5.000]
25.002: (navigate rover2 waypoint7 waypoint6)  [5.000]
25.003: (drop rover2 rover2store)  [1.000]
27.002: (calibrate rover0 camera4 objective1 waypoint0)  [5.000]
28.004: (navigate rover3 waypoint3 waypoint5)  [5.000]
30.003: (navigate rover2 waypoint6 waypoint0)  [5.000]
32.003: (take_image rover0 waypoint0 objective2 camera4 high_res)  [7.000]
33.004: (communicate_soil_data rover1 general waypoint2 waypoint0 waypoint2)  [10.000]
33.005: (sample_soil rover3 rover3store waypoint5)  [10.000]
35.004: (navigate rover2 waypoint0 waypoint4)  [5.000]
39.004: (calibrate rover0 camera4 objective1 waypoint0)  [5.000]
40.005: (sample_soil rover2 rover2store waypoint4)  [10.000]
43.006: (communicate_soil_data rover3 general waypoint5 waypoint5 waypoint2)  [10.000]
44.005: (take_image rover0 waypoint0 objective1 camera4 high_res)  [7.000]
50.006: (drop rover2 rover2store)  [1.000]
51.007: (sample_rock rover2 rover2store waypoint4)  [8.000]
53.007: (communicate_image_data rover0 general objective3 high_res waypoint0 waypoint2)  [15.000]
59.007: (navigate rover2 waypoint4 waypoint0)  [5.000]
59.008: (drop rover2 rover2store)  [1.000]
64.008: (navigate rover2 waypoint0 waypoint1)  [5.000]
68.008: (communicate_image_data rover0 general objective2 high_res waypoint0 waypoint2)  [15.000]
69.009: (sample_rock rover2 rover2store waypoint1)  [8.000]
77.009: (navigate rover2 waypoint1 waypoint0)  [5.000]
83.009: (communicate_image_data rover0 general objective1 high_res waypoint0 waypoint2)  [15.000]
98.010: (communicate_rock_data rover2 general waypoint7 waypoint0 waypoint2)  [10.000]
108.011: (communicate_rock_data rover2 general waypoint6 waypoint0 waypoint2)  [10.000]
118.012: (communicate_rock_data rover2 general waypoint4 waypoint0 waypoint2)  [10.000]
128.013: (communicate_rock_data rover2 general waypoint1 waypoint0 waypoint2)  [10.000]
138.014: (communicate_soil_data rover2 general waypoint4 waypoint0 waypoint2)  [10.000]

 Execution time of Domain\Problem 13 
00:00:05.1363220

 DOMAIN/PROBLEM 14 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile14.pddl
Number of literals: 127
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 26.000
b (25.000 | 5.000)b (24.000 | 5.000)b (22.000 | 10.001)b (21.000 | 20.002)b (20.000 | 30.003)b (19.000 | 30.003)b (18.000 | 30.003)b (17.000 | 30.003)b (16.000 | 30.003)b (15.000 | 30.003)b (14.000 | 30.003)b (13.000 | 45.004)b (12.000 | 55.005)b (11.000 | 70.006)b (10.000 | 85.007)b (9.000 | 95.008)b (8.000 | 128.013)b (7.000 | 128.013)b (6.000 | 133.014)b (5.000 | 138.015)b (2.000 | 143.016)b (1.000 | 151.017);;;; Solution Found
; States evaluated: 1502
; Cost: 161.018
0.000: (calibrate rover3 camera0 objective2 waypoint2)  [5.000]
0.000: (navigate rover1 waypoint4 waypoint6)  [5.000]
0.000: (navigate rover0 waypoint1 waypoint2)  [5.000]
5.000: (navigate rover3 waypoint2 waypoint1)  [5.000]
5.001: (navigate rover0 waypoint2 waypoint3)  [5.000]
5.001: (calibrate rover1 camera3 objective0 waypoint6)  [5.000]
5.001: (calibrate rover1 camera2 objective3 waypoint6)  [5.000]
5.001: (sample_soil rover1 rover1store waypoint6)  [10.000]
10.000: (take_image rover3 waypoint1 objective0 camera0 low_res)  [7.000]
10.002: (sample_soil rover0 rover0store waypoint3)  [10.000]
15.001: (navigate rover1 waypoint6 waypoint4)  [5.000]
15.002: (drop rover1 rover1store)  [1.000]
20.001: (take_image rover1 waypoint4 objective2 camera2 low_res)  [7.000]
20.001: (take_image rover1 waypoint4 objective0 camera3 colour)  [7.000]
20.002: (sample_rock rover1 rover1store waypoint4)  [8.000]
20.003: (communicate_soil_data rover0 general waypoint3 waypoint3 waypoint7)  [10.000]
28.002: (navigate rover1 waypoint4 waypoint2)  [5.000]
28.003: (drop rover1 rover1store)  [1.000]
30.004: (communicate_image_data rover3 general objective0 low_res waypoint1 waypoint7)  [15.000]
30.004: (navigate rover0 waypoint3 waypoint2)  [5.000]
33.003: (navigate rover1 waypoint2 waypoint1)  [5.000]
35.004: (calibrate rover0 camera4 objective3 waypoint2)  [5.000]
40.004: (navigate rover0 waypoint2 waypoint1)  [5.000]
45.004: (take_image rover0 waypoint1 objective2 camera4 low_res)  [7.000]
45.005: (communicate_rock_data rover1 general waypoint4 waypoint1 waypoint7)  [10.000]
55.006: (communicate_image_data rover1 general objective0 colour waypoint1 waypoint7)  [15.000]
70.007: (communicate_image_data rover0 general objective2 low_res waypoint1 waypoint7)  [15.000]
85.008: (communicate_soil_data rover1 general waypoint6 waypoint1 waypoint7)  [10.000]
95.009: (navigate rover1 waypoint1 waypoint2)  [5.000]
100.010: (navigate rover1 waypoint2 waypoint4)  [5.000]
105.011: (navigate rover1 waypoint4 waypoint5)  [5.000]
110.012: (sample_rock rover1 rover1store waypoint5)  [8.000]
118.013: (communicate_rock_data rover1 general waypoint5 waypoint5 waypoint7)  [10.000]
118.013: (drop rover1 rover1store)  [1.000]
128.014: (navigate rover1 waypoint5 waypoint4)  [5.000]
133.015: (navigate rover1 waypoint4 waypoint6)  [5.000]
138.016: (navigate rover1 waypoint6 waypoint8)  [5.000]
143.017: (sample_rock rover1 rover1store waypoint8)  [8.000]
151.018: (communicate_rock_data rover1 general waypoint8 waypoint8 waypoint7)  [10.000]

 Execution time of Domain\Problem 14 
00:00:09.6490882

 DOMAIN/PROBLEM 15 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile15.pddl
Number of literals: 157
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 31.000
b (30.000 | 5.000)b (29.000 | 12.001)b (28.000 | 17.000)b (27.000 | 32.001)b (26.000 | 32.001)b (25.000 | 32.001)b (24.000 | 36.003)b (23.000 | 36.003)b (22.000 | 46.003)b (21.000 | 56.004)b (20.000 | 61.005)b (19.000 | 69.006)b (18.000 | 74.006)b (17.000 | 84.006)b (16.000 | 84.006)b (15.000 | 84.006)b (14.000 | 84.006)b (13.000 | 84.006)b (12.000 | 84.006)b (11.000 | 109.008)b (10.000 | 119.009)b (9.000 | 129.010)b (8.000 | 129.010)b (7.000 | 129.010)b (3.000 | 139.011)b (2.000 | 144.012)b (1.000 | 149.012);;;; Solution Found
; States evaluated: 930
; Cost: 159.012
0.000: (calibrate rover0 camera1 objective4 waypoint4)  [5.000]
0.000: (calibrate rover1 camera2 objective4 waypoint6)  [5.000]
0.000: (navigate rover3 waypoint4 waypoint8)  [5.000]
0.000: (navigate rover2 waypoint6 waypoint10)  [5.000]
5.000: (navigate rover1 waypoint6 waypoint3)  [5.000]
5.001: (take_image rover0 waypoint4 objective1 camera1 low_res)  [7.000]
5.001: (sample_rock rover3 rover3store waypoint8)  [8.000]
5.001: (sample_soil rover2 rover2store waypoint10)  [10.000]
10.000: (take_image rover1 waypoint3 objective1 camera2 high_res)  [7.000]
12.001: (navigate rover0 waypoint4 waypoint8)  [5.000]
13.001: (navigate rover3 waypoint8 waypoint4)  [5.000]
13.002: (drop rover3 rover3store)  [1.000]
15.001: (navigate rover2 waypoint10 waypoint6)  [5.000]
15.002: (drop rover2 rover2store)  [1.000]
17.001: (communicate_image_data rover1 general objective1 high_res waypoint3 waypoint9)  [15.000]
17.002: (sample_soil rover0 rover0store waypoint8)  [10.000]
18.002: (navigate rover3 waypoint4 waypoint2)  [5.000]
20.002: (navigate rover2 waypoint6 waypoint9)  [5.000]
23.003: (sample_rock rover3 rover3store waypoint2)  [8.000]
25.003: (navigate rover2 waypoint9 waypoint3)  [5.000]
27.002: (navigate rover0 waypoint8 waypoint4)  [5.000]
27.003: (drop rover0 rover0store)  [1.000]
31.003: (navigate rover3 waypoint2 waypoint4)  [5.000]
31.004: (drop rover3 rover3store)  [1.000]
32.003: (navigate rover0 waypoint4 waypoint5)  [5.000]
36.003: (communicate_rock_data rover3 general waypoint8 waypoint4 waypoint9)  [10.000]
37.004: (sample_soil rover0 rover0store waypoint5)  [10.000]
46.004: (communicate_rock_data rover3 general waypoint2 waypoint4 waypoint9)  [10.000]
47.004: (navigate rover0 waypoint5 waypoint4)  [5.000]
47.005: (drop rover0 rover0store)  [1.000]
52.005: (navigate rover0 waypoint4 waypoint2)  [5.000]
56.005: (navigate rover3 waypoint4 waypoint1)  [5.000]
57.006: (sample_soil rover0 rover0store waypoint2)  [10.000]
61.006: (sample_rock rover3 rover3store waypoint1)  [8.000]
67.006: (navigate rover0 waypoint2 waypoint4)  [5.000]
67.007: (drop rover0 rover0store)  [1.000]
69.006: (navigate rover3 waypoint1 waypoint3)  [5.000]
72.007: (navigate rover0 waypoint4 waypoint0)  [5.000]
74.006: (communicate_rock_data rover3 general waypoint1 waypoint3 waypoint9)  [10.000]
77.008: (navigate rover0 waypoint0 waypoint3)  [5.000]
84.007: (communicate_soil_data rover0 general waypoint8 waypoint3 waypoint9)  [10.000]
94.008: (communicate_image_data rover0 general objective1 low_res waypoint3 waypoint9)  [15.000]
109.009: (communicate_soil_data rover0 general waypoint5 waypoint3 waypoint9)  [10.000]
119.010: (communicate_soil_data rover0 general waypoint2 waypoint3 waypoint9)  [10.000]
129.011: (communicate_soil_data rover2 general waypoint10 waypoint3 waypoint9)  [10.000]
129.011: (navigate rover0 waypoint3 waypoint0)  [5.000]
134.012: (sample_soil rover0 rover0store waypoint0)  [10.000]
144.012: (navigate rover0 waypoint0 waypoint3)  [5.000]
149.012: (communicate_soil_data rover0 general waypoint0 waypoint3 waypoint9)  [10.000]

 Execution time of Domain\Problem 15 
00:00:09.2612003

 DOMAIN/PROBLEM 16 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile16.pddl
Number of literals: 184
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 31.000
b (30.000 | 5.000)b (29.000 | 10.000)b (28.000 | 10.000)b (27.000 | 13.001)b (26.000 | 20.001)b (25.000 | 20.001)b (24.000 | 32.001)b (23.000 | 42.002)b (22.000 | 52.003)b (21.000 | 52.003)b (20.000 | 52.003)b (19.000 | 67.004)b (18.000 | 67.004)b (17.000 | 67.004)b (16.000 | 67.004)b (15.000 | 67.004)b (14.000 | 67.004)b (13.000 | 67.004)b (12.000 | 72.006)b (10.000 | 92.006)b (8.000 | 92.006)b (7.000 | 102.007)b (6.000 | 102.007)b (5.000 | 102.007)b (4.000 | 102.007)b (3.000 | 112.008)b (2.000 | 122.009)b (1.000 | 132.010);;;; Solution Found
; States evaluated: 303
; Cost: 142.011
0.000: (calibrate rover0 camera2 objective2 waypoint6)  [5.000]
0.000: (navigate rover2 waypoint9 waypoint1)  [5.000]
0.000: (navigate rover1 waypoint2 waypoint9)  [5.000]
0.000: (navigate rover3 waypoint0 waypoint10)  [5.000]
5.000: (navigate rover0 waypoint6 waypoint7)  [5.000]
5.001: (sample_rock rover2 rover2store waypoint1)  [8.000]
5.001: (calibrate rover2 camera3 objective3 waypoint1)  [5.000]
5.001: (sample_soil rover1 rover1store waypoint9)  [10.000]
5.001: (sample_rock rover3 rover3store waypoint10)  [8.000]
10.000: (take_image rover0 waypoint7 objective4 camera2 high_res)  [7.000]
10.001: (sample_soil rover0 rover0store waypoint7)  [10.000]
10.002: (take_image rover2 waypoint1 objective2 camera3 high_res)  [7.000]
13.001: (navigate rover3 waypoint10 waypoint0)  [5.000]
15.001: (navigate rover1 waypoint9 waypoint2)  [5.000]
15.002: (drop rover1 rover1store)  [1.000]
17.001: (communicate_image_data rover0 general objective4 high_res waypoint7 waypoint6)  [15.000]
17.003: (calibrate rover2 camera3 objective3 waypoint1)  [5.000]
18.002: (navigate rover3 waypoint0 waypoint1)  [5.000]
20.002: (drop rover0 rover0store)  [1.000]
20.002: (navigate rover1 waypoint2 waypoint5)  [5.000]
22.004: (take_image rover2 waypoint1 objective0 camera3 high_res)  [7.000]
25.003: (sample_rock rover1 rover1store waypoint5)  [8.000]
32.002: (communicate_rock_data rover2 general waypoint1 waypoint1 waypoint6)  [10.000]
33.003: (navigate rover1 waypoint5 waypoint2)  [5.000]
33.004: (drop rover1 rover1store)  [1.000]
38.004: (navigate rover1 waypoint2 waypoint4)  [5.000]
42.003: (communicate_soil_data rover0 general waypoint7 waypoint7 waypoint6)  [10.000]
43.005: (sample_soil rover1 rover1store waypoint4)  [10.000]
52.004: (communicate_image_data rover2 general objective2 high_res waypoint1 waypoint6)  [15.000]
52.004: (navigate rover0 waypoint7 waypoint6)  [5.000]
53.005: (navigate rover1 waypoint4 waypoint2)  [5.000]
53.006: (drop rover1 rover1store)  [1.000]
57.005: (navigate rover0 waypoint6 waypoint1)  [5.000]
58.006: (navigate rover1 waypoint2 waypoint3)  [5.000]
62.006: (sample_soil rover0 rover0store waypoint1)  [10.000]
63.007: (sample_rock rover1 rover1store waypoint3)  [8.000]
67.005: (communicate_image_data rover2 general objective0 high_res waypoint1 waypoint6)  [15.000]
71.007: (navigate rover1 waypoint3 waypoint1)  [5.000]
82.006: (communicate_soil_data rover0 general waypoint1 waypoint1 waypoint6)  [10.000]
92.007: (communicate_rock_data rover1 general waypoint5 waypoint1 waypoint6)  [10.000]
102.008: (communicate_rock_data rover1 general waypoint3 waypoint1 waypoint6)  [10.000]
112.009: (communicate_rock_data rover3 general waypoint10 waypoint1 waypoint6)  [10.000]
122.010: (communicate_soil_data rover1 general waypoint9 waypoint1 waypoint6)  [10.000]
132.011: (communicate_soil_data rover1 general waypoint4 waypoint1 waypoint6)  [10.000]

 Execution time of Domain\Problem 16 
00:00:04.3472395

 DOMAIN/PROBLEM 17 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile17.pddl
Number of literals: 273
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 39.000
b (38.000 | 5.000)b (37.000 | 5.000)b (36.000 | 8.000)b (35.000 | 10.000)b (34.000 | 13.000)b (33.000 | 13.000)b (32.000 | 17.000)b (31.000 | 18.001)b (30.000 | 18.001)b (29.000 | 18.001)b (28.000 | 23.002)b (27.000 | 26.002)b (26.000 | 31.002)b (25.000 | 31.002)b (24.000 | 31.002)b (23.000 | 31.002)b (22.000 | 32.001)b (21.000 | 34.003)b (20.000 | 37.002)b (19.000 | 42.002)b (18.000 | 42.002)b (17.000 | 52.003)b (16.000 | 52.003)b (15.000 | 67.004)b (14.000 | 82.005)b (13.000 | 97.006)b (12.000 | 112.007)b (11.000 | 122.008)b (10.000 | 132.009)b (9.000 | 142.010)b (8.000 | 152.011)b (7.000 | 177.014)b (6.000 | 177.014)b (5.000 | 192.015)b (2.000 | 197.016)b (1.000 | 207.017);;;; Solution Found
; States evaluated: 1883
; Cost: 217.018
0.000: (calibrate rover3 camera1 objective2 waypoint13)  [5.000]
0.000: (calibrate rover4 camera0 objective2 waypoint1)  [5.000]
0.000: (sample_rock rover0 rover0store waypoint12)  [8.000]
0.000: (navigate rover5 waypoint8 waypoint9)  [5.000]
0.000: (navigate rover2 waypoint5 waypoint1)  [5.000]
5.000: (navigate rover4 waypoint1 waypoint5)  [5.000]
5.000: (navigate rover3 waypoint13 waypoint5)  [5.000]
5.001: (sample_rock rover5 rover5store waypoint9)  [8.000]
5.001: (calibrate rover2 camera6 objective5 waypoint1)  [5.000]
8.000: (navigate rover0 waypoint12 waypoint0)  [5.000]
8.001: (drop rover0 rover0store)  [1.000]
10.000: (take_image rover4 waypoint5 objective2 camera0 low_res)  [7.000]
10.000: (take_image rover3 waypoint5 objective4 camera1 colour)  [7.000]
10.001: (sample_rock rover4 rover4store waypoint5)  [8.000]
10.001: (sample_soil rover3 rover3store waypoint5)  [10.000]
10.001: (navigate rover2 waypoint1 waypoint0)  [5.000]
13.001: (navigate rover0 waypoint0 waypoint3)  [5.000]
13.001: (navigate rover5 waypoint9 waypoint8)  [5.000]
15.001: (take_image rover2 waypoint0 objective5 camera6 colour)  [7.000]
17.001: (communicate_image_data rover4 general objective2 low_res waypoint5 waypoint13)  [15.000]
18.002: (sample_rock rover0 rover0store waypoint3)  [8.000]
18.002: (navigate rover5 waypoint8 waypoint3)  [5.000]
20.001: (navigate rover3 waypoint5 waypoint2)  [5.000]
20.002: (drop rover3 rover3store)  [1.000]
22.002: (calibrate rover2 camera6 objective5 waypoint0)  [5.000]
23.003: (navigate rover5 waypoint3 waypoint0)  [5.000]
25.001: (calibrate rover3 camera1 objective2 waypoint2)  [5.000]
25.002: (sample_soil rover3 rover3store waypoint2)  [10.000]
26.002: (navigate rover0 waypoint3 waypoint0)  [5.000]
27.003: (take_image rover2 waypoint0 objective3 camera6 colour)  [7.000]
30.002: (take_image rover3 waypoint2 objective2 camera1 colour)  [7.000]
32.002: (communicate_rock_data rover4 general waypoint5 waypoint5 waypoint13)  [10.000]
34.004: (calibrate rover2 camera6 objective5 waypoint0)  [5.000]
35.003: (drop rover3 rover3store)  [1.000]
37.002: (navigate rover3 waypoint2 waypoint5)  [5.000]
39.005: (take_image rover2 waypoint0 objective4 camera6 colour)  [7.000]
42.003: (navigate rover3 waypoint5 waypoint13)  [5.000]
42.003: (communicate_rock_data rover5 general waypoint9 waypoint0 waypoint13)  [10.000]
47.004: (navigate rover3 waypoint13 waypoint0)  [5.000]
52.004: (communicate_image_data rover3 general objective2 colour waypoint0 waypoint13)  [15.000]
67.005: (communicate_image_data rover2 general objective5 colour waypoint0 waypoint13)  [15.000]
82.006: (communicate_image_data rover2 general objective4 colour waypoint0 waypoint13)  [15.000]
97.007: (communicate_image_data rover2 general objective3 colour waypoint0 waypoint13)  [15.000]
112.008: (communicate_rock_data rover0 general waypoint3 waypoint0 waypoint13)  [10.000]
122.009: (communicate_rock_data rover0 general waypoint12 waypoint0 waypoint13)  [10.000]
132.010: (communicate_soil_data rover3 general waypoint5 waypoint0 waypoint13)  [10.000]
142.011: (communicate_soil_data rover3 general waypoint2 waypoint0 waypoint13)  [10.000]
152.012: (navigate rover3 waypoint0 waypoint13)  [5.000]
157.013: (navigate rover3 waypoint13 waypoint3)  [5.000]
162.014: (sample_soil rover3 rover3store waypoint3)  [10.000]
172.014: (navigate rover3 waypoint3 waypoint13)  [5.000]
172.015: (drop rover3 rover3store)  [1.000]
177.015: (navigate rover3 waypoint13 waypoint5)  [5.000]
182.015: (communicate_soil_data rover3 general waypoint3 waypoint5 waypoint13)  [10.000]
192.016: (navigate rover3 waypoint5 waypoint14)  [5.000]
197.017: (sample_soil rover3 rover3store waypoint14)  [10.000]
207.018: (communicate_soil_data rover3 general waypoint14 waypoint14 waypoint13)  [10.000]

 Execution time of Domain\Problem 17 
00:00:31.9915319

 DOMAIN/PROBLEM 18 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile18.pddl
Number of literals: 377
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 32.000
b (31.000 | 5.000)b (30.000 | 5.000)b (29.000 | 10.000)b (28.000 | 12.001)b (27.000 | 12.001)b (26.000 | 12.001)b (25.000 | 20.001)b (24.000 | 20.001)b (23.000 | 35.002)b (22.000 | 50.003)b (21.000 | 50.003)b (20.000 | 50.003)b (19.000 | 50.003)b (18.000 | 50.003)b (17.000 | 50.003)b (16.000 | 50.003)b (15.000 | 60.004)b (14.000 | 60.004)b (13.000 | 60.004)b (12.000 | 70.005)b (11.000 | 95.007)b (10.000 | 110.008)b (9.000 | 120.009)b (8.000 | 120.009)b (7.000 | 120.009)b (6.000 | 120.009)b (4.000 | 130.010)b (3.000 | 130.010)b (2.000 | 130.010)b (1.000 | 130.010);;;; Solution Found
; States evaluated: 762
; Cost: 140.011
0.000: (calibrate rover1 camera0 objective4 waypoint9)  [5.000]
0.000: (calibrate rover2 camera1 objective6 waypoint0)  [5.000]
0.000: (sample_soil rover2 rover2store waypoint0)  [10.000]
0.000: (sample_rock rover0 rover0store waypoint2)  [8.000]
0.000: (calibrate rover0 camera5 objective2 waypoint2)  [5.000]
0.000: (navigate rover3 waypoint18 waypoint4)  [5.000]
5.001: (take_image rover1 waypoint9 objective4 camera0 high_res)  [7.000]
5.001: (take_image rover2 waypoint0 objective3 camera1 low_res)  [7.000]
5.001: (sample_rock rover3 rover3store waypoint4)  [8.000]
8.000: (navigate rover0 waypoint2 waypoint7)  [5.000]
8.001: (drop rover0 rover0store)  [1.000]
10.001: (communicate_soil_data rover2 general waypoint0 waypoint0 waypoint17)  [10.000]
12.001: (navigate rover1 waypoint9 waypoint0)  [5.000]
13.001: (sample_rock rover0 rover0store waypoint7)  [8.000]
13.001: (navigate rover3 waypoint4 waypoint18)  [5.000]
13.002: (drop rover3 rover3store)  [1.000]
18.002: (navigate rover3 waypoint18 waypoint0)  [5.000]
20.002: (communicate_image_data rover2 general objective3 low_res waypoint0 waypoint17)  [15.000]
21.001: (navigate rover0 waypoint7 waypoint2)  [5.000]
21.002: (drop rover0 rover0store)  [1.000]
26.001: (take_image rover0 waypoint2 objective5 camera5 colour)  [7.000]
33.001: (navigate rover0 waypoint2 waypoint14)  [5.000]
35.003: (communicate_image_data rover1 general objective4 high_res waypoint0 waypoint17)  [15.000]
38.001: (calibrate rover0 camera5 objective2 waypoint14)  [5.000]
38.002: (sample_soil rover0 rover0store waypoint14)  [10.000]
43.002: (take_image rover0 waypoint14 objective2 camera5 colour)  [7.000]
50.002: (navigate rover0 waypoint14 waypoint2)  [5.000]
50.004: (communicate_rock_data rover3 general waypoint4 waypoint0 waypoint17)  [10.000]
50.004: (navigate rover1 waypoint0 waypoint9)  [5.000]
55.003: (navigate rover0 waypoint2 waypoint0)  [5.000]
55.005: (navigate rover1 waypoint9 waypoint7)  [5.000]
60.005: (communicate_rock_data rover0 general waypoint7 waypoint0 waypoint17)  [10.000]
60.005: (navigate rover3 waypoint0 waypoint1)  [5.000]
60.006: (navigate rover1 waypoint7 waypoint5)  [5.000]
65.006: (navigate rover3 waypoint1 waypoint6)  [5.000]
65.007: (sample_rock rover1 rover1store waypoint5)  [8.000]
70.006: (communicate_rock_data rover0 general waypoint2 waypoint0 waypoint17)  [10.000]
70.007: (sample_rock rover3 rover3store waypoint6)  [8.000]
73.007: (navigate rover1 waypoint5 waypoint7)  [5.000]
73.008: (drop rover1 rover1store)  [1.000]
78.007: (navigate rover3 waypoint6 waypoint1)  [5.000]
78.008: (navigate rover1 waypoint7 waypoint9)  [5.000]
80.007: (communicate_image_data rover0 general objective5 colour waypoint0 waypoint17)  [15.000]
83.008: (navigate rover3 waypoint1 waypoint0)  [5.000]
83.009: (navigate rover1 waypoint9 waypoint0)  [5.000]
95.008: (communicate_image_data rover0 general objective2 colour waypoint0 waypoint17)  [15.000]
110.009: (communicate_soil_data rover0 general waypoint14 waypoint0 waypoint17)  [10.000]
120.010: (communicate_rock_data rover1 general waypoint5 waypoint0 waypoint17)  [10.000]
130.011: (communicate_rock_data rover3 general waypoint6 waypoint0 waypoint17)  [10.000]

 Execution time of Domain\Problem 18 
00:00:31.1620054

 DOMAIN/PROBLEM 19 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile19.pddl
Number of literals: 369
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 51.000
b (50.000 | 5.000)b (48.000 | 5.000)b (46.000 | 5.000)b (45.000 | 13.001)b (44.000 | 13.001)b (43.000 | 18.001)b (42.000 | 18.001)b (41.000 | 20.001)b (40.000 | 20.001)b (39.000 | 20.001)b (38.000 | 28.002)b (37.000 | 28.002)b (36.000 | 28.002)b (35.000 | 28.002)b (34.000 | 28.002)b (33.000 | 28.002)b (32.000 | 28.002)b (31.000 | 28.002)b (30.000 | 29.002)b (29.000 | 30.002)b (28.000 | 37.002)b (27.000 | 38.003)b (26.000 | 38.003)b (25.000 | 42.004)b (24.000 | 47.003)b (23.000 | 57.004)b (22.000 | 72.005)b (21.000 | 72.005)b (20.000 | 72.005)b (19.000 | 72.005)b (18.000 | 79.009)b (17.000 | 84.009)b (16.000 | 89.010)b (15.000 | 89.010)b (14.000 | 89.010)b (13.000 | 90.009)b (12.000 | 90.009)b (11.000 | 102.007)b (10.000 | 112.008)b (9.000 | 127.009)b (8.000 | 137.010)b (7.000 | 152.011)b (6.000 | 162.012)b (5.000 | 177.013)b (4.000 | 187.014)b (3.000 | 202.015)b (2.000 | 212.016)b (1.000 | 222.017);;;; Solution Found
; States evaluated: 8407
; Cost: 232.018
0.000: (calibrate rover2 camera0 objective0 waypoint13)  [5.000]
0.000: (navigate rover3 waypoint11 waypoint9)  [5.000]
0.000: (navigate rover4 waypoint0 waypoint6)  [5.000]
0.000: (navigate rover5 waypoint12 waypoint5)  [5.000]
0.000: (calibrate rover1 camera2 objective0 waypoint6)  [5.000]
0.000: (navigate rover0 waypoint2 waypoint0)  [5.000]
5.000: (navigate rover2 waypoint13 waypoint6)  [5.000]
5.000: (navigate rover1 waypoint6 waypoint0)  [5.000]
5.001: (sample_rock rover3 rover3store waypoint9)  [8.000]
5.001: (sample_rock rover4 rover4store waypoint6)  [8.000]
5.001: (sample_soil rover5 rover5store waypoint5)  [10.000]
5.001: (calibrate rover0 camera3 objective5 waypoint0)  [5.000]
10.000: (take_image rover1 waypoint0 objective7 camera2 low_res)  [7.000]
10.001: (navigate rover2 waypoint6 waypoint8)  [5.000]
10.002: (take_image rover0 waypoint0 objective4 camera3 high_res)  [7.000]
13.001: (navigate rover3 waypoint9 waypoint0)  [5.000]
13.001: (navigate rover4 waypoint6 waypoint0)  [5.000]
13.002: (drop rover4 rover4store)  [1.000]
15.001: (take_image rover2 waypoint8 objective0 camera0 high_res)  [7.000]
15.001: (navigate rover5 waypoint5 waypoint19)  [5.000]
15.002: (sample_soil rover2 rover2store waypoint8)  [10.000]
15.002: (drop rover5 rover5store)  [1.000]
17.001: (calibrate rover1 camera2 objective0 waypoint0)  [5.000]
18.001: (calibrate rover4 camera4 objective2 waypoint0)  [5.000]
18.001: (calibrate rover3 camera5 objective0 waypoint0)  [5.000]
20.002: (sample_rock rover5 rover5store waypoint19)  [8.000]
22.002: (communicate_image_data rover2 general objective0 high_res waypoint8 waypoint6)  [15.000]
22.002: (calibrate rover2 camera0 objective0 waypoint8)  [5.000]
22.002: (take_image rover1 waypoint0 objective6 camera2 low_res)  [7.000]
23.002: (take_image rover4 waypoint0 objective2 camera4 low_res)  [7.000]
23.002: (take_image rover3 waypoint0 objective7 camera5 colour)  [7.000]
27.003: (take_image rover2 waypoint8 objective5 camera0 colour)  [7.000]
28.002: (navigate rover5 waypoint19 waypoint5)  [5.000]
28.003: (drop rover5 rover5store)  [1.000]
29.003: (calibrate rover1 camera2 objective0 waypoint0)  [5.000]
30.003: (calibrate rover3 camera5 objective0 waypoint0)  [5.000]
30.003: (calibrate rover4 camera4 objective2 waypoint0)  [5.000]
33.003: (navigate rover5 waypoint5 waypoint0)  [5.000]
34.004: (take_image rover1 waypoint0 objective2 camera2 low_res)  [7.000]
35.004: (take_image rover4 waypoint0 objective0 camera4 colour)  [7.000]
35.004: (take_image rover3 waypoint0 objective5 camera5 colour)  [7.000]
37.003: (communicate_rock_data rover3 general waypoint9 waypoint0 waypoint6)  [10.000]
38.004: (navigate rover5 waypoint0 waypoint5)  [5.000]
42.004: (navigate rover4 waypoint0 waypoint18)  [5.000]
42.005: (calibrate rover3 camera5 objective0 waypoint0)  [5.000]
43.005: (navigate rover5 waypoint5 waypoint12)  [5.000]
47.004: (communicate_soil_data rover2 general waypoint8 waypoint8 waypoint6)  [10.000]
47.005: (sample_soil rover4 rover4store waypoint18)  [10.000]
47.006: (take_image rover3 waypoint0 objective0 camera5 colour)  [7.000]
48.006: (navigate rover5 waypoint12 waypoint4)  [5.000]
53.007: (navigate rover5 waypoint4 waypoint3)  [5.000]
57.005: (navigate rover2 waypoint8 waypoint6)  [5.000]
57.005: (communicate_image_data rover3 general objective7 colour waypoint0 waypoint6)  [15.000]
57.005: (navigate rover4 waypoint18 waypoint0)  [5.000]
57.006: (drop rover4 rover4store)  [1.000]
58.008: (sample_rock rover5 rover5store waypoint3)  [8.000]
62.006: (navigate rover2 waypoint6 waypoint13)  [5.000]
62.006: (navigate rover4 waypoint0 waypoint2)  [5.000]
66.008: (navigate rover5 waypoint3 waypoint4)  [5.000]
66.009: (drop rover5 rover5store)  [1.000]
67.007: (navigate rover2 waypoint13 waypoint0)  [5.000]
67.007: (navigate rover4 waypoint2 waypoint17)  [5.000]
71.009: (sample_rock rover5 rover5store waypoint4)  [8.000]
72.006: (communicate_image_data rover3 general objective0 colour waypoint0 waypoint6)  [15.000]
72.008: (sample_rock rover4 rover4store waypoint17)  [8.000]
79.009: (navigate rover5 waypoint4 waypoint12)  [5.000]
80.008: (navigate rover4 waypoint17 waypoint2)  [5.000]
84.010: (navigate rover5 waypoint12 waypoint1)  [5.000]
85.009: (navigate rover4 waypoint2 waypoint0)  [5.000]
87.007: (communicate_image_data rover1 general objective7 low_res waypoint0 waypoint6)  [15.000]
102.008: (communicate_rock_data rover5 general waypoint4 waypoint1 waypoint6)  [10.000]
112.009: (communicate_image_data rover1 general objective6 low_res waypoint0 waypoint6)  [15.000]
127.010: (communicate_rock_data rover5 general waypoint3 waypoint1 waypoint6)  [10.000]
137.011: (communicate_image_data rover1 general objective2 low_res waypoint0 waypoint6)  [15.000]
152.012: (communicate_rock_data rover5 general waypoint19 waypoint1 waypoint6)  [10.000]
162.013: (communicate_image_data rover0 general objective4 high_res waypoint0 waypoint6)  [15.000]
177.014: (communicate_soil_data rover5 general waypoint5 waypoint1 waypoint6)  [10.000]
187.015: (communicate_image_data rover2 general objective5 colour waypoint0 waypoint6)  [15.000]
202.016: (communicate_rock_data rover4 general waypoint6 waypoint0 waypoint6)  [10.000]
212.017: (communicate_rock_data rover4 general waypoint17 waypoint0 waypoint6)  [10.000]
222.018: (communicate_soil_data rover4 general waypoint18 waypoint0 waypoint6)  [10.000]

 Execution time of Domain\Problem 19 
00:10:27.7567660

 DOMAIN/PROBLEM 20 

File: E:\unmodified_domains\roverssimpletimedebugged\domain.pddl
File: E:\unmodified_domains\roverssimpletimedebugged\pfile20.pddl
Number of literals: 487
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

All the ground actions in this problem are compression-safe
Initial heuristic = 67.000

 Execution time of Domain\Problem 20 
00:30:00.2158040
