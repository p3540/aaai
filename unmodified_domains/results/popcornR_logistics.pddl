the results of squirrel t1_pt1 tests in popcorn 

 POPCORN-R 


 DOMAIN/PROBLEM 1 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile01.pddl
Number of literals: 204
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 26.000
b (24.000 | 0.000)b (23.000 | 0.000)b (22.000 | 0.001)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.002)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.002)b (13.000 | 0.002)b (12.000 | 0.003)b (11.000 | 0.005)b (10.000 | 0.005)b (9.000 | 0.006)b (8.000 | 0.007)b (7.000 | 0.007)b (6.000 | 0.007)b (5.000 | 0.007)b (4.000 | 0.008)b (3.000 | 0.008)b (2.000 | 0.009)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 105
; Cost: 0.011
0.000: (fly-airplane plane2 city4-2 city6-2) 
0.000: (load-airplane package5 plane1 city4-2) 
0.000: (load-truck package6 truck3 city3-1) 
0.000: (load-truck package4 truck1 city1-1) 
0.000: (load-truck package3 truck1 city1-1) 
0.000: (drive-truck truck6 city6-1 city6-2 city6) 
0.001: (fly-airplane plane1 city4-2 city6-2) 
0.001: (drive-truck truck3 city3-1 city3-2 city3) 
0.001: (drive-truck truck1 city1-1 city1-2 city1) 
0.001: (fly-airplane plane2 city6-2 city3-2) 
0.002: (unload-airplane package5 plane1 city6-2) 
0.002: (unload-truck package6 truck3 city3-2) 
0.002: (unload-truck package4 truck1 city1-2) 
0.002: (unload-truck package3 truck1 city1-2) 
0.003: (fly-airplane plane1 city6-2 city3-2) 
0.003: (load-airplane package6 plane2 city3-2) 
0.004: (fly-airplane plane2 city3-2 city1-2) 
0.004: (fly-airplane plane1 city3-2 city6-2) 
0.005: (unload-airplane package6 plane2 city1-2) 
0.005: (load-airplane package4 plane2 city1-2) 
0.005: (fly-airplane plane1 city6-2 city1-2) 
0.006: (fly-airplane plane2 city1-2 city3-2) 
0.006: (load-airplane package3 plane1 city1-2) 
0.006: (load-airplane package2 plane1 city1-2) 
0.007: (unload-airplane package4 plane2 city3-2) 
0.007: (fly-airplane plane1 city1-2 city6-2) 
0.008: (unload-airplane package3 plane1 city6-2) 
0.008: (unload-airplane package2 plane1 city6-2) 
0.009: (load-truck package3 truck6 city6-2) 
0.010: (drive-truck truck6 city6-2 city6-1 city6) 
0.011: (unload-truck package3 truck6 city6-1) 

 Execution time of Domain\Problem 1 
00:00:03.6864322

 DOMAIN/PROBLEM 2 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile02.pddl
Number of literals: 410
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 29.000
b (28.000 | 0.000)b (27.000 | 0.000)b (26.000 | 0.000)b (25.000 | 0.000)b (24.000 | 0.001)b (23.000 | 0.002)b (22.000 | 0.002)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.003)b (18.000 | 0.003)b (17.000 | 0.003)b (16.000 | 0.004)b (15.000 | 0.005)b (14.000 | 0.006)b (13.000 | 0.006)b (12.000 | 0.006)b (11.000 | 0.006)b (10.000 | 0.006)b (9.000 | 0.006)b (8.000 | 0.006)b (7.000 | 0.006)b (6.000 | 0.006)b (5.000 | 0.007)b (4.000 | 0.008)b (3.000 | 0.009)b (2.000 | 0.009)b (1.000 | 0.009);;;; Solution Found
; States evaluated: 143
; Cost: 0.009
0.000: (fly-airplane plane1 city6-2 city9-2) 
0.000: (fly-airplane plane2 city3-2 city1-2) 
0.000: (load-airplane package3 plane4 city3-2) 
0.000: (load-airplane package4 plane3 city7-2) 
0.000: (load-truck package2 truck10 city10-1) 
0.000: (drive-truck truck6 city6-1 city6-2 city6) 
0.000: (drive-truck truck3 city3-1 city3-2 city3) 
0.000: (drive-truck truck1 city1-1 city1-2 city1) 
0.001: (fly-airplane plane3 city7-2 city6-2) 
0.001: (load-airplane package5 plane2 city1-2) 
0.001: (fly-airplane plane4 city3-2 city2-2) 
0.001: (fly-airplane plane1 city9-2 city10-2) 
0.001: (drive-truck truck10 city10-1 city10-2 city10) 
0.002: (unload-airplane package4 plane3 city6-2) 
0.002: (fly-airplane plane2 city1-2 city4-2) 
0.002: (load-airplane package1 plane4 city2-2) 
0.002: (unload-truck package2 truck10 city10-2) 
0.003: (unload-airplane package5 plane2 city4-2) 
0.003: (fly-airplane plane4 city2-2 city1-2) 
0.003: (load-airplane package2 plane1 city10-2) 
0.003: (load-truck package4 truck6 city6-2) 
0.004: (unload-airplane package3 plane4 city1-2) 
0.004: (fly-airplane plane1 city10-2 city9-2) 
0.004: (drive-truck truck6 city6-2 city6-1 city6) 
0.005: (fly-airplane plane4 city1-2 city3-2) 
0.005: (unload-airplane package2 plane1 city9-2) 
0.005: (unload-truck package4 truck6 city6-1) 
0.005: (load-truck package3 truck1 city1-2) 
0.006: (unload-airplane package1 plane4 city3-2) 
0.006: (drive-truck truck1 city1-2 city1-1 city1) 
0.007: (load-truck package1 truck3 city3-2) 
0.007: (unload-truck package3 truck1 city1-1) 
0.008: (drive-truck truck3 city3-2 city3-1 city3) 
0.009: (unload-truck package1 truck3 city3-1) 

 Execution time of Domain\Problem 2 
00:00:08.4125525

 DOMAIN/PROBLEM 3 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile03.pddl
Number of literals: 1184
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 53.000
b (52.000 | 0.000)b (51.000 | 0.000)b (50.000 | 0.000)b (49.000 | 0.000)b (48.000 | 0.000)b (47.000 | 0.000)b (46.000 | 0.000)b (45.000 | 0.001)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.002)b (40.000 | 0.003)b (39.000 | 0.004)b (38.000 | 0.005)b (37.000 | 0.006)b (36.000 | 0.007)b (35.000 | 0.008)b (34.000 | 0.008)b (33.000 | 0.008)b (32.000 | 0.008)b (31.000 | 0.008)b (30.000 | 0.008)b (29.000 | 0.008)b (28.000 | 0.008)b (27.000 | 0.008)b (26.000 | 0.008)b (25.000 | 0.009)b (24.000 | 0.009)b (23.000 | 0.009)b (22.000 | 0.009)b (21.000 | 0.009)b (19.000 | 0.009)b (18.000 | 0.009)b (17.000 | 0.009)b (16.000 | 0.009)b (15.000 | 0.010)b (14.000 | 0.010)b (13.000 | 0.010)b (12.000 | 0.010)b (11.000 | 0.010)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.011);;;; Solution Found
; States evaluated: 434
; Cost: 0.012
0.000: (fly-airplane plane4 city10-3 city12-3) 
0.000: (fly-airplane plane1 city11-3 city12-3) 
0.000: (drive-truck truck9 city9-2 city9-3 city9) 
0.000: (drive-truck truck8 city8-2 city8-3 city8) 
0.000: (drive-truck truck12 city12-1 city12-3 city12) 
0.000: (drive-truck truck11 city11-1 city11-3 city11) 
0.000: (drive-truck truck1 city1-1 city1-3 city1) 
0.000: (load-truck package9 truck4 city4-2) 
0.000: (fly-airplane plane2 city11-3 city4-3) 
0.000: (fly-airplane plane3 city1-3 city8-3) 
0.000: (drive-truck truck13 city13-1 city13-3 city13) 
0.000: (drive-truck truck10 city10-1 city10-3 city10) 
0.001: (load-airplane package3 plane1 city12-3) 
0.001: (drive-truck truck4 city4-2 city4-3 city4) 
0.001: (drive-truck truck9 city9-3 city9-1 city9) 
0.001: (drive-truck truck8 city8-3 city8-1 city8) 
0.001: (drive-truck truck12 city12-3 city12-2 city12) 
0.001: (drive-truck truck1 city1-3 city1-2 city1) 
0.002: (fly-airplane plane1 city12-3 city9-3) 
0.002: (unload-truck package9 truck4 city4-3) 
0.002: (load-truck package8 truck9 city9-1) 
0.002: (load-truck package6 truck9 city9-1) 
0.002: (load-truck package4 truck8 city8-1) 
0.002: (load-truck package5 truck12 city12-2) 
0.002: (load-truck package7 truck1 city1-2) 
0.003: (load-airplane package9 plane2 city4-3) 
0.003: (drive-truck truck9 city9-1 city9-3 city9) 
0.003: (drive-truck truck8 city8-1 city8-3 city8) 
0.003: (drive-truck truck12 city12-2 city12-3 city12) 
0.003: (drive-truck truck1 city1-2 city1-3 city1) 
0.004: (fly-airplane plane2 city4-3 city11-3) 
0.004: (unload-truck package8 truck9 city9-3) 
0.004: (unload-truck package6 truck9 city9-3) 
0.004: (unload-truck package4 truck8 city8-3) 
0.004: (unload-truck package5 truck12 city12-3) 
0.004: (unload-truck package7 truck1 city1-3) 
0.005: (unload-airplane package9 plane2 city11-3) 
0.005: (load-airplane package8 plane1 city9-3) 
0.005: (load-airplane package4 plane3 city8-3) 
0.005: (load-airplane package5 plane4 city12-3) 
0.006: (load-truck package9 truck11 city11-3) 
0.006: (fly-airplane plane1 city9-3 city5-3) 
0.006: (fly-airplane plane3 city8-3 city13-3) 
0.006: (fly-airplane plane4 city12-3 city1-3) 
0.006: (fly-airplane plane2 city11-3 city10-3) 
0.007: (drive-truck truck11 city11-3 city11-2 city11) 
0.007: (unload-airplane package8 plane1 city5-3) 
0.007: (unload-airplane package4 plane3 city13-3) 
0.007: (load-airplane package7 plane4 city1-3) 
0.008: (unload-truck package9 truck11 city11-2) 
0.008: (fly-airplane plane1 city5-3 city1-3) 
0.008: (load-truck package4 truck13 city13-3) 
0.008: (fly-airplane plane4 city1-3 city10-3) 
0.009: (unload-airplane package3 plane1 city1-3) 
0.009: (drive-truck truck13 city13-3 city13-1 city13) 
0.009: (unload-airplane package7 plane4 city10-3) 
0.009: (unload-airplane package5 plane4 city10-3) 
0.010: (unload-truck package4 truck13 city13-1) 
0.010: (load-truck package5 truck10 city10-3) 
0.011: (drive-truck truck10 city10-3 city10-1 city10) 
0.012: (unload-truck package5 truck10 city10-1) 

 Execution time of Domain\Problem 3 
00:02:17.4303332

 DOMAIN/PROBLEM 4 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile04.pddl
Number of literals: 1821
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 58.000
b (57.000 | 0.000)b (56.000 | 0.000)b (55.000 | 0.000)b (54.000 | 0.000)b (53.000 | 0.000)b (52.000 | 0.000)b (51.000 | 0.001)b (50.000 | 0.001)b (49.000 | 0.001)b (48.000 | 0.002)b (47.000 | 0.002)b (46.000 | 0.002)b (45.000 | 0.002)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.003)b (40.000 | 0.003)b (39.000 | 0.004)b (37.000 | 0.005)b (36.000 | 0.006)b (35.000 | 0.007)b (34.000 | 0.008)b (33.000 | 0.009)b (32.000 | 0.009)b (31.000 | 0.009)b (30.000 | 0.009)b (29.000 | 0.009)b (28.000 | 0.009)b (27.000 | 0.009)b (26.000 | 0.009)b (25.000 | 0.009)b (24.000 | 0.009)b (23.000 | 0.009)b (22.000 | 0.009)b (21.000 | 0.009)b (20.000 | 0.009)b (19.000 | 0.009)b (18.000 | 0.009)b (17.000 | 0.009)b (16.000 | 0.009)b (15.000 | 0.009)b (14.000 | 0.009)b (13.000 | 0.009)b (12.000 | 0.009)b (11.000 | 0.009)b (10.000 | 0.009)b (9.000 | 0.009)b (8.000 | 0.009)b (7.000 | 0.009)b (6.000 | 0.009)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 590
; Cost: 0.010
0.000: (fly-airplane plane5 city12-4 city1-4) 
0.000: (drive-truck truck19 city9-1 city9-4 city9) 
0.000: (drive-truck truck17 city7-3 city7-4 city7) 
0.000: (drive-truck truck14 city4-1 city4-4 city4) 
0.000: (drive-truck truck12 city2-1 city2-3 city2) 
0.000: (drive-truck truck23 city13-2 city13-4 city13) 
0.000: (load-truck package2 truck21 city11-3) 
0.000: (load-truck package5 truck20 city10-1) 
0.000: (fly-airplane plane4 city8-4 city5-4) 
0.000: (fly-airplane plane2 city13-4 city11-4) 
0.000: (fly-airplane plane3 city6-4 city10-4) 
0.000: (drive-truck truck22 city12-3 city12-4 city12) 
0.001: (load-airplane package7 plane5 city1-4) 
0.001: (drive-truck truck21 city11-3 city11-4 city11) 
0.001: (drive-truck truck20 city10-1 city10-4 city10) 
0.001: (load-truck package6 truck12 city2-3) 
0.001: (load-airplane package4 plane4 city5-4) 
0.001: (drive-truck truck14 city4-4 city4-2 city4) 
0.002: (unload-truck package2 truck21 city11-4) 
0.002: (unload-truck package5 truck20 city10-4) 
0.002: (drive-truck truck12 city2-3 city2-4 city2) 
0.002: (fly-airplane plane5 city1-4 city12-4) 
0.002: (fly-airplane plane4 city5-4 city8-4) 
0.002: (load-truck package1 truck14 city4-2) 
0.003: (unload-truck package6 truck12 city2-4) 
0.003: (unload-airplane package7 plane5 city12-4) 
0.003: (unload-airplane package4 plane4 city8-4) 
0.003: (load-airplane package5 plane3 city10-4) 
0.003: (load-airplane package2 plane2 city11-4) 
0.003: (drive-truck truck14 city4-2 city4-4 city4) 
0.003: (drive-truck truck21 city11-4 city11-1 city11) 
0.004: (load-airplane package6 plane1 city2-4) 
0.004: (fly-airplane plane3 city10-4 city12-4) 
0.004: (fly-airplane plane2 city11-4 city7-4) 
0.004: (unload-truck package1 truck14 city4-4) 
0.004: (load-truck package7 truck22 city12-4) 
0.004: (load-truck package3 truck21 city11-1) 
0.004: (fly-airplane plane4 city8-4 city4-4) 
0.004: (fly-airplane plane5 city12-4 city11-4) 
0.005: (fly-airplane plane1 city2-4 city9-4) 
0.005: (unload-airplane package5 plane3 city12-4) 
0.005: (unload-airplane package2 plane2 city7-4) 
0.005: (load-airplane package1 plane4 city4-4) 
0.005: (drive-truck truck21 city11-1 city11-4 city11) 
0.006: (unload-airplane package6 plane1 city9-4) 
0.006: (load-truck package2 truck17 city7-4) 
0.006: (load-truck package5 truck22 city12-4) 
0.006: (fly-airplane plane4 city4-4 city13-4) 
0.006: (unload-truck package3 truck21 city11-4) 
0.006: (fly-airplane plane2 city7-4 city11-4) 
0.006: (fly-airplane plane3 city12-4 city13-4) 
0.007: (load-truck package6 truck19 city9-4) 
0.007: (drive-truck truck17 city7-4 city7-1 city7) 
0.007: (drive-truck truck22 city12-4 city12-3 city12) 
0.007: (unload-airplane package1 plane4 city13-4) 
0.007: (fly-airplane plane1 city9-4 city13-4) 
0.007: (load-airplane package3 plane5 city11-4) 
0.008: (drive-truck truck19 city9-4 city9-3 city9) 
0.008: (unload-truck package2 truck17 city7-1) 
0.008: (unload-truck package7 truck22 city12-3) 
0.008: (unload-truck package5 truck22 city12-3) 
0.008: (load-truck package1 truck23 city13-4) 
0.008: (fly-airplane plane5 city11-4 city13-4) 
0.009: (unload-truck package6 truck19 city9-3) 
0.009: (drive-truck truck23 city13-4 city13-1 city13) 
0.009: (unload-airplane package3 plane5 city13-4) 
0.010: (unload-truck package1 truck23 city13-1) 

 Execution time of Domain\Problem 4 
00:08:19.3429996

 DOMAIN/PROBLEM 5 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile05.pddl
Number of literals: 327
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 22.000
b (21.000 | 0.000)b (20.000 | 0.000)b (19.000 | 0.001)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.003)b (13.000 | 0.004)b (12.000 | 0.005)b (11.000 | 0.007)b (10.000 | 0.008)b (9.000 | 0.010)b (8.000 | 0.011)b (7.000 | 0.012)b (6.000 | 0.013)b (5.000 | 0.014)b (4.000 | 0.015)b (3.000 | 0.015)b (2.000 | 0.016)b (1.000 | 0.016);;;; Solution Found
; States evaluated: 106
; Cost: 0.017
0.000: (load-airplane package2 plane1 city8-2) 
0.000: (load-truck package3 truck11 city9-1) 
0.000: (load-truck package4 truck10 city8-1) 
0.001: (drive-truck truck11 city9-1 city9-2 city9) 
0.001: (drive-truck truck10 city8-1 city8-2 city8) 
0.002: (unload-truck package3 truck11 city9-2) 
0.002: (unload-truck package4 truck10 city8-2) 
0.003: (load-airplane package4 plane1 city8-2) 
0.004: (fly-airplane plane1 city8-2 city9-2) 
0.005: (load-airplane package3 plane1 city9-2) 
0.006: (fly-airplane plane1 city9-2 city7-2) 
0.007: (unload-airplane package3 plane1 city7-2) 
0.008: (fly-airplane plane1 city7-2 city9-2) 
0.009: (fly-airplane plane1 city9-2 city6-2) 
0.010: (load-airplane package1 plane1 city6-2) 
0.011: (fly-airplane plane1 city6-2 city9-2) 
0.012: (unload-airplane package1 plane1 city9-2) 
0.013: (fly-airplane plane1 city9-2 city3-2) 
0.014: (unload-airplane package2 plane1 city3-2) 
0.015: (fly-airplane plane1 city3-2 city2-2) 
0.015: (load-truck package2 truck1 city3-2) 
0.016: (drive-truck truck1 city3-2 city3-1 city3) 
0.016: (unload-airplane package4 plane1 city2-2) 
0.017: (unload-truck package2 truck1 city3-1) 

 Execution time of Domain\Problem 5 
00:00:03.5633376

 DOMAIN/PROBLEM 6 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile06.pddl
Number of literals: 3508
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 69.000

 Execution time of Domain\Problem 6 
00:30:00.2162224

 DOMAIN/PROBLEM 7 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile07.pddl
Number of literals: 698
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 32.000
b (30.000 | 0.000)b (29.000 | 0.000)b (28.000 | 0.001)b (27.000 | 0.002)b (26.000 | 0.002)b (25.000 | 0.002)b (24.000 | 0.002)b (23.000 | 0.002)b (22.000 | 0.003)b (21.000 | 0.003)b (20.000 | 0.004)b (19.000 | 0.004)b (18.000 | 0.004)b (17.000 | 0.004)b (16.000 | 0.004)b (15.000 | 0.005)b (14.000 | 0.005)b (13.000 | 0.005)b (12.000 | 0.005)b (11.000 | 0.006)b (10.000 | 0.007)b (9.000 | 0.008)b (8.000 | 0.008)b (7.000 | 0.008)b (6.000 | 0.008)b (5.000 | 0.008)b (4.000 | 0.008)b (3.000 | 0.008)b (2.000 | 0.008)b (1.000 | 0.008);;;; Solution Found
; States evaluated: 195
; Cost: 0.008
0.000: (fly-airplane plane1 city1-2 city7-2) 
0.000: (load-airplane package9 plane5 city5-2) 
0.000: (load-airplane package7 plane4 city6-2) 
0.000: (load-airplane package10 plane4 city6-2) 
0.000: (load-truck package5 truck4 city4-1) 
0.000: (drive-truck truck9 city9-1 city9-2 city9) 
0.000: (drive-truck truck7 city7-1 city7-2 city7) 
0.000: (drive-truck truck5 city5-1 city5-2 city5) 
0.000: (fly-airplane plane6 city2-2 city10-2) 
0.000: (fly-airplane plane3 city9-2 city10-2) 
0.000: (fly-airplane plane2 city8-2 city10-2) 
0.001: (fly-airplane plane5 city5-2 city9-2) 
0.001: (fly-airplane plane4 city6-2 city9-2) 
0.001: (fly-airplane plane1 city7-2 city4-2) 
0.001: (drive-truck truck4 city4-1 city4-2 city4) 
0.001: (load-airplane package8 plane2 city10-2) 
0.002: (unload-airplane package9 plane5 city9-2) 
0.002: (unload-airplane package10 plane4 city9-2) 
0.002: (unload-truck package5 truck4 city4-2) 
0.002: (fly-airplane plane2 city10-2 city1-2) 
0.003: (fly-airplane plane4 city9-2 city5-2) 
0.003: (load-airplane package5 plane1 city4-2) 
0.003: (load-truck package10 truck9 city9-2) 
0.003: (unload-airplane package8 plane2 city1-2) 
0.003: (load-airplane package6 plane2 city1-2) 
0.004: (unload-airplane package7 plane4 city5-2) 
0.004: (fly-airplane plane1 city4-2 city7-2) 
0.004: (drive-truck truck9 city9-2 city9-1 city9) 
0.004: (fly-airplane plane2 city1-2 city10-2) 
0.005: (unload-airplane package5 plane1 city7-2) 
0.005: (unload-truck package10 truck9 city9-1) 
0.005: (load-truck package7 truck5 city5-2) 
0.005: (unload-airplane package6 plane2 city10-2) 
0.006: (load-truck package5 truck7 city7-2) 
0.006: (drive-truck truck5 city5-2 city5-1 city5) 
0.007: (drive-truck truck7 city7-2 city7-1 city7) 
0.007: (unload-truck package7 truck5 city5-1) 
0.008: (unload-truck package5 truck7 city7-1) 

 Execution time of Domain\Problem 7 
00:00:32.6618210

 DOMAIN/PROBLEM 8 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile08.pddl
Number of literals: 4388
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 42.000

 Execution time of Domain\Problem 8 
00:30:00.2333906

 DOMAIN/PROBLEM 9 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile09.pddl
Number of literals: 3068
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 84.000

 Execution time of Domain\Problem 9 
00:30:00.1896370

 DOMAIN/PROBLEM 10 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile10.pddl
Number of literals: 2391
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 98.000

 Execution time of Domain\Problem 10 
00:30:00.2296038

 DOMAIN/PROBLEM 11 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile11.pddl
Number of literals: 512
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 28.000
b (27.000 | 0.000)b (26.000 | 0.000)b (25.000 | 0.001)b (24.000 | 0.002)b (23.000 | 0.002)b (22.000 | 0.002)b (21.000 | 0.002)b (20.000 | 0.002)b (19.000 | 0.002)b (18.000 | 0.002)b (17.000 | 0.002)b (16.000 | 0.002)b (15.000 | 0.002)b (14.000 | 0.003)b (13.000 | 0.003)b (12.000 | 0.003)b (11.000 | 0.004)b (10.000 | 0.004)b (9.000 | 0.005)b (8.000 | 0.005)b (7.000 | 0.006)b (6.000 | 0.007)b (5.000 | 0.008)b (4.000 | 0.009)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 129
; Cost: 0.010
0.000: (drive-truck truck20 city1-2 city1-3 city1) 
0.000: (load-truck package2 truck22 city3-2) 
0.000: (load-truck package7 truck16 city2-1) 
0.000: (load-truck package8 truck13 city1-2) 
0.000: (load-truck package3 truck12 city2-2) 
0.000: (fly-airplane plane3 city2-3 city3-3) 
0.001: (drive-truck truck22 city3-2 city3-3 city3) 
0.001: (load-truck package1 truck20 city1-3) 
0.001: (drive-truck truck16 city2-1 city2-3 city2) 
0.001: (drive-truck truck12 city2-2 city2-3 city2) 
0.001: (drive-truck truck13 city1-2 city1-3 city1) 
0.002: (unload-truck package2 truck22 city3-3) 
0.002: (drive-truck truck20 city1-3 city1-1 city1) 
0.002: (unload-truck package7 truck16 city2-3) 
0.002: (unload-truck package3 truck12 city2-3) 
0.002: (unload-truck package8 truck13 city1-3) 
0.003: (unload-truck package1 truck20 city1-1) 
0.003: (load-airplane package8 plane1 city1-3) 
0.003: (load-airplane package7 plane2 city2-3) 
0.003: (load-airplane package3 plane2 city2-3) 
0.003: (load-airplane package2 plane3 city3-3) 
0.004: (fly-airplane plane1 city1-3 city3-3) 
0.004: (fly-airplane plane2 city2-3 city3-3) 
0.004: (fly-airplane plane3 city3-3 city2-3) 
0.005: (unload-airplane package8 plane1 city3-3) 
0.005: (unload-airplane package7 plane2 city3-3) 
0.005: (unload-airplane package2 plane3 city2-3) 
0.006: (fly-airplane plane2 city3-3 city1-3) 
0.007: (unload-airplane package3 plane2 city1-3) 
0.008: (load-truck package3 truck13 city1-3) 
0.009: (drive-truck truck13 city1-3 city1-2 city1) 
0.010: (unload-truck package3 truck13 city1-2) 

 Execution time of Domain\Problem 11 
00:00:11.7898531

 DOMAIN/PROBLEM 12 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile12.pddl
Number of literals: 7043
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 42.000

 Execution time of Domain\Problem 12 
00:30:00.1938694

 DOMAIN/PROBLEM 13 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile13.pddl
Number of literals: 7167
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 68.000

 Execution time of Domain\Problem 13 
00:30:00.1779795

 DOMAIN/PROBLEM 14 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile14.pddl
Number of literals: 3355
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 88.000

 Execution time of Domain\Problem 14 
00:30:00.2054656

 DOMAIN/PROBLEM 15 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile15.pddl
Number of literals: 801
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 85.000
b (84.000 | 0.000)b (83.000 | 0.001)b (82.000 | 0.002)b (81.000 | 0.002)b (80.000 | 0.002)b (79.000 | 0.002)b (78.000 | 0.002)b (77.000 | 0.002)b (76.000 | 0.002)b (75.000 | 0.002)b (74.000 | 0.002)b (73.000 | 0.002)b (72.000 | 0.003)b (71.000 | 0.005)b (70.000 | 0.006)b (69.000 | 0.007)b (68.000 | 0.007)b (67.000 | 0.007)b (66.000 | 0.007)b (65.000 | 0.007)b (64.000 | 0.007)b (63.000 | 0.007)b (62.000 | 0.007)b (61.000 | 0.007)b (60.000 | 0.007)b (59.000 | 0.007)b (58.000 | 0.007)b (57.000 | 0.008)b (56.000 | 0.009)b (55.000 | 0.010)b (54.000 | 0.011)b (53.000 | 0.011)b (52.000 | 0.011)b (51.000 | 0.011)b (50.000 | 0.011)b (49.000 | 0.012)b (48.000 | 0.013)b (47.000 | 0.014)b (46.000 | 0.015)b (45.000 | 0.015)b (44.000 | 0.015)b (43.000 | 0.015)b (42.000 | 0.015)b (41.000 | 0.016)b (40.000 | 0.016)b (39.000 | 0.016)b (38.000 | 0.016)b (37.000 | 0.016)b (36.000 | 0.017)b (35.000 | 0.018)b (34.000 | 0.018)b (33.000 | 0.019)b (32.000 | 0.019)b (31.000 | 0.019)b (30.000 | 0.019)b (29.000 | 0.019)b (28.000 | 0.020)b (27.000 | 0.020)b (26.000 | 0.020)b (25.000 | 0.020)b (24.000 | 0.020)b (23.000 | 0.020)b (22.000 | 0.020)b (21.000 | 0.020)b (20.000 | 0.020)b (19.000 | 0.020)b (18.000 | 0.020)b (17.000 | 0.020)b (16.000 | 0.020)b (15.000 | 0.020)b (14.000 | 0.020)b (13.000 | 0.020)b (12.000 | 0.020)b (11.000 | 0.020)b (10.000 | 0.020)b (9.000 | 0.021)b (8.000 | 0.021)b (7.000 | 0.021)b (6.000 | 0.021)b (5.000 | 0.022)b (4.000 | 0.022)b (3.000 | 0.022)b (2.000 | 0.023)b (1.000 | 0.024);;;; Solution Found
; States evaluated: 3349
; Cost: 0.024
0.000: (load-airplane package12 plane2 city1-6) 
0.000: (load-truck package13 truck5 city3-4) 
0.000: (load-truck package20 truck4 city2-3) 
0.000: (load-truck package17 truck4 city2-3) 
0.000: (load-truck package23 truck3 city1-5) 
0.000: (drive-truck truck2 city1-3 city1-6 city1) 
0.000: (drive-truck truck1 city1-6 city1-4 city1) 
0.001: (fly-airplane plane2 city1-6 city3-6) 
0.001: (drive-truck truck5 city3-4 city3-6 city3) 
0.001: (drive-truck truck3 city1-5 city1-6 city1) 
0.001: (drive-truck truck4 city2-3 city2-6 city2) 
0.001: (load-truck package11 truck1 city1-4) 
0.002: (unload-airplane package12 plane2 city3-6) 
0.002: (unload-truck package13 truck5 city3-6) 
0.002: (unload-truck package23 truck3 city1-6) 
0.002: (unload-truck package20 truck4 city2-6) 
0.002: (unload-truck package17 truck4 city2-6) 
0.002: (drive-truck truck1 city1-4 city1-2 city1) 
0.003: (load-truck package12 truck5 city3-6) 
0.003: (load-airplane package20 plane1 city2-6) 
0.003: (load-airplane package17 plane1 city2-6) 
0.003: (drive-truck truck4 city2-6 city2-4 city2) 
0.003: (load-truck package19 truck1 city1-2) 
0.003: (load-airplane package13 plane3 city3-6) 
0.003: (load-airplane package23 plane4 city1-6) 
0.004: (drive-truck truck5 city3-6 city3-5 city3) 
0.004: (load-truck package21 truck4 city2-4) 
0.004: (drive-truck truck1 city1-2 city1-3 city1) 
0.005: (load-truck package16 truck5 city3-5) 
0.005: (drive-truck truck4 city2-4 city2-6 city2) 
0.005: (unload-truck package19 truck1 city1-3) 
0.006: (drive-truck truck5 city3-5 city3-6 city3) 
0.006: (unload-truck package21 truck4 city2-6) 
0.006: (drive-truck truck1 city1-3 city1-6 city1) 
0.007: (unload-truck package16 truck5 city3-6) 
0.007: (load-airplane package21 plane1 city2-6) 
0.007: (drive-truck truck4 city2-6 city2-2 city2) 
0.007: (unload-truck package11 truck1 city1-6) 
0.008: (load-truck package15 truck4 city2-2) 
0.008: (drive-truck truck5 city3-6 city3-3 city3) 
0.008: (drive-truck truck1 city1-6 city1-5 city1) 
0.008: (load-airplane package16 plane2 city3-6) 
0.008: (load-airplane package11 plane4 city1-6) 
0.009: (drive-truck truck4 city2-2 city2-6 city2) 
0.009: (load-truck package18 truck5 city3-3) 
0.009: (load-truck package10 truck5 city3-3) 
0.009: (fly-airplane plane2 city3-6 city1-6) 
0.009: (fly-airplane plane4 city1-6 city2-6) 
0.009: (drive-truck truck1 city1-5 city1-6 city1) 
0.010: (unload-truck package15 truck4 city2-6) 
0.010: (drive-truck truck5 city3-3 city3-6 city3) 
0.010: (unload-airplane package16 plane2 city1-6) 
0.010: (unload-airplane package23 plane4 city2-6) 
0.010: (unload-airplane package11 plane4 city2-6) 
0.011: (load-airplane package15 plane1 city2-6) 
0.011: (unload-truck package18 truck5 city3-6) 
0.011: (unload-truck package10 truck5 city3-6) 
0.011: (drive-truck truck4 city2-6 city2-1 city2) 
0.011: (fly-airplane plane2 city1-6 city3-6) 
0.011: (load-truck package16 truck1 city1-6) 
0.012: (load-truck package14 truck4 city2-1) 
0.012: (drive-truck truck5 city3-6 city3-2 city3) 
0.012: (load-airplane package18 plane3 city3-6) 
0.012: (load-airplane package10 plane3 city3-6) 
0.013: (drive-truck truck4 city2-1 city2-6 city2) 
0.013: (unload-truck package12 truck5 city3-2) 
0.013: (fly-airplane plane3 city3-6 city2-6) 
0.014: (unload-truck package14 truck4 city2-6) 
0.014: (drive-truck truck5 city3-2 city3-6 city3) 
0.014: (unload-airplane package13 plane3 city2-6) 
0.014: (load-truck package23 truck4 city2-6) 
0.015: (load-airplane package14 plane1 city2-6) 
0.015: (fly-airplane plane3 city2-6 city1-6) 
0.015: (load-truck package13 truck4 city2-6) 
0.016: (fly-airplane plane1 city2-6 city3-6) 
0.016: (drive-truck truck4 city2-6 city2-5 city2) 
0.016: (unload-airplane package18 plane3 city1-6) 
0.016: (unload-airplane package10 plane3 city1-6) 
0.017: (unload-airplane package15 plane1 city3-6) 
0.017: (unload-truck package13 truck4 city2-5) 
0.017: (load-truck package18 truck1 city1-6) 
0.018: (fly-airplane plane1 city3-6 city1-6) 
0.018: (load-truck package15 truck5 city3-6) 
0.018: (drive-truck truck4 city2-5 city2-3 city2) 
0.019: (drive-truck truck5 city3-6 city3-3 city3) 
0.019: (unload-airplane package21 plane1 city1-6) 
0.019: (unload-airplane package20 plane1 city1-6) 
0.019: (unload-airplane package17 plane1 city1-6) 
0.019: (unload-airplane package14 plane1 city1-6) 
0.019: (unload-truck package23 truck4 city2-3) 
0.020: (unload-truck package15 truck5 city3-3) 
0.020: (load-truck package20 truck1 city1-6) 
0.020: (load-truck package17 truck2 city1-6) 
0.020: (load-truck package14 truck2 city1-6) 
0.021: (drive-truck truck1 city1-6 city1-3 city1) 
0.021: (drive-truck truck2 city1-6 city1-5 city1) 
0.022: (unload-truck package17 truck2 city1-5) 
0.022: (unload-truck package14 truck2 city1-5) 
0.022: (unload-truck package18 truck1 city1-3) 
0.023: (drive-truck truck1 city1-3 city1-1 city1) 
0.024: (unload-truck package20 truck1 city1-1) 
0.024: (unload-truck package16 truck1 city1-1) 

 Execution time of Domain\Problem 15 
00:06:39.2274542

 DOMAIN/PROBLEM 16 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile16.pddl
Number of literals: 2545
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 55.000
b (54.000 | 0.000)b (53.000 | 0.000)b (52.000 | 0.000)b (51.000 | 0.000)b (50.000 | 0.001)b (49.000 | 0.001)b (48.000 | 0.001)b (47.000 | 0.001)b (46.000 | 0.001)b (45.000 | 0.002)b (44.000 | 0.002)b (43.000 | 0.002)b (42.000 | 0.002)b (41.000 | 0.002)b (40.000 | 0.002)b (39.000 | 0.003)b (38.000 | 0.003)b (37.000 | 0.003)b (36.000 | 0.003)b (35.000 | 0.004)b (34.000 | 0.004)b (33.000 | 0.004)b (32.000 | 0.004)b (31.000 | 0.004)b (30.000 | 0.004)b (29.000 | 0.004)b (28.000 | 0.005)b (27.000 | 0.006)b (26.000 | 0.007)b (25.000 | 0.008)b (24.000 | 0.008)b (23.000 | 0.008)b (22.000 | 0.008)b (21.000 | 0.008)b (20.000 | 0.008)b (19.000 | 0.008)b (18.000 | 0.008)b (17.000 | 0.008)b (16.000 | 0.009)b (15.000 | 0.010)b (14.000 | 0.010)b (13.000 | 0.010)b (12.000 | 0.010)b (11.000 | 0.010)b (10.000 | 0.010)b (9.000 | 0.010)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.011)b (1.000 | 0.012);;;; Solution Found
; States evaluated: 471
; Cost: 0.012
0.000: (fly-airplane plane1 city6-3 city7-3) 
0.000: (drive-truck truck45 city4-1 city4-3 city4) 
0.000: (load-truck package5 truck52 city11-2) 
0.000: (load-truck package4 truck31 city2-2) 
0.000: (load-truck package3 truck26 city7-2) 
0.000: (load-truck package2 truck26 city7-2) 
0.000: (load-truck package1 truck26 city7-2) 
0.000: (load-truck package6 truck24 city4-1) 
0.000: (fly-airplane plane10 city1-3 city2-3) 
0.000: (fly-airplane plane12 city5-3 city4-3) 
0.000: (drive-truck truck1 city5-2 city5-3 city5) 
0.001: (drive-truck truck31 city2-2 city2-1 city2) 
0.001: (drive-truck truck26 city7-2 city7-3 city7) 
0.001: (drive-truck truck24 city4-1 city4-2 city4) 
0.001: (drive-truck truck52 city11-2 city11-3 city11) 
0.002: (unload-truck package3 truck26 city7-3) 
0.002: (unload-truck package2 truck26 city7-3) 
0.002: (unload-truck package1 truck26 city7-3) 
0.002: (load-truck package7 truck24 city4-2) 
0.002: (drive-truck truck31 city2-1 city2-3 city2) 
0.002: (unload-truck package5 truck52 city11-3) 
0.003: (drive-truck truck24 city4-2 city4-3 city4) 
0.003: (load-airplane package3 plane1 city7-3) 
0.003: (load-airplane package2 plane1 city7-3) 
0.003: (load-airplane package1 plane1 city7-3) 
0.003: (unload-truck package4 truck31 city2-3) 
0.003: (load-airplane package5 plane11 city11-3) 
0.004: (unload-truck package7 truck24 city4-3) 
0.004: (unload-truck package6 truck24 city4-3) 
0.004: (fly-airplane plane11 city11-3 city2-3) 
0.004: (fly-airplane plane1 city7-3 city5-3) 
0.004: (load-airplane package4 plane10 city2-3) 
0.005: (unload-airplane package5 plane11 city2-3) 
0.005: (fly-airplane plane10 city2-3 city5-3) 
0.005: (unload-airplane package3 plane1 city5-3) 
0.005: (unload-airplane package2 plane1 city5-3) 
0.005: (load-airplane package7 plane12 city4-3) 
0.005: (load-airplane package6 plane12 city4-3) 
0.006: (load-truck package5 truck31 city2-3) 
0.006: (fly-airplane plane1 city5-3 city4-3) 
0.006: (unload-airplane package4 plane10 city5-3) 
0.006: (fly-airplane plane12 city4-3 city5-3) 
0.006: (load-truck package3 truck1 city5-3) 
0.006: (load-truck package2 truck1 city5-3) 
0.007: (drive-truck truck31 city2-3 city2-1 city2) 
0.007: (unload-airplane package1 plane1 city4-3) 
0.007: (unload-airplane package7 plane12 city5-3) 
0.007: (unload-airplane package6 plane12 city5-3) 
0.008: (unload-truck package5 truck31 city2-1) 
0.008: (load-truck package1 truck17 city4-3) 
0.008: (load-truck package7 truck1 city5-3) 
0.008: (load-truck package6 truck1 city5-3) 
0.009: (drive-truck truck17 city4-3 city4-1 city4) 
0.009: (drive-truck truck1 city5-3 city5-1 city5) 
0.010: (unload-truck package1 truck17 city4-1) 
0.010: (unload-truck package7 truck1 city5-1) 
0.010: (unload-truck package3 truck1 city5-1) 
0.011: (drive-truck truck1 city5-1 city5-2 city5) 
0.012: (unload-truck package6 truck1 city5-2) 
0.012: (unload-truck package2 truck1 city5-2) 

 Execution time of Domain\Problem 16 
00:18:43.8944926

 DOMAIN/PROBLEM 17 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile17.pddl
Number of literals: 1490
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 43.000
b (42.000 | 0.000)b (41.000 | 0.001)b (40.000 | 0.002)b (39.000 | 0.002)b (38.000 | 0.002)b (37.000 | 0.002)b (36.000 | 0.002)b (35.000 | 0.002)b (34.000 | 0.002)b (33.000 | 0.002)b (32.000 | 0.002)b (31.000 | 0.002)b (30.000 | 0.002)b (29.000 | 0.002)b (28.000 | 0.002)b (27.000 | 0.002)b (26.000 | 0.002)b (25.000 | 0.003)b (24.000 | 0.003)b (23.000 | 0.004)b (22.000 | 0.005)b (21.000 | 0.005)b (20.000 | 0.005)b (19.000 | 0.005)b (18.000 | 0.005)b (17.000 | 0.005)b (16.000 | 0.005)b (15.000 | 0.006)b (14.000 | 0.007)b (13.000 | 0.007)b (12.000 | 0.007)b (11.000 | 0.007)b (10.000 | 0.008)b (9.000 | 0.009)b (8.000 | 0.010)b (7.000 | 0.010)b (6.000 | 0.010)b (5.000 | 0.010)b (4.000 | 0.010)b (3.000 | 0.010)b (2.000 | 0.010)b (1.000 | 0.010);;;; Solution Found
; States evaluated: 255
; Cost: 0.010
0.000: (load-airplane package11 plane2 city3-3) 
0.000: (load-truck package14 truck16 city3-1) 
0.000: (load-truck package9 truck15 city3-2) 
0.000: (load-truck package13 truck11 city1-1) 
0.000: (load-truck package12 truck11 city1-1) 
0.000: (load-truck package15 truck10 city1-2) 
0.000: (fly-airplane plane1 city4-3 city3-3) 
0.000: (fly-airplane plane3 city4-3 city1-3) 
0.000: (drive-truck truck1 city4-2 city4-3 city4) 
0.000: (drive-truck truck12 city2-1 city2-3 city2) 
0.001: (fly-airplane plane2 city3-3 city2-3) 
0.001: (drive-truck truck16 city3-1 city3-3 city3) 
0.001: (drive-truck truck15 city3-2 city3-3 city3) 
0.001: (drive-truck truck11 city1-1 city1-3 city1) 
0.001: (drive-truck truck10 city1-2 city1-3 city1) 
0.002: (unload-airplane package11 plane2 city2-3) 
0.002: (unload-truck package14 truck16 city3-3) 
0.002: (unload-truck package9 truck15 city3-3) 
0.002: (unload-truck package13 truck11 city1-3) 
0.002: (unload-truck package12 truck11 city1-3) 
0.002: (unload-truck package15 truck10 city1-3) 
0.003: (load-airplane package9 plane1 city3-3) 
0.003: (load-airplane package14 plane1 city3-3) 
0.003: (load-airplane package15 plane3 city1-3) 
0.003: (load-airplane package13 plane3 city1-3) 
0.003: (load-airplane package12 plane3 city1-3) 
0.003: (load-truck package11 truck12 city2-3) 
0.004: (fly-airplane plane1 city3-3 city4-3) 
0.004: (fly-airplane plane3 city1-3 city2-3) 
0.005: (unload-airplane package9 plane1 city4-3) 
0.005: (unload-airplane package14 plane1 city4-3) 
0.005: (unload-airplane package12 plane3 city2-3) 
0.006: (fly-airplane plane3 city2-3 city4-3) 
0.006: (load-truck package9 truck1 city4-3) 
0.006: (load-truck package14 truck1 city4-3) 
0.006: (load-truck package12 truck12 city2-3) 
0.007: (unload-airplane package15 plane3 city4-3) 
0.007: (unload-airplane package13 plane3 city4-3) 
0.007: (drive-truck truck12 city2-3 city2-2 city2) 
0.008: (load-truck package15 truck1 city4-3) 
0.008: (unload-truck package12 truck12 city2-2) 
0.009: (drive-truck truck1 city4-3 city4-2 city4) 
0.009: (drive-truck truck12 city2-2 city2-1 city2) 
0.010: (unload-truck package9 truck1 city4-2) 
0.010: (unload-truck package15 truck1 city4-2) 
0.010: (unload-truck package14 truck1 city4-2) 
0.010: (unload-truck package11 truck12 city2-1) 

 Execution time of Domain\Problem 17 
00:02:15.5062723

 DOMAIN/PROBLEM 18 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile18.pddl
Number of literals: 5600
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 157.000

 Execution time of Domain\Problem 18 
00:30:00.1932497

 DOMAIN/PROBLEM 19 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile19.pddl
Number of literals: 4003
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 139.000

 Execution time of Domain\Problem 19 
00:30:00.1945905

 DOMAIN/PROBLEM 20 

File: E:\unmodified_domains\logistics\domain.pddl
File: E:\unmodified_domains\logistics\pfile20.pddl
Number of literals: 7069
Constructing lookup tables: [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
Post filtering unreachable actions:  [10%] [20%] [30%] [40%] [50%] [60%] [70%] [80%] [90%] [100%]
No semaphore facts found, returning
[01;34mNo analytic limits found, not considering limit effects of goal-only operators[00m

Initial heuristic = 130.000

 Execution time of Domain\Problem 20 
00:30:00.2140136
