(define (domain auv-2D-1)
(:requirements :typing :durative-actions :fluents :duration-inequalities)

(:predicates
    (sample-takenA)(sample-takenB)(sample-takenC)(sample-takenD)(sample-takenE)
    (sample-takenF)(sample-takenG)(sample-takenH)(sample-takenI)(sample-takenJ)
    (can-refill)
    (free)
)
(:functions 
    (x)
    (fuel-cell-battery)
    (solar-power)
    (fuel)
)

;; Control Variables

(:control-variable vel-x
:bounds (and (>= ?value 0.0) (<= ?value 2.0)))

;; Fuel recharge
(:control-variable recharge-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))

;; octane recharge
(:control-variable solar-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))

;; butane recharge
(:control-variable fuel-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))



(:durative-action glide_forward
:parameters()
:duration (and  
                (>= ?duration 0.1)
                (<= ?duration 200) )
:condition (and 
 (at start (free))

 (over all (<= (x) 100))
 (over all (>= (x) 0))
 (at start (<= (x) 100))
 (at start (>= (x) 0))
 (at end (<= (x) 100))
 (at end (>= (x) 0))

(at start (<= (fuel-cell-battery) 200))
(over all (<= (fuel-cell-battery) 200))
(at end (<= (fuel-cell-battery) 200))

 (at start (>= (fuel-cell-battery) 0))
 (over all (>= (fuel-cell-battery) 0))
 (at end (>= (fuel-cell-battery) 0)))
:effect (and
     (at start (not (free)))
            (at end (free))
 (decrease (fuel-cell-battery) (* (* (vel-x) 2) #t))
 (increase (x) (* (vel-x) #t))
 ))

(:durative-action glide_backward
:parameters()
:duration (and  
                (>= ?duration 0.1)
                (<= ?duration 200) )
:condition (and 
 (at start (free))

 (over all (<= (x) 100))
 (over all (>= (x) 0))
 (at start (<= (x) 100))
 (at start (>= (x) 0))
 (at end (<= (x) 100))
 (at end (>= (x) 0))

(at start (<= (fuel-cell-battery) 200))
(over all (<= (fuel-cell-battery) 200))
(at end (<= (fuel-cell-battery) 200))

 (at start (>= (fuel-cell-battery) 0))
 (over all (>= (fuel-cell-battery) 0))
 (at end (>= (fuel-cell-battery) 0)))
:effect (and
     (at start (not (free)))
            (at end (free))
 (decrease (fuel-cell-battery) (* (* (vel-x) 2) #t))
 (decrease (x) (* (vel-x) #t))
 ))

(:durative-action recharge
:parameters ()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10)
            )
:condition (and 
 (at start (free))
 (at start (>= (fuel-cell-battery) 0))
 (at start (<= (fuel-cell-battery) 200))                
 (at end (<= (fuel-cell-battery) 200))

(at start (<= (solar-power) 20000))
(over all (<= (solar-power) 20000))
(at end (<= (solar-power) 20000))

 (at start (>= (solar-power) 0))
 (over all (>= (solar-power) 0))
 (at end (>= (solar-power) 0))

(at start (<= (fuel) 20000))
(over all (<= (fuel) 20000))
(at end (<= (fuel) 20000))

 (at start (>= (fuel) 0))
 (over all (>= (fuel) 0))
 (at end (>= (fuel) 0))

 )
:effect (and
 (at start (not (free)))
            (at end (free))

    (decrease (solar-power) (* (* (recharge-rate) 2) #t))
    (decrease (fuel) (* (* (recharge-rate) 2) #t))

  ;; Refuel as continuous effect (20 units/second)
    (increase (fuel-cell-battery) (* (recharge-rate) #t))

 )  )


(:durative-action turn_solar_panels_on
:parameters()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10))
:condition(and 
 (at start (free))

(at start (<= (solar-power) 20000))
(over all (<= (solar-power) 20000))
(at end (<= (solar-power) 20000))

 (at start (>= (solar-power) 0))
 (over all (>= (solar-power) 0))
 (at end (>= (solar-power) 0))

   )
:effect (and 
     (at start (not (free)))
            (at end (free))

    (increase (solar-power)  (* (solar-rate) #t))
    ))
    
 (:durative-action refuel_tank
:parameters()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10))
:condition(and 
 (at start (free))
 (at start (>= (fuel) 0))

(at start (<= (fuel) 20000))
(over all (<= (fuel) 20000))
(at end (<= (fuel) 20000))

 (at start (>= (fuel) 0))
 (over all (>= (fuel) 0))
 (at end (>= (fuel) 0))
   )
:effect (and 
     (at start (not (free)))
            (at end (free))

    (increase (fuel)  (* (fuel-rate) #t))
    ))


(:durative-action take-sampleA
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 12))
 (at start (<= (x) 17))
 (over all (>= (x) 12))
 (over all (<= (x) 17))
 (at end (>= (x) 12))
 (at end (<= (x) 17))
)
:effect (and (at end (sample-takenA))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleB
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 42))
 (at start (<= (x) 47))
 (over all (>= (x) 42))
 (over all (<= (x) 47))
 (at end (>= (x) 42))
 (at end (<= (x) 47))
)
:effect (and 
            (at end (sample-takenB))
            (at start (not (free)))
            (at end (free))
            ))

(:durative-action take-sampleC
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 72))
 (at start (<= (x) 77))
 (over all (>= (x) 72))
 (over all (<= (x) 77))
 (at end (>= (x) 72))
 (at end (<= (x) 77))
)
:effect (and (at end (sample-takenC))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleD
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 92))
 (at start (<= (x) 97))
 (over all (>= (x) 92))
 (over all (<= (x) 97))
 (at end (>= (x) 92))
 (at end (<= (x) 97))
)
:effect (and (at end (sample-takenD))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleE
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 18))
 (at start (<= (x) 21))
 (over all (>= (x) 18))
 (over all (<= (x) 21))
 (at end (>= (x) 18))
 (at end (<= (x) 21))
)
:effect (and (at end (sample-takenE))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleF
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 48))
 (at start (<= (x) 51))
 (over all (>= (x) 48))
 (over all (<= (x) 51))
 (at end (>= (x) 48))
 (at end (<= (x) 51))
)
:effect (and (at end (sample-takenF))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleG
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 82))
 (at start (<= (x) 87))
 (over all (>= (x) 82))
 (over all (<= (x) 87))
 (at end (>= (x) 82))
 (at end (<= (x) 87))
)
:effect (and (at end (sample-takenG))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleH
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 5))
 (at start (<= (x) 10))
 (over all (>= (x) 5))
 (over all (<= (x) 10))
 (at end (>= (x) 5))
 (at end (<= (x) 10))
)
:effect (and (at end (sample-takenH))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleI
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 7))
 (at start (<= (x) 12))
 (over all (>= (x) 7))
 (over all (<= (x) 12))
 (at end (>= (x) 7))
 (at end (<= (x) 12))
)
:effect (and (at end (sample-takenI))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleJ
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 88))
 (at start (<= (x) 92))
 (over all (>= (x) 88))
 (over all (<= (x) 92))
 (at end (>= (x) 88))
 (at end (<= (x) 92))
)
:effect (and (at end (sample-takenJ))
             (at start (not (free)))
             (at end (free))
            ))

)