(define (problem auv-3D-problem-1)
	(:domain auv-2D-1)
    (:init 
        (= (x) 0)
        (= (fuel-cell-battery) 20)
        (= (solar-power) 20)
        (= (fuel) 20)
        (free)
 
        )


	(:goal
		(and
    (sample-takenA)(sample-takenB)(sample-takenC)(sample-takenD)(sample-takenE)
    (sample-takenF)(sample-takenG)(sample-takenH)(sample-takenI);(sample-takenJ)
   ; (sample-takenK)(sample-takenL)
        )))