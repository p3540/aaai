(define (domain auv-2D-1)
(:requirements :typing :durative-actions :fluents :duration-inequalities)

(:predicates
    (sample-takenA)(sample-takenB)
    (free)
)
(:functions 
    (x)
    (fuel-cell-battery)
    (solar-power)
    (fuel)
)

;; Control Variables

(:control-variable vel-x
:bounds (and (>= ?value 0.0) (<= ?value 2.0)))

;; Fuel recharge
(:control-variable recharge-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))

;; octane recharge
(:control-variable solar-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))

;; butane recharge
(:control-variable fuel-rate
:bounds (and (>= ?value 0.0) (<= ?value 20.0)))



(:durative-action glide_forward
:parameters()
:duration (and  
                (>= ?duration 0.1)
                (<= ?duration 200) )
:condition (and 
 (at start (free))

 (over all (<= (x) 100))
 (over all (>= (x) 0))
 (at start (<= (x) 100))
 (at start (>= (x) 0))
 (at end (<= (x) 100))
 (at end (>= (x) 0))

(at start (<= (fuel-cell-battery) 200))
(over all (<= (fuel-cell-battery) 200))
(at end (<= (fuel-cell-battery) 200))

 (at start (>= (fuel-cell-battery) 0))
 (over all (>= (fuel-cell-battery) 0))
 (at end (>= (fuel-cell-battery) 0)))
:effect (and
     (at start (not (free)))
            (at end (free))
 (decrease (fuel-cell-battery) (* (* (vel-x) 2) #t))
 (increase (x) (* (vel-x) #t))
 ))

(:durative-action glide_backward
:parameters()
:duration (and  
                (>= ?duration 0.1)
                (<= ?duration 200) )
:condition (and 
 (at start (free))

 (over all (<= (x) 100))
 (over all (>= (x) 0))
 (at start (<= (x) 100))
 (at start (>= (x) 0))
 (at end (<= (x) 100))
 (at end (>= (x) 0))

(at start (<= (fuel-cell-battery) 200))
(over all (<= (fuel-cell-battery) 200))
(at end (<= (fuel-cell-battery) 200))

 (at start (>= (fuel-cell-battery) 0))
 (over all (>= (fuel-cell-battery) 0))
 (at end (>= (fuel-cell-battery) 0)))
:effect (and
     (at start (not (free)))
            (at end (free))
 (decrease (fuel-cell-battery) (* (* (vel-x) 2) #t))
 (decrease (x) (* (vel-x) #t))
 ))

(:durative-action recharge
:parameters ()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10)
            )
:condition (and 
 (at start (free))
 (at start (>= (fuel-cell-battery) 0))
 (at start (<= (fuel-cell-battery) 200))                
 (at end (<= (fuel-cell-battery) 200))

(at start (<= (solar-power) 20000))
(over all (<= (solar-power) 20000))
(at end (<= (solar-power) 20000))

 (at start (>= (solar-power) 0))
 (over all (>= (solar-power) 0))
 (at end (>= (solar-power) 0))

(at start (<= (fuel) 20000))
(over all (<= (fuel) 20000))
(at end (<= (fuel) 20000))

 (at start (>= (fuel) 0))
 (over all (>= (fuel) 0))
 (at end (>= (fuel) 0))

 )
:effect (and
 (at start (not (free)))
            (at end (free))

    (decrease (solar-power) (* (* (recharge-rate) 2) #t))
    (decrease (fuel) (* (* (recharge-rate) 2) #t))

  ;; Refuel as continuous effect (20 units/second)
    (increase (fuel-cell-battery) (* (recharge-rate) #t))

 )  )


(:durative-action turn_solar_panels_on
:parameters()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10))
:condition(and 
 (at start (free))

(at start (<= (solar-power) 20000))
(over all (<= (solar-power) 20000))
(at end (<= (solar-power) 20000))

 (at start (>= (solar-power) 0))
 (over all (>= (solar-power) 0))
 (at end (>= (solar-power) 0))

   )
:effect (and 
     (at start (not (free)))
            (at end (free))

    (increase (solar-power)  (* (solar-rate) #t))
    ))
    
 (:durative-action refuel_tank
:parameters()
:duration(and   (>= ?duration 0.1)
                (<= ?duration 10))
:condition(and 
 (at start (free))
 (at start (>= (fuel) 0))

(at start (<= (fuel) 20000))
(over all (<= (fuel) 20000))
(at end (<= (fuel) 20000))

 (at start (>= (fuel) 0))
 (over all (>= (fuel) 0))
 (at end (>= (fuel) 0))
   )
:effect (and 
     (at start (not (free)))
            (at end (free))

    (increase (fuel)  (* (fuel-rate) #t))
    ))


(:durative-action take-sampleA
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 12))
 (at start (<= (x) 17))
 (over all (>= (x) 12))
 (over all (<= (x) 17))
 (at end (>= (x) 12))
 (at end (<= (x) 17))
)
:effect (and (at end (sample-takenA))
             (at start (not (free)))
             (at end (free))
            ))

(:durative-action take-sampleB
 :parameters()
 :duration (and (>= ?duration 2) (<= ?duration 8))
 :condition (and  (at start (free))
    (at start (>= (fuel-cell-battery) 0))
 (at start (>= (x) 42))
 (at start (<= (x) 47))
 (over all (>= (x) 42))
 (over all (<= (x) 47))
 (at end (>= (x) 42))
 (at end (<= (x) 47))
)
:effect (and 
            (at end (sample-takenB))
            (at start (not (free)))
            (at end (free))
            ))

)